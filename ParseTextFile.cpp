
#include "StdAfx.h"
#include "ParseTextFile.h"
/*

CParseTextFileTrakt::CParseTextFileTrakt(void)
{
	CStringA datum="";			// 1
	CStringA region="";			// 2
	CStringA distrikt="";		// 3
	CStringA typ="";			// 4
	CStringA ursprung="";		// 5
	CStringA obj_nr="";			// 6
	CStringA obj_namn="";		// 7
	CStringA areal="";			// 8
	CStringA mask_nr="";		// 9
	CStringA entr="";			// 10
	CStringA mask_storlek="";	// 11
	CStringA atg_tidp_nr="";	// 12
	CStringA atg_tidp_text="";	// 13
	m_bIsOld=FALSE;
	Avdelningar.clear();
}


//---------------------------------------------------------------------------
//------------------------------------------------
// Hitta och spara header variabler             
//------------------------------------------------
int CParseTextFileTrakt::New_find_variabel(CStringA *variabel,FILE *in, int variabel_nr)
{
	char buff;
	CString buff2;
	int found=0;

	do
	{
		buff=fgetc(in);
	}while( !feof(in) &&
		!(((buff>='a')&&(buff<='z')) || ((buff>='A')&&(buff<='Z')))
		&& (buff!='�') && (buff!='�') && (buff!='�')
		&& (buff!='�') && (buff!='�') && (buff!='�') );

	if(feof(in))
		return -1;

	do
	{
		if(!feof(in) && ( (((buff>='a')&&(buff<='z')) || ((buff>='A')&&(buff<='Z'))) || (buff=='�') || (buff=='�') || (buff=='�')|| (buff=='�')|| (buff=='�')|| (buff=='�')))
			buff2+=buff;
		else
			break;
		buff=fgetc(in);
	}while(1);

	if(feof(in))
		return -1;

	switch(variabel_nr)
	{
	case 1:     //Uppf�ljningsdatum
		if(buff2.CompareNoCase(_T("Uppf�ljningsdatum"))!=0)
			return -1;
		break;
	case 2:     //Region
		if(buff2.CompareNoCase(_T("Region"))!=0)
			if(buff2.CompareNoCase(_T("F�rem�l"))==0)
				return 0;
			else
				return -1;
		break;
	case 3:     //Distrikt
		if(buff2.CompareNoCase(_T("Distrikt"))!=0)
			return -1;
		break;
	case 4:     //Typ
		if(buff2.CompareNoCase(_T("Typ"))!=0)
			return -1;
		break;
	case 5:     //Ursprung
		if(buff2.CompareNoCase(_T("Ursprung"))!=0)
			return -1;
		break;
	case 6:     //Objektsnummer
		if(buff2.CompareNoCase(_T("Objektsnummer"))!=0)
			return -1;
		break;
	case 7:     //Objektsnamn
		if(buff2.CompareNoCase(_T("Objektsnamn"))!=0)
			return -1;
		break;
	case 8:     //Areal
		if(buff2.CompareNoCase(_T("Areal"))!=0)
			return -1;
		break;
	case 9:     //Maskinnummer
		if(buff2.CompareNoCase(_T("Maskinnummer"))!=0)
			return -1;
		break;
	case 10:     //Entrepren�r
		if(buff2.CompareNoCase(_T("Entrepren�r"))!=0)
			return -1;
		break;
	case 11:     //Maskinstorlek
		if(buff2.CompareNoCase(_T("Maskinstorlek"))!=0)
			return -1;
		break;
	case 12:     //R�tt �tg�rdstidpunkt
		if(buff2.CompareNoCase(_T("R�tt �tg�rdstidpunkt"))!=0)
			return -1;
		break;
	}
	buff2="";
	do
	{
		if(buff=='\n')
			return 1;
		buff=fgetc(in);
		if((buff=='\t')||(buff=='\n'))
			return 1;
	}while( !feof(in) &&
		!(((buff>='a')&&(buff<='z')) || ((buff>='A')&&(buff<='Z')))
		&& (buff!='�') && (buff!='�') && (buff!='�')
		&& (buff!='�') && (buff!='�') && (buff!='�')
		&& !(buff<='9' && buff>='0'));

	do
	{
		if(!feof(in)
			&& ( (((buff>='a')&&(buff<='z')) || ((buff>='A')&&(buff<='Z')))
			|| (buff=='�') || (buff=='�') || (buff=='�')
			|| (buff=='�')|| (buff=='�')|| (buff=='�')  || (buff=='.')
			|| (buff<='9' && buff>='0') ))
		{
			buff2+=buff;
			buff=fgetc(in);
			found=1;
		}
		else
			break;
	}while(1);
	if(found)
		*variabel=buff2;
	return 1;
}


//------------------------------------------------
// H�mta antal avdelningar                      
//------------------------------------------------
int  CParseTextFileTrakt::GetAntAvd()
{
   return Avdelningar.size();
}
//------------------------------------------------
// H�mta id f�r Avdelning med index i           
// Returnerar 0 om inte hittat                  
//------------------------------------------------
int CParseTextFileTrakt::GetAvdId(int index,long *id)
{
	int i=0,found=0,size=Avdelningar.size();

	if(index<size)
	{
		*id=atol(Avdelningar[i].Id);
			found=1;
	}
	return found;
}
//------------------------------------------------
// H�mta antal ytor                             
//------------------------------------------------
int CParseTextFileTrakt::GetAntYt(int avd)
{
	int found=0,ant=0,size=Avdelningar.size(),i=0;

	if(size>0)
	{
		for(i=0;i<size;i++)
		{
			if(atoi(Avdelningar[i].Id)==avd)
				Avdelningar[i].mYta[0].size();

		}
	}

		return ant;
}
//-----------------------------------------------
// H�mta ytvariabler 26st returnerar 0 om inte hittat  
//-----------------------------------------------
//int GetYtVar(char *avd,int yta,vector <char *> *vect)
int CParseTextFileTrakt::GetYtVar(int avd,int yta,char *vars[])
{
	int found=0,storlek,i,row,size=Avdelningar.size(),avdI=0;

	if(size>0)
	{
		for(avdI=0;i<size;avdI++)
		{
			if(atoi(Avdelningar[avdI].Id)==avd)
			{
				storlek=Avdelningar[avdI].mYta[0].size();
				if(storlek!=0)
				{
					for(i=0;i<storlek-1;i++)
					{
						if(i==yta)
						{
							found=1;
							for(row=0;row<26;row++)
								vars[row]=Avdelningar[avdI].mYta[row][i].GetBuffer();
						}
					}
				}
			}
		}
	}


	return found;
}
//-----------------------------------------------
// H�mta avdelningsvariabler 26 st returnerar 0 om inte hittat  
//-----------------------------------------------
//int GetAvdVar(char *avd,vector <char *> *vect)
int CParseTextFileTrakt::GetAvdVar(int avd,char *vars[])
{
	int found=0,storlek,i,row,size=Avdelningar.size(),avdI=0;

	if(size>0)
	{
		for(avdI=0;i<size;avdI++)
		{
		
			if(atoi(Avdelningar[avdI].Id)==avd)
			{
				storlek=Avdelningar[avdI].mYta[0].size();
				if(storlek>0)
				{
					found=1;
					for(row=0;row<26;row++)
						vars[row]=Avdelningar[avdI].mYta[row][storlek-1].GetBuffer();
				}
			}
		}
	}

	return found;
}
//------------------------------------------------
// H�mta Traktvariabler 24 st                   
//------------------------------------------------
//int GetTraktVar(vector <char *> *vect)
int CParseTextFileTrakt::GetTraktVar(char *vars[])
{
	int row;
	vars[0]=value[0].GetBuffer();
	vars[1]="0";
	vars[2]=value[1].GetBuffer();
	vars[3]="0";
	for(row=2;row<24;row++)
	{
		vars[row+2]=value[row].GetBuffer();
		//vect->resize(vect->size()+1,Trakt.value[row].c_str());
	}
	return 1;

}
//------------------------------------------------
// H�mta Traktheader 10 st                      
//------------------------------------------------
//int GetTraktHead(vector <char *> *vect)
int CParseTextFileTrakt::GetTraktHead(char *vars[])
{
	vars[0]=m_sDatum.GetBuffer();
	vars[1]=m_sDistrikt.GetBuffer();
	vars[2]=m_sAreal.GetBuffer();
	vars[3]=m_sObj_nr.GetBuffer();
	vars[4]=m_sMask_nr.GetBuffer();
	vars[5]=m_sObj_namn.GetBuffer();
	vars[6]=m_sEntr.GetBuffer();
	vars[7]=m_sRegion.GetBuffer();
	vars[8]=m_sTyp.GetBuffer();
	vars[9]=m_sUrsprung.GetBuffer();
	return 1;
}

void CParseTextFileTrakt::Insert_YtDataOnLastAvdelning(int varcount,CStringA buff2)
{
   int size=Avdelningar.size();

   Avdelningar[size-1].mYta[varcount].resize(Avdelningar[size-1].mYta[varcount].size()+1,buff2);

//   ai=Trakt.Avdelningar.end();
//   ai--;
//   ai->value[varcount].resize(ai->value[varcount].size()+1,buff2);
}

int CParseTextFileTrakt::GallBas_OpenFile(char *filename)
{

	m_bIsOld=FALSE;
	if(m_bIsOld)
	return Old_GallBas_OpenFile(filename);
	else
	return New_GallBas_OpenFile(filename);
}

int CParseTextFileTrakt::New_GallBas_OpenFile(char *filename)
{
	FILE *in;
	CStringA buff2,buff3="",bull="";
	int nr,nr2,count=0,max_var=24;
	int ytcount,ytcount2,varcount,noregion=0;
	char buff;
	

	if ((in = fopen(filename, "rt")) == NULL)
	{
		//ShowMessage("Kan ej �ppna fil");
		return -2;
	}

	//Avdelningar.clear();
	//buff=fgetc(in);
	nr=-4;
	nr2=4;
	while(!feof(in))
	{
		switch(nr)
		{
		case -4:
			// Uppf�ljningsdatum	1
			if(New_find_variabel(&m_sDatum,in,1)==-1)
			{
				fclose(in);
				return -1;
			}
			// Region	2
			noregion=0;
			switch(New_find_variabel(&m_sRegion,in,2))
			{
			case -1:
				fclose(in);
				return -1;
			case 1:       //Ta typ & ursprung
				noregion=0;
				break;                
			case 0:       //Ingen region eller typ eller ursprung
				noregion=1;
				m_sRegion="1";
				m_sTyp="E";
				m_sUrsprung="4";
				break;
			}
			break;
			// Distrikt	3
			if(New_find_variabel(&m_sDistrikt,in,3)==-1)
			{
				fclose(in);
				return -1;
			}
			if(noregion==0)
			{
				// Typ	4
				if(New_find_variabel(&m_sTyp,in,4)==-1)
				{
					fclose(in);
					return -1;
				}
				// Ursprung	5
				if(New_find_variabel(&m_sUrsprung,in,5)==-1)
				{
					fclose(in);
					return -1;
				}
			}
			// Objektsnummer	6
			if(New_find_variabel(&m_sObj_nr,in,6)==-1)
			{
				fclose(in);
				return -1;
			}
			// Objektsnamn	7
			if(New_find_variabel(&m_sObj_namn,in,7)==-1)
			{
				fclose(in);
				return -1;
			}
			// Areal	8
			if(New_find_variabel(&m_sAreal,in,8)==-1)
			{
				fclose(in);
				return -1;
			}
			// Maskinnummer	9
			if(New_find_variabel(&m_sMask_nr,in,9)==-1)
			{
				fclose(in);
				return -1;
			}

			// Entrepren�r	10
			if(New_find_variabel(&m_sEntr,in,10)==-1)
			{
				fclose(in);
				return -1;
			}

			// Maskinstorlek	11
			if(New_find_variabel(&m_sMask_storlek,in,11)==-1)
			{
				fclose(in);
				return -1;
			}
			// �tg�rdstidpunkt nr(0/1) 	12
			if(New_find_variabel(&m_sAtg_tidp_nr,in,12)==-1)
			{
				fclose(in);
				return -1;
			}
			// �tg�rdstidpunkt text 	13
			if(New_find_variabel(&m_sAtg_tidp_text,in,13)==-1)
			{
				fclose(in);
				return -1;
			}
			nr=nr2=2;
			break;		       
		case -2:      
			if(buff=='\t')
			{
				buff2="";
				nr+=nr2+2;
			}
			else
				buff2+=buff;
			break;
		case -1:        //Letar ':'
			if(buff==':')
			{
				nr+=nr2+1;
				buff2="";
			}
			break;
		case 0:         // Letar '\t'
			if(buff=='\t')
			{
				nr=nr2;
				//buff2="";
			}
			else
				buff3+=buff;
			break;
		case 1:         // Letar '\n'
			if(buff=='\n')
			{
				nr=nr2;
			}
			else
				buff3+=buff;
			break;
		case 2:        //Hitta '\n'
			nr=1;
			nr2++;
			break;
		case 3:         // Hitta '\t'
			nr=0;
			nr2++;
			buff2="";
			count=0;
			break;
		case 4:        // Traktmedel
			if((buff=='\t')||(buff=='\n'))
			{
				nr=0;
				//value=atof(buff2.c_str());
				if(count==13)
				{
					if((buff3.CompareNoCase("\nV.Tot     (m3sk/ha):")==0)
						||(buff3.CompareNoCase("\nV.Tot  m3sk/ha:")==0))
					{
						max_var=22;
						value[count]="0";
						value[count+1]=buff2;
						count+=2;
					}
					else
					{
						max_var=24;
						value[count]=buff2;
						count++;
					}

				}
				else
				{
					if(count==18)
					{
						if(max_var==22)
						{
							value[count]=_T("");//0;
							value[count+1]=buff2;
							count+=2;
						}
						else
						{
							value[count]=buff2;
							count++;
						}
					}
					else
					{
						value[count]=buff2;
						count++;
					}
				}
				if(count==24)
				{
					//value=0;
					nr=-1;
					nr2++;
				}
				buff2="";
				buff3="";
			}
			else
			{
				buff2+=buff;
			}
			break;
		case 5:        // Avdelning Id
			if((buff=='\t')||(buff=='\n'))
			{
				nr=0;
				nr2++;
				struct Avd_struct avdstr;
				avdstr.mYta->clear();
				avdstr.Id=buff2;
				Avdelningar.resize(Avdelningar.size()+1,avdstr);
				buff2="";
				ytcount=1;
			}
			else
			{
				buff2+=buff;
			}
			break;
		case 6:        // R�kna ytor
			if(buff=='\t')
				ytcount++;
			else
				if(buff=='\n')
				{
					//nr=0;
					nr=-2;
					nr2++;
					varcount=0;
					ytcount2=0;
				}
				break;
		case 7:        // Ytdata
			if((buff=='\t')||(buff=='\n'))
			{
				if((buff=='\n')&&(ytcount2!=ytcount))
					nr=0;
				else
				{

					if(varcount==15 ||varcount==20)
					{
						if(max_var==22)
						{
							Insert_YtDataOnLastAvdelning(varcount,"0");
							Insert_YtDataOnLastAvdelning(varcount+1,buff2);
						}
						else
							Insert_YtDataOnLastAvdelning(varcount,buff2);
					}
					else
						Insert_YtDataOnLastAvdelning(varcount,buff2);
					ytcount2++;

					if(ytcount2==(ytcount+1))
					{
						if((max_var==22)&&((varcount==15)||(varcount==20)))
							varcount+=2;
						else
							varcount++;
						ytcount2=0;
					}
					if(varcount==26)
					{
						nr2=5;
						nr=-1;
					}
				}
				buff2="";
			}
			else
			{
				buff2+=buff;
			}
			break;
		case 8:
			break;
		}
		buff=fgetc(in);
	}
	fclose(in);
	return 1;
}



//-----------------------------------------------
// Hitta och spara header variabler             
//-----------------------------------------------
int CParseTextFileTrakt::Old_find_variabel(CStringA *variabel,FILE *in, int variabel_nr)
{
char buff;
CString buff2;
int found=0;

    do
    {
    buff=fgetc(in);
    }while( !feof(in) &&
    !(((buff>='a')&&(buff<='z')) || ((buff>='A')&&(buff<='Z')))
    && (buff!='�') && (buff!='�') && (buff!='�')
    && (buff!='�') && (buff!='�') && (buff!='�') );

    if(feof(in))
       return -1;

    do
    {
       if(!feof(in) && ( (((buff>='a')&&(buff<='z')) || ((buff>='A')&&(buff<='Z'))) || (buff=='�') || (buff=='�') || (buff=='�')|| (buff=='�')|| (buff=='�')|| (buff=='�')))
          buff2+=buff;
       else
         break;
    buff=fgetc(in);
    }while(1);

    if(feof(in))
       return -1;

    switch(variabel_nr)
    {
    case 0:     //Uppf�ljningsdatum
		if(buff2.CompareNoCase(_T("Uppf�ljningsdatum"))!=0)
           return -1;
        break;
    case 1:     //Avdelning
        if(buff2.CompareNoCase(_T("Avdelning"))!=0)
           return -1;
        else
           return 1;
    case 2:     //Distrikt
        if(buff2.CompareNoCase(_T("Distrikt"))!=0)
           return -1;
        break;
    case 3:     //Areal
        if(buff2.CompareNoCase(_T("Areal"))!=0)
           return -1;
        break;
    case 4:     //Objektsnummer
        if(buff2.CompareNoCase(_T("Objektsnummer"))!=0)
           return -1;
        break;
    case 5:     //Maskinnummer
        if(buff2.CompareNoCase(_T("Maskinnummer"))!=0)
           return -1;
        break;
    case 6:     //Objektsnamn
        if(buff2.CompareNoCase(_T("Objektsnamn"))!=0)
           return -1;
        break;
    case 7:     //Entrepren�r
        if(buff2.CompareNoCase(_T("Entrepren�r"))!=0)
           return -1;
        break;
    case 8:     //Region
        if(buff2.CompareNoCase(_T("Region"))!=0)
           if(buff2.CompareNoCase(_T("F�rem�l"))==0)
              return 0;
           else
              return -1;
        break;
    case 9:     //Typ
        if(buff2.CompareNoCase(_T("Typ"))!=0)
           return -1;
        break;
    case 10:     //Ursprung
        if(buff2.CompareNoCase(_T("Ursprung"))!=0)
           return -1;
        break;
    }
    buff2="";
    do
    {
      if(buff=='\n')
         return 1;
    buff=fgetc(in);
       if((buff=='\t')||(buff=='\n'))
          return 1;
    }while( !feof(in) &&
    !(((buff>='a')&&(buff<='z')) || ((buff>='A')&&(buff<='Z')))
    && (buff!='�') && (buff!='�') && (buff!='�')
    && (buff!='�') && (buff!='�') && (buff!='�')
    && !(buff<='9' && buff>='0'));

    do
    {
       if(!feof(in)
       && ( (((buff>='a')&&(buff<='z')) || ((buff>='A')&&(buff<='Z')))
       || (buff=='�') || (buff=='�') || (buff=='�')
       || (buff=='�')|| (buff=='�')|| (buff=='�')  || (buff=='.')
       || (buff<='9' && buff>='0') ))
       {
          buff2+=buff;
          buff=fgetc(in);
          found=1;
       }
       else
        break;
    }while(1);
    if(found)
    *variabel=buff2;
    return 1;
}


int CParseTextFileTrakt::Old_GallBas_OpenFile(char *filename)
{
   FILE *in;
   CStringA buff2,buff3="",bull="";
   int col,row,avd,size1,size2,size3,trakt,nr,nr2,count=0,max_var=24;
   int ytcount,ytcount2,varcount,gamla_klaven=0;
   char buff;

   if ((in = fopen(filename, "rt")) == NULL)
   {
      //ShowMessage("Kan ej �ppna fil");
      return -2;
   }
   Avdelningar.clear();
   //buff=fgetc(in);
   nr=-4;
   nr2=4;
   while(!feof(in))
   {
      switch(nr)
      {
     case -4:
         // Uppf�ljningsdatum
          if(Old_find_variabel(&m_sDatum,in,0)==-1)
          {
             fclose(in);
             return -1;
          }
         // Avdelning
          if(Old_find_variabel(&bull,in,1)==-1)
          {
             fclose(in);
             return -1;
          }
        // Distrikt
          if(Old_find_variabel(&m_sDistrikt,in,2)==-1)
          {
             fclose(in);
             return -1;
          }
         // Areal
          if(Old_find_variabel(&m_sAreal,in,3)==-1)
          {
             fclose(in);
             return -1;
          }
        // Objektsnummer
          if(Old_find_variabel(&m_sObj_nr,in,4)==-1)
          {
             fclose(in);
             return -1;
          }
         // Maskinnummer
          if(Old_find_variabel(&m_sMask_nr,in,5)==-1)
          {
             fclose(in);
             return -1;
          }
         // Objektsnamn
          if(Old_find_variabel(&m_sObj_namn,in,6)==-1)
          {
             fclose(in);
             return -1;
          }
        // Entrepren�r
          if(Old_find_variabel(&m_sEntr,in,7)==-1)
          {
             fclose(in);
             return -1;
          }
       // Region
          switch(Old_find_variabel(&m_sRegion,in,8))
          {
          case -1:
             fclose(in);
             return -1;
          case 1:       //Ta typ & ursprung
                // Typ
              if(Old_find_variabel(&m_sTyp,in,9)==-1)
              {
                fclose(in);
                return -1;
              }
                // Ursprung
              if(Old_find_variabel(&m_sUrsprung,in,10)==-1)
              {
                 fclose(in);
                 return -1;
              }
              nr=nr2=2;
              break;
          case 0:       //Ingen region eller typ eller ursprung
            m_sRegion="1";
            m_sTyp="E";
            m_sUrsprung="4";
            nr=nr2=2;
              break;
          }
          break;
      case -2:        //Kolla om klave utskr eller DP
         if(buff=='\t')
         {
            if(buff2.CompareNoCase("V�gbredd  (m)      :")==0)
               gamla_klaven=1;
            else
               gamla_klaven=0;
            buff2="";
            nr+=nr2+2;
         }
         else
            buff2+=buff;
         break;
      case -1:        //Letar ':'
         if(buff==':')
         {
            nr+=nr2+1;
            buff2="";
         }
         break;
      case 0:         // Letar '\t'
         if(buff=='\t')
         {
            nr=nr2;
            //buff2="";
         }
         else
            buff3+=buff;
         break;
      case 1:         // Letar '\n'
         if(buff=='\n')
         {
            nr=nr2;
            //buff2="";
         }
         else
            buff3+=buff;
         break;
      case 2:        //Hitta '\n'
         nr=1;
         nr2++;
         break;
      case 3:         // Hitta '\t'
         nr=0;
         nr2++;
         buff2="";
         count=0;
         break;
      case 4:        // Traktmedel
         if((buff=='\t')||(buff=='\n'))
         {
            nr=0;
            //value=atof(buff2.c_str());
               if(count==13)
               {
				   if((buff3.CompareNoCase("\nV.Tot     (m3sk/ha):")==0)
					   ||(buff3.CompareNoCase("\nV.Tot  m3sk/ha:")==0))
                  {
                  max_var=22;
                  value[count]="0";
                  value[count+1]=buff2;
                  count+=2;
                  }
                  else
                  {
                  max_var=24;
                  value[count]=buff2;
                  count++;
                  }

               }
               else
               {
                  if(count==18)
                  {
                     if(max_var==22)
                     {
                     value[count]=_T("");//0;
                     value[count+1]=buff2;
                     count+=2;
                     }
                     else
                     {
                     value[count]=buff2;
                     count++;
                     }
                  }
                  else
                  {
                  value[count]=buff2;
                  count++;
                  }
               }
            if(count==24)
            {
            //value=0;
            nr=-1;
            nr2++;
            }
            buff2="";
            buff3="";
         }
         else
         {
            buff2+=buff;
         }
         break;
      case 5:        // Avdelning Id
         if((buff=='\t')||(buff=='\n'))
         {
            nr=0;
            nr2++;
			struct Avd_struct avdstr;
			avdstr.mYta->clear();
			avdstr.Id=buff2;
			Avdelningar.resize(Avdelningar.size()+1,avdstr);

            //avdstr.Id=buff2;
            //Avdelningar.insert(Trakt.Avdelningar.end(),avdstr);
            buff2="";
            ytcount=1;
         }
         else
         {
            buff2+=buff;
         }
         break;
      case 6:        // R�kna ytor
         if(buff=='\t')
            ytcount++;
         else
            if(buff=='\n')
            {
               //nr=0;
               nr=-2;
               nr2++;
               varcount=0;
               ytcount2=0;
            }
         break;
      case 7:        // Ytdata
         if((buff=='\t')||(buff=='\n'))
         {
            if((buff=='\n')&&(ytcount2!=ytcount))
               nr=0;
            else
            {
               if(gamla_klaven&&((varcount==0)||(varcount==1)))
               {
                  if(varcount==0)
                  {
                     Insert_YtDataOnLastAvdelning(varcount,buff2);
                     Insert_YtDataOnLastAvdelning(varcount+1,buff2);
                  }
                  else
                  {
                     Insert_YtDataOnLastAvdelning(varcount+1,buff2);
                     Insert_YtDataOnLastAvdelning(varcount+2,buff2);
                  }
                  ytcount2++;
               }
               else
               {
                  if(varcount==15 ||varcount==20)
                  {
                     if(max_var==22)
                     {
                     Insert_YtDataOnLastAvdelning(varcount,"0");
                     Insert_YtDataOnLastAvdelning(varcount+1,buff2);
                     }
                     else
                     Insert_YtDataOnLastAvdelning(varcount,buff2);
                  }
                  else
                  Insert_YtDataOnLastAvdelning(varcount,buff2);
               ytcount2++;
               }
               if(ytcount2==(ytcount+1))
               {
                  if(gamla_klaven&&(varcount==1))
                     varcount=4;
                  else
                     if((max_var==22)&&((varcount==15)||(varcount==20)))
                     varcount+=2;
                     else
                     varcount++;
               ytcount2=0;
               }
               if(varcount==26)
               {
                  nr2=5;
                  nr=-1;
               }
            }
            buff2="";
         }
         else
         {
            buff2+=buff;
         }
         break;
      case 8:
         break;
      }
      buff=fgetc(in);
   }
   fclose(in);
   return 1;
}
*/

