///////////////////////////////////////////////////////////////////////////////
//
// This dialog will be used to select Machine also; 2006-10-06 P�D
//
//
///////////////////////////////////////////////////////////////////////////////

// SelectRegionDistDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMGallBasDB.h"
#include "SelectRegionDistDlg.h"

#include "ResLangFileReader.h"
#include ".\selectregiondistdlg.h"

// CSelectRegionDistDlg dialog

IMPLEMENT_DYNAMIC(CSelectRegionDistDlg, CDialog)

BEGIN_MESSAGE_MAP(CSelectRegionDistDlg, CDialog)
	ON_WM_WINDOWPOSCHANGING()
	ON_NOTIFY(NM_DBLCLK, IDC_REGION_REPORT, OnReportItemDblClick)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CSelectRegionDistDlg::CSelectRegionDistDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectRegionDistDlg::IDD, pParent)
{
	m_enumWTL = WTL_NOTHING;
}

CSelectRegionDistDlg::CSelectRegionDistDlg(enumWHAT_TO_LIST wtl,CWnd* pParent /*=NULL*/)
	: CDialog(CSelectRegionDistDlg::IDD, pParent)
{
	m_enumWTL = wtl;
}

CSelectRegionDistDlg::~CSelectRegionDistDlg()
{
}

void CSelectRegionDistDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BOOL CSelectRegionDistDlg::OnInitDialog()
{

	CDialog::OnInitDialog();

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			if (m_enumWTL == WTL_REGION_DISTRICT)
			{
				// Get District in database; 061002 p�d	
				pDB->getDistricts(m_vecDistrictData);
			}
			else if (m_enumWTL == WTL_MACHINE)
			{
				// Get Machine in database; 061006 p�d	
				pDB->getMachines(m_vecMachineData);
			}
			else if (m_enumWTL == WTL_MACHINE_REG)
			{
				// Get Machine in database; 061006 p�d	
				pDB->getMachineReg(m_vecMachineRegData);
			}
			else if (m_enumWTL == WTL_TRAKT)
			{
				// Get Trakt in database; 061006 p�d	
				pDB->getTrakts(m_vecTraktData);
			}

			delete pDB;
			setupReport();
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)

	CRect rect;
	GetParent()->GetWindowRect(&rect);

	SetWindowPos(NULL,rect.left + 200,rect.top+20,500,500,SWP_NOOWNERZORDER | SWP_NOZORDER|SWP_NOSIZE);
	return TRUE;
}

BOOL CSelectRegionDistDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CDialog::OnCopyData(pWnd, pData);
}

// CSelectRegionDistDlg message handlers
BOOL CSelectRegionDistDlg::setupReport(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, IDC_REGION_REPORT,FALSE,FALSE ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndReport.GetSafeHwnd() != NULL)
				{

					if (m_enumWTL == WTL_REGION_DISTRICT)
					{
						m_wndReport.ShowWindow( SW_NORMAL );
						// Set Dialog caption; 061005 p�d
						SetWindowText(xml->str(IDS_STRING2120));
						pCol = m_wndReport.AddColumn(new CXTPReportColumn(0, xml->str(IDS_STRING300), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(1, xml->str(IDS_STRING301), 150));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_LEFT );
						pCol->SetAlignment( DT_LEFT );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(2, xml->str(IDS_STRING302), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(3, xml->str(IDS_STRING303), 150));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_LEFT );
						pCol->SetAlignment( DT_LEFT );
					}
					else if (m_enumWTL == WTL_MACHINE)
					{
						m_wndReport.ShowWindow( SW_NORMAL );
						// Set Dialog caption; 061005 p�d
						SetWindowText(xml->str(IDS_STRING2121));
						pCol = m_wndReport.AddColumn(new CXTPReportColumn(0, xml->str(IDS_STRING300), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(1, xml->str(IDS_STRING302), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(2, xml->str(IDS_STRING304), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(3, xml->str(IDS_STRING305), 150));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_LEFT );
						pCol->SetAlignment( DT_LEFT );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(4, xml->str(IDS_STRING306), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(5, xml->str(IDS_STRING307), 150));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_LEFT );
						pCol->SetAlignment( DT_LEFT );

					}
					else if (m_enumWTL == WTL_MACHINE_REG)
					{
						m_wndReport.ShowWindow( SW_NORMAL );
						// Set Dialog caption; 061005 p�d
						SetWindowText(xml->str(IDS_STRING2121));
						pCol = m_wndReport.AddColumn(new CXTPReportColumn(0, xml->str(IDS_STRING304), 60));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(1, xml->str(IDS_STRING305), 240));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_LEFT );
						pCol->SetAlignment( DT_LEFT );

					}
					else if (m_enumWTL == WTL_TRAKT)
					{
						// Get text from languagefile; 061207 p�d
						m_sEnteredManual	= xml->str(IDS_STRING112);
						m_sEnteredFromFile	= xml->str(IDS_STRING113);

						m_wndReport.ShowWindow( SW_NORMAL );
						// Set Dialog caption; 061005 p�d
						SetWindowText(xml->str(IDS_STRING2122));
						pCol = m_wndReport.AddColumn(new CXTPReportColumn(0, xml->str(IDS_STRING300), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(1, xml->str(IDS_STRING302), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(2, xml->str(IDS_STRING304), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(3, xml->str(IDS_STRING306), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(4, xml->str(IDS_STRING307), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_LEFT );
						pCol->SetAlignment( DT_LEFT );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(5, xml->str(IDS_STRING308), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_LEFT );
						pCol->SetAlignment( DT_LEFT );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(6, xml->str(IDS_STRING255), 80));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_LEFT );
						pCol->SetAlignment( DT_LEFT );

						pCol = m_wndReport.AddColumn(new CXTPReportColumn(7, xml->str(IDS_STRING311), 50));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_LEFT );
						pCol->SetAlignment( DT_LEFT );
					}

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 061002 p�d
					setResize(&m_wndReport,1,1,rect.right - 2,rect.bottom - 45);

					populateReport();
				}	// if (m_wndReport.GetSafeHwnd() != NULL)

			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CSelectRegionDistDlg::populateReport(void)
{
	CString sEnteredAs;
	if (m_enumWTL == WTL_REGION_DISTRICT)
	{
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			m_wndReport.AddRecord(new CDistrictReportDataRec(i,data.m_nRegionID,
																												data.m_sRegionName,
																												data.m_nDistrictID,
																												data.m_sDistrictName));
		}
	}
	else if (m_enumWTL == WTL_MACHINE)
	{
		for (UINT i = 0;i < m_vecMachineData.size();i++)
		{
			MACHINE_DATA data = m_vecMachineData[i];
			m_wndReport.AddRecord(new CMachineReportDataRec(i,data.m_nRegionID,
																												data.m_sRegionName,
																												data.m_nDistrictID,
																												data.m_sDistrictName,
																												data.m_nMachineID,
																												data.m_sContractorName));
		}
	}
	else if (m_enumWTL == WTL_MACHINE_REG)
	{
		for (UINT i = 0;i < m_vecMachineRegData.size();i++)
		{
			MACHINE_REG_DATA data = m_vecMachineRegData[i];
			m_wndReport.AddRecord(new CMachineRegReportDataRec(i,data.m_nMachineRegID,
																												 data.m_sContractorName));
		}
	}
	else if (m_enumWTL == WTL_TRAKT)
	{
		for (UINT i = 0;i < m_vecTraktData.size();i++)
		{
			TRAKT_DATA data = m_vecTraktData[i];
			if (data.n_nEnterType == 1)
				sEnteredAs = m_sEnteredManual;
			else
				sEnteredAs = m_sEnteredFromFile;
			m_wndReport.AddRecord(new CTraktReportDataRec(i,data.m_nRegionID,
																											data.m_nDistrictID,
																											data.m_nMachineID,
																											data.m_sTraktNum,
																											getInvType(data.m_sType.Trim()),
																											data.m_sOriginName,
																											data.m_sTraktDate,
																											sEnteredAs));
		}
	}

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}


void CSelectRegionDistDlg::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify->pRow)
	{
		if (m_enumWTL == WTL_REGION_DISTRICT)
		{
			CRegionReportDataRec *pRec = (CRegionReportDataRec*)pItemNotify->pItem->GetRecord();
			m_recDistrict = m_vecDistrictData[pRec->getIndex()];
		}
		else if (m_enumWTL == WTL_MACHINE)
		{
			CMachineReportDataRec *pRec = (CMachineReportDataRec*)pItemNotify->pItem->GetRecord();
			m_recMachine = m_vecMachineData[pRec->getIndex()];
		}
		else if (m_enumWTL == WTL_MACHINE_REG)
		{
			CMachineRegReportDataRec *pRec = (CMachineRegReportDataRec*)pItemNotify->pItem->GetRecord();
			m_recMachineReg = m_vecMachineRegData[pRec->getIndex()];
		}
		else if (m_enumWTL == WTL_TRAKT)
		{
			CTraktReportDataRec *pRec = (CTraktReportDataRec*)pItemNotify->pItem->GetRecord();
			m_recTrakt = m_vecTraktData[pRec->getIndex()];
		}

	}	// if (pItemNotify->pRow)
	CDialog::OnOK();
}

void CSelectRegionDistDlg::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	CDialog::OnWindowPosChanging(lpwndpos);
}


void CSelectRegionDistDlg::OnBnClickedOk()
{
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	if (pRow)
	{
		if (m_enumWTL == WTL_REGION_DISTRICT)
		{
			CRegionReportDataRec *pRec = (CRegionReportDataRec*)pRow->GetRecord();
			m_recDistrict = m_vecDistrictData[pRec->getIndex()];
		}
		else if (m_enumWTL == WTL_MACHINE)
		{
			CMachineReportDataRec *pRec = (CMachineReportDataRec*)pRow->GetRecord();
			m_recMachine = m_vecMachineData[pRec->getIndex()];
		}
		else if (m_enumWTL == WTL_MACHINE_REG)
		{
			CMachineRegReportDataRec *pRec = (CMachineRegReportDataRec*)pRow->GetRecord();
			m_recMachineReg = m_vecMachineRegData[pRec->getIndex()];
		}
		else if (m_enumWTL == WTL_TRAKT)
		{
			CTraktReportDataRec *pRec = (CTraktReportDataRec*)pRow->GetRecord();
			m_recTrakt = m_vecTraktData[pRec->getIndex()];
		}

	}

	OnOK();
}
