// GallBasDistrictSelectList.cpp : implementation file
//

#include "stdafx.h"
#include "UMGallBasGenerics.h"
#include "GallBasDistrictSelectList.h"

#include "ResLangFileReader.h"

#include "MDIGallBasDistrictFormView.h"
// CGallBasDistrictSelectList

IMPLEMENT_DYNCREATE(CGallBasDistrictSelectList, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CGallBasDistrictSelectList, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_CLICK, IDC_DISTRICT_REPORT, OnReportItemDblClick)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CGallBasDistrictSelectList::CGallBasDistrictSelectList()
	: CXTResizeFormView(CGallBasDistrictSelectList::IDD)
{
}

CGallBasDistrictSelectList::~CGallBasDistrictSelectList()
{
}

void CGallBasDistrictSelectList::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CGallBasDistrictSelectList::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CGallBasDistrictSelectList::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			
			pDB->getDistricts(m_vecDistrictData);
			delete pDB;
			setupReport();
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

BOOL CGallBasDistrictSelectList::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CGallBasDistrictSelectList diagnostics

#ifdef _DEBUG
void CGallBasDistrictSelectList::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CGallBasDistrictSelectList::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CGallBasDistrictSelectList message handlers
void CGallBasDistrictSelectList::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndDistrictReport.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndDistrictReport,1,1,rect.right - 2,rect.bottom - 2);
	}
}

void CGallBasDistrictSelectList::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CGallBasDistrictSelectList::setupReport(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndDistrictReport.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndDistrictReport.Create(this, IDC_DISTRICT_REPORT,FALSE,FALSE ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndDistrictReport.GetSafeHwnd() != NULL)
				{
					m_wndDistrictReport.ShowWindow( SW_NORMAL );
					pCol = m_wndDistrictReport.AddColumn(new CXTPReportColumn(0, xml->str(IDS_STRING300), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndDistrictReport.AddColumn(new CXTPReportColumn(1, xml->str(IDS_STRING301), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_LEFT );
					pCol->SetAlignment( DT_LEFT );

					pCol = m_wndDistrictReport.AddColumn(new CXTPReportColumn(2, xml->str(IDS_STRING302), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndDistrictReport.AddColumn(new CXTPReportColumn(3, xml->str(IDS_STRING303), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_LEFT );
					pCol->SetAlignment( DT_LEFT );

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 061002 p�d
					setResize(&m_wndDistrictReport,1,1,rect.right - 2,rect.bottom - 2);

					populateReport();
				}	// if (m_wndDistrictReport.GetSafeHwnd() != NULL)

			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CGallBasDistrictSelectList::populateReport(void)
{
	for (UINT i = 0;i < m_vecDistrictData.size();i++)
	{
		DISTRICT_DATA data = m_vecDistrictData[i];
		m_wndDistrictReport.AddRecord(new CDistrictReportDataRec(i,data.m_nRegionID,data.m_sRegionName,data.m_nDistrictID,data.m_sDistrictName));
	}
	m_wndDistrictReport.Populate();
	m_wndDistrictReport.UpdateWindow();
}

void CGallBasDistrictSelectList::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify->pRow)
	{
		CDistrictReportDataRec *pRec = (CDistrictReportDataRec*)pItemNotify->pItem->GetRecord();
		CMDIGallBasDistrictFormView *pView = (CMDIGallBasDistrictFormView *)getFormViewByID(IDD_FORMVIEW1);
		if (pView)
		{
			pView->doPopulate(pRec->getIndex());
		}
	}
}
