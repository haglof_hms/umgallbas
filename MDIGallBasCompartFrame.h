#pragma once


///////////////////////////////////////////////////////////////////////////////////////////

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIGallBasCompartFrame frame

class CMDIGallBasCompartFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDIGallBasCompartFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:


	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDIGallBasCompartFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIGallBasCompartFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CGallBasCompartSelectListFrame frame

class CGallBasCompartSelectListFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CGallBasCompartSelectListFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:


	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CGallBasCompartSelectListFrame();           // protected constructor used by dynamic creation
	virtual ~CGallBasCompartSelectListFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
