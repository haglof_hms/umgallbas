// stdafx.cpp : source file that includes just the standard includes
// UMGallBas.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

long filesize(FILE *stream) 
{
	long curpos, length;
	curpos = ftell(stream);
	fseek(stream,0L,SEEK_END);
	length = ftell(stream);
	fseek(stream, curpos,SEEK_SET);
	return length;
}


CString getInvType(LPCTSTR abbrev_type)
{
	for (int i = 0;i < (sizeof(INVENTORY_TYPE_ARRAY)/sizeof(INVENTORY_TYPE_ARRAY[0]));i++)
	{
		if (_tcscmp(abbrev_type,INVENTORY_TYPE_ARRAY[i].sAbbrevInvTypeName) == 0)
			return INVENTORY_TYPE_ARRAY[i].sInvTypeName;
	}
	return _T("");
}


void setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
    COPYDATASTRUCT HSData;
    memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv,WM_COPYDATA,(WPARAM)hWndSend, (LPARAM)&HSData);
	}
}

BOOL runSQLScriptFileEx1(vecScriptFiles &vec,Scripts::actionTypes action)
{
	TCHAR sDB_PATH[127]=_T("");
	TCHAR sUserName[127]=_T("");
	TCHAR sPSW[127]=_T("");
	TCHAR sDSN[127]=_T("");
	TCHAR sLocation[127]=_T("");
	TCHAR sDBName[127]=_T("");
	int nAuthentication=0;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	Scripts scripts;

	CString sScript=_T("");

	try
	{

		if (vec.size() > 0)
		{

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{

							for (UINT i = 0;i < vec.size();i++)
							{
								scripts = vec[i];

								if (!scripts.getDBName().IsEmpty())
											_tcscpy_s(sDBName,127,scripts.getDBName());

								// Set database as set in sDBName; 061109 p�d
								// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d

								if (pDB->existsDatabase(sDBName))
								{
									if (action == Scripts::TBL_CREATE)
									{
										if (!pDB->existsTableInDB(sDBName,scripts.getTableName()))
										{
											pDB->setDefaultDatabase(sDBName);
											sScript.Format(scripts.getScript(),scripts.getTableName());
											pDB->commandExecute(sScript);
											bReturn = TRUE;
										}	// if (!pDB->existsTableInDB(sDBName,scripts._TableName))
									}	// if (scripts._CreateTable)
									else if (action == Scripts::TBL_ALTER)
									{
										if (pDB->existsTableInDB(sDBName,scripts.getTableName()))
										{
											pDB->setDefaultDatabase(sDBName);
											sScript.Format(scripts.getScript(),scripts.getTableName(),scripts.getTableName());
											pDB->commandExecute(sScript);
											bReturn = TRUE;
										}	// if (pDB->existsTableInDB(sDBName,scripts._TableName))
									}	// else if (!scripts._CreateTable)
								}	// if (pDB->existsDatabase(sDBName))
							} // for (UINT i = 0;i < vec.size();i++)
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (!script.IsEmpty())
	}
	catch(_com_error &e)
	{
		MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

