#include "StdAfx.h"

#include "UMGallBasGenerics.h"

#include "ResLangFileReader.h"

#include "MessageDlg.h"

#include "AddDataToDataBase.h"

#include "UMGallBasDB.h"

#include <fstream>
#include <iostream>

void showFormView(int idd,LPCTSTR lang_fn)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;


	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					pView->SendMessage(ID_UPDATE_ITEM);
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle;
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
				}

				break;
			}
		}
	}
}


CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

BOOL messageDialog(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR cancel_btn,LPCTSTR msg)
{
	BOOL bReturn = FALSE;
	CMyMessageDlg *dlg = new CMyMessageDlg(cap,ok_btn,cancel_btn,msg);

	bReturn = (dlg->DoModal() == IDOK);

	delete dlg;

	return bReturn;
}


BOOL createFromFile(DB_CONNECTION_DATA con,LPCTSTR lang_fn,TRAKT_IDENTIFER *data)
{
	CString sTitle;
	CString sFileName;
	CString sMsg1;
	CString sMsg2;
	CString sMsg3;
	CString sMsg4;
	CString sCaption;
	if (fileExists(lang_fn))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(lang_fn))
		{
			sTitle = xml->str(IDS_STRING200);
			sMsg1.Format(_T("%s\n\n%s"),xml->str(IDS_STRING5210),xml->str(IDS_STRING5211));
			sMsg2.Format(_T("%s\n\n%s"),xml->str(IDS_STRING5210),xml->str(IDS_STRING5212));
			sMsg3.Format(_T("%s\n\n%s"),xml->str(IDS_STRING5210),xml->str(IDS_STRING5213));
			sMsg4.Format(_T("%s\n\n%s"),xml->str(IDS_STRING5210),xml->str(IDS_STRING5214));
			sCaption = xml->str(IDS_STRING109);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	CMyFileDialog fdlg(sTitle,TRUE,DEFAULT_EXTENSION,_T(""),OFN_FILEMUSTEXIST | OFN_HIDEREADONLY /*| OFN_ALLOWMULTISELECT*/,FILEOPEN_FILTER);
	if (fdlg.DoModal() == IDOK)
	{
		CJonasDLL_handling *hnd = new CJonasDLL_handling();
		if (hnd)
		{
			POSITION pos = fdlg.GetStartPosition();
			while (pos)
			{
				sFileName = fdlg.GetNextPathName(pos);
#ifdef UNICODE
				USES_CONVERSION;
#endif
				hnd->enterDataToDB(con,
#ifdef UNICODE
										W2A(sFileName.GetString()),
										W2A(extractFileName(sFileName).GetString()),
#else
										sFileName.GetString(),
										extractFileName(sFileName).GetString(),
#endif
										data,
										sCaption,
										sMsg1,
										sMsg2,
										sMsg3,
										sMsg4);
			}
			
			delete hnd;
		
		}	// if (hnd)
		return TRUE;
	}	// if (fdlg.DoModal() == IDOK)
	return FALSE;
}


// Get integer value from a Combo box; 061017 p�d
int getCBoxInt(const CComboBox *cb)
{
	CString sText;
	if (cb != NULL)
	{
		cb->GetWindowText(sText);
		return _tstoi(sText);
	}

	return -1;
}

// Get string value from a Combo box; 061017 p�d
CString getCBoxStr(const CComboBox *cb)
{
	CString sText;
	if (cb != NULL)
	{
		cb->GetWindowText(sText);
		return sText;
	}

	return _T("");
}


// Check if there's a database to connect to; 061027 p�d
// This function is, more or less, obsolete when using
// connection through HMSShell program.
// Not using this function, inproves the overall action
// of the DB handling; 010722 p�d
BOOL isDBConnection(LPCTSTR lang_fn)
{
/*
	CGallBasDB *pDB;
	BOOL bConnected = FALSE;
	TCHAR m_sDB_PATH[127];
	TCHAR m_sUserName[127];
	TCHAR m_sPSW[127];
	SAClient_t m_saClient;
	CString sMsgCap;
	CString sMsg;

	if (getDBUserInfo(m_sDB_PATH,m_sUserName,m_sPSW,&m_saClient))
	{
		if (strcmp(m_sDB_PATH,"") != 0 &&
			  strcmp(m_sUserName,"") != 0 &&
				strcmp(m_sPSW,"") != 0)
		{
			pDB = new CGallBasDB(m_saClient,m_sDB_PATH,m_sUserName,m_sPSW);
			if (pDB != NULL)
			{
				bConnected = pDB->connectToDataBase();
			}	// if (m_pDB != NULL)
		}
	}	// if (getDBUserInfo(m_sDB_PATH,m_sUserName,m_sPSW,&m_saClient))

	// If not connected to a database, tell user ... ; 061027 p�d
	if (!bConnected)
	{
		RLFReader *xml = new RLFReader();
		if (xml->Load(lang_fn))
		{
			sMsgCap = xml->str(IDS_STRING109);
			sMsg.Format("%s\n\n%s\n\n%s",
									xml->str(IDS_STRING5151),
									xml->str(IDS_STRING5152),
									xml->str(IDS_STRING5153));
		}
		delete xml;

		::MessageBox(0,sMsg,sMsgCap,MB_ICONASTERISK | MB_OK);
	}
	return bConnected;
*/
	return TRUE;
}

// Create database tables form SQL scriptfiles; 061109 p�d
#define BUFFER_SIZE		1024*100		// 100 kb buffer
// Create database tables form SQL scriptfiles; 061109 p�d
BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table)
{
	UINT nFileSize;
	TCHAR sBuffer[BUFFER_SIZE];	// 10 kb buffer

	TCHAR sSQL[127];
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;
	int nAuthentication;

	CString S;

	try
	{

		if (fileExists(fn))
		{
			CFile file(fn,CFile::modeRead);
			nFileSize = file.Read(sBuffer,BUFFER_SIZE);
			file.Close();

			sBuffer[nFileSize] = '\0';

			GetAuthentication(&nAuthentication);
			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T(""))	!= 0 &&
							 _tcscmp(sDBName,_T(""))	!= 0 &&
							 _tcscmp(sUserName,_T(""))	!= 0 &&
							 _tcscmp(sPSW,_T(""))		!= 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
						CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
						if (pDB)
						{
							if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
							{
								// Set database as set in sDBName; 061109 p�d
								// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
								if (pDB->existsDatabase(sDBName))
								{
									if (!pDB->existsTableInDB(sDBName,check_table))
									{
										pDB->setDefaultDatabase(sDBName);
										pDB->commandExecute(sBuffer);
									}
								}	// if (pDB->existsDatabase(sDBName))
								else
								{
									_stprintf(sSQL,_T("create database %s"),sDBName);
									if (pDB->commandExecute(sBuffer))
									{
										pDB->setDefaultDatabase(sDBName);
										pDB->commandExecute(sBuffer);
									}
								}
							}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
							delete pDB;
						}	// if (pDB)
					bReturn = TRUE;
				}	// if (bIsOK)
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}
