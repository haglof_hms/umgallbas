#pragma once
#include <SQLAPI.h> // main SQLAPI++ header

#include "UMGallBasDB.h"

#include "Resource.h"

// CMDIGallBasMachinesFormView form view

class CMDIGallBasMachinesFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIGallBasMachinesFormView)

protected:
	CMDIGallBasMachinesFormView();           // protected constructor used by dynamic creation
	virtual ~CMDIGallBasMachinesFormView();

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;

	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;

	CXTResizeGroupBox m_wndGroup;

	vecMachineData m_vecMachineData;
	UINT m_nDBIndex;

	CString m_sLangFN;

	void getMachinesFromDB(void);

	void setLanguage(void);

	void populateData(UINT);

	void setNavigationButtons(BOOL,BOOL);

	void deleteMachine(void);
	void addMachine(void);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:
	enum { IDD = IDD_FORMVIEW2 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	BOOL getIsDirty(void) { return m_wndEdit6.isDirty(); }

	// Needs to be Public, to be visible in CMDIGallBasMachinesFrame; 061017 p�d
	void saveMachine(void);

	void doPopulate(UINT);

	void resetMachine(int todo);
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIGallBasRegionFormView)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


