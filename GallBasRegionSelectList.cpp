
// GallBasRegionSelectList.cpp : implementation file
//

#include "stdafx.h"
#include "UMGallBasGenerics.h"
#include "GallBasRegionSelectList.h"

#include "ResLangFileReader.h"

#include "MDIGallBasRegionFormView.h"
// CGallBasRegionSelectList

IMPLEMENT_DYNCREATE(CGallBasRegionSelectList, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CGallBasRegionSelectList, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_CLICK, IDC_REGION_REPORT, OnReportItemDblClick)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CGallBasRegionSelectList::CGallBasRegionSelectList()
	: CXTResizeFormView(CGallBasRegionSelectList::IDD)
{
}

CGallBasRegionSelectList::~CGallBasRegionSelectList()
{
}

void CGallBasRegionSelectList::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CGallBasRegionSelectList::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CGallBasRegionSelectList::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getRegions(m_vecRegionData);
			delete pDB;

			setupReport();
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

BOOL CGallBasRegionSelectList::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CGallBasRegionSelectList diagnostics

#ifdef _DEBUG
void CGallBasRegionSelectList::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CGallBasRegionSelectList::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CGallBasRegionSelectList message handlers

void CGallBasRegionSelectList::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndRegionReport.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndRegionReport,1,1,rect.right - 2,rect.bottom - 2);
	}
}

void CGallBasRegionSelectList::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}


// Create and add Assortment settings reportwindow
BOOL CGallBasRegionSelectList::setupReport(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndRegionReport.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndRegionReport.Create(this, IDC_REGION_REPORT,FALSE,FALSE ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndRegionReport.GetSafeHwnd() != NULL)
				{

					m_wndRegionReport.ShowWindow( SW_NORMAL );
          pCol = m_wndRegionReport.AddColumn(new CXTPReportColumn(0, xml->str(IDS_STRING300), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndRegionReport.AddColumn(new CXTPReportColumn(1, xml->str(IDS_STRING301), 200));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_LEFT );
					pCol->SetAlignment( DT_LEFT );

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 061002 p�d
					setResize(&m_wndRegionReport,1,1,rect.right - 2,rect.bottom - 2);

					populateReport();
				}	// if (m_wndRegionReport.GetSafeHwnd() != NULL)

			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CGallBasRegionSelectList::populateReport(void)
{
	for (UINT i = 0;i < m_vecRegionData.size();i++)
	{
		REGION_DATA data = m_vecRegionData[i];
		m_wndRegionReport.AddRecord(new CRegionReportDataRec(i,data.m_nRegionID,data.m_sRegionName));
	}
	m_wndRegionReport.Populate();
	m_wndRegionReport.UpdateWindow();
}

void CGallBasRegionSelectList::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify->pRow)
	{
		CRegionReportDataRec *pRec = (CRegionReportDataRec*)pItemNotify->pItem->GetRecord();
		CMDIGallBasRegionFormView *pView = (CMDIGallBasRegionFormView *)getFormViewByID(IDD_FORMVIEW);
		if (pView)
		{
			pView->doPopulate(pRec->getIndex());
		}
	}
}
