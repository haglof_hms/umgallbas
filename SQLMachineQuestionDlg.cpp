// SQLMachineQuestionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SQLMachineQuestionDlg.h"
#include "UMGallBasDB.h"

#include "ResLangFileReader.h"
#include ".\sqlmachinequestiondlg.h"

// CSQLMachineQuestionDlg dialog

IMPLEMENT_DYNAMIC(CSQLMachineQuestionDlg, CDialog)

BEGIN_MESSAGE_MAP(CSQLMachineQuestionDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_WM_COPYDATA()
	ON_CBN_SELCHANGE(IDC_COMBO1, &CSQLMachineQuestionDlg::OnCbnSelchangeCombo1)
END_MESSAGE_MAP()

CSQLMachineQuestionDlg::CSQLMachineQuestionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSQLMachineQuestionDlg::IDD, pParent)
{
}

CSQLMachineQuestionDlg::~CSQLMachineQuestionDlg()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

void CSQLMachineQuestionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_COMBO2, m_wndCBox2);

	DDX_Control(pDX, IDC_BUTTON1, m_wndBtn1);
	DDX_Control(pDX, IDOK, m_wndOK);
	//}}AFX_DATA_MAP
}

BOOL CSQLMachineQuestionDlg::OnInitDialog()
{

	CDialog::OnInitDialog();

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	setLanguage();

	// Get regions; 061204 p�d
	getRegionsFromDB();
	addRegionsToCBox();
	// Get districts (and region info); 061011 p�d
	getDistrictFromDB();

	setSQLQuestionData();

	return TRUE;
}

BOOL CSQLMachineQuestionDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CGallBasDB(m_dbConnectionData);
		}
	}
	return CDialog::OnCopyData(pWnd, pData);
}

// CSQLMachineQuestionDlg message handlers


// MY METHODS

void CSQLMachineQuestionDlg::setLanguage(void)
{

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText(xml->str(IDS_STRING401));
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING4020));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING403));
			m_wndLbl3.SetWindowText(xml->str(IDS_STRING404));

			m_wndBtn1.SetWindowText(xml->str(IDS_STRING4000));

			m_sMsgCap = xml->str(IDS_STRING109);
			m_sMsg1 = xml->str(IDS_STRING508);
		}
		delete xml;
	}
}

// Read an setup the SQL question last set into dialog fields; 061013 p�d
void CSQLMachineQuestionDlg::setSQLQuestionData(void)
{
	FILE *fp;
	SQL_MACHINE_QUESTION recMachine;
	CString sPathAndFN;
	CString sTmp;
	int nIndex = -1;

	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_MACHINE_QUESTION_FN);

	if (fileExists(sPathAndFN))
	{
		if ((fp = _tfopen(sPathAndFN,_T("rb"))) != NULL)
		{
			fread(&recMachine,sizeof(SQL_MACHINE_QUESTION),1,fp);
			fclose(fp);

			if (recMachine.nRegionID > 0)
				setRegionLastSelected(recMachine.nRegionID);
			if (recMachine.nDistrictID > 0)
				setDistrictLastSelected(recMachine.nDistrictID);
		}	// if ((fp = fopen(sPathAndFN,"rb")) != NULL)
	}	// if (fileExists(sPathAndFN))
}

void CSQLMachineQuestionDlg::getRegionsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		if (m_pDB != NULL)
		{
			m_pDB->getRegions(m_vecRegionData);
		}	// if (m_pDB != NULL)
	} // 	if (m_bConnected)
}

void CSQLMachineQuestionDlg::setRegionLastSelected(int reg_id)
{
	if (m_wndCBox1.GetCount() > 0 && m_vecRegionData.size() > 0)
	{
		for (UINT i = 0;i < m_vecRegionData.size();i++)
		{
			REGION_DATA data = m_vecRegionData[i];
			if (data.m_nRegionID == reg_id)
			{
				m_wndCBox1.SetCurSel(i+1);
				addDistrictToCBox(data.m_nRegionID);
			}	// if (data.m_nRegionID == reg_id)
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_wndCBox1.GetCount() > 0 && m_vecRegionData.size() > 0)
}

// Add regions in registry, into ComboBox3; 061204 p�d
void CSQLMachineQuestionDlg::addRegionsToCBox(void)
{
	CString sText;
	if (m_vecRegionData.size() > 0)
	{
		m_wndCBox1.ResetContent();
		m_wndCBox1.AddString(sText);	// To set empty box; 070419 p�d
		m_wndCBox1.SetItemData(0,-1);
		for (UINT i = 0;i < m_vecRegionData.size();i++)
		{
			REGION_DATA data = m_vecRegionData[i];
			sText.Format(_T("%d - %s"),data.m_nRegionID,data.m_sRegionName);
			m_wndCBox1.AddString(sText);
			m_wndCBox1.SetItemData(i+1,data.m_nRegionID);
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}

void CSQLMachineQuestionDlg::getDistrictFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		if (m_pDB != NULL)
		{
			m_pDB->getDistricts(m_vecDistrictData);
		}	// if (m_pDB != NULL)
	} // 	if (m_bConnected)
}

void CSQLMachineQuestionDlg::setDistrictLastSelected(int district_id)
{
	if (m_wndCBox2.GetCount() > 0 && m_vecDistrictData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			if (data.m_nDistrictID == district_id)
			{
				m_wndCBox2.SetCurSel(i+1);
			}
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_wndCBox1.GetCount() > 0 && m_vecRegionData.size() > 0)
}

// Add regions in registry, into ComboBox3; 061204 p�d
void CSQLMachineQuestionDlg::addDistrictToCBox(int reg_id)
{
	CString sText;
	if (m_vecDistrictData.size() > 0)
	{
		m_wndCBox2.ResetContent();
		m_wndCBox2.AddString(sText);	// To set empty box; 070419 p�d
		m_wndCBox2.SetItemData(0,-1);
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			if (data.m_nRegionID == reg_id)
			{
				sText.Format(_T("%d - %s"),data.m_nDistrictID,data.m_sDistrictName);
				m_wndCBox2.AddString(sText);
				m_wndCBox2.SetItemData(i+1,data.m_nDistrictID);
			}
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}


void CSQLMachineQuestionDlg::OnBnClickedOk()
{
	FILE *fp;
	int nRegionID;
	int nDistrictID;
	CString sPathAndFN;
	SQL_MACHINE_QUESTION recMachine;
	TCHAR szBuffer[1024];
	TCHAR szSQL[1024];

	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_MACHINE_QUESTION_FN);
	nRegionID = m_wndCBox1.GetItemData(m_wndCBox1.GetCurSel());
	nDistrictID = m_wndCBox2.GetItemData(m_wndCBox2.GetCurSel());

	recMachine.nRegionID = nRegionID;
	recMachine.nDistrictID = nDistrictID;
	
	if (nRegionID > 0 && nDistrictID == -1)
	{
		_tcscpy_s(szBuffer,1024,SQL_MACHINE_QUESTION2);
		_stprintf(szSQL,szBuffer,TBL_MACHINE,TBL_DISTRICT,TBL_REGION,nRegionID);
	} // if (nRegionID > 0 && nDistrictID == -1)
	if (nRegionID > -1 && nDistrictID > -1)
	{
		_tcscpy_s(szBuffer,1024,SQL_MACHINE_QUESTION4);
		_stprintf(szSQL,szBuffer,TBL_MACHINE,TBL_DISTRICT,TBL_REGION,nRegionID,nDistrictID);
	}	// if (nRegionID > -1 && nDistrictID > -1)
	if (nRegionID == -1 && nDistrictID == -1)
	{
		_tcscpy_s(szBuffer,1024,SQL_MACHINE_QUESTION1);
		_stprintf(szSQL,szBuffer,TBL_MACHINE,TBL_DISTRICT,TBL_REGION);
	}	// if (nRegionID == -1 && nDistrictID == -1)
	if ((fp = _tfopen(sPathAndFN,_T("wb"))) != NULL)
	{
		_tcscpy_s(recMachine.szSQL,1024,szSQL);

		fwrite(&recMachine,sizeof(SQL_MACHINE_QUESTION),1,fp);

		fclose(fp);
	}

	// Check if SQL Question gives any result. If not, tell user to redefine the question; 061012 p�d
	if (doesSQLQuestionReturnsAResult())
		OnOK();
	else
		::MessageBox(0,m_sMsg1,m_sMsgCap,MB_OK);
}

BOOL CSQLMachineQuestionDlg::doesSQLQuestionReturnsAResult(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		if (m_pDB != NULL)
		{
			bReturn = m_pDB->isThereAnyMachineData();
		}	// if (m_pDB != NULL)
	} // 	if (m_bConnected)
	return bReturn;
}

void CSQLMachineQuestionDlg::OnBnClickedButton1()
{
	FILE *fp;
	SQL_MACHINE_QUESTION recMachine;
	TCHAR szBuffer[512];
	TCHAR szSQL[512];
	CString sPathAndFN;
	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_MACHINE_QUESTION_FN);

	_tcscpy(szBuffer,SQL_MACHINE_QUESTION1);
	_stprintf(szSQL,szBuffer,TBL_MACHINE,TBL_DISTRICT,TBL_REGION);

	recMachine.nRegionID	= 0;
	recMachine.nDistrictID = 0;
	_tcscpy(recMachine.szSQL,szSQL);

	if ((fp = _tfopen(sPathAndFN,_T("wb"))) != NULL)
	{

		fwrite(&recMachine,sizeof(SQL_MACHINE_QUESTION),1,fp);

		fclose(fp);
	}

	OnOK();
}

void CSQLMachineQuestionDlg::OnCbnSelchangeCombo1()
{
	int nIndex = m_wndCBox1.GetCurSel();	
	int nRegId = m_wndCBox1.GetItemData(nIndex);
	addDistrictToCBox(nRegId);
}
