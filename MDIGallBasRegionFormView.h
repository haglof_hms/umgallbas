#pragma once
#include <SQLAPI.h> // main SQLAPI++ header

#include "UMGallBasDB.h"
#include "Resource.h"

// CMDIGallBasRegionFormView form view

class CMDIGallBasRegionFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIGallBasRegionFormView)

	CString m_sLangAbbrev;
	CString m_sLangFN;

protected:
	CMDIGallBasRegionFormView();           // protected constructor used by dynamic creation
	virtual ~CMDIGallBasRegionFormView();

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;

	CXTResizeGroupBox m_wndGroup;

	vecRegionData m_vecRegionData;
	UINT m_nDBIndex;

	void setLanguage(void);

	void populateData(UINT);

	void setNavigationButtons(BOOL,BOOL);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:
	enum { IDD = IDD_FORMVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void doPopulate(UINT);
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIGallBasRegionFormView)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};