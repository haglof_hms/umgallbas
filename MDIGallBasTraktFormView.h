#pragma once

#include "UMGallBasDB.h"

#include "DatePickerCombo.h"

#include "Resource.h"


// REGION DATA STRUCTURES
typedef struct _trakt_avslut_data
{
	int m_nID;
	CString m_sName;

	_trakt_avslut_data(void)
	{
		m_nID = -1;
		m_sName = "";
	}

	_trakt_avslut_data(int id,LPCTSTR name)
	{
		m_nID = id;
		m_sName = name;
	}
} TRAKT_AVSLUT_DATA;



// CMDIGallBasTraktFormView form view

class CMDIGallBasTraktFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIGallBasTraktFormView)

protected:
	CMDIGallBasTraktFormView();           // protected constructor used by dynamic creation
	virtual ~CMDIGallBasTraktFormView();
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;
	CMyExtEdit m_wndEdit7;
	CMyExtEdit m_wndEdit8;
	CMyExtEdit m_wndEdit9;
	CMyExtEdit m_wndEdit10;
	CMyExtEdit m_wndEdit11;
	CMyExtEdit m_wndEdit12;
	CMyExtEdit m_wndEdit13;
	CMyExtEdit m_wndEdit14;
	CMyExtEdit m_wndEdit15;
	CMyExtEdit m_wndEdit16;
	CMyExtEdit m_wndEdit17;
	CMyExtEdit m_wndEdit18;
	CMyExtEdit m_wndEdit19;
	CMyExtEdit m_wndEdit20;
	CMyExtEdit m_wndEdit21;
	CMyExtEdit m_wndEdit22;
	CMyExtEdit m_wndEdit23;
	CMyExtEdit m_wndEdit24;
	CMyExtEdit m_wndEdit26;
	CMyExtEdit m_wndEdit27;
	CMyExtEdit m_wndEdit28;
	CMyExtEdit m_wndEdit29;
	CMyExtEdit m_wndEdit30;
	CMyExtEdit m_wndEdit31;
	CMyExtEdit m_wndEdit32;
	CMyExtEdit m_wndEdit33;	// Added 061018 p�d (Areal)
	CMyExtEdit m_wndEdit34;	// Added 070419 p�d (Notes)
	
	CMyExtEdit m_wndEdit_Forrensat;
	CMyExtStatic m_wndLbl_Forrensat;

	CMyExtStatic m_wndLbl3_1;
	CMyExtStatic m_wndLbl3_2;
	CMyExtStatic m_wndLbl3_5;
	CMyExtStatic m_wndLbl3_6;
	CMyExtStatic m_wndLbl3_7;
	CMyExtStatic m_wndLbl3_8;
	CMyExtStatic m_wndLbl3_9;
	CMyExtStatic m_wndLbl3_10;
	CMyExtStatic m_wndLbl3_11;
	CMyExtStatic m_wndLbl3_12;
	CMyExtStatic m_wndLbl3_13;
	CMyExtStatic m_wndLbl3_14;
	CMyExtStatic m_wndLbl3_15;
	CMyExtStatic m_wndLbl3_16;
	CMyExtStatic m_wndLbl3_17;
	CMyExtStatic m_wndLbl3_18;
	CMyExtStatic m_wndLbl3_19;
	CMyExtStatic m_wndLbl3_20;
	CMyExtStatic m_wndLbl3_21;
	CMyExtStatic m_wndLbl3_22;
	CMyExtStatic m_wndLbl3_23;
	CMyExtStatic m_wndLbl3_24;
//	CMyExtStatic m_wndLbl3_25;
	CMyExtStatic m_wndLbl3_26;
	CMyExtStatic m_wndLbl3_27;
	CMyExtStatic m_wndLbl3_28;
	CMyExtStatic m_wndLbl3_29;
	CMyExtStatic m_wndLbl3_30;
	CMyExtStatic m_wndLbl3_31;
	CMyExtStatic m_wndLbl3_32;
	CMyExtStatic m_wndLbl3_33;
	CMyExtStatic m_wndLbl3_34;
	CMyExtStatic m_wndLbl3_35;	// Added 061018 p�d (Areal)
	CMyExtStatic m_wndLbl3_36;	// Added 061201 p�d (Uppf�ljningsdatum)

	CXTResizeGroupBox m_wndGroup_Trakt_Avslut;

	CMyExtStatic m_wndLbl_Trakt_Avslut_Slutford;
	CMyExtStatic m_wndLbl_Trakt_Avslut_RattAtgTp;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Forklaring;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Maskinstorlek;
	CMyExtStatic m_wndLbl_Trakt_Avslut_MaskinNummer;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Myr;

	CMyExtStatic m_wndLbl_Trakt_Avslut_Hallmark;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Hansyn;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Fdkulturmark;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Kulturmiljo;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Lovandel;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Skyddszon;
	CMyExtStatic m_wndLbl_Trakt_Avslut_NVtrad;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Framtidstrad;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Hogstubbar;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Dodatrad;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Markskador;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Markskador2;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Nedskrapning;
	CMyExtStatic m_wndLbl_Trakt_Avslut_MindreKorskada;
	CMyExtStatic m_wndLbl_Trakt_Avslut_AllvarligKorskada;
	CMyExtStatic m_wndLbl_Trakt_Avslut_Fritext;



	CMyExtEdit m_wndEdit_Trakt_Avslut_Slutford;
	CMyExtEdit m_wndEdit_Trakt_Avslut_MaskinNummer;
	CMyExtEdit m_wndEdit_Trakt_Avslut_Forklaring;

	CMyComboBox m_wndCBox_Trakt_Avslut_RattAtgTp;
	CMyComboBox m_wndCBox_Trakt_Avslut_Maskinstorlek;
	CMyComboBox m_wndCBox_Trakt_Avslut_Myr;
	CMyComboBox m_wndCBox_Trakt_Avslut_Hallmark;
	CMyComboBox m_wndCBox_Trakt_Avslut_Hansyn;
	CMyComboBox m_wndCBox_Trakt_Avslut_Fdkulturmark;
	CMyComboBox m_wndCBox_Trakt_Avslut_Kulturmiljo;
	CMyComboBox m_wndCBox_Trakt_Avslut_Lovandel;
	CMyComboBox m_wndCBox_Trakt_Avslut_Skyddszon;
	CMyComboBox m_wndCBox_Trakt_Avslut_NVtrad;
	CMyComboBox m_wndCBox_Trakt_Avslut_Framtidstrad;
	CMyComboBox m_wndCBox_Trakt_Avslut_Hogstubbar;
	CMyComboBox m_wndCBox_Trakt_Avslut_Dodatrad;
	CMyComboBox m_wndCBox_Trakt_Avslut_Markskador;
	CMyComboBox m_wndCBox_Trakt_Avslut_Markskador2;
	CMyComboBox m_wndCBox_Trakt_Avslut_Nedskrapning;
	CMyComboBox m_wndCBox_Trakt_Avslut_MindreKorskada;
	CMyComboBox m_wndCBox_Trakt_Avslut_AllvarligKorskada;

	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Myr;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Hallmark;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Hansyn;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Fdkulturmark;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Kulturmiljo;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Lovandel;	
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Skyddszon;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_NVtrad;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Framtidstrad;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Hogstubbar;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Dodatrad;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Markskador;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Markskador2;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_Nedskrapning;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_MindreKorskada;
	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_AllvarligKorskada;

	vector<TRAKT_AVSLUT_DATA> vec_Trakt_Avslut_RattAtgTp;
	vector<TRAKT_AVSLUT_DATA> vec_Avslut_Forklaring;
	vector<TRAKT_AVSLUT_DATA> vec_Avslut_Maskinstorlek;





	void setTraktAvslutArrays();


	CMyComboBox m_wndCBox1;
	CMyComboBox m_wndCBox2;
	CMyComboBox m_wndCBox3;	// Region
	CMyComboBox m_wndCBox4;	// District
	
	CDatePickerCombo m_wndDatePicker;

	CXTResizeGroupBox m_wndIDGroup;
	CXTResizeGroupBox m_wndTraktGroup;
	CXTResizeGroupBox m_wndRoadGroup;
	CXTResizeGroupBox m_wndGYGroup;
	CXTResizeGroupBox m_wndVolGroup;
	CXTResizeGroupBox m_wndPropGroup;
	CXTResizeGroupBox m_wndRestGroup;
	CXTResizeGroupBox m_wndNotesGroup;

	CXTButton m_wndListMachineBtn;

	vecRegionData m_vecRegionData;
	vecDistrictData m_vecDistrictData;
	vecMachineData m_vecMachineData;
	vecTraktData m_vecTraktData;
	CString m_sMachineIDInfo;
	UINT m_nDBIndex;

	MACHINE_DATA m_recNewMachine;
	TRAKT_DATA m_recNewTrakt;
	TRAKT_DATA m_recActiveTrakt;

	CString m_sLangFN;
	BOOL m_bIsDirty;
	CString m_sErrCap;
	CString m_sErrMsg;
	CString m_sUpdateTraktMsg;
	CString m_sNotOkToSave;
	CString m_sSaveData;

	enumACTION m_enumAction;
	
	void setLanguage(void);

	void getRegionsFromDB(void);
	void getDistrictFromDB(void);
	void getMachineFromDB(void);
	void getTraktsFromDB(void);

	void populateData(UINT);

	void setNavigationButtons(BOOL,BOOL);

	void addTraktManually(void);
	void deleteTrakt(void);
	void addTrakt(void);

	void setDefaultTypeAndOrigin(void);

	void setAllReadOnly(BOOL ro1,BOOL ro2);
	void clearAll(void);
	void getEnteredData(void);
	CString getType_setByUser(void);
	CString getTypeName_setByUser(void);
	int getOrigin_setByUser(void);
	CString getOriginName_setByUser(void);

	TRAKT_IDENTIFER	m_structTraktIdentifer;

	CString getRegionAndDistrictData(int region,int dist,DISTRICT_DATA &rec);
	CString getMachineEntr(int region,int dist,int machine);

	void addRegionsToCBox(void);
	CString getRegionSelected(int region_num);
	int getRegionID(void);
	void addDistrictsToCBox(void);
	CString getDistrictSelected(int region_num,int district_num);
	int getDistrictID(void);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:
	enum { IDD = IDD_FORMVIEW3 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	BOOL getIsDirty(void);
	void resetIsDirty(void);	// Set ALL edit boxes etc. to NOT DIRTY; 061113 p�d
	// Needs to be Public, to be visible in CMDIGallBasTraktFrame; 061017 p�d
	void saveTrakt(void);
	// Check if it's ok to save trakt; 061113 p�d
	BOOL isOKToSave(void);
	// Check if active data's chnged by user.
	// I.e. manually enterd data; 061113 p�d
	BOOL isDataChanged(void);

	void doPopulate(UINT);

	void resetTrakt(enumRESET reset);

	// Calculate Road surface; 061113 p�d
	void calculateRoadSurface(void);
	// Calculate Total volume; 061113 p�d
	void calculateTotalVolume(void);
	// Calculate "Tr�dslagsandel", based on volume/specie; 061113 p�d
	void calculateSpecieDiv(void);
	// Calculate GY Withdraw and Quaota; 061113 p�d
	void claculateWDraw(void);

	
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIGallBasRegionFormView)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton2();
	afx_msg void OnEnChangeEdit7();
	afx_msg void OnEnChangeEdit8();
	afx_msg void OnEnChangeEdit14();
	afx_msg void OnEnChangeEdit15();
	afx_msg void OnEnChangeEdit16();
	afx_msg void OnEnChangeEdit17();
	afx_msg void OnEnChangeEdit10();
	afx_msg void OnEnChangeEdit11();
	afx_msg void OnCbnSelchangeCombo3();
	CString m_Text;
};


