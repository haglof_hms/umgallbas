/////////////////////////////////////////////////////////////////////////////////////
// AddDataToDataBase; 
#include "StdAfx.h"
#include "AddDataToDataBase.h"
#include "ComfirmationDlg.h"
//#include "ParseTextFile.h"


double my_atof(LPCTSTR var)
{
	CStringA v;
	v = var;
	struct lconv *conv = localeconv();
	v.Replace(".", conv->decimal_point);

	return atof(v);
}



CMyFileDialog::CMyFileDialog(LPCTSTR title,BOOL open,LPCTSTR def_ext,LPCTSTR filename,DWORD flags,LPCTSTR filter)
	: CFileDialog(open,def_ext,filename,flags,filter)
{
	m_sTitle = title;
}

BOOL CMyFileDialog::OnInitDialog()
{
	GetParent()->SetWindowText(m_sTitle);

	return CFileDialog::OnInitDialog();
}

///////////////////////////////////////////////////////////////////////////////////
//	CJonasDLL_handling



// PROTECTED
int CJonasDLL_handling::openHnd(char *fn)
{
	int nReturn = 0;
	
	nReturn = mTrakt.OpenFile(fn);
	return nReturn;
}

BOOL CJonasDLL_handling::getFileHeader(void)
{
	BOOL bIsOK = FALSE;
	m_clsHeader = CTraktHeader(&mTrakt);
	return bIsOK;
}

BOOL CJonasDLL_handling::getTraktVar(void)
{
	double fPropCont = 0.0;
	BOOL bIsOK = FALSE;
	m_clsTraktVar = CVariables(&mTrakt,TH_LEVEL_TRAKT);

	// Added 2007-08-15 p�d
	// Check volume and "andel" for specie "contorta", to see that we have 100 %
	// Criteria; if volume for specie equals 0.0 then "andel" also should be 0.0; 070815 p�d
	// Criteria; if volume for specie > 0.0 then calculate "andel" from total volume; 070815 p�d
	if (m_clsTraktVar.getVolCont() > 0.0 && m_clsTraktVar.getVolTotal() > 0.0)
	{
		fPropCont = (m_clsTraktVar.getVolCont() / m_clsTraktVar.getVolTotal()) * 100.0;
	}

	m_clsTraktVar.setPropCont(fPropCont);



	return bIsOK;
}

BOOL CJonasDLL_handling::getCompartVar(void)

{
	BOOL bIsOK = FALSE;
	long lCompartID;
	int	nNumOf = mTrakt.Avdelningar.size();
	m_vecCompartVar.clear();
	for (int i = 0;i < nNumOf;i++)
	{

			// If Compartment number is found, get Variables foe this Compartment; 061004 p�d
		lCompartID=mTrakt.Avdelningar[i].m_n_Id;

		if (lCompartID>=0)
		{
			bIsOK =TRUE;
						m_vecCompartVar.push_back(CVariables(&mTrakt,TH_LEVEL_COMPARTMENT,lCompartID));
		}
	}	// for (int i = 0;i < nNumOf;i++)

	return bIsOK;
}

BOOL CJonasDLL_handling::getPlotsVarPerCompart(void)
{
	BOOL bIsOK = FALSE;
	long lCompartID;
	int	nNumOfCompart = 0;
	int	nNumOfPlotsPerCompart = 0;

	// Add "Plots" to vector; 061004 p�d
	m_vecPlotsVar.clear();

	nNumOfCompart = mTrakt.Avdelningar.size();
	CString S;
	for (int i = 0;i < nNumOfCompart;i++)
	{
		lCompartID=mTrakt.Avdelningar[i].m_n_Id;
			if (lCompartID >=0)
			{

				nNumOfPlotsPerCompart =mTrakt.Avdelningar[i].mYta.size();				
				for (int j = 0;j < nNumOfPlotsPerCompart;j++)
				{
					bIsOK=TRUE;
					m_vecPlotsVar.push_back(CVariables(&mTrakt,TH_LEVEL_PLOT,lCompartID,mTrakt.Avdelningar[i].mYta[j].m_n_Id));
						m_nTotNumOfPlots++;

				}	// for (int j = 0;j < nNumOfPlotsPerCompart;j++)

			}	// if (procGetAvdId(0,&nCompartID) == 1)
		
		m_nTotNumOfCompart++;

	}	// for (int i = 0;i < nNumOfCompart;i++)

	return bIsOK;
}

// PUBLIC

// This function takes care of entering data into the database.
// It enters: Machine,Trakt,Compartment and Plot for each selected
// file (fn); 061005 p�d
BOOL CJonasDLL_handling::enterDataToDB(DB_CONNECTION_DATA con,char *fn,char *nopath_fn,
																			 TRAKT_IDENTIFER *data,LPCTSTR cap,LPCTSTR msg_1,LPCTSTR msg_2,LPCTSTR msg_3,LPCTSTR msg_4)
{
	// Datbase info data members
	double fTracks;
	double fNumOfStumps;
	double fRoadDist;
	double fStumpsPerHA;
	int nReturn;
	BOOL bReturn;
	CString S;
	int nNumOfCompart = 0;
	m_nTotNumOfCompart = 0;
	m_nTotNumOfPlots = 0;
	MACHINE_REG_DATA recMachineRegData;
	if ((nReturn = openHnd(fn)) == 1)
	{
		CGallBasDB *pDB = new CGallBasDB(con);
		CComfirmationDlg *dlg = new CComfirmationDlg();

		getFileHeader();
		getTraktVar();
		getCompartVar();
		getPlotsVarPerCompart();

#ifdef UNICODE
		USES_CONVERSION;
		dlg->setDataFileName(A2W(nopath_fn));
#else
		dlg->setDataFileName(nopath_fn);
#endif
		dlg->setRegion(m_clsHeader.getRegion());
		dlg->setDistrict(m_clsHeader.getDistrict());
		// Based on machinenumber set in file,
		// try to get the Contractors name from Databasetable
		// instead of the fileheader; 070419 p�d
		if (pDB != NULL)
		{
			recMachineRegData = MACHINE_REG_DATA(_tstoi(m_clsHeader.getMachineNum()),_T(""));
			pDB->getMachineInRegByNumber(recMachineRegData);
		}
		dlg->setMachine(m_clsHeader.getMachineNum(),
										recMachineRegData.m_sContractorName);
		dlg->setTrakt(m_clsHeader.getTraktNum(),
			            m_clsHeader.getType(),
									m_clsHeader.getOrigin(),
									m_clsHeader.getAreal());
		dlg->setNumOfCompart(m_nTotNumOfCompart);
		dlg->setNumOfPlots(m_nTotNumOfPlots);

		bReturn = (dlg->DoModal() == IDOK);
		if (bReturn)
		{
			// Get data from Dialog and add name of Contractor; 061005 p�d
			MACHINE_DATA recMachine;
			TRAKT_DATA recTrakt;
			COMPART_DATA dataCompart;
			PLOT_DATA dataPlot;

			// Get data set in Dialog; 061005 p�d
			dlg->getMachineData(recMachine);
			dlg->getTraktData(recTrakt,&mTrakt);
			recTrakt.m_sTraktDate	= m_clsHeader.getDate();
			//-------------------------------------------------------------------------------------------
			// Calculate "Sp�rbildning %" and "H�ga stbbar st/ha" here; 061201 p�d
			// "Sp�rbildning" is calculates tracks from file/20 in percent; 061201 p�d
			// "H�ga stubbar" is calculated as "Trakt medelyta" = "stickv�gsavst" * 10m; 061201 p�d
			// "H�ga stubbar st/ha = Number of hig stumps * (10000.0/"Trakt medelyta"); 061201 p�d
			//fTracks = (m_clsTraktVar.getTracks()/20.0) * 100.0;
			//m_clsTraktVar.setTracks(fTracks);
			//fNumOfStumps = m_clsTraktVar.getHighStumps();
			//fRoadDist = m_clsTraktVar.getRDist1();
			//if (fRoadDist > 0.0)
			//	fStumpsPerHA = (fNumOfStumps * (10000.0/(fRoadDist*10.0)));
			//else
			//	fStumpsPerHA = 0.0;

			//m_clsTraktVar.setHighStumps(fStumpsPerHA);
			//-------------------------------------------------------------------------------------------

			recTrakt.m_recData = m_clsTraktVar;
			recTrakt.n_nEnterType	= 0;	// = Data entered from a file; 061011 p�d

			// Add data to Database; 061005 p�d
			// Create instance of CGallBasDB class for
			// actual data transaction; 061005 p�d

			CGallBasDB *pDB = new CGallBasDB(con);
			if (pDB != NULL)
			{
					if (!pDB->addMachine(recMachine))
						pDB->updMachine(recMachine);

					if (!pDB->addTrakt(recTrakt))
						pDB->updTrakt(recTrakt);
				
					if (m_vecCompartVar.size() > 0)
					{
						// Add compartment(s) and plot(s); 061012 p�d
						for (UINT i = 0;i < m_vecCompartVar.size();i++)
						{
							// Define primary keys for Compartment, based on keys in Trakt; 061012 p�d
							dataCompart.m_lCompartID = m_vecCompartVar[i].getCompartID();
							dataCompart.m_nRegionID = recTrakt.m_nRegionID;
							dataCompart.m_nDistrictID = recTrakt.m_nDistrictID;
							dataCompart.m_nMachineID = recTrakt.m_nMachineID;
							dataCompart.m_sTraktNum	= recTrakt.m_sTraktNum;
							dataCompart.m_sType = recTrakt.m_sType;
							dataCompart.m_nOrigin = recTrakt.m_nOrigin;
							dataCompart.m_recData = m_vecCompartVar[i];

							if (!pDB->addCompart(dataCompart))
								pDB->updCompart(dataCompart);

						}	// for (UINT i = 0;i < m_vecCompartVar.size();i++)
					}	// if (m_vecCompartVar.size() > 0)
					if (m_vecPlotsVar.size() > 0)
					{

						for (UINT j = 0;j < m_vecPlotsVar.size();j++)
						{
							dataPlot.m_nPlotID    = m_vecPlotsVar[j].getPlotID();
							dataPlot.m_lCompartID = m_vecPlotsVar[j].getCompartID();
							dataPlot.m_nRegionID = recTrakt.m_nRegionID;
							dataPlot.m_nDistrictID = recTrakt.m_nDistrictID;
							dataPlot.m_nMachineID = recTrakt.m_nMachineID;
							dataPlot.m_sTraktNum	= recTrakt.m_sTraktNum;
							dataPlot.m_sType = recTrakt.m_sType;
							dataPlot.m_nOrigin = recTrakt.m_nOrigin;
							dataPlot.m_recData = m_vecPlotsVar[j];							
							if (!pDB->addPlot(dataPlot))
								pDB->updPlot(dataPlot);
						}
					}	// if (m_vecPlotsVar.size() > 0)

					data->nRegionID		= recTrakt.m_nRegionID;
					data->nDistrictID = recTrakt.m_nDistrictID;
					data->nMachineID	= recTrakt.m_nMachineID;
					data->sTraktNum		= recTrakt.m_sTraktNum;
					data->sType				= recTrakt.m_sType;
					data->nOrigin			= recTrakt.m_nOrigin;

		}	// if (pDB != NULL)
		} // if (bReturn)
		if (dlg != NULL)
			delete dlg;
		if (pDB != NULL)
			delete pDB;
	}
	else
	{
		// Check for returnvalue and tell user accordingly. 
		// Returnvalues are set by Jons �.; 070425 p�d
		switch(nReturn)
		{
			case RETURN_ERR_FILEOPEN:
				::MessageBox(0,msg_1,cap,MB_ICONEXCLAMATION | MB_OK);
				break;
			case RETURN_ERR_TRAKT:
				::MessageBox(0,msg_2,cap,MB_ICONEXCLAMATION | MB_OK);
				break;
			case RETURN_ERR_AVD:
				::MessageBox(0,msg_3,cap,MB_ICONEXCLAMATION | MB_OK);
				break;
			case RETURN_ERR_PLOT:
				::MessageBox(0,msg_4,cap,MB_ICONEXCLAMATION | MB_OK);
				break;
		}
		return FALSE;
	}

	return TRUE;
}

