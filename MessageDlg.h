#pragma once

#include "Resource.h"

#include "XHTMLStatic.h"
// CMyMessageDlg dialog

class CMyMessageDlg : public CDialog
{
	DECLARE_DYNAMIC(CMyMessageDlg)

	CString m_sCaption;
	CString m_sOKBtn;
	CString m_sCancelBtn;
	CString m_sMsgText;
public:
	CMyMessageDlg(CWnd* pParent = NULL);   // standard constructor
	CMyMessageDlg(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR cancel_btn,LPCTSTR msg,CWnd* pParent = NULL); 
	virtual ~CMyMessageDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG4 };

	CXHTMLStatic m_wndHTML;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

protected:
	//{{AFX_VIRTUAL(CComfirmationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
