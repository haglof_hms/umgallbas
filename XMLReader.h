#if !defined(AFX_XMLREADER_H)
#define AFX_XMLREADER_H

//#4031 20140429 J� xml parser
#include "xmlParser.h"
#include <vector>
using namespace std;
#pragma once

#define XML_TAG_VER _T("GallupXmlVer")
#define XML_TAG_TRAKT_GALLUPHXL	_T("GALLUPHXL")
#define XML_TAG_NODE_TRAKT	_T("Trakt")

#define XML_TAG_TRAKT_Uppfdatum			_T("Uppfdatum")
#define XML_TAG_TRAKT_Slutford			_T("Slutford")
#define XML_TAG_TRAKT_Atgardstidpunkt2	_T("Atgardstidpunkt2")
#define XML_TAG_TRAKT_Atgardstidpunkt3	_T("Atgardstidpunkt3")
#define XML_TAG_TRAKT_Atgardstp_forklar	_T("Forklaring")
#define XML_TAG_TRAKT_Distrikt			_T("Distrikt")
#define XML_TAG_TRAKT_Areal				_T("Areal")		
#define XML_TAG_TRAKT_Objektsnummer		_T("Objektsnummer")
#define XML_TAG_TRAKT_Maskinnummer		_T("Maskinnummer")
#define XML_TAG_TRAKT_Maskinstorlek		_T("Maskinstorlek")
#define XML_TAG_TRAKT_Maskinstorlek2	_T("Maskinstorlek2")
#define XML_TAG_TRAKT_Objektsnamn		_T("Objektsnamn")
#define XML_TAG_TRAKT_Entreprenor		_T("Entreprenor")
#define XML_TAG_TRAKT_Region			_T("Region")
#define XML_TAG_TRAKT_Typ				_T("Typ")
#define XML_TAG_TRAKT_Ursprung			_T("Ursprung")
#define XML_TAG_TRAKT_Cirkelytor		_T("Cirkelytor")
#define XML_TAG_TRAKT_Lan				_T("Lan")
#define XML_TAG_TRAKT_Lan2				_T("Lan2")
#define XML_TAG_TRAKT_Breddgrad			_T("Breddgrad")
#define XML_TAG_TRAKT_Myr				_T("Myr")
#define XML_TAG_TRAKT_Myr2				_T("Myr2")
#define XML_TAG_TRAKT_Hallmark			_T("Hallmark")
#define XML_TAG_TRAKT_Hallmark2			_T("Hallmark2")
#define XML_TAG_TRAKT_Hansyn			_T("Hansyn")
#define XML_TAG_TRAKT_Hansyn2			_T("Hansyn2")
#define XML_TAG_TRAKT_FdKulturmark		_T("FdKulturmark")
#define XML_TAG_TRAKT_FdKulturmark2		_T("FdKulturmark2")
#define XML_TAG_TRAKT_Kulturmiljo		_T("Kulturmiljo")
#define XML_TAG_TRAKT_Kulturmiljo2		_T("Kulturmiljo2")
#define XML_TAG_TRAKT_Lovandel			_T("Lovandel")
#define XML_TAG_TRAKT_Lovandel2			_T("Lovandel2")
#define XML_TAG_TRAKT_Skyddszon			_T("Skyddszon")
#define XML_TAG_TRAKT_Skyddszon2		_T("Skyddszon2")
#define XML_TAG_TRAKT_NvTrad			_T("NvTrad")
#define XML_TAG_TRAKT_NvTrad2			_T("NvTrad2")
#define XML_TAG_TRAKT_Hogstubbar		_T("Hogstubbar")
#define XML_TAG_TRAKT_Hogstubbar2		_T("Hogstubbar2")
#define XML_TAG_TRAKT_Framtidstrad		_T("Framtidstrad")
#define XML_TAG_TRAKT_Framtidstrad2		_T("Framtidstrad2")
#define XML_TAG_TRAKT_DodaTrad			_T("DodaTrad")
#define XML_TAG_TRAKT_DodaTrad2			_T("DodaTrad2")
#define XML_TAG_TRAKT_Markskador		_T("Markskador")
#define XML_TAG_TRAKT_Markskador2		_T("Markskador2")
#define XML_TAG_TRAKT_Markskador3		_T("Markskador3")
#define XML_TAG_TRAKT_Markskador4		_T("Markskador4")
#define XML_TAG_TRAKT_MindreKorskada	_T("MindreKorskada")
#define XML_TAG_TRAKT_MindreKorskada2	_T("MindreKorskada2")
#define XML_TAG_TRAKT_AllvarligKorskada	_T("AllvarligKorskada")
#define XML_TAG_TRAKT_AllvarligKorskada2	_T("AllvarligKorskada2")
#define XML_TAG_TRAKT_Nedskrapning		_T("Nedskrapning")
#define XML_TAG_TRAKT_Nedskrapning2		_T("Nedskrapning2")
#define XML_TAG_TRAKT_Fritext			_T("Fritext")

#define XML_TAG_NODE_AVDEL			_T("Traktdel")
#define XML_TAG_AVDEL_Id			_T("Id")
#define XML_TAG_AVDEL_Gallringstyp	_T("Gallringstyp")
#define XML_TAG_AVDEL_Gallringstyp2	_T("Gallringstyp2")
#define XML_TAG_AVDEL_Metod			_T("Metod")
#define XML_TAG_AVDEL_Metod2		_T("Metod2")
#define XML_TAG_AVDEL_Datum			_T("Datum")
#define XML_TAG_AVDEL_Areal			_T("Areal") 
#define XML_TAG_AVDEL_SI			_T("SI") 
#define XML_TAG_AVDEL_Antal_Stubbar	_T("AntalStubbar")

#define XML_TAG_AVDEL_SI_SPEC		_T("SISpec") 
#define XML_TAG_AVDEL_SI_NUM		_T("SINum") 

#define XML_TAG_AVDEL_TraktDirLagstaGy	_T("TraktDirLagstaGy") 
#define XML_TAG_AVDEL_TraktDirGyfore	_T("TraktDirGyfore") 
#define XML_TAG_AVDEL_TraktDirGyefter	_T("TraktDirGyefter") 
#define XML_TAG_AVDEL_TraktDirGauttag	_T("TraktDirGauttag") 
#define XML_TAG_AVDEL_Vagbredd			_T("Vagbredd") 
#define XML_TAG_AVDEL_Vagavst			_T("Vagavst") 
#define XML_TAG_AVDEL_Vagyta			_T("Vagyta") 
#define XML_TAG_AVDEL_Ovrehojd			_T("Ovrehojd") 
#define XML_TAG_AVDEL_SI				_T("SI") 
#define XML_TAG_AVDEL_Gyfore			_T("Gyfore") 
#define XML_TAG_AVDEL_Gyefter			_T("Gyefter") 
#define XML_TAG_AVDEL_Gauttag			_T("Gauttag") 
#define XML_TAG_AVDEL_Gauttagiskog		_T("Gauttagiskog")
#define XML_TAG_AVDEL_Gauttagivag		_T("Gauttagivag") 
#define XML_TAG_AVDEL_Gakvot			_T("Gakvot") 
#define XML_TAG_AVDEL_Stammar			_T("Stammar") 
#define XML_TAG_AVDEL_Medeldiam			_T("Medeldiam") 
#define XML_TAG_AVDEL_H25				_T("H25") 
#define XML_TAG_AVDEL_VolTall			_T("VolTall") 
#define XML_TAG_AVDEL_VolGran			_T("VolGran") 
#define XML_TAG_AVDEL_VolLov			_T("VolLov") 
#define XML_TAG_AVDEL_VolCont			_T("VolCont") 
#define XML_TAG_AVDEL_VolTot			_T("VolTot") 
#define XML_TAG_AVDEL_Forrensat			_T("Forrensat") 
#define XML_TAG_AVDEL_AndelTall			_T("AndelTall") 
#define XML_TAG_AVDEL_AndelGran			_T("AndelGran") 
#define XML_TAG_AVDEL_AndelLov			_T("AndelLov") 
#define XML_TAG_AVDEL_AndelCont			_T("AndelCont") 
#define XML_TAG_AVDEL_Skadade			_T("Skadade") 
#define XML_TAG_AVDEL_Sparbild			_T("Sparbild") 
#define XML_TAG_AVDEL_Hogast			_T("Hogast") 
#define XML_TAG_AVDEL_Genomgallrat		_T("Genomgallrat") 
#define XML_TAG_AVDEL_Risning			_T("Risning")


#define XML_TAG_NODE_YTA			_T("Yta")

#define XML_TAG_YTA_Nr				_T("Nr") 
#define XML_TAG_YTA_Vagbredd		_T("Vagbredd") 
#define XML_TAG_YTA_Vagbredd2		_T("Vagbredd2") 
#define XML_TAG_YTA_Vagavst			_T("Vagavst") 
#define XML_TAG_YTA_Vagavst2		_T("Vagavst2") 
#define XML_TAG_YTA_Vagyta			_T("Vagyta") 
#define XML_TAG_YTA_Ovrehojd		_T("Ovrehojd") 
#define XML_TAG_YTA_SI				_T("SI") 
#define XML_TAG_YTA_Gyfore			_T("Gyfore") 
#define XML_TAG_YTA_Gyefter			_T("Gyefter") 
#define XML_TAG_YTA_Gauttag			_T("Gauttag") 
#define XML_TAG_YTA_Gauttagiskog	_T("Gauttagiskog") 
#define XML_TAG_YTA_Gauttagivag		_T("Gauttagivag") 
#define XML_TAG_YTA_Gakvot			_T("Gakvot") 
#define XML_TAG_YTA_Stammar			_T("Stammar") 
#define XML_TAG_YTA_Medeldiam		_T("Medeldiam") 
#define XML_TAG_YTA_H25				_T("H25") 
#define XML_TAG_YTA_VolTall			_T("VolTall") 
#define XML_TAG_YTA_VolGran			_T("VolGran") 
#define XML_TAG_YTA_VolLov			_T("VolLov") 
#define XML_TAG_YTA_VolCont			_T("VolCont") 
#define XML_TAG_YTA_VolTot			_T("VolTot") 
#define XML_TAG_YTA_Forrensat		_T("Forrensat") 
#define XML_TAG_YTA_AndelTall		_T("AndelTall") 
#define XML_TAG_YTA_AndelGran		_T("AndelGran") 
#define XML_TAG_YTA_AndelLov		_T("AndelLov") 
#define XML_TAG_YTA_AndelCont		_T("AndelCont") 
#define XML_TAG_YTA_Skadade			_T("Skadade") 
#define XML_TAG_YTA_Sparbild		_T("Sparbild") 
#define XML_TAG_YTA_Hogast			_T("Hogast") 
#define XML_TAG_YTA_Genomgallrat	_T("Genomgallrat") 
#define XML_TAG_YTA_Risning			_T("Risning") 
#define XML_TAG_YTA_Provtradslag	_T("Provtradslag") 
#define XML_TAG_YTA_Provtraddiam	_T("Provtraddiam") 
#define XML_TAG_YTA_Provtradhojd	_T("Provtradhojd")

#define XML_TAG_YTA_Ytradie				_T("Ytradie")
#define XML_TAG_YTA_AntalTrad			_T("AntalTrad")
#define XML_TAG_YTA_AntalStubbar		_T("AntalStubbar")
#define XML_TAG_YTA_AntalStubbarivag	_T("AntalStubbarivag")
#define XML_TAG_YTA_AntalStubbariskog	_T("AntalStubbariskog")
#define XML_TAG_YTA_Totdiamkvarvarande	_T("Totdiamkvarvarande")
#define XML_TAG_YTA_Totdiamuttag		_T("Totdiamuttag")
#define XML_TAG_YTA_AntalHogaStubbar	_T("AntalHogaStubbar")

#define XML_REPLACE_LOVANDEL_TEXT		"vandel >20% utan direktiv"
#define XML_REPLACE_LOVANDEL_NEWTEXT	"vandel &gt;20% utan direktiv"
#define XML_REPLACE_DODATRAD_TEXT		"llen >3 m3sk/ha"
#define XML_REPLACE_DODATRAD_NEWTEXT	"llen &gt;3 m3sk/ha"
#define NUM_OF_YT_VARS 27

#define RETURN_ERR_FILEOPEN				-1
#define RETURN_ERR_TRAKT				-2
#define RETURN_ERR_AVD					-3
#define RETURN_ERR_PLOT					-4

typedef struct Yta_struct
{
	int m_n_Id;
	double m_f_YtVars[NUM_OF_YT_VARS];

	int m_n_Ytradie;
	int m_n_AntalTrad;
	int m_n_AntalStubbar;
	int m_n_AntalStubbarivag;
	int m_n_AntalStubbariskog;
	int m_n_Totdiamkvarvarande;
	int m_n_Totdiamuttag;
	int m_n_AntalHogaStubbar;

}YTSTRUCT;

typedef struct Avd_struct
{
	int m_n_Id;

	int m_n_compart_gallrtyp;
	int m_n_compart_metod;
	CStringA m_s_compart_spec_and_si;
	int m_n_compart_si_spec;
	int m_n_compart_si;
	int m_n_compart_lagsta_gy_enl_mall;
	int m_n_compart_traktdir_gy_fore_m2ha;
	int m_n_compart_traktdir_gy_efter_m2ha;
	int m_n_compart_traktdir_ga_uttag;
	double m_f_compart_gy_wdraw_forest;
	double	m_f_compart_gy_wdraw_road;
	double	m_f_compart_oh;
	double	m_f_compart_areal;
	double  m_f_compart_antal_stubbar;

   double m_fAvdVars[NUM_OF_YT_VARS];
   vector <YTSTRUCT> mYta;
} AVDSTRUCT;


class CXmlReader
{
private:
	//void Insert_YtDataOnLastAvdelning(int varcount,CString buff2);

	void getAvdGallrTypIndexFromText(CString typ_text,AVDSTRUCT *str);
	void getAvdMetodIndexFromText(CString meth_text,AVDSTRUCT *str);

	void getAvdSiSpecAndSiNum(AVDSTRUCT *str);

	int	HXL_HandleHxlFile(XMLNode xNode);
	int HXL_RecTrakt(XMLNode xNode);
	int HXL_RecAvd(XMLNode xNode,int n_NumAvds);
	int HXL_RecPlots(XMLNode xNode,int n_NumPlots);
	void HXL_Calculate_Trakt_Average(void);
	int HXL_CheckXmlVer(char *fname);
public:
	CString m_sLangFN;
	CString m_sDatum;			// 1
	CString m_sRegion;			// 2
	CString m_sDistrikt;		// 3
	CString m_sTyp;			// 4
	CString m_sUrsprung;		// 5
	CString m_sObj_nr;			// 6
	CString m_sObj_namn;		// 7
	CString m_sAreal;			// 8
	CString m_sMask_nr;		// 9
	CString m_sEntr;			// 10
	CString m_sMask_storlek;	// 11
	CString m_sAtg_tidp_nr;	// 12
	CString m_sAtg_tidp_text;	// 13

	int m_n_XmlVer;
	int m_n_trakt_maskinstorlek;
	int m_n_trakt_avslut_myr;
	int m_n_trakt_avslut_hallmark;
	int m_n_trakt_avslut_hansyn;
	int m_n_trakt_avslut_fdkulturmark;
	int m_n_trakt_avslut_kulturmiljo;
	int m_n_trakt_avslut_lovandel;
	int m_n_trakt_avslut_skyddszon;
	int m_n_trakt_avslut_nvtrad;
	int m_n_trakt_avslut_framtidstrad;
	int m_n_trakt_avslut_dodatrad;
	int m_n_trakt_avslut_hogstubbar;
	int m_n_trakt_avslut_mindrekorskada;
	int m_n_trakt_avslut_allvarligkorskada;
	int m_n_trakt_avslut_markskador;
	int m_n_trakt_avslut_markskador2;
	int m_n_trakt_avslut_nedskrapning;
	CString m_s_trakt_avslut_fritext;
	int m_n_trakt_avslut_atgardstp;
	CString m_s_trakt_avslut_atgardstp_forklaring;
	CString m_s_trakt_avslut_datum_slutford;
	int m_n_trakt_calc_si_spec;
	int m_n_trakt_calc_si;

	double m_fTrakt_Medel[NUM_OF_YT_VARS];
	vector<AVDSTRUCT>Avdelningar;

	// Default constructor
	CXmlReader(void);
	
	~CXmlReader(void)
	{

	}


	/************************************************/
	/* H�mta antal avdelningar                      */
	/************************************************/
	//int GetAntAvd();
	/************************************************/
	/* H�mta id f�r Avdelning med index i           */
	/* Returnerar 0 om inte hittat                  */
	/************************************************/
	//int GetAvdId(int index,long *id);
	/************************************************/
	/* H�mta antal ytor                             */
	/************************************************/
	//int  GetAntYt(int avd);
	/************************************************/
	/* �ppna klavefil                               */
	/************************************************/
	int OpenFile(char *filename);
	

};

#endif