// MDIGallBasCompartFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIGallBasCompartFormView.h"

#include "ResLangFileReader.h"

#include "UMGallBasGenerics.h"
#include ".\mdigallbascompartformview.h"

// CMDIGallBasCompartFormView

IMPLEMENT_DYNCREATE(CMDIGallBasCompartFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIGallBasCompartFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CMDIGallBasCompartFormView::CMDIGallBasCompartFormView()
	: CXTResizeFormView(CMDIGallBasCompartFormView::IDD)
{
}

CMDIGallBasCompartFormView::~CMDIGallBasCompartFormView()
{
}

BOOL CMDIGallBasCompartFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIGallBasCompartFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL4_1, m_wndLbl4_1);
	DDX_Control(pDX, IDC_LBL4_2, m_wndLbl4_2);
	DDX_Control(pDX, IDC_LBL4_3, m_wndLbl4_3);
	DDX_Control(pDX, IDC_LBL4_4, m_wndLbl4_4);
	DDX_Control(pDX, IDC_LBL4_5, m_wndLbl4_5);
	DDX_Control(pDX, IDC_LBL4_7, m_wndLbl4_7);
	DDX_Control(pDX, IDC_LBL4_8, m_wndLbl4_8);
	DDX_Control(pDX, IDC_LBL4_9, m_wndLbl4_9);
	DDX_Control(pDX, IDC_LBL4_10, m_wndLbl4_10);
	DDX_Control(pDX, IDC_LBL4_11, m_wndLbl4_11);
	DDX_Control(pDX, IDC_LBL4_12, m_wndLbl4_12);
	DDX_Control(pDX, IDC_LBL4_13, m_wndLbl4_13);
	DDX_Control(pDX, IDC_LBL4_14, m_wndLbl4_14);
	DDX_Control(pDX, IDC_LBL4_15, m_wndLbl4_15);
	DDX_Control(pDX, IDC_LBL4_16, m_wndLbl4_16);
	DDX_Control(pDX, IDC_LBL4_17, m_wndLbl4_17);
	DDX_Control(pDX, IDC_LBL4_18, m_wndLbl4_18);
	DDX_Control(pDX, IDC_LBL4_19, m_wndLbl4_19);
	DDX_Control(pDX, IDC_LBL4_20, m_wndLbl4_20);
	DDX_Control(pDX, IDC_LBL4_21, m_wndLbl4_21);
	DDX_Control(pDX, IDC_LBL4_22, m_wndLbl4_22);
	DDX_Control(pDX, IDC_LBL4_23, m_wndLbl4_23);
	DDX_Control(pDX, IDC_LBL4_24, m_wndLbl4_24);
	DDX_Control(pDX, IDC_LBL4_25, m_wndLbl4_25);
	DDX_Control(pDX, IDC_LBL4_26, m_wndLbl4_26);
	DDX_Control(pDX, IDC_LBL4_27, m_wndLbl4_27);
	DDX_Control(pDX, IDC_LBL4_28, m_wndLbl4_28);
	DDX_Control(pDX, IDC_LBL4_29, m_wndLbl4_29);
	DDX_Control(pDX, IDC_LBL4_30, m_wndLbl4_30);


	DDX_Control(pDX, IDC_LBL_COMPARTM_FORRENSAT, m_wndLbl_Forrensat);
	DDX_Control(pDX,IDC_EDIT_COMPARTM_FORRENSAT, m_wndEdit_Forrensat);
	
	DDX_Control(pDX, IDC_LBL_COMPARTM_GALLRTYP, m_wndLb_Gallrtyp);
	DDX_Control(pDX, IDC_LBL_COMPARTM_METOD, m_wndLb_Metod);
	DDX_Control(pDX, IDC_LBL_COMPARTM_SI, m_wndLb_Si);
	DDX_Control(pDX, IDC_LBL_COMPARTM_TRAKTDIR_LAGSTAGY, m_wndLb_LagstaGy);
	DDX_Control(pDX, IDC_LBL_COMPARTM_TRAKTDIR_GYFORE, m_wndLb_GyFore);
	DDX_Control(pDX, IDC_LBL_COMPARTM_TRAKTDIR_GYEFTER, m_wndLb_GyEfter);
	DDX_Control(pDX, IDC_LBL_COMPARTM_TRAKTDIR_GAUTTAG, m_wndLb_GaUttag);

	DDX_Control(pDX,IDC_EDIT_COMPARTM_SI, m_wndEdit_Si);
	DDX_Control(pDX,IDC_EDIT_COMPARTM_TRAKTDIR_LAGSTAGY, m_wndEdit_Traktdir_LagstaGy);
	DDX_Control(pDX,IDC_EDIT_COMPARTM_TRAKTDIR_GYFORE, m_wndEdit_Traktdir_GyFore);
	DDX_Control(pDX,IDC_EDIT_COMPARTM_TRAKTDIR_GYEFTER2, m_wndEdit_Traktdir_GyEfter);
	DDX_Control(pDX,IDC_EDIT_COMPARTM_TRAKTDIR_GAUTTAG, m_wndEdit_Traktdir_GaUttag);

	DDX_Control(pDX, IDC_CBOX_COMP_GALLRTYP, m_wndCBox_GallrTyp);
	DDX_Control(pDX, IDC_CBOX_COMP_METOD, m_wndCBox_Metod);

	
		

	DDX_Control(pDX, IDC_GRP_COMPARTM_TRAKTDIR, m_wndGroup_TraktDir);
	

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit8);
	DDX_Control(pDX, IDC_EDIT9, m_wndEdit9);
	DDX_Control(pDX, IDC_EDIT10, m_wndEdit10);
	DDX_Control(pDX, IDC_EDIT11, m_wndEdit11);
	DDX_Control(pDX, IDC_EDIT12, m_wndEdit12);
	DDX_Control(pDX, IDC_EDIT13, m_wndEdit13);
	DDX_Control(pDX, IDC_EDIT14, m_wndEdit14);
	DDX_Control(pDX, IDC_EDIT15, m_wndEdit15);
	DDX_Control(pDX, IDC_EDIT16, m_wndEdit16);
	DDX_Control(pDX, IDC_EDIT17, m_wndEdit17);
	DDX_Control(pDX, IDC_EDIT18, m_wndEdit18);
	DDX_Control(pDX, IDC_EDIT19, m_wndEdit19);
	DDX_Control(pDX, IDC_EDIT20, m_wndEdit20);
	DDX_Control(pDX, IDC_EDIT21, m_wndEdit21);
	DDX_Control(pDX, IDC_EDIT22, m_wndEdit22);
	DDX_Control(pDX, IDC_EDIT23, m_wndEdit23);
	DDX_Control(pDX, IDC_EDIT24, m_wndEdit24);
	DDX_Control(pDX, IDC_EDIT25, m_wndEdit25);
	DDX_Control(pDX, IDC_EDIT26, m_wndEdit26);
	DDX_Control(pDX, IDC_EDIT27, m_wndEdit27);
	DDX_Control(pDX, IDC_EDIT28, m_wndEdit28);
	DDX_Control(pDX, IDC_EDIT29, m_wndEdit29);
	DDX_Control(pDX, IDC_EDIT30, m_wndEdit30);

	DDX_Control(pDX, IDC_GRP1, m_wndIDGroup);
	DDX_Control(pDX, IDC_GRP_ROADS, m_wndRoadGroup);
	DDX_Control(pDX, IDC_GRP_GY, m_wndGYGroup);
	DDX_Control(pDX, IDC_GRP_VOLUME, m_wndVolGroup);
	DDX_Control(pDX, IDC_GRP_PROP, m_wndPropGroup);
	DDX_Control(pDX, IDC_GRP_REST, m_wndRestGroup);
	//}}AFX_DATA_MAP

}

void CMDIGallBasCompartFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));
	m_wndEdit1.SetEnabledColor(BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit1.SetReadOnly();

	m_wndEdit2.SetEnabledColor(BLACK, WHITE );
	m_wndEdit2.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit2.SetReadOnly();
	
	m_wndEdit3.SetEnabledColor(BLACK, WHITE );
	m_wndEdit3.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit3.SetReadOnly();
	
	m_wndEdit5.SetEnabledColor(BLACK, WHITE );
	m_wndEdit5.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit5.SetReadOnly();
	
	m_wndEdit6.SetEnabledColor(BLACK, WHITE );
	m_wndEdit6.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit6.SetReadOnly();
	
	m_wndEdit7.SetEnabledColor(BLACK, WHITE );
	m_wndEdit7.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit7.SetReadOnly();
	
	m_wndEdit8.SetEnabledColor(BLACK, WHITE );
	m_wndEdit8.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit8.SetReadOnly();

	m_wndEdit9.SetEnabledColor(BLACK, WHITE );
	m_wndEdit9.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit9.SetReadOnly();

	m_wndEdit10.SetEnabledColor(BLACK, WHITE );
	m_wndEdit10.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit10.SetReadOnly();

	m_wndEdit11.SetEnabledColor(BLACK, WHITE );
	m_wndEdit11.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit11.SetReadOnly();

	m_wndEdit12.SetEnabledColor(BLACK, WHITE );
	m_wndEdit12.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit12.SetReadOnly();

	m_wndEdit13.SetEnabledColor(BLACK, WHITE );
	m_wndEdit13.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit13.SetReadOnly();

	m_wndEdit14.SetEnabledColor(BLACK, WHITE );
	m_wndEdit14.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit14.SetReadOnly();

	m_wndEdit15.SetEnabledColor(BLACK, WHITE );
	m_wndEdit15.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit15.SetReadOnly();

	m_wndEdit16.SetEnabledColor(BLACK, WHITE );
	m_wndEdit16.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit16.SetReadOnly();

	m_wndEdit17.SetEnabledColor(BLACK, WHITE );
	m_wndEdit17.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit17.SetReadOnly();

	m_wndEdit18.SetEnabledColor(BLACK, WHITE );
	m_wndEdit18.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit18.SetReadOnly();

	m_wndEdit19.SetEnabledColor(BLACK, WHITE );
	m_wndEdit19.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit19.SetReadOnly();

	m_wndEdit20.SetEnabledColor(BLACK, WHITE );
	m_wndEdit20.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit20.SetReadOnly();

	m_wndEdit21.SetEnabledColor(BLACK, WHITE );
	m_wndEdit21.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit21.SetReadOnly();

	m_wndEdit22.SetEnabledColor(BLACK, WHITE );
	m_wndEdit22.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit22.SetReadOnly();

	m_wndEdit23.SetEnabledColor(BLACK, WHITE );
	m_wndEdit23.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit23.SetReadOnly();

	m_wndEdit24.SetEnabledColor(BLACK, WHITE );
	m_wndEdit24.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit24.SetReadOnly();

	m_wndEdit25.SetEnabledColor(BLACK, WHITE );
	m_wndEdit25.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit25.SetReadOnly();

	m_wndEdit26.SetEnabledColor(BLACK, WHITE );
	m_wndEdit26.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit26.SetReadOnly();

	m_wndEdit27.SetEnabledColor(BLACK, WHITE );
	m_wndEdit27.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit27.SetReadOnly();

	m_wndEdit28.SetEnabledColor(BLACK, WHITE );
	m_wndEdit28.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit28.SetReadOnly();

	m_wndEdit29.SetEnabledColor(BLACK, WHITE );
	m_wndEdit29.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit29.SetReadOnly();

	m_wndEdit30.SetEnabledColor(BLACK, WHITE );
	m_wndEdit30.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit30.SetReadOnly();

	m_wndCBox_GallrTyp.SetEnabledColor(BLACK, WHITE );
	m_wndCBox_GallrTyp.SetDisabledColor(BLACK, INFOBK );

	m_wndCBox_Metod.SetEnabledColor(BLACK, WHITE );
	m_wndCBox_Metod.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit_Forrensat.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Forrensat.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Forrensat.SetReadOnly();

	m_wndEdit_Si.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Si.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Si.SetReadOnly();

	m_wndEdit_Traktdir_LagstaGy.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Traktdir_LagstaGy.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Traktdir_LagstaGy.SetReadOnly();

	m_wndEdit_Traktdir_GyFore.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Traktdir_GyFore.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Traktdir_GyFore.SetReadOnly();

	m_wndEdit_Traktdir_GyEfter.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Traktdir_GyEfter.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Traktdir_GyEfter.SetReadOnly();

	m_wndEdit_Traktdir_GaUttag.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Traktdir_GaUttag.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Traktdir_GaUttag.SetReadOnly();


	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

//	CString sPath;
//	sPath.Format("%s\\%s\\%s",getProgDir(),SUBDIR_SCRIPTS,GALLBAS_TABLES);
//	if (runSQLScriptFile(sPath,TBL_REGION))
//	{
		// Get data from database; 061012 p�d
		getDistrictFromDB();

		// Get data from database; 061012 p�d
		getMachineFromDB();

		// Get data from database; 061012 p�d
		getTraktFromDB();

		// Get data from database; 061012 p�d
		getCompartFromDB();
//	}
	
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	setLanguage();

	setCompArrays();
	
	m_nDBIndex = 0;
	populateData(m_nDBIndex);
}

void CMDIGallBasCompartFormView::setCompArrays(void)
{
	COMP_ARRAY_DATA data;
	CString s_Data=_T("");
	UINT i=0;




	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{

			vec_Comp_GallrTyp.clear();
			vec_Comp_GallrTyp.push_back(_comp_array_data(_tstoi(xml->str(IDS_STRING7011)),xml->str(IDS_STRING7010)));
			vec_Comp_GallrTyp.push_back(_comp_array_data(_tstoi(xml->str(IDS_STRING7013)),xml->str(IDS_STRING7012)));

			vec_Comp_Metod.clear();
			vec_Comp_Metod.push_back(_comp_array_data(_tstoi(xml->str(IDS_STRING7021)),xml->str(IDS_STRING7020)));
			vec_Comp_Metod.push_back(_comp_array_data(_tstoi(xml->str(IDS_STRING7023)),xml->str(IDS_STRING7022)));
		}
	}

	for (i = 0;i < vec_Comp_GallrTyp.size();i++)
	{
		data = vec_Comp_GallrTyp[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_GallrTyp.AddString(s_Data);
		m_wndCBox_GallrTyp.SetItemData(i, data.m_nID);		
	}
	for (i = 0;i < vec_Comp_Metod.size();i++)
	{
		data = vec_Comp_Metod[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Metod.AddString(s_Data);
		m_wndCBox_Metod.SetItemData(i, data.m_nID);		
	}

}

BOOL CMDIGallBasCompartFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDIGallBasCompartFormView::OnSetFocus(CWnd*)
{
	if (!m_vecCompartData.empty())
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecCompartData.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}
}

// CMDIGallBasCompartFormView diagnostics

#ifdef _DEBUG
void CMDIGallBasCompartFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIGallBasCompartFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CMDIGallBasCompartFormView message handlers

void CMDIGallBasCompartFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndLbl4_1.SetWindowText(xml->str(IDS_STRING204));
			m_wndLbl4_2.SetWindowText(xml->str(IDS_STRING107));
			m_wndLbl4_3.SetWindowText(xml->str(IDS_STRING108));
			m_wndLbl4_4.SetWindowText(xml->str(IDS_STRING247));
			m_wndLbl4_5.SetWindowText(xml->str(IDS_STRING252));

			// Roads "Stickv�gar"
			m_wndRoadGroup.SetWindowText(xml->str(IDS_STRING214));
			m_wndLbl4_7.SetWindowText(xml->str(IDS_STRING215));
			m_wndLbl4_8.SetWindowText(xml->str(IDS_STRING218));
			m_wndLbl4_9.SetWindowText(xml->str(IDS_STRING221));

			// Baselarea "Grundyta"
			m_wndGYGroup.SetWindowText(xml->str(IDS_STRING222));
			m_wndLbl4_10.SetWindowText(xml->str(IDS_STRING223));
			m_wndLbl4_11.SetWindowText(xml->str(IDS_STRING224));
			m_wndLbl4_12.SetWindowText(xml->str(IDS_STRING225));
			m_wndLbl4_13.SetWindowText(xml->str(IDS_STRING226));
	
			// Volumes
			m_wndVolGroup.SetWindowText(xml->str(IDS_STRING230));
			m_wndLbl4_14.SetWindowText(xml->str(IDS_STRING231));
			m_wndLbl4_15.SetWindowText(xml->str(IDS_STRING232));
			m_wndLbl4_16.SetWindowText(xml->str(IDS_STRING233));
			m_wndLbl4_17.SetWindowText(xml->str(IDS_STRING234));
			m_wndLbl4_18.SetWindowText(xml->str(IDS_STRING235));
			
			// Prop. species "Tr�dslagsf�rdelning"
			m_wndPropGroup.SetWindowText(xml->str(IDS_STRING236));
			m_wndLbl4_19.SetWindowText(xml->str(IDS_STRING237));
			m_wndLbl4_20.SetWindowText(xml->str(IDS_STRING238));
			m_wndLbl4_21.SetWindowText(xml->str(IDS_STRING239));
			m_wndLbl4_22.SetWindowText(xml->str(IDS_STRING240));

			// Remaining data
			m_wndRestGroup.SetWindowText(xml->str(IDS_STRING246));
			m_wndLbl4_23.SetWindowText(xml->str(IDS_STRING227));
			m_wndLbl4_24.SetWindowText(xml->str(IDS_STRING228));
			m_wndLbl4_25.SetWindowText(xml->str(IDS_STRING229));
			m_wndLbl4_26.SetWindowText(xml->str(IDS_STRING241));
			m_wndLbl4_27.SetWindowText(xml->str(IDS_STRING242));
			m_wndLbl4_28.SetWindowText(xml->str(IDS_STRING243));
			m_wndLbl4_29.SetWindowText(xml->str(IDS_STRING244));
			m_wndLbl4_30.SetWindowText(xml->str(IDS_STRING245));

				
			m_wndLb_Gallrtyp.SetWindowText(xml->str(IDS_STRING7000));
			m_wndLb_Metod.SetWindowText(xml->str(IDS_STRING7001));
			m_wndLb_Si.SetWindowText(xml->str(IDS_STRING7002));
			m_wndLbl_Forrensat.SetWindowText(xml->str(IDS_STRING256));
			m_wndGroup_TraktDir.SetWindowText(xml->str(IDS_STRING7003));
			m_wndLb_LagstaGy.SetWindowText(xml->str(IDS_STRING7004));
			m_wndLb_GyFore.SetWindowText(xml->str(IDS_STRING7005));
			m_wndLb_GyEfter.SetWindowText(xml->str(IDS_STRING7006));
			m_wndLb_GaUttag.SetWindowText(xml->str(IDS_STRING7007));


		}
		delete xml;
	}

}
void CMDIGallBasCompartFormView::populateData(UINT idx)
{
	DISTRICT_DATA dataDist;
	COMPART_DATA data;
	CString s_Data=_T("");
	if (!m_vecCompartData.empty() && 
		  idx >= 0 && 
			idx < m_vecCompartData.size())
	{
		data = m_vecCompartData[idx];

		m_wndEdit1.SetWindowText(getRegionAndDistrictData(data.m_nRegionID,data.m_nDistrictID,dataDist));
		m_wndEdit5.setInt(data.m_nMachineID);
		m_wndEdit2.SetWindowText(getMachineEntr(data.m_nRegionID,data.m_nDistrictID,data.m_nMachineID));
		m_wndEdit3.SetWindowText(getTraktInfo(data.m_nRegionID,data.m_nDistrictID,data.m_nMachineID,data.m_sTraktNum.Trim()));
		m_wndEdit6.setInt(data.m_lCompartID);
		//  Road data
		m_wndEdit7.setFloat(data.m_recData.getRWidth1(),1);
		m_wndEdit8.setFloat(data.m_recData.getRDist1(),1);
		m_wndEdit9.setFloat(data.m_recData.getRArea(),1);
		// Basel area
		m_wndEdit10.setFloat(data.m_recData.getGYBefore(),1);
		m_wndEdit11.setFloat(data.m_recData.getGYAfter(),1);
		m_wndEdit12.setFloat(data.m_recData.getGYWDraw(),1);
		m_wndEdit13.setFloat(data.m_recData.getGYQuota(),1);
		// Volumes
		m_wndEdit14.setFloat(data.m_recData.getVolPine(),1);
		m_wndEdit15.setFloat(data.m_recData.getVolSpruce(),1);
		m_wndEdit16.setFloat(data.m_recData.getVolBirch(),1);
		m_wndEdit17.setFloat(data.m_recData.getVolCont(),1);
		m_wndEdit18.setFloat(data.m_recData.getVolTotal(),1);
		// Specie diviation
		m_wndEdit19.setFloat(data.m_recData.getPropPine(),1);
		m_wndEdit20.setFloat(data.m_recData.getPropSpruce(),1);
		m_wndEdit21.setFloat(data.m_recData.getPropBirch(),1);
		m_wndEdit22.setFloat(data.m_recData.getPropCont(),1);
		// Remaining data
		m_wndEdit23.setFloat(data.m_recData.getStems(),0);
		m_wndEdit24.setFloat(data.m_recData.getAvgDiam(),1);
		m_wndEdit25.setFloat(data.m_recData.getH25(),1);
		m_wndEdit26.setFloat(data.m_recData.getDamaged(),1);
		m_wndEdit27.setFloat(data.m_recData.getTracks(),1);
		m_wndEdit28.setFloat(data.m_recData.getHighStumps(),1);
		m_wndEdit29.setFloat(data.m_recData.getStumpTreat(),1);
		m_wndEdit30.setFloat(data.m_recData.getTwigs(),1);

		m_wndEdit_Forrensat.setFloat(data.m_recData.getForrensat(),1);
		m_wndEdit_Si.SetWindowText(data.m_recData.getCompSpecAndSi());
		m_wndEdit_Traktdir_LagstaGy.setInt(data.m_recData.getCompLagstaGy());
		m_wndEdit_Traktdir_GyFore.setInt(data.m_recData.getCompGyFore());
		m_wndEdit_Traktdir_GyEfter.setInt(data.m_recData.getCompGyEfter());
		m_wndEdit_Traktdir_GaUttag.setInt(data.m_recData.getCompGaUttag());

		s_Data.Format(_T("%d"),data.m_recData.getCompGallrTyp());
		m_wndCBox_GallrTyp.SelectString(0,s_Data);

		s_Data.Format(_T("%d"),data.m_recData.getCompMetod());
		m_wndCBox_Metod.SelectString(0,s_Data);

	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 061010 p�d
LRESULT CMDIGallBasCompartFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
				addCompart();
				break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			deleteCompart();
			resetCompart((int)lParam);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			resetCompart((int)lParam);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_PREV :
		{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;

				if (m_nDBIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_NEXT :
		{
				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecCompartData.size() - 1))
					m_nDBIndex = (UINT)m_vecCompartData.size() - 1;
				
				if (m_nDBIndex == (UINT)m_vecCompartData.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_END :
		{
				m_nDBIndex = (UINT)m_vecCompartData.size()-1;

				setNavigationButtons(TRUE,FALSE);
			
				populateData(m_nDBIndex);
				break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

// Create a string containing Region and District in the same form as displayed in
// dialog for "Bekr�fta inl�sning"; 061011 p�d
CString CMDIGallBasCompartFormView::getRegionAndDistrictData(int region,int dist,DISTRICT_DATA &rec)
{
	CString sRetStr;
	if (m_vecDistrictData.empty())
	{
		return _T("");
	}

	for (UINT i = 0;i < m_vecDistrictData.size();i++)
	{
		DISTRICT_DATA	data = m_vecDistrictData[i];
		if (data.m_nRegionID == region && data.m_nDistrictID == dist)
		{
			sRetStr.Format(_T("(%d) %s  -  (%d) %s"),data.m_nRegionID,
																		data.m_sRegionName,
																		data.m_nDistrictID,
																		data.m_sDistrictName);
			rec = data;
			break;
		}
	}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	return sRetStr;
}

void CMDIGallBasCompartFormView::getDistrictFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getDistricts(m_vecDistrictData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasCompartFormView::getMachineFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getMachines(m_vecMachineData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)

}

void CMDIGallBasCompartFormView::getTraktFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTrakts(m_vecTraktData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasCompartFormView::getCompartFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getCompart(m_vecCompartData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)

}

// Create a string containing Region and District in the same form as displayed in
// dialog for "Bekr�fta inl�sning"; 061011 p�d
CString CMDIGallBasCompartFormView::getMachineEntr(int region,int dist,int machine)
{
	CString sRetStr;
	MACHINE_DATA data;
	if (m_vecMachineData.empty())
	{
		return _T("");
	}

	for (UINT i = 0;i < m_vecMachineData.size();i++)
	{
		data = m_vecMachineData[i];
		if (data.m_nRegionID == region && data.m_nDistrictID == dist && data.m_nMachineID == machine)
		{
			sRetStr = data.m_sContractorName;
			break;
		}
	}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	return sRetStr;
}

CString CMDIGallBasCompartFormView::getTraktInfo(int region,int dist,int machine,LPCTSTR trakt)
{
	TCHAR szRetStr[64];
	TRAKT_DATA data;
	if (m_vecTraktData.empty())
	{
		return _T("");
	}

	for (UINT i = 0;i < m_vecTraktData.size();i++)
	{
		data = m_vecTraktData[i];
		if (data.m_nRegionID == region && 
			  data.m_nDistrictID == dist && 
				data.m_nMachineID == machine &&
				_tcscmp(data.m_sTraktNum.Trim(),trakt) == 0 )
		{
			_stprintf(szRetStr,_T("%s / %s / %s"),data.m_sTraktNum,data.m_sTypeName.Trim(),data.m_sOriginName);
			break;
		}
	}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	return szRetStr;
}

void CMDIGallBasCompartFormView::addCompart(void)
{
	TRAKT_IDENTIFER	m_structTraktIdentifer;
	if (createFromFile(m_dbConnectionData,m_sLangFN,&m_structTraktIdentifer))
		resetCompart(0);
}

void CMDIGallBasCompartFormView::deleteCompart(void)
{
	DISTRICT_DATA dataDistrict;
	COMPART_DATA dataCompart;
	CString sMsg;
	CString sText1;
	CString sText2;
	CString sText3;
	CString sText4;
	CString sRegion;
	CString sDistrict;
	CString sMachine;
	CString sTrakt;
	CString sCompart;
	CString sCaption;
	CString sOKBtn;
	CString sCancelBtn;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sText1 = xml->str(IDS_STRING5002);
			sText2 = xml->str(IDS_STRING5012);
			sText3 = xml->str(IDS_STRING5022);
			sText4 = xml->str(IDS_STRING5032);
			sRegion = xml->str(IDS_STRING504);
			sDistrict = xml->str(IDS_STRING505);
			sMachine = xml->str(IDS_STRING506);
			sTrakt = xml->str(IDS_STRING507);
			sCompart = xml->str(IDS_STRING512);

			sCaption = xml->str(IDS_STRING109);
			sOKBtn = xml->str(IDS_STRING110);
			sCancelBtn = xml->str(IDS_STRING111);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	// Check that there's any data in m_vecTraktData vector; 061010 p�d
	if (m_vecCompartData.size() > 0 && 	m_bConnected)
	{

		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			// Get Regions in database; 061002 p�d	
			dataCompart = m_vecCompartData[m_nDBIndex];
			// Setup a message for user upon deleting machine; 061010 p�d

			getRegionAndDistrictData(dataCompart.m_nRegionID,
																dataCompart.m_nDistrictID,
																dataDistrict);
			sMsg.Format(_T("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%s : <b>%d</b>   (%s)<br>%s : <b>%d</b>  (%s)<br>%s : <b>%d</b><br>%s : <b>%s</b><br>%s : <b>%d</b><br><hr><br>%s<br>%s<br><br><font size=\"+4\" color=\"#ff0000\"><center>%s</center></font>"),
									sText1,
									sRegion,
									dataDistrict.m_nRegionID,
									dataDistrict.m_sRegionName,
									sDistrict,
									dataDistrict.m_nDistrictID,
									dataDistrict.m_sDistrictName,
									sMachine,
									dataCompart.m_nMachineID,
									sTrakt,
									dataCompart.m_sTraktNum,
									sCompart,
									dataCompart.m_lCompartID,
									sText2,
									sText3,
									sText4);

			if (messageDialog(sCaption,sOKBtn,sCancelBtn,sMsg))
			{
				// Delete Machine and ALL underlying 
				//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
				pDB->delCompart(dataCompart);
			}
			delete pDB;
		}	// if (pDB != NULL)
	}	// if (m_vecMachineData.size() > 0)
	resetCompart(0);
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIGallBasCompartFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

// PUBLIC
void CMDIGallBasCompartFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons(m_nDBIndex > 0,
												m_nDBIndex < (m_vecCompartData.size()-1));
}

// Reset and get data from database; 061010 p�d
void CMDIGallBasCompartFormView::resetCompart(int todo)
{
	// Get updated data from Database
	getMachineFromDB();
	// Get updated data from Database
	getCompartFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecCompartData.size() > 0)
	{

		// Reset to first item in m_vecMachineData
		m_nDBIndex = 0;
		// Populate data (first item) on User interface
		populateData(m_nDBIndex);
		// Set toolbar on HMSShell
		if (todo >= 0)
		{
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecCompartData.size()-1));
		}
	}	// if (m_vecMachineData.size() > 0)
	else
	{
		if (todo >= 0)
		{
			setNavigationButtons(m_nDBIndex > 0,
			    								m_nDBIndex < (m_vecCompartData.size()-1));
			clearAll();
		}
	}
}

void CMDIGallBasCompartFormView::clearAll(void)
{
	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_wndEdit3.SetWindowText(_T(""));
	m_wndEdit5.SetWindowText(_T(""));
	m_wndEdit6.SetWindowText(_T(""));
	m_wndEdit7.SetWindowText(_T(""));
	m_wndEdit8.SetWindowText(_T(""));
	m_wndEdit9.SetWindowText(_T(""));
	m_wndEdit10.SetWindowText(_T(""));
	m_wndEdit11.SetWindowText(_T(""));
	m_wndEdit12.SetWindowText(_T(""));
	m_wndEdit13.SetWindowText(_T(""));
	m_wndEdit14.SetWindowText(_T(""));
	m_wndEdit15.SetWindowText(_T(""));
	m_wndEdit16.SetWindowText(_T(""));
	m_wndEdit17.SetWindowText(_T(""));
	m_wndEdit18.SetWindowText(_T(""));
	m_wndEdit19.SetWindowText(_T(""));
	m_wndEdit20.SetWindowText(_T(""));
	m_wndEdit21.SetWindowText(_T(""));
	m_wndEdit22.SetWindowText(_T(""));
	m_wndEdit23.SetWindowText(_T(""));
	m_wndEdit24.SetWindowText(_T(""));
	m_wndEdit25.SetWindowText(_T(""));
	m_wndEdit26.SetWindowText(_T(""));
	m_wndEdit27.SetWindowText(_T(""));
	m_wndEdit28.SetWindowText(_T(""));
	m_wndEdit29.SetWindowText(_T(""));
	m_wndEdit30.SetWindowText(_T(""));

	m_wndEdit_Forrensat.SetWindowText(_T(""));
	m_wndEdit_Si.SetWindowText(_T(""));
	m_wndEdit_Traktdir_LagstaGy.SetWindowText(_T(""));
	m_wndEdit_Traktdir_GyFore.SetWindowText(_T(""));
	m_wndEdit_Traktdir_GyEfter.SetWindowText(_T(""));
	m_wndEdit_Traktdir_GaUttag.SetWindowText(_T(""));


}


