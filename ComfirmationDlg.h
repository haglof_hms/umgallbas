#pragma once

#include "UMGallBasDB.h"

#include "Resource.h"

// CComfirmationDlg dialog

class CComfirmationDlg : public CXTPDialog
{
	DECLARE_DYNAMIC(CComfirmationDlg)

	CString m_sLangAbbrev;
	CString m_sLangFN;

public:
	CComfirmationDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CComfirmationDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

	void setRegion(LPCTSTR region)
	{
		CComfirmationDlg::m_nRegionID = _tstoi(region);
	}
	void setDistrict(LPCTSTR dist)
	{
		CComfirmationDlg::m_nDistrictID = _tstoi(dist);
	}
	void setMachine(LPCTSTR machine,LPCTSTR contr)
	{
		m_nMachineID = _tstoi(machine);
		m_sContractor = contr;
	}
	void setTrakt(LPCTSTR trakt,LPCTSTR type,LPCTSTR origin,double areal)
	{
		m_sTrakt = trakt;
		m_sType  = type;
		m_nOrigin = _tstoi(origin);
		m_fAreal = areal;

	}
	void setNumOfCompart(int numof)
	{
		m_nNumOfCompart = numof;
	}
	void setNumOfPlots(int numof)
	{
		m_nNumOfPlots = numof;
	}

	void setDataFileName(LPCTSTR fn)
	{
		m_sDataFileName = fn;
	}

	void getMachineData(MACHINE_DATA &rec)
	{
		rec = m_recMachineData;
	}
	void getTraktData(TRAKT_DATA &rec,CXmlReader *Trakt)
	{
		

		m_recTraktData.m_nMaskinstorlek		= Trakt->m_n_trakt_maskinstorlek;
		m_recTraktData.m_nAvslut_myr			= Trakt->m_n_trakt_avslut_myr;
		m_recTraktData.m_nAvslut_hallmark		= Trakt->m_n_trakt_avslut_hallmark;
		m_recTraktData.m_nAvslut_hansyn		= Trakt->m_n_trakt_avslut_hansyn;
		m_recTraktData.m_nAvslut_fdkulturmark	= Trakt->m_n_trakt_avslut_fdkulturmark;
		m_recTraktData.m_nAvslut_kulturmiljo	= Trakt->m_n_trakt_avslut_kulturmiljo;
		m_recTraktData.m_nAvslut_lovandel		= Trakt->m_n_trakt_avslut_lovandel;
		m_recTraktData.m_nAvslut_skyddszon		= Trakt->m_n_trakt_avslut_skyddszon;
		m_recTraktData.m_nAvslut_nvtrad		= Trakt->m_n_trakt_avslut_nvtrad;
		m_recTraktData.m_nAvslut_framtidstrad	= Trakt->m_n_trakt_avslut_framtidstrad;
		m_recTraktData.m_nAvslut_hogstubbar	= Trakt->m_n_trakt_avslut_hogstubbar;
		m_recTraktData.m_nAvslut_dodatrad		= Trakt->m_n_trakt_avslut_dodatrad;
		m_recTraktData.m_nAvslut_mindrekorskada= Trakt->m_n_trakt_avslut_mindrekorskada;
		m_recTraktData.m_nAvslut_allvarligkorskada = Trakt->m_n_trakt_avslut_allvarligkorskada;
		m_recTraktData.m_nAvslut_markskador	= Trakt->m_n_trakt_avslut_markskador;
		m_recTraktData.m_nAvslut_markskador2	= Trakt->m_n_trakt_avslut_markskador2;
		m_recTraktData.m_nAvslut_nedskrapning	= Trakt->m_n_trakt_avslut_nedskrapning;
		m_recTraktData.m_sAvslut_fritext		= Trakt->m_s_trakt_avslut_fritext;
		m_recTraktData.m_nAvslut_atgardstp		= Trakt->m_n_trakt_avslut_atgardstp;
		m_recTraktData.m_sAvslut_atgardstp_forklaring	= Trakt->m_s_trakt_avslut_atgardstp_forklaring;
		m_recTraktData.m_sAvslut_datum_slutford		= Trakt->m_s_trakt_avslut_datum_slutford;
		m_recTraktData.m_nCalc_si_spec			= Trakt->m_n_trakt_calc_si_spec;
		m_recTraktData.m_nCalc_si				= Trakt->m_n_trakt_calc_si;

		m_recTraktData.m_sTraktName	= Trakt->m_sObj_namn;
		
		rec = m_recTraktData;
	}

protected:
	CComboBox m_wndCBox1;
	CComboBox m_wndCBox2;

	CButton m_wndBtn1;
	CButton m_wndBtn2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl9;
	CMyExtStatic m_wndLbl10;
	CMyExtStatic m_wndLbl11;
	CMyExtStatic m_wndLbl12;
	CMyExtStatic m_wndLbl13;
	CMyExtStatic m_wndLbl14;
	CMyExtStatic m_wndLbl15;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;
	CMyExtEdit m_wndEdit7;

	CXTButton m_wndListRegionDistBtn;
	CXTButton m_wndListMachineBtn;
	CXTButton m_wndListTraktBtn;

	CString m_sErrCap;
	CString m_sErrMsg;
	CString	m_sErrMsg1;
	CString m_sErrMsg2;
	CString m_sCaption;
	CString m_sMachineNum;
	CString m_sContractCap;

	vecDistrictData m_vecDistrictData;
	vecMachineData m_vecMachineData;
	vecMachineRegData m_vecMachineRegData;
	vecTraktData m_vecTraktData;

	CString m_sDataFileName;

	// Set STATIC datammembers to hold information on
	// PRIMARY Keys
	static int m_nRegionID;
	static int m_nDistrictID;
	int m_nMachineID;
	CString m_sContractor;
	CString m_sTrakt;
	CString m_sType;
	int m_nOrigin;
	double m_fAreal;
	CString m_sOriginName;


	int m_nNumOfCompart;
	int m_nNumOfPlots;

	MACHINE_DATA m_recMachineData;
	TRAKT_DATA m_recTraktData;

	void setLanguage(void);

	void setRegionAndDistrictInfo(void);
	void setMachineInfo(void);
	void setTraktInfo(void);

	int getMachineID_setByUser(void);
	CString getTraktID_setByUser(void);
	CString getContractor_setByUser(void);
	CString getType_setByUser(void);
	CString getTypeName_setByUser(void);
	int getOrigin_setByUser(void);
	CString getOriginName_setByUser(void);

	BOOL isMachineNumOK(void);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

protected:
	//{{AFX_VIRTUAL(CComfirmationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	//{{AFX_MSG(CComfirmationDlg)
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
};
