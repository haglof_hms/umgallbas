#pragma once

#include "Resource.h"

// CSQLCompartQuestionDlg dialog

class CSQLCompartQuestionDlg : public CDialog
{
	DECLARE_DYNAMIC(CSQLCompartQuestionDlg)

public:
	CSQLCompartQuestionDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSQLCompartQuestionDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG6 };

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;

	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;

	CComboBox m_wndCBox1;
	CComboBox m_wndCBox2;

	CComboBox m_wndCBox3;	// Regions
	CComboBox m_wndCBox4;	// Districts

	CButton m_wndBtn1;
	CButton m_wndOK;

	CString m_sMsgCap;
	CString m_sMsg1;

	CString m_sLangFN;
	void setLanguage(void);
	void setSQLQuestionData(void);
	BOOL doesSQLQuestionReturnsAResult(void);
	CString getType(void);
	int getOrigin(void);

	vecRegionData m_vecRegionData;
	vecDistrictData m_vecDistrictData;
	void getRegionsFromDB(void);
	void setRegionLastSelected(int reg_id);
	void addRegionsToCBox(void);

	void getDistrictFromDB(void);
	void setDistrictLastSelected(int district_id);
	void addDistrictToCBox(int reg_id);

	CGallBasDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

protected:
	//{{AFX_VIRTUAL(CSQLMachineQuestionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	//{{AFX_MSG(CMDIGallBasRegionFormView)
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton1();
public:
	afx_msg void OnCbnSelchangeCombo3();
};
