#pragma once

#include "Resource.h"

#include "UMGallBasDB.h"

// CGallBasPlotsSelectList form view

class CGallBasPlotsSelectList : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CGallBasPlotsSelectList)

	CString m_sLangAbbrev;
	CString m_sLangFN;

protected:
	CGallBasPlotsSelectList();           // protected constructor used by dynamic creation
	virtual ~CGallBasPlotsSelectList();

	CButton m_wndBtn1;

	CMyReportCtrl m_wndPlotReport;
//	CXTPReportControl m_wndPlotReport;
	BOOL setupReport(void);
	void populateReport(void);

	vecPlotData m_vecPlotData;

	void getPlotsFromDB(void);

	CString getTypeName(LPCTSTR type);
	CString getOriginName(int num);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:
	enum { IDD = IDD_FORMVIEW11 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
};


