// SQLCompartQuestionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMGallBasDB.h"
#include "SQLPlotQuestionDlg.h"

#include "ResLangFileReader.h"
#include ".\sqltraktquestiondlg.h"

#include "UMGallBasGenerics.h"

// CSQLPlotsQuestionDlg dialog

IMPLEMENT_DYNAMIC(CSQLPlotsQuestionDlg, CDialog)

BEGIN_MESSAGE_MAP(CSQLPlotsQuestionDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_WM_COPYDATA()
	ON_CBN_SELCHANGE(IDC_COMBO3, &CSQLPlotsQuestionDlg::OnCbnSelchangeCombo3)
END_MESSAGE_MAP()

CSQLPlotsQuestionDlg::CSQLPlotsQuestionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSQLPlotsQuestionDlg::IDD, pParent)
{
}

CSQLPlotsQuestionDlg::~CSQLPlotsQuestionDlg()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

void CSQLPlotsQuestionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL7, m_wndLbl7);
	DDX_Control(pDX, IDC_LBL11, m_wndLbl8);
	DDX_Control(pDX, IDC_LBL17, m_wndLbl9);

	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit6);

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_COMBO2, m_wndCBox2);
	DDX_Control(pDX, IDC_COMBO3, m_wndCBox3);
	DDX_Control(pDX, IDC_COMBO4, m_wndCBox4);

	DDX_Control(pDX, IDC_BUTTON1, m_wndBtn1);
	DDX_Control(pDX, IDOK, m_wndOK);

	//}}AFX_DATA_MAP
}

BOOL CSQLPlotsQuestionDlg::OnInitDialog()
{

	CDialog::OnInitDialog();

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	m_wndEdit3.SetEnabledColor(BLACK, WHITE );
	m_wndEdit3.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit3.SetAsNumeric();

	m_wndEdit4.SetEnabledColor(BLACK, WHITE );
	m_wndEdit4.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit4.SetAsNumeric();

	m_wndEdit5.SetEnabledColor(BLACK, WHITE );
	m_wndEdit5.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit5.SetAsNumeric();

	m_wndEdit6.SetEnabledColor(BLACK, WHITE );
	m_wndEdit6.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit6.SetAsNumeric();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	setLanguage();

	// Get regions; 061204 p�d
	getRegionsFromDB();
	addRegionsToCBox();
	// Get districts (and region info); 061011 p�d
	getDistrictFromDB();

	setSQLQuestionData();

	return TRUE;

}

BOOL CSQLPlotsQuestionDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CGallBasDB(m_dbConnectionData);
		}

	}
	return CDialog::OnCopyData(pWnd, pData);
}

// MY METHODS

void CSQLPlotsQuestionDlg::setLanguage(void)
{

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText(xml->str(IDS_STRING401));

			m_wndLbl1.SetWindowText(xml->str(IDS_STRING4023));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING403));
			m_wndLbl3.SetWindowText(xml->str(IDS_STRING404));
			m_wndLbl4.SetWindowText(xml->str(IDS_STRING405));
			m_wndLbl5.SetWindowText(xml->str(IDS_STRING4060));
			m_wndLbl8.SetWindowText(xml->str(IDS_STRING4061));
			m_wndLbl9.SetWindowText(xml->str(IDS_STRING4062));
			m_wndLbl6.SetWindowText(xml->str(IDS_STRING407));
			m_wndLbl7.SetWindowText(xml->str(IDS_STRING408));

			m_wndBtn1.SetWindowText(xml->str(IDS_STRING4003));

			m_sMsgCap = xml->str(IDS_STRING109);
			m_sMsg1 = xml->str(IDS_STRING508);

		}
		delete xml;
	}
}

// Read an setup the SQL question last set into dialog fields; 061013 p�d
void CSQLPlotsQuestionDlg::setSQLQuestionData(void)
{
	FILE *fp;
	SQL_PLOT_QUESTION recPlot;
	CString sPathAndFN;
	CString sTmp;
	int nCnt;
	int nIndex = -1;

	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_PLOT_QUESTION_FN);

	if (fileExists(sPathAndFN))
	{
		if ((fp = _tfopen(sPathAndFN,_T("rb"))) != NULL)
		{
			fread(&recPlot,sizeof(SQL_PLOT_QUESTION),1,fp);
			fclose(fp);

			if (recPlot.nRegionID > 0)
				setRegionLastSelected(recPlot.nRegionID);
			if (recPlot.nDistrictID > 0)
				setDistrictLastSelected(recPlot.nDistrictID);
			if (recPlot.nMachineID > 0)
				m_wndEdit3.setInt(recPlot.nMachineID);
			if (recPlot.nTraktID > 0)
				m_wndEdit4.setInt(recPlot.nTraktID);
			if (recPlot.nCompartID > 0)
				m_wndEdit5.setInt(recPlot.nCompartID);
			if (recPlot.nPlotID > 0)
				m_wndEdit6.setInt(recPlot.nPlotID);
		}
	}

	nCnt = 0;
	for (int i = 0;i < (sizeof(INVENTORY_TYPE_ARRAY)/sizeof(INVENTORY_TYPE_ARRAY[0])) + 1;i++)
	{
		if (i > 0)
		{
			m_wndCBox1.AddString(INVENTORY_TYPE_ARRAY[nCnt].sInvTypeName);
			if (_tcscmp(recPlot.szType,INVENTORY_TYPE_ARRAY[nCnt].sAbbrevInvTypeName) == 0)
			{
				nIndex = i;
			}
			nCnt++;
		}
		else
		{
			m_wndCBox1.AddString(_T(""));
		}
	}
	if (nIndex > -1)
		m_wndCBox1.SetCurSel(nIndex);

	nCnt = 0;
	for (int i = 0;i < (sizeof(ORIGIN_ARRAY)/sizeof(ORIGIN_ARRAY[0])) + 1;i++)
	{
		if (i > 0)
		{
			m_wndCBox2.AddString(ORIGIN_ARRAY[nCnt].sOrigin);
			if (recPlot.nOrigin == ORIGIN_ARRAY[nCnt].nOriginNum)
			{
				nIndex = i;
			}
			nCnt++;
		}
		else
		{
			m_wndCBox2.AddString(_T(""));
			nIndex = i;
		}
	} // for (int i = 0;i < (sizeof(ORIGIN_ARRAY)/sizeof(ORIGIN_ARRAY[0])) + 1;i++)
	if (nIndex > -1)
		m_wndCBox2.SetCurSel(nIndex);

}

void CSQLPlotsQuestionDlg::getRegionsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		if (m_pDB != NULL)
		{
			m_pDB->getRegions(m_vecRegionData);
		}	// if (m_pDB != NULL)
	} // 	if (m_bConnected)
}

void CSQLPlotsQuestionDlg::setRegionLastSelected(int reg_id)
{
	if (m_wndCBox3.GetCount() > 0 && m_vecRegionData.size() > 0)
	{
		for (UINT i = 0;i < m_vecRegionData.size();i++)
		{
			REGION_DATA data = m_vecRegionData[i];
			if (data.m_nRegionID == reg_id)
			{
				m_wndCBox3.SetCurSel(i+1);
				addDistrictToCBox(data.m_nRegionID);
			}	// if (data.m_nRegionID == reg_id)
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_wndCBox3.GetCount() > 0 && m_vecRegionData.size() > 0)
}

// Add regions in registry, into ComboBox3; 061204 p�d
void CSQLPlotsQuestionDlg::addRegionsToCBox(void)
{
	CString sText;
	if (m_vecRegionData.size() > 0)
	{
		m_wndCBox3.ResetContent();
		m_wndCBox3.AddString(sText);	// To set empty box; 070419 p�d
		m_wndCBox3.SetItemData(0,-1);
		for (UINT i = 0;i < m_vecRegionData.size();i++)
		{
			REGION_DATA data = m_vecRegionData[i];
			sText.Format(_T("%d - %s"),data.m_nRegionID,data.m_sRegionName);
			m_wndCBox3.AddString(sText);
			m_wndCBox3.SetItemData(i+1,data.m_nRegionID);
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}

void CSQLPlotsQuestionDlg::getDistrictFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		if (m_pDB != NULL)
		{
			m_pDB->getDistricts(m_vecDistrictData);
		}	// if (m_pDB != NULL)
	} // 	if (m_bConnected)
}

void CSQLPlotsQuestionDlg::setDistrictLastSelected(int district_id)
{
	int nIndex = -1;
	CString sStr;
	if (m_wndCBox4.GetCount() > 0 && m_vecDistrictData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			if (data.m_nDistrictID == district_id)
			{
				sStr.Format(_T("%d - %s"),data.m_nDistrictID,data.m_sDistrictName);
				nIndex = m_wndCBox4.FindString(0,sStr);
				if (nIndex != CB_ERR)
				{
					m_wndCBox4.SetCurSel(nIndex);
					break;
				}
			}
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_wndCBox3.GetCount() > 0 && m_vecRegionData.size() > 0)
}

// Add regions in registry, into ComboBox3; 061204 p�d
void CSQLPlotsQuestionDlg::addDistrictToCBox(int reg_id)
{
	CString sText;
	int nCounter = 1;
		if (m_vecDistrictData.size() > 0)
	{
		m_wndCBox4.ResetContent();
		m_wndCBox4.AddString(sText);	// To set empty box; 070419 p�d
		m_wndCBox4.SetItemData(0,-1);
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			if (data.m_nRegionID == reg_id)
			{
				sText.Format(_T("%d - %s"),data.m_nDistrictID,data.m_sDistrictName);
				m_wndCBox4.AddString(sText);
				m_wndCBox4.SetItemData(nCounter,data.m_nDistrictID);
				nCounter++;
			}
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}

CString CSQLPlotsQuestionDlg::getType(void)
{
	CString sText;
	int nIdx = m_wndCBox1.GetCurSel();
	if (nIdx != CB_ERR)
	{
		sText = INVENTORY_TYPE_ARRAY[nIdx-1].sAbbrevInvTypeName;
	}
	return sText;
}

int CSQLPlotsQuestionDlg::getOrigin(void)
{
	int nNum = -1;
	int nIdx = m_wndCBox2.GetCurSel();
	if (nIdx != CB_ERR && nIdx > 0)
	{
		nNum = ORIGIN_ARRAY[nIdx-1].nOriginNum;
	}
	return nNum;
}


// CSQLPlotsQuestionDlg message handlers

void CSQLPlotsQuestionDlg::OnBnClickedOk()
{
	FILE *fp;
	int nRegionID;
	int nDistrictID;
	int nMachineID;
	int nTraktID;
	CString sType;
	int nOrigin;
	int nCompartID;
	int nPlotID;
	BOOL bRegionSet = FALSE;
	BOOL bDistrictSet = FALSE;
	BOOL bMachineSet = FALSE;
	BOOL bTraktSet = FALSE;
	BOOL bTypeSet = FALSE;
	BOOL bOriginSet = FALSE;
	BOOL bCompartSet = FALSE;
	BOOL bPlotSet = FALSE;

	int nNumOf = 0;
	CString sPathAndFN;
	CString sSQL;
	CString sSQLSet;
	SQL_PLOT_QUESTION recPlot;

	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_PLOT_QUESTION_FN);
	nRegionID = m_wndCBox3.GetItemData(m_wndCBox3.GetCurSel());
	nDistrictID = m_wndCBox4.GetItemData(m_wndCBox4.GetCurSel());
	nMachineID = m_wndEdit3.getInt();
	nTraktID = m_wndEdit4.getInt();
	sType = getType();
	nOrigin = getOrigin();
	nCompartID = m_wndEdit5.getInt();
	nPlotID = m_wndEdit6.getInt();

	recPlot.nRegionID = nRegionID;
	recPlot.nDistrictID = nDistrictID;
	recPlot.nMachineID = nMachineID;
	recPlot.nTraktID = nTraktID;
	_tcscpy(recPlot.szType,sType);
	recPlot.nOrigin = nOrigin;
	recPlot.nCompartID = nCompartID;
	recPlot.nPlotID = nPlotID;
	
	sSQL.Format(SQL_SELECT,TBL_PLOT);
	// Find out how many fields in were clause of select qestion; 061013 p�d
	if (nRegionID > 0) nNumOf++;
	if (nDistrictID > 0) nNumOf++;
	if (nMachineID > 0) nNumOf++;
	if (nTraktID > 0) nNumOf++;
	if (sType.GetLength() > 0) nNumOf++;
	if (nOrigin > 0) nNumOf++;
	if (nCompartID > 0) nNumOf++;
	if (nPlotID > 0) nNumOf++;

	if (nNumOf > 0)
	{
		for (int i = 0;i < nNumOf;i++)
		{
			if (nRegionID > 0 && !bRegionSet)
			{
				sSQLSet.Format(SQL_PLOT_REGION_QUEST,nRegionID);
				sSQL += sSQLSet + _T(" and ");

				bRegionSet = TRUE;

			}
			if (nDistrictID > 0 && !bDistrictSet)
			{
				sSQLSet.Format(SQL_PLOT_DISTRICT_QUEST,nDistrictID);
				sSQL += sSQLSet + _T(" and ");

				bDistrictSet = TRUE;
			}
			if (nMachineID > 0 && !bMachineSet)
			{
				sSQLSet.Format(SQL_PLOT_MACHINE_QUEST,nMachineID);
				sSQL += sSQLSet + _T(" and ");
		
				bMachineSet = TRUE;
			}
			if (nTraktID > 0 && !bTraktSet)
			{
				sSQLSet.Format(SQL_PLOT_TRAKT_QUEST,nTraktID);
				sSQL += sSQLSet + _T(" and ");
		
				bTraktSet = TRUE;
			}
			if (sType.GetLength() > 0 && !bTypeSet)
			{
				sSQLSet.Format(SQL_PLOT_TYPE_QUEST,sType);
				sSQL += sSQLSet + _T(" and ");
		
				bTypeSet = TRUE;
			}
			if (nOrigin > 0 && !bOriginSet)
			{
				sSQLSet.Format(SQL_PLOT_ORIGIN_QUEST,nOrigin);
				sSQL += sSQLSet + _T(" and ");
		
				bOriginSet = TRUE;
			}

			if (nCompartID > 0 && !bCompartSet)
			{
				sSQLSet.Format(SQL_PLOT_COMPART_QUEST,nCompartID);
				sSQL += sSQLSet + _T(" and ");
		
				bCompartSet = TRUE;
			}
			if (nPlotID > 0 && !bPlotSet)
			{
				sSQLSet.Format(SQL_PLOT_QUEST,nPlotID);
				sSQL += sSQLSet + _T(" and ");
		
				bPlotSet = TRUE;
			}
		}
		// Remove last and in sSQL string
		sSQL.Delete(sSQL.GetLength()-5,5);
		sSQLSet = SQL_PLOT_ORDER_BY;
		sSQL += sSQLSet;
	}
	else
	{
		sSQL.Format(SQL_PLOT_QUESTION1,TBL_PLOT);
	}

	if ((fp = _tfopen(sPathAndFN,_T("wb"))) != NULL)
	{
		_tcscpy(recPlot.szSQL,sSQL);

		fwrite(&recPlot,sizeof(SQL_PLOT_QUESTION),1,fp);

		fclose(fp);
	}
	
	// Check if SQL Question gives any result. If not, tell user to redefine the question; 061012 p�d
	if (doesSQLQuestionReturnsAResult())
		OnOK();
	else
		::MessageBox(0,m_sMsg1,m_sMsgCap,MB_OK);
}

BOOL CSQLPlotsQuestionDlg::doesSQLQuestionReturnsAResult(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		if (m_pDB != NULL)
		{
			bReturn = m_pDB->isThereAnyPlotData();
		}	// if (m_pDB != NULL)
	} // 	if (m_bConnected)
	return bReturn;
}


void CSQLPlotsQuestionDlg::OnBnClickedButton1()
{
	FILE *fp;
	SQL_PLOT_QUESTION recPlot;
	TCHAR szBuffer[512];
	TCHAR szSQL[512];
	CString sPathAndFN;
	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_PLOT_QUESTION_FN);

	_tcscpy(szBuffer,SQL_PLOT_QUESTION1);
	_stprintf(szSQL,szBuffer,TBL_PLOT);
	
	recPlot.nRegionID = 0;
	recPlot.nDistrictID = 0;
	recPlot.nMachineID = 0;
	recPlot.nTraktID = 0;
	_tcscpy(recPlot.szType,_T(""));
	recPlot.nOrigin = 0;
	recPlot.nCompartID = 0;
	recPlot.nPlotID = 0;
	_tcscpy(recPlot.szSQL,szSQL);

	if ((fp = _tfopen(sPathAndFN,_T("wb"))) != NULL)
	{

		fwrite(&recPlot,sizeof(SQL_PLOT_QUESTION),1,fp);

		fclose(fp);
	}

	OnOK();
}

void CSQLPlotsQuestionDlg::OnCbnSelchangeCombo3()
{
	int nIndex = m_wndCBox3.GetCurSel();	
	int nRegId = m_wndCBox3.GetItemData(nIndex);
	addDistrictToCBox(nRegId);
}
