// MDIGallBasActiveMachineFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIGallBasActiveMachineFormView.h"

#include "ResLangFileReader.h"

#include "UMGallBasGenerics.h"

// CMDIGallBasActiveMachineFormView

IMPLEMENT_DYNCREATE(CMDIGallBasActiveMachineFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIGallBasActiveMachineFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_CLICK, IDC_MACHINE_REPORT, OnReportItemDblClick)
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CMDIGallBasActiveMachineFormView::CMDIGallBasActiveMachineFormView()
	: CXTResizeFormView(CMDIGallBasActiveMachineFormView::IDD)
{
	m_bIsDirty = FALSE;
}

CMDIGallBasActiveMachineFormView::~CMDIGallBasActiveMachineFormView()
{
}

BOOL CMDIGallBasActiveMachineFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIGallBasActiveMachineFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

void CMDIGallBasActiveMachineFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sText1 = xml->str(IDS_STRING5004);
			m_sText2 = xml->str(IDS_STRING5041);
			m_sText3 = xml->str(IDS_STRING5042);

			m_sMachineNum = xml->str(IDS_STRING304);
			m_sMachineName = xml->str(IDS_STRING305);
			m_sCaption = xml->str(IDS_STRING109);
			m_sOKBtn = xml->str(IDS_STRING110);
			m_sCancelBtn = xml->str(IDS_STRING111);

			m_sDelMsg1 = xml->str(IDS_STRING5191);
			m_sDelMsg2 = xml->str(IDS_STRING5192);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))
/*
	CString sPath;
	sPath.Format("%s\\%s\\%s",getProgDir(),SUBDIR_SCRIPTS,GALLBAS_MACHINES_TABLE);
	if (runSQLScriptFile(sPath,TBL_ACTIVE_MACHINE))
	{
*/
		getMachineRegFromDB();
//	}

	setupReport();
}

BOOL CMDIGallBasActiveMachineFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CMDIGallBasActiveMachineFormView diagnostics

#ifdef _DEBUG
void CMDIGallBasActiveMachineFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIGallBasActiveMachineFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CMDIGallBasActiveMachineFormView message handlers

// CMDIGallBasActiveMachineFormView message handlers
void CMDIGallBasActiveMachineFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndMachineRegReport.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndMachineRegReport,1,1,rect.right - 2,rect.bottom - 2);
	}
}

void CMDIGallBasActiveMachineFormView::OnSetFocus(CWnd*)
{
}

// Create and add Assortment settings reportwindow
BOOL CMDIGallBasActiveMachineFormView::setupReport(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndMachineRegReport.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndMachineRegReport.Create(this, IDC_MACHINE_REPORT,FALSE,FALSE ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndMachineRegReport.GetSafeHwnd() != NULL)
				{
					m_wndMachineRegReport.ShowWindow( SW_NORMAL );
          pCol = m_wndMachineRegReport.AddColumn(new CXTPReportColumn(0, xml->str(IDS_STRING304), 60));

					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndMachineRegReport.AddColumn(new CXTPReportColumn(1, xml->str(IDS_STRING305), 250));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->SetHeaderAlignment( DT_LEFT );
					pCol->SetAlignment( DT_LEFT );

					m_wndMachineRegReport.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndMachineRegReport.SetMultipleSelection( FALSE );
				  m_wndMachineRegReport.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndMachineRegReport.AllowEdit(TRUE);
					m_wndMachineRegReport.FocusSubItems(TRUE);
					m_wndMachineRegReport.SetFocus();

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 061002 p�d
					setResize(&m_wndMachineRegReport,1,1,rect.right - 2,rect.bottom - 2);

					populateReport();
				}	// if (m_wndMachineRegReport.GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CMDIGallBasActiveMachineFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			setNewItem();
			m_bIsDirty = TRUE;
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			saveMachineRegToDB();
			m_bIsDirty = FALSE;
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			m_bIsDirty = FALSE;
			getMachineFromDB();
			deleteMachineReg();
			// Reload machines from databaseM 061110 p�d
			getMachineRegFromDB();
			// Repopulate report; 061110 p�d
			populateReport();
			break;
		}	// case ID_NEW_ITEM :
	};
	return 0L;
}


void CMDIGallBasActiveMachineFormView::populateReport(void)
{
	m_wndMachineRegReport.ClearReport();
	for (UINT i = 0;i < m_vecMachineRegData.size();i++)
	{
		MACHINE_REG_DATA data = m_vecMachineRegData[i];
		m_wndMachineRegReport.AddRecord(new CMachineRegReportDataRec(i,data.m_nMachineRegID,data.m_sContractorName));
	}
	m_wndMachineRegReport.Populate();
	m_wndMachineRegReport.UpdateWindow();
}

void CMDIGallBasActiveMachineFormView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	int nItems = 0;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	CXTPReportRecords *pRecords = m_wndMachineRegReport.GetRecords();
	if (pRecords && pItemNotify->pRow)
	{
		nItems = pRecords->GetCount();

		if (pItemNotify->pRow->GetIndex() == nItems-1)
		{
			pItemNotify->pItem->GetEditOptions(0)->m_bAllowEdit = TRUE;
		}

	}
}

void CMDIGallBasActiveMachineFormView::getMachineFromDB(void)
{
	if (m_bConnected)
	{
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getMachines(m_vecMachineData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasActiveMachineFormView::getMachineRegFromDB(void)
{
	if (m_bConnected)
	{
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getMachineReg(m_vecMachineRegData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasActiveMachineFormView::saveMachineRegToDB(void)
{
	MACHINE_REG_DATA data;
	CMachineRegReportDataRec *pRec;
	CXTPReportRecords *pRecords = m_wndMachineRegReport.GetRecords();
	if (m_bConnected)
	{
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			if (pRecords)
			{
				for (int i = 0;i < pRecords->GetCount();i++)
				{
					pRec = (CMachineRegReportDataRec *)pRecords->GetAt(i);
					data.m_nMachineRegID = pRec->getColumnInt(0);
					data.m_sContractorName = pRec->getColumnText(1);
					if (!pDB->addMachineReg(data))
						pDB->updMachineReg(data);
				}
			}
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasActiveMachineFormView::deleteMachineReg(void)
{
	CXTPReportRow *pRow = NULL;
	MACHINE_REG_DATA data;
	CString sMsg;
	int nIndex = -1;

	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecMachineRegData.size() > 0 && m_bConnected)
	{
		pRow = m_wndMachineRegReport.GetFocusedRow();
		if (pRow)
		{
			nIndex = pRow->GetIndex();
		}

		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			// Get Regions in database; 061002 p�d	
			data = m_vecMachineRegData[nIndex];
			if (!isMachineUsed(data))
			{
				// Setup a message for user upon deleting machine; 061010 p�d
				sMsg.Format(_T("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%s : <b>%d</b><br>%s : <b>%s</b><br><hr><br>%s<br><br><font size=\"+4\" color=\"#ff0000\"><center>%s</center></font>"),
										m_sText1,
										m_sMachineNum,
										data.m_nMachineRegID,
										m_sMachineName,
										data.m_sContractorName,
										m_sText2,
										m_sText3);

				if (messageDialog(m_sCaption,m_sOKBtn,m_sCancelBtn,sMsg))
				{
					// Delete Machine and ALL underlying 
					//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
					pDB->delMachineReg(data);
				}	// if (messageDlg(m_sCaption,m_sOKBtn,m_sCancelBtn,sMsg))
			}	// if (!isMachineUsed(data))
			delete pDB;
		}	// if (pDB != NULL)
	}	// if (m_vecMachineData.size() > 0)
}

void CMDIGallBasActiveMachineFormView::setNewItem(void)
{
		CXTPReportRecords *pRecords = m_wndMachineRegReport.GetRecords();
		if (pRecords)
		{
			int	nItems = pRecords->GetCount();
			CXTPReportRecord *pRec = m_wndMachineRegReport.AddRecord(new CMachineRegReportDataRec(nItems));
			m_wndMachineRegReport.Populate();
			m_wndMachineRegReport.UpdateWindow();
			CXTPReportRow* pRow = m_wndMachineRegReport.GetRows()->Find(pRec);
			if (pRow)
			{
				m_wndMachineRegReport.SetFocusedRow(pRow);
			}

		}
}

BOOL CMDIGallBasActiveMachineFormView::isMachineUsed(MACHINE_REG_DATA &rec)
{
	CString sMsg;
	if (m_vecMachineData.size() > 0)
	{
		for (UINT i = 0;i < m_vecMachineData.size();i++)
		{
			MACHINE_DATA data = m_vecMachineData[i];
			if (rec.m_nMachineRegID == data.m_nMachineID &&
			 	  rec.m_sContractorName == data.m_sContractorName)
			{
				sMsg.Format(_T("%s\n\n%s"),m_sDelMsg1,m_sDelMsg2);
				::MessageBox(0,sMsg,m_sCaption,MB_ICONSTOP | MB_OK);
				return TRUE;
			}
		}
	}
	return FALSE;
}
