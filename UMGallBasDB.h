#if !defined(AFX_UMGALLBASDB_H)
#define AFX_UMGALLBASDB_H

#include "StdAfx.h"

#include "AddDataToDataBase.h"

//////////////////////////////////////////////////////////////////////////////////
// CGallBasDBDB; Handle ALL transactions for GallBas

// REGION DATA STRUCTURES
typedef struct _region_data
{
	int m_nRegionID;
	CString m_sRegionName;

	_region_data(void)
	{
		m_nRegionID = -1;
		m_sRegionName = "";
	}

	_region_data(int id,LPCTSTR name)
	{
		m_nRegionID = id;
		m_sRegionName = name;
	}
} REGION_DATA;
typedef std::vector<REGION_DATA> vecRegionData;


// DISTRICT DATA STRUCTURES
typedef struct _district_data
{
	int m_nRegionID;
	CString m_sRegionName;
	int m_nDistrictID;
	CString m_sDistrictName;

	_district_data(void)
	{
		m_nRegionID = -1;
		m_sRegionName = "";
		m_nDistrictID = -1;
		m_sDistrictName = "";
	}

	_district_data(int region_id,LPCTSTR region_name,int district_id,LPCTSTR district_name)
	{
		m_nRegionID = region_id;
		m_sRegionName = region_name;
		m_nDistrictID = district_id;
		m_sDistrictName = district_name;
	}
} DISTRICT_DATA;
typedef std::vector<DISTRICT_DATA> vecDistrictData;

// MACHINE DATA STRUCTURES
typedef struct _machine_data
{
	int m_nMachineID;
	CString m_sContractorName;
	int m_nRegionID;
	CString m_sRegionName;
	int m_nDistrictID;
	CString m_sDistrictName;

	_machine_data(void)
	{
		m_nMachineID = -1;
		m_nRegionID = -1;
		m_nDistrictID = -1;
		m_sContractorName = "";
	}

	_machine_data(int machine_id,
							  LPCTSTR contractor_name,
								int region_id,
								LPCTSTR region_name,
								int district_id,
								LPCTSTR district_name)
	{
		m_nMachineID = machine_id;
		m_sContractorName = contractor_name;
		m_nRegionID = region_id;
		m_sRegionName = region_name;
		m_nDistrictID = district_id;
		m_sDistrictName = district_name;
	}
} MACHINE_DATA;
typedef std::vector<MACHINE_DATA> vecMachineData;

// MACHINE REGISTRY DATA STRUCTURES
typedef struct _machine_reg_data
{
	int m_nMachineRegID;
	CString m_sContractorName;

	_machine_reg_data(void)
	{
		m_nMachineRegID = -1;
		m_sContractorName = "";
	}

	_machine_reg_data(int machine_reg_id,
							  LPCTSTR contractor_name)
	{
		m_nMachineRegID = machine_reg_id;
		m_sContractorName = contractor_name;
	}
} MACHINE_REG_DATA;
typedef std::vector<MACHINE_REG_DATA> vecMachineRegData;


typedef struct _sql_machine_question
{
	int nRegionID;
	int nDistrictID;
	TCHAR szSQL[1024];	// Buffer to hold actual question; 061010 p�d

} SQL_MACHINE_QUESTION;

typedef struct _sql_trakt_question
{
	int nRegionID;
	int nDistrictID;
	int nMachineID;
	int nTraktID;
	TCHAR szType[8];
	int nOrigin;
	TCHAR szSQL[1024];	// Buffer to hold actual question; 061010 p�d

} SQL_TRAKT_QUESTION;

typedef struct _sql_compart_question
{
	int nRegionID;
	int nDistrictID;
	int nMachineID;
	int nTraktID;
	TCHAR szType[8];
	int nOrigin;
	int nCompartID;
	TCHAR szSQL[1024];	// Buffer to hold actual question; 061010 p�d

} SQL_COMPART_QUESTION;

typedef struct _sql_plot_question
{
	int nRegionID;
	int nDistrictID;
	int nMachineID;
	int nTraktID;
	TCHAR szType[8];
	int nOrigin;
	int nCompartID;
	int nPlotID;
	TCHAR szSQL[1024];	// Buffer to hold actual question; 061010 p�d

} SQL_PLOT_QUESTION;

// TRAKT DATA STRUCTURES
typedef struct _trakt_data
{
	CString m_sTraktNum;
	CString m_sType;
	int m_nOrigin;
	int m_nRegionID;
	int m_nDistrictID;
	int m_nMachineID;
	CString m_sTraktName;
	CString m_sTraktDate;
	int n_nEnterType;
	CString m_sCreateDate;
	double m_fTraktAreal;	// Added 061018 p�d
	CString m_sOriginName;
	CString m_sTypeName;
	CString m_sNotes;	// Added 070419 Holmen
	CVariables m_recData;


	int m_nMaskinstorlek;
	int m_nAvslut_myr;
	int m_nAvslut_hallmark;
	int m_nAvslut_hansyn;
	int m_nAvslut_fdkulturmark;
	int m_nAvslut_kulturmiljo;
	int m_nAvslut_lovandel;
	int m_nAvslut_skyddszon;
	int m_nAvslut_nvtrad;
	int m_nAvslut_framtidstrad;
	int m_nAvslut_hogstubbar;
	int m_nAvslut_dodatrad;
	int m_nAvslut_mindrekorskada;
	int m_nAvslut_allvarligkorskada;
	int m_nAvslut_markskador;
	int m_nAvslut_markskador2;
	int m_nAvslut_nedskrapning;
	CString m_sAvslut_fritext;
	int m_nAvslut_atgardstp;
	CString m_sAvslut_atgardstp_forklaring;
	CString m_sAvslut_datum_slutford;
	int m_nCalc_si_spec;
	int m_nCalc_si;


	_trakt_data(void)
	{
		m_sTraktNum		= "";
		m_sType				= "";
		m_nOrigin			= -1;
		m_nRegionID		= -1;
		m_nDistrictID	= -1;
		m_nMachineID	= -1;
		m_sTraktName	= "";
		m_sTraktDate	= "";
		m_fTraktAreal	= 0.0;
		n_nEnterType	= -1; 
		m_sOriginName	= "";
		m_sTypeName		= "";
		m_sCreateDate = "";
		m_sNotes			= "";

		m_nMaskinstorlek		= -1;
		m_nAvslut_myr			= -1;
		m_nAvslut_hallmark		= -1;
		m_nAvslut_hansyn		= -1;
		m_nAvslut_fdkulturmark	= -1;
		m_nAvslut_kulturmiljo	= -1;
		m_nAvslut_lovandel		= -1;
		m_nAvslut_skyddszon		= -1;
		m_nAvslut_nvtrad		= -1;
		m_nAvslut_framtidstrad	= -1;
		m_nAvslut_hogstubbar	= -1;
		m_nAvslut_dodatrad		= -1;
		m_nAvslut_mindrekorskada= -1;
		m_nAvslut_allvarligkorskada		= -1;
		m_nAvslut_markskador	= -1;
		m_nAvslut_markskador2	= -1;
		m_nAvslut_nedskrapning	= -1;
		m_sAvslut_fritext		= "";
		m_nAvslut_atgardstp		= -1;
		m_sAvslut_atgardstp_forklaring	= "";
		m_sAvslut_datum_slutford		= "";
		m_nCalc_si_spec			= -1;
		m_nCalc_si				= -1;

	}

	_trakt_data(LPCTSTR trakt_num,
						  LPCTSTR type,
							int origin,
							int region_id,
							int district_id,
							int machine_id,
							LPCTSTR name,
							LPCTSTR date,
							int enter_type,
							LPCTSTR create_date,
							double trakt_areal,
							LPCTSTR trakt_origin_name,
							LPCTSTR trakt_type_name,
							LPCTSTR trakt_notes,
							CVariables &data,
							int nMaskinstorlek,
							int nAvslut_myr,
							int nAvslut_hallmark,
							int nAvslut_hansyn,
							int nAvslut_fdkulturmark,
							int nAvslut_kulturmiljo,
							int nAvslut_lovandel,
							int nAvslut_skyddszon,
							int nAvslut_nvtrad,
							int nAvslut_framtidstrad,
							int nAvslut_hogstubbar,
							int nAvslut_dodatrad,
							int nAvslut_mindrekorskada,
							int nAvslut_allvarligkorskada,
							int nAvslut_markskador,
							int nAvslut_markskador2,
							int nAvslut_nedskrapning,
							LPCTSTR sAvslut_fritext,
							int nAvslut_atgardstp,
							LPCTSTR sAvslut_atgardstp_forklaring,
							LPCTSTR sAvslut_datum_slutford,
							int nCalc_si_spec,
							int nCalc_si)
	{
		m_sTraktNum		= trakt_num;
		m_sType				= type;
		m_nOrigin			= origin;
		m_nRegionID		= region_id;
		m_nDistrictID	= district_id;
		m_nMachineID	= machine_id;
		m_sTraktName	= name;
		m_sTraktDate	= date;
		n_nEnterType	= enter_type;
		m_fTraktAreal	= trakt_areal;
		m_sCreateDate = create_date;
		m_sOriginName	= trakt_origin_name;
		m_sTypeName		= trakt_type_name;
		m_sNotes			= trakt_notes;
		m_recData			= data;

		m_nMaskinstorlek		= nMaskinstorlek;
		m_nAvslut_myr			= nAvslut_myr;
		m_nAvslut_hallmark		= nAvslut_hallmark;
		m_nAvslut_hansyn		= nAvslut_hansyn;
		m_nAvslut_fdkulturmark	= nAvslut_fdkulturmark;
		m_nAvslut_kulturmiljo	= nAvslut_kulturmiljo;
		m_nAvslut_lovandel		= nAvslut_lovandel;
		m_nAvslut_skyddszon		= nAvslut_skyddszon;
		m_nAvslut_nvtrad		= nAvslut_nvtrad;
		m_nAvslut_framtidstrad	= nAvslut_framtidstrad;
		m_nAvslut_hogstubbar	=	nAvslut_hogstubbar;
		m_nAvslut_dodatrad		= nAvslut_dodatrad;
		m_nAvslut_mindrekorskada= nAvslut_mindrekorskada;
		m_nAvslut_allvarligkorskada = nAvslut_allvarligkorskada;
		m_nAvslut_markskador	= nAvslut_markskador;
		m_nAvslut_markskador2	= nAvslut_markskador2;
		m_nAvslut_nedskrapning	= nAvslut_nedskrapning;
		m_sAvslut_fritext		= sAvslut_fritext;
		m_nAvslut_atgardstp		= nAvslut_atgardstp;
		m_sAvslut_atgardstp_forklaring	= sAvslut_atgardstp_forklaring;
		m_sAvslut_datum_slutford		= sAvslut_datum_slutford;
		m_nCalc_si_spec			= nCalc_si_spec;
		m_nCalc_si				= nCalc_si;
	}




} TRAKT_DATA;
typedef std::vector<TRAKT_DATA> vecTraktData;


// COMPARTMENT (avdelning) DATA STRUCTURES
typedef struct _compart_data
{
	long m_lCompartID;
	CString m_sType;
	int m_nOrigin;
	int m_nRegionID;
	int m_nDistrictID;
	int m_nMachineID;
	CString m_sTraktNum;
	CString m_sCreateDate;
	CVariables m_recData;

	
	int m_nGallrtyp;
	int m_nMetod;
	CString m_sSpec_and_si;
	int m_nSi_spec;
	int m_nSi;
	int m_nLagsta_gy_enl_mall;
	int m_nTraktdir_gy_fore_m2ha;
	int m_nTraktdir_gy_efter_m2ha;
	int m_nTraktdir_ga_uttag;

	double m_f_compart_gy_wdraw_forest;
	double m_f_compart_gy_wdraw_road;
	double m_f_compart_oh;
	double m_f_compart_areal;
	double m_f_compart_antal_stubbar;
	
	_compart_data(void)
	{
		m_lCompartID	= -1;
		m_sType				= "";
		m_nOrigin			= -1;
		m_nRegionID		= -1;
		m_nDistrictID	= -1;
		m_nMachineID	= -1;
		m_sTraktNum		= "";
		m_sCreateDate = "";
		m_nGallrtyp=-1;
		m_nMetod=-1;
		m_sSpec_and_si= "";
		m_nSi_spec=-1;
		m_nSi=-1;
		m_nLagsta_gy_enl_mall=-1;
		m_nTraktdir_gy_fore_m2ha=-1;
		m_nTraktdir_gy_efter_m2ha=-1;
		m_nTraktdir_ga_uttag=-1;
		m_f_compart_gy_wdraw_forest=0.0;
		m_f_compart_gy_wdraw_road=0.0;
		m_f_compart_oh=0.0;
		m_f_compart_areal=0.0;
		m_f_compart_antal_stubbar=0.0;
		
	}

	_compart_data(long compart_id,
		LPCTSTR type,
		int origin,
		int region_id,
		int district_id,
		int machine_id,
		LPCTSTR trakt_num,
		LPCTSTR create_date,
		CVariables &data,
		int nGallrtyp,
		int nMetod,
		LPCTSTR sSpec_and_si,
		int nSi_spec,
		int nSi,
		int nLagsta_gy_enl_mall,
		int nTraktdir_gy_fore_m2ha,
		int nTraktdir_gy_efter_m2ha,
		int nTraktdir_ga_uttag,
		double f_compart_gy_wdraw_forest,
		double f_compart_gy_wdraw_road,
		double f_compart_oh,
		double f_compart_areal,
		double f_compart_antal_stubbar)
	{
		m_lCompartID	= compart_id;
		m_sType			= type;
		m_nOrigin		= origin;
		m_nRegionID		= region_id;
		m_nDistrictID	= district_id;
		m_nMachineID	= machine_id;
		m_sTraktNum		= trakt_num;
		m_sCreateDate	= create_date;
		m_recData		= data;
		m_nGallrtyp		=nGallrtyp;
		m_nMetod		=nMetod;
		m_sSpec_and_si	=sSpec_and_si;
		m_nSi_spec		=nSi_spec;
		m_nSi			=nSi;
		m_nLagsta_gy_enl_mall		=nLagsta_gy_enl_mall;
		m_nTraktdir_gy_fore_m2ha	=nTraktdir_gy_fore_m2ha;
		m_nTraktdir_gy_efter_m2ha	=nTraktdir_gy_efter_m2ha;
		m_nTraktdir_ga_uttag		=nTraktdir_ga_uttag;
		m_f_compart_gy_wdraw_forest=f_compart_gy_wdraw_forest;
		m_f_compart_gy_wdraw_road=f_compart_gy_wdraw_road;
		m_f_compart_oh=f_compart_oh;
		m_f_compart_areal=f_compart_areal;
		m_f_compart_antal_stubbar=f_compart_antal_stubbar;
		}
} COMPART_DATA;
typedef std::vector<COMPART_DATA> vecCompartData;


// PLOT (Ytor) DATA STRUCTURES
typedef struct _plot_data
{
	int m_nPlotID;
	CString m_sType;
	int m_nOrigin;
	int m_nRegionID;
	int m_nDistrictID;
	int m_nMachineID;
	CString m_sTraktNum;
	long m_lCompartID;
	CString m_sCreateDate;
	CVariables m_recData;

	_plot_data(void)
	{
		m_nPlotID			= -1;
		m_sType				= "";
		m_nOrigin			= -1;
		m_nRegionID		= -1;
		m_nDistrictID	= -1;
		m_nMachineID	= -1;
		m_sTraktNum		= "";
		m_lCompartID	= -1;
		m_sCreateDate = "";
	}

	_plot_data(int plot_id,
								LPCTSTR type,
								int origin,
								int region_id,
								int district_id,
								int machine_id,
								LPCTSTR trakt_num,
								long compart_id,
								LPCTSTR create_date,
								CVariables &data
								)
	{
		m_nPlotID			= plot_id;
		m_sType				= type;
		m_nOrigin			= origin;
		m_nRegionID		= region_id;
		m_nDistrictID	= district_id;
		m_nMachineID	= machine_id;
		m_sTraktNum		= trakt_num;
		m_lCompartID	= compart_id;
		m_sCreateDate = create_date;
		m_recData			= data;
	}
} PLOT_DATA;
typedef std::vector<PLOT_DATA> vecPlotData;


////////////////////////////////////////////////////////////////////////////////
// These classes is used in CXTPReportControls for region and district; 061005 p�d

// CRegionReportDataRec

class CRegionReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CRegionReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CRegionReportDataRec(UINT index,int id,LPCTSTR name)
	{
		m_nIndex = index;
		AddItem(new CIntItem(id));
		AddItem(new CTextItem(name));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

// CDistrictReportDataRec

class CDistrictReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CDistrictReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CDistrictReportDataRec(UINT index,int region_id,LPCTSTR region_name,int district_id,LPCTSTR district_name)
	{
		m_nIndex = index;
		AddItem(new CIntItem(region_id));
		AddItem(new CTextItem(region_name));
		AddItem(new CIntItem(district_id));
		AddItem(new CTextItem(district_name));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};


// CMachineReportDataRec

class CMachineReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CMachineReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CMachineReportDataRec(UINT index,int region_id,LPCTSTR region_name,int district_id,LPCTSTR district_name,int machine_id,LPCTSTR contractor)
	{
		m_nIndex = index;
		AddItem(new CIntItem(region_id));
		AddItem(new CTextItem(region_name));
		AddItem(new CIntItem(district_id));
		AddItem(new CTextItem(district_name));
		AddItem(new CIntItem(machine_id));
		AddItem(new CTextItem(contractor));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

// CMachineRegReportDataRec

class CMachineRegReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CMachineRegReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
	}

	CMachineRegReportDataRec(UINT index)
	{
		m_nIndex = index;
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
	}

	CMachineRegReportDataRec(UINT index,int machine_id,LPCTSTR contractor)
	{
		m_nIndex = index;
		AddItem(new CIntItem(machine_id));
		AddItem(new CTextItem(contractor));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};


// CTraktReportDataRec

class CTraktReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CTraktReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CTraktReportDataRec(UINT index,int region_id,int district_id,int machine_id,LPCTSTR trakt_num,
											LPCTSTR type,LPCTSTR origin,LPCTSTR date,LPCTSTR entered_as)
	{
		m_nIndex = index;
		AddItem(new CIntItem(region_id));
		AddItem(new CIntItem(district_id));
		AddItem(new CIntItem(machine_id));
		AddItem(new CTextItem(trakt_num));
		AddItem(new CTextItem(type));
		AddItem(new CTextItem(origin));
		AddItem(new CTextItem(date));
		AddItem(new CTextItem(entered_as));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};


// CCompartReportDataRec

class CCompartReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CCompartReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CCompartReportDataRec(UINT index,int region_id,int district_id,int machine_id,LPCTSTR trakt_num,LPCTSTR type,LPCTSTR origin,int compart)
	{
		m_nIndex = index;
		AddItem(new CIntItem(region_id));
		AddItem(new CIntItem(district_id));
		AddItem(new CIntItem(machine_id));
		AddItem(new CTextItem(trakt_num));
		AddItem(new CTextItem(type));
		AddItem(new CTextItem(origin));
		AddItem(new CIntItem(compart));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

// CPlotsReportDataRec

class CPlotsReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CPlotsReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CPlotsReportDataRec(int index,int region_id,int district_id,int machine_id,LPCTSTR trakt_num,LPCTSTR type,LPCTSTR origin,int compart,int plot)
	{
		m_nIndex = index;
		AddItem(new CIntItem(region_id));
		AddItem(new CIntItem(district_id));
		AddItem(new CIntItem(machine_id));
		AddItem(new CTextItem(trakt_num));
		AddItem(new CTextItem(type));
		AddItem(new CTextItem(origin));
		AddItem(new CIntItem(compart));
		AddItem(new CIntItem(plot));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

class CGallBasDB : public CDBBaseClass_SQLApi //CDBHandlerBase
{
	vecRegionData m_vecRegionData;
	vecDistrictData m_vecDistrictData;
	vecMachineData m_vecMachineData;
protected:
	void getSQLMachineQuestion(LPTSTR);
	void getSQLTraktQuestion(LPTSTR);
	void getSQLCompartQuestion(LPTSTR);
	void getSQLPlotQuestion(LPTSTR);
public:
	CGallBasDB();
	CGallBasDB(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CGallBasDB(DB_CONNECTION_DATA &db_connection);

	BOOL getRegions(vecRegionData&);
	
	BOOL getDistricts(vecDistrictData&);
	
	BOOL getMachines(vecMachineData&);
	BOOL isThereAnyMachineData(void);
	BOOL addMachine(MACHINE_DATA &);
	BOOL updMachine(MACHINE_DATA &);
	// Delete machine and ALL trakt(s) associated
	BOOL delMachine(MACHINE_DATA &);

	BOOL getTrakts(vecTraktData&);
	BOOL isThereAnyTraktData(void);
	BOOL addTrakt(TRAKT_DATA &);
	BOOL updTrakt(TRAKT_DATA &);
	// Delete trakt and ALL associated Copmpartments and Plots
	BOOL delTrakt(TRAKT_DATA &);

	BOOL getCompart(vecCompartData&);
	BOOL isThereAnyCompartData(void);
	BOOL addCompart(COMPART_DATA &);
	BOOL updCompart(COMPART_DATA &);
	// Delete compartment (Avdelning) and ALL associated Plots
	BOOL delCompart(COMPART_DATA &);

	BOOL getPlot(vecPlotData&);
	BOOL isThereAnyPlotData(void);
	BOOL addPlot(PLOT_DATA &);
	BOOL updPlot(PLOT_DATA &);
	// Delete plot (yta)
	BOOL delPlot(PLOT_DATA &);

	// Machine regiter
	BOOL getMachineReg(vecMachineRegData&);
	BOOL getMachineInRegByNumber(MACHINE_REG_DATA &);
	BOOL isThereAnyMachineRegData(void);
	BOOL addMachineReg(MACHINE_REG_DATA &);
	BOOL updMachineReg(MACHINE_REG_DATA &);
	// Delete machine and ALL trakt(s) associated
	BOOL delMachineReg(MACHINE_REG_DATA &);

};


#endif

