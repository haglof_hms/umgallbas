// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT


#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#include <SQLAPI.h> // main SQLAPI++ header

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions


#include "pad_hms_miscfunc.h"
#include "pad_transaction_classes.h"	// HMSFuncLib header
// ... in PAD_DBTransactionLib
#include "DBBaseClass_SQLApi.h"	
#include "DBBaseClass_ADODirect.h"	// ... in DBTransaction_lib


/////////////////////////////////////////////////////////////////////
// My own includes

#define MSG_IN_SUITE				 				(WM_USER + 10)		// This identifer's used to send messages internally

/////////////////////////////////////////////////////////////////////
// Strings ids

#define IDS_STRING101					101
#define IDS_STRING102					102
#define IDS_STRING104					104
#define IDS_STRING105					105
#define IDS_STRING107					107
#define IDS_STRING108					108
#define IDS_STRING109					109
#define IDS_STRING110					110
#define IDS_STRING111					111
#define IDS_STRING112					112
#define IDS_STRING113					113

#define IDS_STRING200					200
#define IDS_STRING201					201
#define IDS_STRING202					202
#define IDS_STRING203					203
#define IDS_STRING204					204
#define IDS_STRING2040					2040
#define IDS_STRING2041					2041
#define IDS_STRING205					205
#define IDS_STRING206					206
#define IDS_STRING207					207
#define IDS_STRING208					208
#define IDS_STRING209					209
#define IDS_STRING210					210
#define IDS_STRING211					211
#define IDS_STRING2120					2120
#define IDS_STRING2121					2121
#define IDS_STRING2122					2122
#define IDS_STRING213					213
#define IDS_STRING214					214
#define IDS_STRING215					215
#define IDS_STRING216					216
#define IDS_STRING217					217
#define IDS_STRING218					218
#define IDS_STRING219					219
#define IDS_STRING220					220
#define IDS_STRING221					221
#define IDS_STRING222					222
#define IDS_STRING223					223
#define IDS_STRING224					224
#define IDS_STRING225					225
#define IDS_STRING226					226
#define IDS_STRING227					227
#define IDS_STRING228					228
#define IDS_STRING229					229
#define IDS_STRING230					230
#define IDS_STRING231					231
#define IDS_STRING232					232
#define IDS_STRING233					233
#define IDS_STRING234					234
#define IDS_STRING235					235
#define IDS_STRING236					236
#define IDS_STRING237					237
#define IDS_STRING238					238
#define IDS_STRING239					239
#define IDS_STRING240					240
#define IDS_STRING241					241
#define IDS_STRING242					242
#define IDS_STRING243					243
#define IDS_STRING244					244
#define IDS_STRING245					245
#define IDS_STRING246					246
#define IDS_STRING247					247
#define IDS_STRING248					248
#define IDS_STRING249					249
#define IDS_STRING250					250
#define IDS_STRING251					251
#define IDS_STRING252					252
#define IDS_STRING253					253
#define IDS_STRING254					254
#define IDS_STRING255					255
#define IDS_STRING256					256
#define IDS_STRING257					257

#define IDS_STRING300					300
#define IDS_STRING301					301
#define IDS_STRING302					302
#define IDS_STRING303					303
#define IDS_STRING304					304
#define IDS_STRING305					305
#define IDS_STRING306					306
#define IDS_STRING307					307
#define IDS_STRING308					308
#define IDS_STRING309					309
#define IDS_STRING310					310
#define IDS_STRING311					311

#define IDS_STRING4000					4000
#define IDS_STRING4001					4001
#define IDS_STRING4002					4002
#define IDS_STRING4003					4003
#define IDS_STRING401					401
#define IDS_STRING4020					4020
#define IDS_STRING4021					4021
#define IDS_STRING4022					4022
#define IDS_STRING4023					4023
#define IDS_STRING403					403
#define IDS_STRING404					404
#define IDS_STRING405					405
#define IDS_STRING4060					4060
#define IDS_STRING4061					4061
#define IDS_STRING4062					4062
#define IDS_STRING407					407
#define IDS_STRING408					408

#define IDS_STRING5000					5000
#define IDS_STRING5010					5010
#define IDS_STRING5020					5020
#define IDS_STRING5030					5030
#define IDS_STRING5001					5001
#define IDS_STRING5011					5011
#define IDS_STRING5021					5021
#define IDS_STRING5031					5031
#define IDS_STRING5002					5002
#define IDS_STRING5012					5012
#define IDS_STRING5022					5022
#define IDS_STRING5032					5032
#define IDS_STRING5003					5003
#define IDS_STRING5033					5033
#define IDS_STRING5004					5004
#define IDS_STRING5041					5041
#define IDS_STRING5042					5042
#define IDS_STRING504					504
#define IDS_STRING505					505
#define IDS_STRING506					506
#define IDS_STRING507					507
#define IDS_STRING508					508
#define IDS_STRING509					509
#define IDS_STRING510					510
#define IDS_STRING511					511
#define IDS_STRING512					512
#define IDS_STRING513					513
#define IDS_STRING514					514
#define IDS_STRING5151					5151
#define IDS_STRING5152					5152
#define IDS_STRING5153					5153
#define IDS_STRING5161					5161
#define IDS_STRING5162					5162
#define IDS_STRING5163					5163
#define IDS_STRING5171					5171
#define IDS_STRING5172					5172
#define IDS_STRING5181					5181
#define IDS_STRING5182					5182
#define IDS_STRING5191					5191
#define IDS_STRING5192					5192
#define IDS_STRING5201					5201
#define IDS_STRING5202					5202
#define IDS_STRING5210					5210
#define IDS_STRING5211					5211
#define IDS_STRING5212					5212
#define IDS_STRING5213					5213
#define IDS_STRING5214					5214
#define IDS_STRING5220					5220
#define IDS_STRING5221					5221
#define IDS_STRING5222					5222

#define IDS_STRING6001					6001
#define IDS_STRING6002					6002
#define IDS_STRING6003					6003
#define IDS_STRING6004					6004
#define IDS_STRING6005					6005
#define IDS_STRING6006					6006
#define IDS_STRING6007					6007
#define IDS_STRING6008					6008

#define IDS_STRING6101					6101
#define IDS_STRING6102					6102
#define IDS_STRING6103					6103
#define IDS_STRING6104					6104
#define IDS_STRING6105					6105
#define IDS_STRING6106					6106
#define IDS_STRING6107					6107
#define IDS_STRING6108					6108
#define IDS_STRING6109					6109
#define IDS_STRING6110					6110
#define IDS_STRING6111					6111
#define IDS_STRING6112					6112
#define IDS_STRING6113					6113
#define IDS_STRING6114					6114
#define IDS_STRING6115					6115
#define IDS_STRING6116					6116
#define IDS_STRING6117					6117
#define IDS_STRING6118					6118
#define IDS_STRING6119					6119
#define IDS_STRING6120					6120
#define IDS_STRING6121					6121

#define IDS_STRING6200					6200
#define IDS_STRING6201					6201
#define IDS_STRING6202					6202
#define IDS_STRING6203					6203

  //Trakt avslut Maskinstorlek -->
#define IDS_STRING6210	6210
#define IDS_STRING6211	6211
#define IDS_STRING6212	6212
#define IDS_STRING6213	6213
#define IDS_STRING6214	6214
#define IDS_STRING6215	6215
  //Trakt avslut Myr --
#define IDS_STRING6220	6220
#define IDS_STRING6221	6221
#define IDS_STRING6222	6222
#define IDS_STRING6223	6223
#define IDS_STRING6224	6224
#define IDS_STRING6225	6225

      
  //Trakt avslut strHallmark-->
#define IDS_STRING6230	6230
#define IDS_STRING6231	6231
#define IDS_STRING6232	6232
#define IDS_STRING6233	6233
#define IDS_STRING6234	6234
#define IDS_STRING6235	6235
  //Trakt avslut strHansyn-->
#define IDS_STRING6240	6240
#define IDS_STRING6241	6241
#define IDS_STRING6242	6242
#define IDS_STRING6243	6243
#define IDS_STRING6244	6244
#define IDS_STRING6245	6245
#define IDS_STRING6246	6246
#define IDS_STRING6247	6247
  //Trakt avslut strFdkulturmark-->
#define IDS_STRING6250	6250
#define IDS_STRING6251	6251
#define IDS_STRING6252	6252
#define IDS_STRING6253	6253
#define IDS_STRING6254	6254
#define IDS_STRING6255	6255
  //Trakt avslut strKulturmljo-->
#define IDS_STRING6260	6260
#define IDS_STRING6261	6261
#define IDS_STRING6262	6262
#define IDS_STRING6263	6263
#define IDS_STRING6264	6264
#define IDS_STRING6265	6265
  //Trakt avslut strLovandel-->
#define IDS_STRING6270	6270
#define IDS_STRING6271	6271
#define IDS_STRING6272	6272
#define IDS_STRING6273	6273
#define IDS_STRING6274	6274
#define IDS_STRING6275	6275
#define IDS_STRING6276	6276
#define IDS_STRING6277	6277
  //Trakt avslut strSkyddszon-->
#define IDS_STRING6280	6280
#define IDS_STRING6281	6281
#define IDS_STRING6282	6282
#define IDS_STRING6283	6283
#define IDS_STRING6284	6284
#define IDS_STRING6285	6285
#define IDS_STRING6286	6286
#define IDS_STRING6287	6287
  //Trakt avslut strNVtrad-->
#define IDS_STRING6290	6290
#define IDS_STRING6291	6291
#define IDS_STRING6292	6292
#define IDS_STRING6293	6293
  //Trakt avslut strFramtidtrad-->
#define IDS_STRING6300	6300
#define IDS_STRING6301	6301
#define IDS_STRING6302	6302
#define IDS_STRING6303	6303
#define IDS_STRING6304	6304
#define IDS_STRING6305	6305
  //Trakt avslut strHogstubbar-->
#define IDS_STRING6310	6310
#define IDS_STRING6311	6311
#define IDS_STRING6312	6312
#define IDS_STRING6313	6313
#define IDS_STRING6314	6314
#define IDS_STRING6315	6315
#define IDS_STRING6316	6316
#define IDS_STRING6317	6317
  //Trakt avslut strDodatrad-->
#define IDS_STRING6320	6320
#define IDS_STRING6321	6321
#define IDS_STRING6322	6322
#define IDS_STRING6323	6323
#define IDS_STRING6324	6324
#define IDS_STRING6325	6325
  //Trakt avslut strMarkskador-->
#define IDS_STRING6330	6330
#define IDS_STRING6331	6331
#define IDS_STRING6332	6332
#define IDS_STRING6333	6333
#define IDS_STRING6334	6334
#define IDS_STRING6335	6335
  //Trakt avslut strMarkskador2-->
#define IDS_STRING6340	6340
#define IDS_STRING6341	6341
#define IDS_STRING6342	6342
#define IDS_STRING6343	6343
#define IDS_STRING6344	6344
#define IDS_STRING6345	6345
#define IDS_STRING6346	6346
#define IDS_STRING6347	6347
#define IDS_STRING6348	6348
#define IDS_STRING6349	6349
#define IDS_STRING6350	6350
#define IDS_STRING6351	6351
#define IDS_STRING6352	6352
#define IDS_STRING6353	6353
#define IDS_STRING6354	6354
#define IDS_STRING6355	6355
  //Trakt avslut strNedskrapning-->
#define IDS_STRING6360	6360
#define IDS_STRING6361	6361
#define IDS_STRING6362	6362
#define IDS_STRING6363	6363
  //Trakt avslut strMindreKorskada-->
#define IDS_STRING6370	6370
#define IDS_STRING6371	6371
#define IDS_STRING6372	6372
#define IDS_STRING6373	6373
#define IDS_STRING6374	6374
#define IDS_STRING6375	6375
  //Trakt avslut strAllvarligKorskada-->
#define IDS_STRING6380	6380
#define IDS_STRING6381	6381
#define IDS_STRING6382	6382
#define IDS_STRING6383	6383
#define IDS_STRING6384	6384
#define IDS_STRING6385	6385
#define IDS_STRING6386	6386
#define IDS_STRING6387	6387
#define IDS_STRING6388	6388
#define IDS_STRING6389	6389
#define IDS_STRING6390	6390
#define IDS_STRING6391	6391
#define IDS_STRING6392	6392
#define IDS_STRING6393	6393
#define IDS_STRING6394	6394
#define IDS_STRING6395	6395
#define IDS_STRING6396	6396
#define IDS_STRING6397	6397

#define IDS_STRING7000	7000
#define IDS_STRING7001	7001
#define IDS_STRING7002	7002
#define IDS_STRING7003	7003
#define IDS_STRING7004	7004
#define IDS_STRING7005	7005
#define IDS_STRING7006	7006
#define IDS_STRING7007	7007

#define IDS_STRING7010	7010
#define IDS_STRING7011	7011
#define IDS_STRING7012	7012
#define IDS_STRING7013	7013

#define IDS_STRING7020	7020
#define IDS_STRING7021	7021
#define IDS_STRING7022	7022
#define IDS_STRING7023	7023


#define IDC_REGION_REPORT				8000
#define IDC_DISTRICT_REPORT				8001
#define IDC_MACHINE_REPORT				8002
#define IDC_TRAKT_REPORT				8003
#define IDC_COMPART_REPORT				8004
#define IDC_PLOT_REPORT					8005

#define ID_MSG_REGION_REPORT_SEL		8100
#define ID_MSG_DISTRICT_REPORT_SEL		8101
#define ID_MSG_MACHINE_REPORT_SEL		8102
#define ID_MSG_TRAKT_REPORT_SEL			8103
#define ID_MSG_COMPART_REPORT_SEL		8104
#define ID_MSG_PLOT_REPORT_SEL			8105

//////////////////////////////////////////////////////////////////////////////////////////
// Registry settings keys; 060918 p�d


//////////////////////////////////////////////////////////////////////////////////////////
// Misc. strings

#define PROGRAM_NAME				_T("UMGallBas")	// Used for Languagefile, registry entries etc.; 051114 p�d

//////////////////////////////////////////////////////////////////////////////////////////
// Enumerated value for Manually added or by file; 061201 p�d

typedef enum _action { MANUALLY,FROM_FILE,NOTHING } enumACTION;

//////////////////////////////////////////////////////////////////////////////////////////
// Enumerated value for reset values (e.g. resetTrakt); 061207 p�d

typedef enum _reset { 
					RESET_TO_FIRST_SET_NB,			// Set to first item and set Navigationbar
					RESET_TO_FIRST_NO_NB,			// Set to first item and no Navigationbar (disable all)
					RESET_TO_LAST_SET_NB,			// Set to last item and set Navigationbar
					RESET_TO_LAST_NO_NB,			// Set to last item and no Navigationbar (disable all)
					RESET_TO_JUST_ENTERED_SET_NB,	// Set to just entered data (from file or manually) and set Navigationbar
					RESET_TO_JUST_ENTERED_NO_NB		// Set to just entered data (from file or manually) and no Navigationbar
					} enumRESET;


//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of Window(s); 060209 p�d

#define MIN_X_SIZE_GALLBAS_REGION			400
#define MIN_Y_SIZE_GALLBAS_REGION			280

#define MIN_X_SIZE_GALLBAS_LIST_REGION		400
#define MIN_Y_SIZE_GALLBAS_LIST_REGION		280


#define MIN_X_SIZE_GALLBAS_DISTRICT			400
#define MIN_Y_SIZE_GALLBAS_DISTRICT			290

#define MIN_X_SIZE_GALLBAS_LIST_DISTRICT	400
#define MIN_Y_SIZE_GALLBAS_LIST_DISTRICT	280


#define MIN_X_SIZE_GALLBAS_MACHINES			400
#define MIN_Y_SIZE_GALLBAS_MACHINES			340

#define MIN_X_SIZE_GALLBAS_LIST_MACHINES	620
#define MIN_Y_SIZE_GALLBAS_LIST_MACHINES	400


#define MIN_X_SIZE_GALLBAS_TRAKT			780
#define MIN_Y_SIZE_GALLBAS_TRAKT			500

#define MIN_X_SIZE_GALLBAS_LIST_TRAKT		620
#define MIN_Y_SIZE_GALLBAS_LIST_TRAKT		400


#define MIN_X_SIZE_GALLBAS_COMPART			780
#define MIN_Y_SIZE_GALLBAS_COMPART			480

#define MIN_X_SIZE_GALLBAS_LIST_COMPART		620
#define MIN_Y_SIZE_GALLBAS_LIST_COMPART		400


#define MIN_X_SIZE_GALLBAS_PLOTS			780
#define MIN_Y_SIZE_GALLBAS_PLOTS			430

#define MIN_X_SIZE_GALLBAS_LIST_PLOTS		620
#define MIN_Y_SIZE_GALLBAS_LIST_PLOTS		400

#define MIN_X_SIZE_GALLBAS_MACHINE_REG		420
#define MIN_Y_SIZE_GALLBAS_MACHINE_REG		320


//////////////////////////////////////////////////////////////////////////////////////////
// Window placement registry key; 060904 p�d

#define REG_WP_GALLBAS_REGION_KEY			_T("UMGallBas\\Region\\Placement")
#define REG_WP_GALLBAS_LIST_REGION_KEY		_T("UMGallBas\\ListRegion\\Placement")

#define REG_WP_GALLBAS_DISTRICT_KEY			_T("UMGallBas\\District\\Placement")
#define REG_WP_GALLBAS_LIST_DISTRICT_KEY	_T("UMGallBas\\ListDistrict\\Placement")

#define REG_WP_GALLBAS_MACHINES_KEY			_T("UMGallBas\\Machines\\Placement")
#define REG_WP_GALLBAS_LIST_MACHINES_KEY	_T("UMGallBas\\ListMachines\\Placement")

#define REG_WP_GALLBAS_TRAKT_KEY			_T("UMGallBas\\Trakt\\Placement")
#define REG_WP_GALLBAS_LIST_TRAKT_KEY		_T("UMGallBas\\ListTrakt\\Placement")

#define REG_WP_GALLBAS_COMPART_KEY			_T("UMGallBas\\Compart\\Placement")
#define REG_WP_GALLBAS_LIST_COMPART_KEY		_T("UMGallBas\\ListCompart\\Placement")

#define REG_WP_GALLBAS_PLOTS_KEY			_T("UMGallBas\\Plots\\Placement")
#define REG_WP_GALLBAS_LIST_PLOTS_KEY		_T("UMGallBas\\ListPlots\\Placement")

#define REG_WP_GALLBAS_MACHINE_REG_KEY		_T("UMGallBas\\ActiveMachineReg\\Placement")

//////////////////////////////////////////////////////////////////////////////////////////
// Name of Tables in GallBas database; 061002 p�d

#define TBL_REGION				_T("region_table")
#define TBL_DISTRICT			_T("district_table")
#define TBL_MACHINE				_T("machine_table")
#define TBL_TRAKT				_T("trakt_table")
#define TBL_COMPART				_T("compart_table")
#define TBL_PLOT				_T("plot_table")

#define TBL_ACTIVE_MACHINE		_T("active_machine_table")

//////////////////////////////////////////////////////////////////////////////////////////
// SQL Script files, used in GallBas; 061109 p�d
#define GALLBAS_TABLES			_T("GallBas_tables.sql")
#define GALLBAS_MACHINES_TABLE	_T("GallBas_machine_table.sql")

//////////////////////////////////////////////////////////////////////////////////////////
// Array of Type and Origin

struct _invertory_type_data
{
	CString sAbbrevInvTypeName;
	CString sInvTypeName;
};

const _invertory_type_data INVENTORY_TYPE_ARRAY[6] = { {_T("E"), _T("Egen")}, {_T("K"), _T("Kontroll")}, {_T("C"), _T("Central")}, {_T("U"), _T("Uppf�ljning")}, {_T("D"), _T("Distrikt")}, {_T("CD"), _T("Central-Distrikt")} };


struct _origin_data
{
	int nOriginNum;
	CString sOrigin;
};

const _origin_data ORIGIN_ARRAY[4] = { {1, _T("Egen skog")}, {2, _T("Rotpost")}, {3, _T("Service uppdrag")}, {4, _T("Avverkningsuppdrag")} };

//////////////////////////////////////////////////////////////////////////////////////////
// Misc. strings

#define DEFAULT_EXTENSION			_T("*.xml")
//#define FILEOPEN_FILTER				_T("GallUp (*.txt;*.prn)|*.txt;*.prn|")

#define FILEOPEN_FILTER				_T("GallUp (*.xml)|*.xml|")

// These files'll be created, if it doesn't already exists.
// It is used to hold information on settings, for report.
// I.e. FromRegion,ToRegion,FromDistrict,ToDistrict etc; 061023 p�d
#define REPORT_SETTINGS_FILE		_T("report_settings.txt")
#define REPORT_SETTINGS2_FILE		_T("report_settings2.txt")
#define REPORT_SETTINGS3_FILE		_T("report_settings3.txt")

// Set SQL Questions used in getMachines( ... )


//

#define SQL_MACHINE_QUESTION_FN		_T("SQL_machine.bin")

// Question 1 selects ALL machines for ALL regions and Districts; 061010 p�d
#define SQL_MACHINE_QUESTION1		_T("select a.machine_num,a.machine_contractor_num,c.region_num,c.region_name,b.district_num,b.district_name ") \
									_T("from %s a,%s b,%s c ") \
									_T("where a.machine_district_num = b.district_num and ") \
									_T("a.machine_region_num = b.district_region_num and ") \
									_T("a.machine_region_num = c.region_num ") \
									_T("order by a.machine_region_num,a.machine_district_num,a.machine_num")

// Question 2 selects machines for region = id and districts; 061010 p�d
#define SQL_MACHINE_QUESTION2		_T("select a.machine_num,a.machine_contractor_num,c.region_num,c.region_name,b.district_num,b.district_name ") \
									_T("from %s a,%s b,%s c ") \
									_T("where a.machine_district_num = b.district_num and ") \
									_T("a.machine_region_num = b.district_region_num and ") \
									_T("a.machine_region_num = c.region_num and ") \
									_T("c.region_num = %d ") \
									_T("order by a.machine_region_num,a.machine_district_num,a.machine_num")

// Question 3 selects machines for districts = id; 061010 p�d
#define SQL_MACHINE_QUESTION3		_T("select a.machine_num,a.machine_contractor_num,c.region_num,c.region_name,b.district_num,b.district_name ") \
									_T("from %s a,%s b,%s c ") \
									_T("where a.machine_district_num = b.district_num and ") \
									_T("a.machine_region_num = b.district_region_num and ") \
									_T("a.machine_region_num = c.region_num and ") \
									_T("b.district_num = %d ") \
									_T("order by a.machine_region_num,a.machine_district_num,a.machine_num")

// Question 3 selects machines for region = id1 and districts = id2; 061010 p�d
#define SQL_MACHINE_QUESTION4		_T("select a.machine_num,a.machine_contractor_num,c.region_num,c.region_name,b.district_num,b.district_name ") \
									_T("from %s a,%s b,%s c ") \
									_T("where a.machine_district_num = b.district_num and ") \
									_T("a.machine_region_num = b.district_region_num and ") \
									_T("a.machine_region_num = c.region_num and ") \
									_T("c.region_num = %d and b.district_num = %d ") \
									_T("order by a.machine_region_num,a.machine_district_num,a.machine_num")



// SELECT SQL question, used in Trakt,Compartment and Plots; 061016 p�d
#define SQL_SELECT					_T("select * from %s where ")

// Set SQL Questions used in getTrakts( ... )

#define SQL_TRAKT_QUESTION_FN		_T("SQL_trakt.bin")

// Question 1 selects ALL "trakts" for region,district and machine; 061011 p�d
#define SQL_TRAKT_QUESTION1			_T("select * from %s order by trakt_region_num,trakt_district_num,trakt_machine_num,trakt_num")


#define SQL_TRAKT_ORDER_BY			_T("order by trakt_region_num,trakt_district_num,trakt_machine_num,trakt_num")

#define SQL_TRAKT_QUEST				_T("trakt_num=%d ")
#define SQL_TRAKT_TYPE_QUEST		_T("trakt_type=\'%s\' ")
#define SQL_TRAKT_ORIGIN_QUEST		_T("trakt_origin=%d ")
#define SQL_TRAKT_REGION_QUEST		_T("trakt_region_num=%d ")
#define SQL_TRAKT_DISTRICT_QUEST	_T("trakt_district_num=%d ")
#define SQL_TRAKT_MACHINE_QUEST		_T("trakt_machine_num=%d ")

// Set SQL Questions used in getCompart( ... )

#define SQL_COMPART_QUESTION_FN		_T("SQL_compart.bin")

// Question 1 selects ALL "compartments" for region,district,machine and trakt; 061012 p�d
#define SQL_COMPART_QUESTION1		_T("select * from %s order by compart_region_num,compart_district_num,compart_machine_num,compart_num")

#define SQL_COMPART_ORDER_BY		_T("order by compart_region_num,compart_district_num,compart_machine_num,compart_num")

#define SQL_COMPART_QUEST			_T("compart_num=%d ")
#define SQL_COMPART_TYPE_QUEST		_T("compart_type=\'%s\' ")
#define SQL_COMPART_ORIGIN_QUEST	_T("compart_origin=%d ")
#define SQL_COMPART_REGION_QUEST	_T("compart_region_num=%d ")
#define SQL_COMPART_DISTRICT_QUEST	_T("compart_district_num=%d ")
#define SQL_COMPART_MACHINE_QUEST	_T("compart_machine_num=%d ")
#define SQL_COMPART_TRAKT_QUEST		_T("compart_trakt_num=%d ")

// Set SQL Questions used in getPlot( ... )

#define SQL_PLOT_QUESTION_FN		_T("SQL_plot.bin")

// Question 1 selects ALL plots for region,district,machine,trakt and compartment; 061013 p�d
#define SQL_PLOT_QUESTION1			_T("select * from %s order by plot_region_num,plot_district_num,plot_machine_num,plot_trakt_num,plot_compart_num,plot_num")

#define SQL_PLOT_ORDER_BY			_T("order by plot_region_num,plot_district_num,plot_machine_num,plot_trakt_num,plot_compart_num,plot_num")

#define SQL_PLOT_QUEST				_T("plot_num=%d ")
#define SQL_PLOT_TYPE_QUEST			_T("plot_type=\'%s\' ")
#define SQL_PLOT_ORIGIN_QUEST		_T("plot_origin=%d ")
#define SQL_PLOT_REGION_QUEST		_T("plot_region_num=%d ")
#define SQL_PLOT_DISTRICT_QUEST		_T("plot_district_num=%d ")
#define SQL_PLOT_MACHINE_QUEST		_T("plot_machine_num=%d ")
#define SQL_PLOT_TRAKT_QUEST		_T("plot_trakt_num=%d ")
#define SQL_PLOT_COMPART_QUEST		_T("plot_compart_num=%d ")

//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions

long filesize(FILE *);

CString getInvType(LPCTSTR abbrev_type);

void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

class Scripts
{
	CString _TableName;
	CString _Script;
	CString _DBName;
public:
	enum actionTypes { TBL_CREATE,TBL_ALTER };

	Scripts(void)
	{
		_TableName = _T("");
		_Script = _T("");
		_DBName = _T("");
	}

	Scripts(LPCTSTR table_name,LPCTSTR script,LPCTSTR db_name)
	{
		_TableName = table_name;
		_Script = script;
		_DBName = db_name;
	}

	CString getTableName()	{ return _TableName; }
	CString getScript()			{ return _Script; }
	CString getDBName()			{ return _DBName; }
};


typedef std::vector<Scripts> vecScriptFiles;

BOOL runSQLScriptFileEx1(vecScriptFiles &vec,Scripts::actionTypes action);

