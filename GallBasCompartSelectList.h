#pragma once

#include "Resource.h"

#include "UMGallBasDB.h"

// CGallBasCompartSelectList form view

class CGallBasCompartSelectList : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CGallBasCompartSelectList)

	CString m_sLangAbbrev;
	CString m_sLangFN;

protected:
	CGallBasCompartSelectList();           // protected constructor used by dynamic creation
	virtual ~CGallBasCompartSelectList();

	CButton m_wndBtn1;

	CMyReportCtrl m_wndCompartReport;
	BOOL setupReport(void);
	void populateReport(void);

	vecCompartData m_vecCompartData;

	void getCompartFromDB(void);

	CString getTypeName(LPCTSTR type);
	CString getOriginName(int num);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:
	enum { IDD = IDD_FORMVIEW10 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
};


