// GallBasCompartSelectList.cpp : implementation file
//

#include "stdafx.h"
#include "UMGallBasGenerics.h"
#include "GallBasPlotsSelectList.h"

#include "ResLangFileReader.h"
#include "SQLPlotQuestionDlg.h"

#include "MDIGallBasPlotsFormView.h"

// CGallBasPlotsSelectList

IMPLEMENT_DYNCREATE(CGallBasPlotsSelectList, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CGallBasPlotsSelectList, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_CLICK, IDC_PLOT_REPORT, OnReportItemDblClick)
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CGallBasPlotsSelectList::CGallBasPlotsSelectList()
	: CXTResizeFormView(CGallBasPlotsSelectList::IDD)
{
}

CGallBasPlotsSelectList::~CGallBasPlotsSelectList()
{
}

void CGallBasPlotsSelectList::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_BUTTON1, m_wndBtn1);
	//}}AFX_DATA_MAP
}

void CGallBasPlotsSelectList::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	getPlotsFromDB();

	setupReport();
}

BOOL CGallBasPlotsSelectList::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


BOOL CGallBasPlotsSelectList::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CGallBasPlotsSelectList diagnostics

#ifdef _DEBUG
void CGallBasPlotsSelectList::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CGallBasPlotsSelectList::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CGallBasPlotsSelectList message handlers
void CGallBasPlotsSelectList::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndPlotReport.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndPlotReport,1,40,rect.right - 2,rect.bottom - 42);
	}
}

void CGallBasPlotsSelectList::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CGallBasPlotsSelectList::setupReport(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndPlotReport.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndPlotReport.Create(this, IDC_PLOT_REPORT, FALSE, FALSE ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndPlotReport.GetSafeHwnd() != NULL)
				{

					m_wndPlotReport.ShowWindow( SW_NORMAL );
					pCol = m_wndPlotReport.AddColumn(new CXTPReportColumn(0, xml->str(IDS_STRING300), 60));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndPlotReport.AddColumn(new CXTPReportColumn(1, xml->str(IDS_STRING302), 60));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndPlotReport.AddColumn(new CXTPReportColumn(2, xml->str(IDS_STRING304), 60));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndPlotReport.AddColumn(new CXTPReportColumn(3, xml->str(IDS_STRING306), 60));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_RIGHT );

					pCol = m_wndPlotReport.AddColumn(new CXTPReportColumn(4, xml->str(IDS_STRING307), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndPlotReport.AddColumn(new CXTPReportColumn(5, xml->str(IDS_STRING308), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndPlotReport.AddColumn(new CXTPReportColumn(6, xml->str(IDS_STRING309), 60));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndPlotReport.AddColumn(new CXTPReportColumn(7, xml->str(IDS_STRING310), 60));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 061002 p�d
					setResize(&m_wndPlotReport,1,40,rect.right - 2,rect.bottom - 42);

					populateReport();
				}	// if (m_wndPlotReport.GetSafeHwnd() != NULL)


				// Also set language for Buttons etc; 061009 p�d
				m_wndBtn1.SetWindowText(xml->str(IDS_STRING401));
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CGallBasPlotsSelectList::populateReport(void)
{
	m_wndPlotReport.ClearReport();
	for (UINT i = 0;i < m_vecPlotData.size();i++)
	{
		PLOT_DATA data = m_vecPlotData[i];
		m_wndPlotReport.AddRecord(new CPlotsReportDataRec(i,data.m_nRegionID,
																							 				 data.m_nDistrictID,
																							  			 data.m_nMachineID,
																											 data.m_sTraktNum.Trim(),
																											 getTypeName(data.m_sType.Trim()),
																										   getOriginName(data.m_nOrigin),
																											 data.m_lCompartID,
																											 data.m_nPlotID));
	}
	m_wndPlotReport.Populate();
	m_wndPlotReport.UpdateWindow();
}

void CGallBasPlotsSelectList::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify->pRow)
	{
		CTraktReportDataRec *pRec = (CTraktReportDataRec*)pItemNotify->pItem->GetRecord();
		CMDIGallBasPlotsFormView *pView = (CMDIGallBasPlotsFormView *)getFormViewByID(IDD_FORMVIEW5);
		if (pView)
		{
			pView->doPopulate(pRec->getIndex());
		}
	}
}

// CGallBasPlotsSelectList message handlers

void CGallBasPlotsSelectList::OnBnClickedButton1()
{

	BOOL bRet;
	CSQLPlotsQuestionDlg *pDlg = new CSQLPlotsQuestionDlg();

	if (pDlg)
	{

		bRet = (pDlg->DoModal() == IDOK);
	
		delete pDlg;

		if (bRet)
		{
			getPlotsFromDB();
			populateReport();
			CMDIGallBasPlotsFormView *pView = (CMDIGallBasPlotsFormView *)getFormViewByID(IDD_FORMVIEW5);
			if (pView)
			{
				pView->resetPlots(0);
			}
		}

	}

}

void CGallBasPlotsSelectList::getPlotsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getPlot(m_vecPlotData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

CString CGallBasPlotsSelectList::getTypeName(LPCTSTR type)
{
	for (int i = 0;i < sizeof(INVENTORY_TYPE_ARRAY)/sizeof(INVENTORY_TYPE_ARRAY[0]);i++)
	{
		if (INVENTORY_TYPE_ARRAY[i].sAbbrevInvTypeName == type)
			return INVENTORY_TYPE_ARRAY[i].sInvTypeName;
	}
	return _T("");
}

CString CGallBasPlotsSelectList::getOriginName(int num)
{
	for (int i = 0;i < sizeof(ORIGIN_ARRAY)/sizeof(ORIGIN_ARRAY[0]);i++)
	{
		if (ORIGIN_ARRAY[i].nOriginNum == num)
			return ORIGIN_ARRAY[i].sOrigin;
	}
	return _T("");
}
