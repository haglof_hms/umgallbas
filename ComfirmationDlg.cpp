// ComfirmationDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ComfirmationDlg.h"

#include "ResLangFileReader.h"
#include ".\comfirmationdlg.h"

#include "SelectRegionDistDlg.h"

// CComfirmationDlg dialog

IMPLEMENT_DYNAMIC(CComfirmationDlg, CXTPDialog)

BEGIN_MESSAGE_MAP(CComfirmationDlg, CXTPDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnBnClickedButton3)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

int CComfirmationDlg::m_nRegionID;
int CComfirmationDlg::m_nDistrictID;


CComfirmationDlg::CComfirmationDlg(CWnd* pParent /*=NULL*/)
	: CXTPDialog(CComfirmationDlg::IDD, pParent)
{
}

CComfirmationDlg::~CComfirmationDlg()
{
}

void CComfirmationDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTPDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_COMBO2, m_wndCBox2);

	DDX_Control(pDX, IDOK, m_wndBtn1);
	DDX_Control(pDX, IDCANCEL, m_wndBtn2);

	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL7, m_wndLbl7);
	DDX_Control(pDX, IDC_LBL8, m_wndLbl8);
	DDX_Control(pDX, IDC_LBL9, m_wndLbl9);
	DDX_Control(pDX, IDC_LBL10, m_wndLbl10);
	DDX_Control(pDX, IDC_LBL11, m_wndLbl11);
	DDX_Control(pDX, IDC_LBL12, m_wndLbl12);
	DDX_Control(pDX, IDC_LBL13, m_wndLbl13);
	DDX_Control(pDX, IDC_LBL14, m_wndLbl14);
	DDX_Control(pDX, IDC_LBL15, m_wndLbl15);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);	// Region och dist
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);	// Maskin
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);	// Antal
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);	// Antal
	DDX_Control(pDX, IDC_EDIT9, m_wndEdit5);	// Entrepen�r
	DDX_Control(pDX, IDC_EDIT10, m_wndEdit6);	// Trakt
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit7);	// Areal (ha)

	DDX_Control(pDX, IDC_BUTTON1, m_wndListRegionDistBtn);
	DDX_Control(pDX, IDC_BUTTON2, m_wndListMachineBtn);
	DDX_Control(pDX, IDC_BUTTON3, m_wndListTraktBtn);
	//}}AFX_DATA_MAP

}

BOOL CComfirmationDlg::OnInitDialog()
{

	CDialog::OnInitDialog();

	m_wndEdit5.SetEnabledColor(BLACK, WHITE );
	m_wndEdit5.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit6.SetEnabledColor(BLACK, WHITE );
	m_wndEdit6.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit7.SetEnabledColor(BLACK, WHITE );
	m_wndEdit7.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit1.SetEnabledColor(BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit1.SetReadOnly( TRUE );

	m_wndEdit2.SetEnabledColor(BLACK, WHITE );
	m_wndEdit2.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit2.SetReadOnly( TRUE );
	m_wndEdit2.SetAsNumeric();

	m_wndEdit3.SetEnabledColor(BLACK, WHITE );
	m_wndEdit3.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit3.SetReadOnly( TRUE );

	m_wndEdit4.SetEnabledColor(BLACK, WHITE );
	m_wndEdit4.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit4.SetReadOnly( TRUE );

	m_wndLbl6.SetTextColor( RGB(255,0,0) );
	m_wndLbl7.SetTextColor( RGB(255,0,0) );
	m_wndLbl8.SetTextColor( RGB(255,0,0) );
	m_wndLbl9.SetTextColor( RGB(255,0,0) );
	m_wndLbl13.SetTextColor( RGB(255,0,0) );
	m_wndLbl14.SetTextColor( RGB(255,0,0) );

	m_wndListRegionDistBtn.SetBitmap(CSize(18,14),IDB_BITMAP1);
	m_wndListRegionDistBtn.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	m_wndListMachineBtn.SetBitmap(CSize(18,14),IDB_BITMAP1);
	m_wndListMachineBtn.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	m_wndListTraktBtn.SetBitmap(CSize(18,14),IDB_BITMAP1);
	m_wndListTraktBtn.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			// Get Districts in database; 061004 p�d	
			pDB->getDistricts(m_vecDistrictData);	
			// Get Machines in database; 061031 p�d	
			pDB->getMachines(m_vecMachineData);	
			// Get Machines machione register in database; 061113 p�d	
			pDB->getMachineReg(m_vecMachineRegData);	
			// Get Trakt in database; 061031 p�d	
			pDB->getTrakts(m_vecTraktData);	
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)

	setLanguage();
	setRegionAndDistrictInfo();
	setMachineInfo();
	setTraktInfo();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CComfirmationDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CDialog::OnCopyData(pWnd, pData);
}

// CComfirmationDlg message handlers

void CComfirmationDlg::setLanguage(void)
{
	CString sMsg;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sMsg.Format(_T("%s : %s"),xml->str(IDS_STRING201),m_sDataFileName);
			SetWindowText(sMsg);
			m_wndBtn1.SetWindowText(xml->str(IDS_STRING202));
			m_wndBtn2.SetWindowText(xml->str(IDS_STRING203));
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING204));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING205));
			m_wndLbl3.SetWindowText(xml->str(IDS_STRING206));
			m_wndLbl4.SetWindowText(xml->str(IDS_STRING207));
			m_wndLbl5.SetWindowText(xml->str(IDS_STRING208));
			m_wndLbl10.SetWindowText(xml->str(IDS_STRING210));
			m_wndLbl11.SetWindowText(xml->str(IDS_STRING211));
			m_wndLbl12.SetWindowText(xml->str(IDS_STRING213));
			m_wndLbl15.SetWindowText(xml->str(IDS_STRING254));

			m_sErrCap = xml->str(IDS_STRING509);
			m_sErrMsg = xml->str(IDS_STRING209);
			m_sErrMsg1 = xml->str(IDS_STRING5201);
			m_sErrMsg2 = xml->str(IDS_STRING5202);
			m_sCaption = xml->str(IDS_STRING109);
			m_sMachineNum = xml->str(IDS_STRING304);
			m_sContractCap = xml->str(IDS_STRING305);

		}
		delete xml;
	}
}

// Compare machinenumber from user-file to machinenumber 
// in the machineregister; 061114 p�d
BOOL CComfirmationDlg::isMachineNumOK()
{
	BOOL bReturn = FALSE;
	if (m_vecMachineRegData.size() > 0)
	{
		for (UINT i = 0;i < m_vecMachineRegData.size();i++)
		{
			if (m_vecMachineRegData[i].m_nMachineRegID == m_nMachineID)
			{
				bReturn = TRUE;
				break;
			}	// if (m_vecMachineRegData[i].m_nMachineRegID == num)
		}
	}
	if (!bReturn)
	{
		CString sMsg;
		sMsg.Format(_T("%s\n\n%s : %d\n%s : %s\n\n%s"),
								m_sErrMsg1,
								m_sMachineNum,
								m_nMachineID,
								m_sContractCap,
								m_sContractor,
								m_sErrMsg2);
		::MessageBox(0,sMsg,m_sCaption,MB_ICONSTOP | MB_OK);
	}

	return bReturn;
}


void CComfirmationDlg::setRegionAndDistrictInfo(void)
{
	CString sTmp,S;
	DISTRICT_DATA dataDistrict;
	BOOL bRegionFound = FALSE;
	BOOL bDistrictFound = FALSE;

	// REGION AND DISTRICT
	if (m_vecDistrictData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			dataDistrict = m_vecDistrictData[i];
			// Also check if the Region and District from caliper file is in DB; 061004 p�d
			if (dataDistrict.m_nRegionID == CComfirmationDlg::m_nRegionID && !bRegionFound)
			{
				bRegionFound = TRUE;
			}	// if (data.m_nRegionID == CComfirmationDlg::m_nRegionID && !m_bRegionFound)

			if (dataDistrict.m_nDistrictID == CComfirmationDlg::m_nDistrictID && !bDistrictFound && bRegionFound)
			{
				bDistrictFound = TRUE;
				sTmp.Format(_T("(%d) %s  -  (%d) %s"),dataDistrict.m_nRegionID,
																					dataDistrict.m_sRegionName,
																					dataDistrict.m_nDistrictID,
																					dataDistrict.m_sDistrictName);
				m_wndEdit1.SetWindowText( sTmp );
			}	// if (data.m_nDistrictID == CComfirmationDlg::m_nDistrictID && !m_bDistrictFound)

		} // for (UINT i = 0;i < m_vecDistrictData.size();i++)
		// Select machine if found; 061005 p�d
		if (!bRegionFound && !bDistrictFound)
		{
			m_wndLbl6.SetWindowText(m_sErrMsg);
			m_wndLbl7.SetWindowText(_T("*"));
		} // else

		// If there's no Machines, disable the Button for adding
		// a machine from a list (CSelectRegionDistDlg); 061031 p�d
		m_wndListMachineBtn.EnableWindow( m_vecMachineRegData.size() > 0);

	} // if (m_vecDistrictData.size() > 0)

}


void CComfirmationDlg::setMachineInfo(void)
{
	if (m_nMachineID > 0)
	{
		// Check if the machinenumber set in User file
		// is also in Machineregistry; 061114 p�d
		if (isMachineNumOK())
		{
			m_wndEdit2.setInt(m_nMachineID);
			m_wndEdit5.SetWindowText( m_sContractor );
		}
	}
	else
	{
		m_wndLbl6.SetWindowText(m_sErrMsg);
		m_wndLbl8.SetWindowText(_T("*"));
	}
}

void CComfirmationDlg::setTraktInfo(void)
{
	CString sTmp;
	if (!m_sTrakt.IsEmpty())
	{
		sTmp.Format(_T("%s"),m_sTrakt.Trim());
		m_wndEdit6.SetWindowText(sTmp);
	}
	else
	{
		m_wndLbl6.SetWindowText(m_sErrMsg);
		m_wndLbl9.SetWindowText(_T("*"));
	}

	// Add Type(s) into ComboBox
	int	nIndex = -1;
	m_wndCBox1.ResetContent();
	for (int i = 0;i < sizeof(INVENTORY_TYPE_ARRAY)/sizeof(INVENTORY_TYPE_ARRAY[0]);i++)
	{
		m_wndCBox1.AddString(INVENTORY_TYPE_ARRAY[i].sInvTypeName);
		// Try to find out if the type set in file matches
		// a type in TYPE_ABREV_ARRAY; 061006 p�d
		if (INVENTORY_TYPE_ARRAY[i].sAbbrevInvTypeName == m_sType.Trim())
			nIndex = i;
	}
	// Try to set selection, if there's 
	m_wndCBox1.SetCurSel(nIndex);
	if (nIndex == -1)
	{
		m_wndLbl6.SetWindowText(m_sErrMsg);
		m_wndLbl13.SetWindowText(_T("*"));
	}

	nIndex = -1;
	m_wndCBox2.ResetContent();
	for (int i = 0;i < sizeof(ORIGIN_ARRAY)/sizeof(ORIGIN_ARRAY[0]);i++)
	{
		m_wndCBox2.AddString(ORIGIN_ARRAY[i].sOrigin);
		// Try to find out if the origin set in file matches
		// a type in ORIGIN_NUM_ARRAY; 061006 p�d
		if (ORIGIN_ARRAY[i].nOriginNum == m_nOrigin)
			nIndex = i;
	}
	// Try to set selection, if there's 
	m_wndCBox2.SetCurSel(nIndex);
	if (nIndex == -1)
	{
		m_wndLbl6.SetWindowText(m_sErrMsg);
		m_wndLbl14.SetWindowText(_T("*"));
	}

	// Add rest of information; 061005 p�d
	m_wndEdit3.setInt(m_nNumOfCompart);
	m_wndEdit4.setInt(m_nNumOfPlots);
	m_wndEdit7.setFloat(m_fAreal,1);

	// If there's no Trakts, disable the Button for adding
	// a Trakt from a list (CSelectRegionDistDlg); 061031 p�d
	m_wndListTraktBtn.EnableWindow( m_vecTraktData.size() > 0);

}

int CComfirmationDlg::getMachineID_setByUser(void)
{
	return m_wndEdit2.getInt();
}

CString CComfirmationDlg::getTraktID_setByUser(void)
{
	return m_wndEdit6.getText();
}

CString CComfirmationDlg::getContractor_setByUser(void)
{
	return m_wndEdit5.getText();
}

CString CComfirmationDlg::getType_setByUser(void)
{
	int nIndex = m_wndCBox1.GetCurSel();
	if (nIndex != CB_ERR)
		return INVENTORY_TYPE_ARRAY[nIndex].sAbbrevInvTypeName;
	else
		return _T("");
}

CString CComfirmationDlg::getTypeName_setByUser(void)
{
	int nIndex = m_wndCBox1.GetCurSel();
	if (nIndex != CB_ERR)
		return INVENTORY_TYPE_ARRAY[nIndex].sInvTypeName;
	else
		return _T("");
}

int CComfirmationDlg::getOrigin_setByUser(void)
{
	int nIndex = m_wndCBox2.GetCurSel();
	if (nIndex != CB_ERR)
		return ORIGIN_ARRAY[nIndex].nOriginNum;
	else
		return 0;
}

CString CComfirmationDlg::getOriginName_setByUser(void)
{
	int nIndex = m_wndCBox2.GetCurSel();
	if (nIndex != CB_ERR)
		return ORIGIN_ARRAY[nIndex].sOrigin;
	else
		return _T("");
}


void CComfirmationDlg::OnBnClickedOk()
{
	BOOL bIsOK = TRUE;
	// Set fields to define the PRIMARY KEY in Machine table; 061005 p�d
	m_recMachineData.m_nRegionID		= CComfirmationDlg::m_nRegionID;
	m_recMachineData.m_nDistrictID		= CComfirmationDlg::m_nDistrictID;
	m_recMachineData.m_nMachineID		= getMachineID_setByUser();
	m_recMachineData.m_sContractorName	= getContractor_setByUser();


	// Set fields to define the PRIMARY KEY trakt table; 061005 p�d
	m_recTraktData.m_sTraktNum		= getTraktID_setByUser();
	m_recTraktData.m_sType				= getType_setByUser();
	m_recTraktData.m_nOrigin			= getOrigin_setByUser();
	m_recTraktData.m_sOriginName	= getOriginName_setByUser();
	m_recTraktData.m_sTypeName		= getTypeName_setByUser();
	m_recTraktData.m_fTraktAreal	= m_wndEdit7.getFloat();	// Added 061018 p�d (Areal)
	m_recTraktData.m_nMachineID		= m_recMachineData.m_nMachineID;
	m_recTraktData.m_nRegionID		= m_recMachineData.m_nRegionID;
	m_recTraktData.m_nDistrictID	= m_recMachineData.m_nDistrictID;
  

	// Check that data's been entered for Region,District,Machine and Trakt; 061013 p�d
	if (m_recTraktData.m_nRegionID == 0 ||
		  m_recTraktData.m_nDistrictID == 0 ||
			m_recTraktData.m_nMachineID == 0 ||
			m_recTraktData.m_sTraktNum.IsEmpty())
	{
		::MessageBox(0,m_sErrCap,m_sErrMsg,MB_OK | MB_ICONASTERISK);
		bIsOK = FALSE; 
	}

	if (bIsOK)
		CDialog::OnOK();
}

// Show a list of Regions and Districts, to select from; 061005 p�d
void CComfirmationDlg::OnBnClickedButton1()
{
	DISTRICT_DATA recDistrict;
	CSelectRegionDistDlg *dlg = new CSelectRegionDistDlg(WTL_REGION_DISTRICT);

	if (dlg->DoModal())
	{
		dlg->getRegionAndDistrict(recDistrict);
		CComfirmationDlg::m_nRegionID = recDistrict.m_nRegionID;
		CComfirmationDlg::m_nDistrictID = recDistrict.m_nDistrictID;
		setRegionAndDistrictInfo();
	}

	delete dlg;
}

void CComfirmationDlg::OnBnClickedButton2()
{
	MACHINE_REG_DATA recMachineReg;
	CSelectRegionDistDlg *dlg = new CSelectRegionDistDlg(WTL_MACHINE_REG);

	if (dlg->DoModal())
	{
		dlg->getMachineReg(recMachineReg);
		m_nMachineID = recMachineReg.m_nMachineRegID;
		m_sContractor = recMachineReg.m_sContractorName;
		setMachineInfo();
	}

	delete dlg;
}

void CComfirmationDlg::OnBnClickedButton3()
{
	TRAKT_DATA recTrakt;
	CSelectRegionDistDlg *dlg = new CSelectRegionDistDlg(WTL_TRAKT);

	if (dlg->DoModal())
	{
		dlg->getTrakt(recTrakt);
		CComfirmationDlg::m_nRegionID = recTrakt.m_nRegionID;
		CComfirmationDlg::m_nDistrictID = recTrakt.m_nDistrictID;
		m_nMachineID = recTrakt.m_nMachineID;
		m_sTrakt = recTrakt.m_sTraktNum;
		m_sType = recTrakt.m_sType;
		m_nOrigin = recTrakt.m_nOrigin;
		m_sOriginName = recTrakt.m_sOriginName;
		setTraktInfo();
	}

	delete dlg;
}

