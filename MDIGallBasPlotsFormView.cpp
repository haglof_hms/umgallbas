// MDIGallBasPlotsFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIGallBasPlotsFormView.h"

#include "ResLangFileReader.h"

#include "UMGallBasGenerics.h"

// CMDIGallBasPlotsFormView

IMPLEMENT_DYNCREATE(CMDIGallBasPlotsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIGallBasPlotsFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CMDIGallBasPlotsFormView::CMDIGallBasPlotsFormView()
	: CXTResizeFormView(CMDIGallBasPlotsFormView::IDD)
{
}

CMDIGallBasPlotsFormView::~CMDIGallBasPlotsFormView()
{
}

BOOL CMDIGallBasPlotsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIGallBasPlotsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL4_1, m_wndLbl4_1);
	DDX_Control(pDX, IDC_LBL4_2, m_wndLbl4_2);
	DDX_Control(pDX, IDC_LBL4_3, m_wndLbl4_3);
	DDX_Control(pDX, IDC_LBL4_4, m_wndLbl4_4);
	DDX_Control(pDX, IDC_LBL4_5, m_wndLbl4_5);
	DDX_Control(pDX, IDC_LBL4_7, m_wndLbl4_7);
	DDX_Control(pDX, IDC_LBL4_8, m_wndLbl4_8);
	DDX_Control(pDX, IDC_LBL4_9, m_wndLbl4_9);
	DDX_Control(pDX, IDC_LBL4_10, m_wndLbl4_10);
	DDX_Control(pDX, IDC_LBL4_11, m_wndLbl4_11);
	DDX_Control(pDX, IDC_LBL4_12, m_wndLbl4_12);
	DDX_Control(pDX, IDC_LBL4_13, m_wndLbl4_13);
	DDX_Control(pDX, IDC_LBL4_14, m_wndLbl4_14);
	DDX_Control(pDX, IDC_LBL4_15, m_wndLbl4_15);
	DDX_Control(pDX, IDC_LBL4_16, m_wndLbl4_16);
	DDX_Control(pDX, IDC_LBL4_17, m_wndLbl4_17);
	DDX_Control(pDX, IDC_LBL4_18, m_wndLbl4_18);
	DDX_Control(pDX, IDC_LBL4_19, m_wndLbl4_19);
	DDX_Control(pDX, IDC_LBL4_20, m_wndLbl4_20);
	DDX_Control(pDX, IDC_LBL4_21, m_wndLbl4_21);
	DDX_Control(pDX, IDC_LBL4_22, m_wndLbl4_22);
	DDX_Control(pDX, IDC_LBL4_23, m_wndLbl4_23);
	DDX_Control(pDX, IDC_LBL4_24, m_wndLbl4_24);
	DDX_Control(pDX, IDC_LBL4_25, m_wndLbl4_25);
	DDX_Control(pDX, IDC_LBL4_26, m_wndLbl4_26);
	DDX_Control(pDX, IDC_LBL4_27, m_wndLbl4_27);
	DDX_Control(pDX, IDC_LBL4_28, m_wndLbl4_28);
	DDX_Control(pDX, IDC_LBL4_29, m_wndLbl4_29);
	DDX_Control(pDX, IDC_LBL4_30, m_wndLbl4_30);
	DDX_Control(pDX, IDC_LBL4_31, m_wndLbl4_31);
	DDX_Control(pDX, IDC_LBL4_32, m_wndLbl4_32);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit8);
	DDX_Control(pDX, IDC_EDIT9, m_wndEdit9);
	DDX_Control(pDX, IDC_EDIT10, m_wndEdit10);
	DDX_Control(pDX, IDC_EDIT11, m_wndEdit11);
	DDX_Control(pDX, IDC_EDIT12, m_wndEdit12);
	DDX_Control(pDX, IDC_EDIT13, m_wndEdit13);
	DDX_Control(pDX, IDC_EDIT14, m_wndEdit14);
	DDX_Control(pDX, IDC_EDIT15, m_wndEdit15);
	DDX_Control(pDX, IDC_EDIT16, m_wndEdit16);
	DDX_Control(pDX, IDC_EDIT17, m_wndEdit17);
	DDX_Control(pDX, IDC_EDIT18, m_wndEdit18);
	DDX_Control(pDX, IDC_EDIT19, m_wndEdit19);
	DDX_Control(pDX, IDC_EDIT20, m_wndEdit20);
	DDX_Control(pDX, IDC_EDIT21, m_wndEdit21);
	DDX_Control(pDX, IDC_EDIT22, m_wndEdit22);
	DDX_Control(pDX, IDC_EDIT23, m_wndEdit23);
	DDX_Control(pDX, IDC_EDIT24, m_wndEdit24);
	DDX_Control(pDX, IDC_EDIT25, m_wndEdit25);
	DDX_Control(pDX, IDC_EDIT26, m_wndEdit26);
	DDX_Control(pDX, IDC_EDIT27, m_wndEdit27);
	DDX_Control(pDX, IDC_EDIT28, m_wndEdit28);
	DDX_Control(pDX, IDC_EDIT29, m_wndEdit29);
	DDX_Control(pDX, IDC_EDIT30, m_wndEdit30);
	DDX_Control(pDX, IDC_EDIT31, m_wndEdit31);
	DDX_Control(pDX, IDC_EDIT32, m_wndEdit32);

	DDX_Control(pDX, IDC_GRP1, m_wndIDGroup);
	DDX_Control(pDX, IDC_GRP_ROADS, m_wndRoadGroup);
	DDX_Control(pDX, IDC_GRP_GY, m_wndGYGroup);
	DDX_Control(pDX, IDC_GRP_VOLUME, m_wndVolGroup);
	DDX_Control(pDX, IDC_GRP_PROP, m_wndPropGroup);
	DDX_Control(pDX, IDC_GRP_REST, m_wndRestGroup);
	//}}AFX_DATA_MAP

}

void CMDIGallBasPlotsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));
	m_wndEdit1.SetEnabledColor(BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit1.SetReadOnly();

	m_wndEdit2.SetEnabledColor(BLACK, WHITE );
	m_wndEdit2.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit2.SetReadOnly();
	
	m_wndEdit3.SetEnabledColor(BLACK, WHITE );
	m_wndEdit3.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit3.SetReadOnly();
	
	m_wndEdit5.SetEnabledColor(BLACK, WHITE );
	m_wndEdit5.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit5.SetReadOnly();
	
	m_wndEdit6.SetEnabledColor(BLACK, WHITE );
	m_wndEdit6.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit6.SetReadOnly();
	
	m_wndEdit7.SetEnabledColor(BLACK, WHITE );
	m_wndEdit7.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit7.SetReadOnly();
	
	m_wndEdit8.SetEnabledColor(BLACK, WHITE );
	m_wndEdit8.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit8.SetReadOnly();

	m_wndEdit9.SetEnabledColor(BLACK, WHITE );
	m_wndEdit9.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit9.SetReadOnly();

	m_wndEdit10.SetEnabledColor(BLACK, WHITE );
	m_wndEdit10.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit10.SetReadOnly();

	m_wndEdit11.SetEnabledColor(BLACK, WHITE );
	m_wndEdit11.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit11.SetReadOnly();

	m_wndEdit12.SetEnabledColor(BLACK, WHITE );
	m_wndEdit12.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit12.SetReadOnly();

	m_wndEdit13.SetEnabledColor(BLACK, WHITE );
	m_wndEdit13.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit13.SetReadOnly();

	m_wndEdit14.SetEnabledColor(BLACK, WHITE );
	m_wndEdit14.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit14.SetReadOnly();

	m_wndEdit15.SetEnabledColor(BLACK, WHITE );
	m_wndEdit15.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit15.SetReadOnly();

	m_wndEdit16.SetEnabledColor(BLACK, WHITE );
	m_wndEdit16.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit16.SetReadOnly();

	m_wndEdit17.SetEnabledColor(BLACK, WHITE );
	m_wndEdit17.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit17.SetReadOnly();

	m_wndEdit18.SetEnabledColor(BLACK, WHITE );
	m_wndEdit18.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit18.SetReadOnly();

	m_wndEdit19.SetEnabledColor(BLACK, WHITE );
	m_wndEdit19.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit19.SetReadOnly();

	m_wndEdit20.SetEnabledColor(BLACK, WHITE );
	m_wndEdit20.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit20.SetReadOnly();

	m_wndEdit21.SetEnabledColor(BLACK, WHITE );
	m_wndEdit21.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit21.SetReadOnly();

	m_wndEdit22.SetEnabledColor(BLACK, WHITE );
	m_wndEdit22.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit22.SetReadOnly();

	m_wndEdit23.SetEnabledColor(BLACK, WHITE );
	m_wndEdit23.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit23.SetReadOnly();

	m_wndEdit24.SetEnabledColor(BLACK, WHITE );
	m_wndEdit24.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit24.SetReadOnly();

	m_wndEdit25.SetEnabledColor(BLACK, WHITE );
	m_wndEdit25.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit25.SetReadOnly();

	m_wndEdit26.SetEnabledColor(BLACK, WHITE );
	m_wndEdit26.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit26.SetReadOnly();

	m_wndEdit27.SetEnabledColor(BLACK, WHITE );
	m_wndEdit27.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit27.SetReadOnly();

	m_wndEdit28.SetEnabledColor(BLACK, WHITE );
	m_wndEdit28.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit28.SetReadOnly();

	m_wndEdit29.SetEnabledColor(BLACK, WHITE );
	m_wndEdit29.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit29.SetReadOnly();

	m_wndEdit30.SetEnabledColor(BLACK, WHITE );
	m_wndEdit30.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit30.SetReadOnly();

	m_wndEdit31.SetEnabledColor(BLACK, WHITE );
	m_wndEdit31.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit31.SetReadOnly();

	m_wndEdit32.SetEnabledColor(BLACK, WHITE );
	m_wndEdit32.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit32.SetReadOnly();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	// Get data from database; 061012 p�d
	getDistrictFromDB();

	// Get data from database; 061012 p�d
	getMachineFromDB();

	// Get data from database; 061012 p�d
	getTraktFromDB();

	// Get data from database; 061012 p�d
	getCompartFromDB();
	
	// Get data from database; 061012 p�d
	getPlotsFromDB();

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	setLanguage();
	
	m_nDBIndex = 0;
	populateData(m_nDBIndex);
}

BOOL CMDIGallBasPlotsFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDIGallBasPlotsFormView::OnSetFocus(CWnd*)
{
	if (!m_vecPlotsData.empty())
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecPlotsData.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}
}

// CMDIGallBasPlotsFormView diagnostics

#ifdef _DEBUG
void CMDIGallBasPlotsFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIGallBasPlotsFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CMDIGallBasPlotsFormView message handlers

void CMDIGallBasPlotsFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndLbl4_1.SetWindowText(xml->str(IDS_STRING204));
			m_wndLbl4_2.SetWindowText(xml->str(IDS_STRING107));
			m_wndLbl4_3.SetWindowText(xml->str(IDS_STRING108));
			m_wndLbl4_4.SetWindowText(xml->str(IDS_STRING247));
			CString tmp;
			tmp.Format(_T("%s/%s"),xml->str(IDS_STRING252),xml->str(IDS_STRING253));
			m_wndLbl4_5.SetWindowText(tmp);

			// Roads "Stickv�gar"
			m_wndRoadGroup.SetWindowText(xml->str(IDS_STRING214));
			m_wndLbl4_7.SetWindowText(xml->str(IDS_STRING216));
			m_wndLbl4_31.SetWindowText(xml->str(IDS_STRING217));
			m_wndLbl4_8.SetWindowText(xml->str(IDS_STRING219));
			m_wndLbl4_32.SetWindowText(xml->str(IDS_STRING220));
			m_wndLbl4_9.SetWindowText(xml->str(IDS_STRING221));

			// Baselarea "Grundyta"
			m_wndGYGroup.SetWindowText(xml->str(IDS_STRING222));
			m_wndLbl4_10.SetWindowText(xml->str(IDS_STRING223));
			m_wndLbl4_11.SetWindowText(xml->str(IDS_STRING224));
			m_wndLbl4_12.SetWindowText(xml->str(IDS_STRING225));
			m_wndLbl4_13.SetWindowText(xml->str(IDS_STRING226));
	
			// Volumes
			m_wndVolGroup.SetWindowText(xml->str(IDS_STRING230));
			m_wndLbl4_14.SetWindowText(xml->str(IDS_STRING231));
			m_wndLbl4_15.SetWindowText(xml->str(IDS_STRING232));
			m_wndLbl4_16.SetWindowText(xml->str(IDS_STRING233));
			m_wndLbl4_17.SetWindowText(xml->str(IDS_STRING234));
			m_wndLbl4_18.SetWindowText(xml->str(IDS_STRING235));
			
			// Prop. species "Tr�dslagsf�rdelning"
			m_wndPropGroup.SetWindowText(xml->str(IDS_STRING236));
			m_wndLbl4_19.SetWindowText(xml->str(IDS_STRING237));
			m_wndLbl4_20.SetWindowText(xml->str(IDS_STRING238));
			m_wndLbl4_21.SetWindowText(xml->str(IDS_STRING239));
			m_wndLbl4_22.SetWindowText(xml->str(IDS_STRING240));

			// Remaining data
			m_wndRestGroup.SetWindowText(xml->str(IDS_STRING246));
			m_wndLbl4_23.SetWindowText(xml->str(IDS_STRING227));
			m_wndLbl4_24.SetWindowText(xml->str(IDS_STRING228));
			m_wndLbl4_25.SetWindowText(xml->str(IDS_STRING229));
			m_wndLbl4_26.SetWindowText(xml->str(IDS_STRING241));
			m_wndLbl4_27.SetWindowText(xml->str(IDS_STRING242));
			m_wndLbl4_28.SetWindowText(xml->str(IDS_STRING243));
			m_wndLbl4_29.SetWindowText(xml->str(IDS_STRING244));
			m_wndLbl4_30.SetWindowText(xml->str(IDS_STRING245));
		}
		delete xml;
	}

}
void CMDIGallBasPlotsFormView::populateData(UINT idx)
{
	DISTRICT_DATA dataDist;
	PLOT_DATA data;
	if (!m_vecPlotsData.empty() && 
		  idx >= 0 && 
			idx < m_vecPlotsData.size())
	{
		data = m_vecPlotsData[idx];

		m_wndEdit1.SetWindowText(getRegionAndDistrictData(data.m_nRegionID,data.m_nDistrictID,dataDist));
		m_wndEdit5.setInt(data.m_nMachineID);
		m_wndEdit2.SetWindowText(getMachineEntr(data.m_nRegionID,data.m_nDistrictID,data.m_nMachineID));
		m_wndEdit3.SetWindowText(getTraktInfo(data.m_nRegionID,data.m_nDistrictID,data.m_nMachineID,data.m_sTraktNum.Trim()));
		CString tmp;
		tmp.Format(_T("%d / %d"),data.m_lCompartID,data.m_nPlotID);
		m_wndEdit6.SetWindowText(tmp);
		//  Road data
		m_wndEdit7.setFloat(data.m_recData.getRWidth1(),1);
		m_wndEdit31.setFloat(data.m_recData.getRWidth2(),1);
		m_wndEdit8.setFloat(data.m_recData.getRDist1(),1);
		m_wndEdit32.setFloat(data.m_recData.getRDist2(),1);
		m_wndEdit9.setFloat(data.m_recData.getRArea(),1);
		// Basel area
		m_wndEdit10.setFloat(data.m_recData.getGYBefore(),1);
		m_wndEdit11.setFloat(data.m_recData.getGYAfter(),1);
		m_wndEdit12.setFloat(data.m_recData.getGYWDraw(),1);
		m_wndEdit13.setFloat(data.m_recData.getGYQuota(),1);
		// Volumes
		m_wndEdit14.setFloat(data.m_recData.getVolPine(),1);
		m_wndEdit15.setFloat(data.m_recData.getVolSpruce(),1);
		m_wndEdit16.setFloat(data.m_recData.getVolBirch(),1);
		m_wndEdit17.setFloat(data.m_recData.getVolCont(),1);
		m_wndEdit18.setFloat(data.m_recData.getVolTotal(),1);
		// Specie diviation
		m_wndEdit19.setFloat(data.m_recData.getPropPine(),1);
		m_wndEdit20.setFloat(data.m_recData.getPropSpruce(),1);
		m_wndEdit21.setFloat(data.m_recData.getPropBirch(),1);
		m_wndEdit22.setFloat(data.m_recData.getPropCont(),1);
		// Remaining data
		m_wndEdit23.setFloat(data.m_recData.getStems(),0);
		m_wndEdit24.setFloat(data.m_recData.getAvgDiam(),1);
		m_wndEdit25.setFloat(data.m_recData.getH25(),1);
		m_wndEdit26.setFloat(data.m_recData.getDamaged(),1);
		m_wndEdit27.setFloat(data.m_recData.getTracks(),1);
		m_wndEdit28.setFloat(data.m_recData.getHighStumps(),1);
		m_wndEdit29.setFloat(data.m_recData.getStumpTreat(),1);
		m_wndEdit30.setFloat(data.m_recData.getTwigs(),1);

	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 061010 p�d
LRESULT CMDIGallBasPlotsFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
			addPlot();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			deletePlot();
			resetPlots((int)lParam);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			resetPlots((int)lParam);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_PREV :
		{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;

				if (m_nDBIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_NEXT :
		{
				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecPlotsData.size() - 1))
					m_nDBIndex = (UINT)m_vecPlotsData.size() - 1;
				
				if (m_nDBIndex == (UINT)m_vecPlotsData.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_END :
		{
				m_nDBIndex = (UINT)m_vecPlotsData.size()-1;

				setNavigationButtons(TRUE,FALSE);
			
				populateData(m_nDBIndex);
				break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

// Create a string containing Region and District in the same form as displayed in
// dialog for "Bekr�fta inl�sning"; 061011 p�d
CString CMDIGallBasPlotsFormView::getRegionAndDistrictData(int region,int dist,DISTRICT_DATA &rec)
{
	CString sRetStr;
	if (m_vecDistrictData.empty())
	{
		return _T("");
	}

	for (UINT i = 0;i < m_vecDistrictData.size();i++)
	{
		DISTRICT_DATA	data = m_vecDistrictData[i];
		if (data.m_nRegionID == region && data.m_nDistrictID == dist)
		{
			sRetStr.Format(_T("(%d) %s  -  (%d) %s"),data.m_nRegionID,
																	data.m_sRegionName,
																	data.m_nDistrictID,
																	data.m_sDistrictName);
			rec = data;
			break;
		}
	}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	return sRetStr;
}

void CMDIGallBasPlotsFormView::getDistrictFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getDistricts(m_vecDistrictData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasPlotsFormView::getMachineFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getMachines(m_vecMachineData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasPlotsFormView::getTraktFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTrakts(m_vecTraktData);
			delete pDB;
		}	// if (pDB != NULL)
	}
}

void CMDIGallBasPlotsFormView::getCompartFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getCompart(m_vecCompartData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasPlotsFormView::getPlotsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getPlot(m_vecPlotsData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

// Create a string containing Region and District in the same form as displayed in
// dialog for "Bekr�fta inl�sning"; 061011 p�d
CString CMDIGallBasPlotsFormView::getMachineEntr(int region,int dist,int machine)
{
	CString sRetStr;
	MACHINE_DATA data;
	if (m_vecMachineData.empty())
	{
		return _T("");
	}

	for (UINT i = 0;i < m_vecMachineData.size();i++)
	{
		data = m_vecMachineData[i];
		if (data.m_nRegionID == region && data.m_nDistrictID == dist && data.m_nMachineID == machine)
		{
			sRetStr = data.m_sContractorName;
			break;
		}
	}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	return sRetStr;
}

CString CMDIGallBasPlotsFormView::getTraktInfo(int region,int dist,int machine,LPCTSTR trakt)
{
	TCHAR szRetStr[64];
	TRAKT_DATA data;
	if (m_vecTraktData.empty())
	{
		return _T("");
	}

	for (UINT i = 0;i < m_vecTraktData.size();i++)
	{
		data = m_vecTraktData[i];
		if (data.m_nRegionID == region && 
			  data.m_nDistrictID == dist && 
				data.m_nMachineID == machine &&
				_tcscmp(data.m_sTraktNum.Trim(),trakt) == 0 )
		{
			_stprintf(szRetStr,_T("%s / %s / %s"),data.m_sTraktNum,data.m_sTypeName.Trim(),data.m_sOriginName);
			break;
		}
	}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	return szRetStr;
}

void CMDIGallBasPlotsFormView::addPlot(void)
{
	TRAKT_IDENTIFER	m_structTraktIdentifer;
	if (createFromFile(m_dbConnectionData,m_sLangFN,&m_structTraktIdentifer))
		resetPlots(0);
}


void CMDIGallBasPlotsFormView::deletePlot(void)
{
	DISTRICT_DATA dataDistrict;
	PLOT_DATA dataPlot;
	CString sMsg;
	CString sText1;
	CString sText2;
	CString sText3;
	CString sText4;
	CString sRegion;
	CString sDistrict;
	CString sMachine;
	CString sTrakt;
	CString sCompart;
	CString sPlot;
	CString sCaption;
	CString sOKBtn;
	CString sCancelBtn;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sText1 = xml->str(IDS_STRING5003);
			sText2 = "";
			sText3 = "";
			sText4 = xml->str(IDS_STRING5033);
			sRegion = xml->str(IDS_STRING504);
			sDistrict = xml->str(IDS_STRING505);
			sMachine = xml->str(IDS_STRING506);
			sTrakt = xml->str(IDS_STRING507);
			sCompart = xml->str(IDS_STRING512);
			sPlot = xml->str(IDS_STRING513);

			sCaption = xml->str(IDS_STRING109);
			sOKBtn = xml->str(IDS_STRING110);
			sCancelBtn = xml->str(IDS_STRING111);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	// Check that there's any data in m_vecTraktData vector; 061010 p�d
	if (m_vecTraktData.size() > 0 && m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{	
			// Get Regions in database; 061002 p�d	
			dataPlot = m_vecPlotsData[m_nDBIndex];
			// Setup a message for user upon deleting machine; 061010 p�d

			getRegionAndDistrictData(dataPlot.m_nRegionID,
																dataPlot.m_nDistrictID,
																dataDistrict);
			sMsg.Format(_T("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%s : <b>%d</b>   (%s)<br>%s : <b>%d</b>  (%s)<br>%s : <b>%d</b><br>%s : <b>%s</b><br>%s : <b>%d</b><br>%s : <b>%d</b><br><hr><br>%s<br>%s<br><br><font size=\"+4\" color=\"#ff0000\"><center>%s</center></font>"),
									sText1,
									sRegion,
									dataDistrict.m_nRegionID,
									dataDistrict.m_sRegionName,
									sDistrict,
									dataDistrict.m_nDistrictID,
									dataDistrict.m_sDistrictName,
									sMachine,
									dataPlot.m_nMachineID,
									sTrakt,
									dataPlot.m_sTraktNum,
									sCompart,
									dataPlot.m_lCompartID,
									sPlot,
									dataPlot.m_nPlotID,
									sText2,
									sText3,
									sText4);

			if (messageDialog(sCaption,sOKBtn,sCancelBtn,sMsg))
			{
				// Delete Machine and ALL underlying 
				//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
				pDB->delPlot(dataPlot);
			}

			delete pDB;
		}	// if (pDB != NULL)
	}	// if (m_vecMachineData.size() > 0)
	resetPlots(0);
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIGallBasPlotsFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

// PUBLIC
void CMDIGallBasPlotsFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons(m_nDBIndex > 0,
												m_nDBIndex < (m_vecPlotsData.size()-1));
}

// Reset and get data from database; 061010 p�d
void CMDIGallBasPlotsFormView::resetPlots(int todo)
{
	// Get data from database; 061012 p�d
	getDistrictFromDB();
	// Get data from database; 061012 p�d
	getMachineFromDB();
	// Get data from database; 061012 p�d
	getTraktFromDB();
	// Get data from database; 061012 p�d
	getCompartFromDB();
	// Get data from database; 061012 p�d
	getPlotsFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecPlotsData.size() > 0)
	{

		// Reset to first item in m_vecMachineData
		m_nDBIndex = 0;
		// Populate data (first item) on User interface
		populateData(m_nDBIndex);
		// Set toolbar on HMSShell
		if (todo >= 0)
		{
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecPlotsData.size()-1));
		}
	}	// if (m_vecMachineData.size() > 0)
	else
	{
		if (todo >= 0)
		{
			setNavigationButtons(m_nDBIndex > 0,
			    								m_nDBIndex < (m_vecPlotsData.size()-1));
			clearAll();
		}
	}
}

void CMDIGallBasPlotsFormView::clearAll(void)
{
	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_wndEdit3.SetWindowText(_T(""));
	m_wndEdit5.SetWindowText(_T(""));
	m_wndEdit6.SetWindowText(_T(""));
	m_wndEdit7.SetWindowText(_T(""));
	m_wndEdit8.SetWindowText(_T(""));
	m_wndEdit9.SetWindowText(_T(""));
	m_wndEdit10.SetWindowText(_T(""));
	m_wndEdit11.SetWindowText(_T(""));
	m_wndEdit12.SetWindowText(_T(""));
	m_wndEdit13.SetWindowText(_T(""));
	m_wndEdit14.SetWindowText(_T(""));
	m_wndEdit15.SetWindowText(_T(""));
	m_wndEdit16.SetWindowText(_T(""));
	m_wndEdit17.SetWindowText(_T(""));
	m_wndEdit18.SetWindowText(_T(""));
	m_wndEdit19.SetWindowText(_T(""));
	m_wndEdit20.SetWindowText(_T(""));
	m_wndEdit21.SetWindowText(_T(""));
	m_wndEdit22.SetWindowText(_T(""));
	m_wndEdit23.SetWindowText(_T(""));
	m_wndEdit24.SetWindowText(_T(""));
	m_wndEdit25.SetWindowText(_T(""));
	m_wndEdit26.SetWindowText(_T(""));
	m_wndEdit27.SetWindowText(_T(""));
	m_wndEdit28.SetWindowText(_T(""));
	m_wndEdit29.SetWindowText(_T(""));
	m_wndEdit30.SetWindowText(_T(""));
	m_wndEdit31.SetWindowText(_T(""));
	m_wndEdit32.SetWindowText(_T(""));
}

