// CMDIGallBasDistrictFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIGallBasDistrictFormView.h"

#include "ResLangFileReader.h"

#include "UMGallBasGenerics.h"

// CMDIGallBasDistrictFormView

IMPLEMENT_DYNCREATE(CMDIGallBasDistrictFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIGallBasDistrictFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CMDIGallBasDistrictFormView::CMDIGallBasDistrictFormView()
	: CXTResizeFormView(CMDIGallBasDistrictFormView::IDD)
{
}

CMDIGallBasDistrictFormView::~CMDIGallBasDistrictFormView()
{
}

void CMDIGallBasDistrictFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL5, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL6, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL7, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL8, m_wndLbl5);

	DDX_Control(pDX, IDC_EDIT3, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT6, m_wndEdit4);

	DDX_Control(pDX, IDC_GROUP, m_wndGroup);
	//}}AFX_DATA_MAP
}

BOOL CMDIGallBasDistrictFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIGallBasDistrictFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_wndEdit1.SetEnabledColor(BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit1.SetReadOnly( TRUE );

	m_wndEdit2.SetEnabledColor(BLACK, WHITE );
	m_wndEdit2.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit2.SetReadOnly( TRUE );

	m_wndEdit3.SetEnabledColor(BLACK, WHITE );
	m_wndEdit3.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit3.SetReadOnly( TRUE );

	m_wndEdit4.SetEnabledColor(BLACK, WHITE );
	m_wndEdit4.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit4.SetReadOnly( TRUE );

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getDistricts(m_vecDistrictData);
			delete pDB;
		}	// if (pDB != NULL)
		m_nDBIndex = 0;
		populateData(m_nDBIndex);
	}	// 	if (m_bConnected)
	setLanguage();
	
}

void CMDIGallBasDistrictFormView::OnSetFocus(CWnd*)
{
	setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecDistrictData.size()-1));
}

BOOL CMDIGallBasDistrictFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


// CMDIGallBasDistrictFormView diagnostics

#ifdef _DEBUG
void CMDIGallBasDistrictFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIGallBasDistrictFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CMDIGallBasDistrictFormView message handlers

void CMDIGallBasDistrictFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING101) + _T(" :"));
			m_wndLbl3.SetWindowText(xml->str(IDS_STRING102) + _T(" :"));
			m_wndLbl4.SetWindowText(xml->str(IDS_STRING104) + _T(" :"));
			m_wndLbl5.SetWindowText(xml->str(IDS_STRING105) + _T(" :"));
		}
		delete xml;
	}

}

void CMDIGallBasDistrictFormView::populateData(UINT idx)
{
	DISTRICT_DATA data;
	if (!m_vecDistrictData.empty() && 
		  idx >= 0 && 
			idx < m_vecDistrictData.size())
	{
		data = m_vecDistrictData[idx];

		m_wndEdit1.setInt(data.m_nRegionID);
		m_wndEdit2.SetWindowText(data.m_sRegionName);
		m_wndEdit3.setInt(data.m_nDistrictID);
		m_wndEdit4.SetWindowText(data.m_sDistrictName);
	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CMDIGallBasDistrictFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_PREV :
		{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;

				if (m_nDBIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_NEXT :
		{
				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecDistrictData.size() - 1))
					m_nDBIndex = (UINT)m_vecDistrictData.size() - 1;
				
				if (m_nDBIndex == (UINT)m_vecDistrictData.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_END :
		{
				m_nDBIndex = (UINT)m_vecDistrictData.size()-1;

				setNavigationButtons(TRUE,FALSE);
			
				populateData(m_nDBIndex);
				break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIGallBasDistrictFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

// PUBLIC
void CMDIGallBasDistrictFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons(m_nDBIndex > 0,
											 m_nDBIndex < (m_vecDistrictData.size()-1));
}
