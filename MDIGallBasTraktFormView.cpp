// MDIGallBasTraktFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIGallBasTraktFormView.h"

#include "ResLangFileReader.h"

#include "SelectRegionDistDlg.h"

#include "UMGallBasGenerics.h"
#include ".\mdigallbastraktformview.h"


// CMDIGallBasTraktFormView

IMPLEMENT_DYNCREATE(CMDIGallBasTraktFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIGallBasTraktFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_BN_CLICKED(IDC_BUTTON2, OnBnClickedButton2)
	ON_EN_CHANGE(IDC_EDIT7, OnEnChangeEdit7)
	ON_EN_CHANGE(IDC_EDIT8, OnEnChangeEdit8)
	ON_EN_CHANGE(IDC_EDIT14, OnEnChangeEdit14)
	ON_EN_CHANGE(IDC_EDIT15, OnEnChangeEdit15)
	ON_EN_CHANGE(IDC_EDIT16, OnEnChangeEdit16)
	ON_EN_CHANGE(IDC_EDIT17, OnEnChangeEdit17)
	ON_EN_CHANGE(IDC_EDIT10, OnEnChangeEdit10)
	ON_EN_CHANGE(IDC_EDIT11, OnEnChangeEdit11)
	ON_CBN_SELCHANGE(IDC_COMBO3, OnCbnSelchangeCombo3)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CMDIGallBasTraktFormView::CMDIGallBasTraktFormView()
	: CXTResizeFormView(CMDIGallBasTraktFormView::IDD)
	, m_Text(_T(""))
{
}

CMDIGallBasTraktFormView::~CMDIGallBasTraktFormView()
{
}

BOOL CMDIGallBasTraktFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIGallBasTraktFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL3_1, m_wndLbl3_1);
	DDX_Control(pDX, IDC_LBL3_2, m_wndLbl3_2);
	DDX_Control(pDX, IDC_LBL3_5, m_wndLbl3_5);
	DDX_Control(pDX, IDC_LBL3_6, m_wndLbl3_6);
	DDX_Control(pDX, IDC_LBL3_7, m_wndLbl3_7);
	DDX_Control(pDX, IDC_LBL3_8, m_wndLbl3_8);
	DDX_Control(pDX, IDC_LBL3_9, m_wndLbl3_9);
	DDX_Control(pDX, IDC_LBL3_10, m_wndLbl3_10);
	DDX_Control(pDX, IDC_LBL3_11, m_wndLbl3_11);
	DDX_Control(pDX, IDC_LBL3_12, m_wndLbl3_12);
	DDX_Control(pDX, IDC_LBL3_13, m_wndLbl3_13);
	DDX_Control(pDX, IDC_LBL3_14, m_wndLbl3_14);
	DDX_Control(pDX, IDC_LBL3_15, m_wndLbl3_15);
	DDX_Control(pDX, IDC_LBL3_16, m_wndLbl3_16);
	DDX_Control(pDX, IDC_LBL3_17, m_wndLbl3_17);
	DDX_Control(pDX, IDC_LBL3_18, m_wndLbl3_18);
	DDX_Control(pDX, IDC_LBL3_19, m_wndLbl3_19);
	DDX_Control(pDX, IDC_LBL3_20, m_wndLbl3_20);
	DDX_Control(pDX, IDC_LBL3_21, m_wndLbl3_21);
	DDX_Control(pDX, IDC_LBL3_22, m_wndLbl3_22);
	DDX_Control(pDX, IDC_LBL3_23, m_wndLbl3_23);
	DDX_Control(pDX, IDC_LBL3_24, m_wndLbl3_24);
	DDX_Control(pDX, IDC_LBL3_26, m_wndLbl3_26);
	DDX_Control(pDX, IDC_LBL3_27, m_wndLbl3_27);
	DDX_Control(pDX, IDC_LBL3_28, m_wndLbl3_28);
	DDX_Control(pDX, IDC_LBL3_29, m_wndLbl3_29);
	DDX_Control(pDX, IDC_LBL3_30, m_wndLbl3_30);
	DDX_Control(pDX, IDC_LBL3_31, m_wndLbl3_31);
	DDX_Control(pDX, IDC_LBL3_32, m_wndLbl3_32);
	DDX_Control(pDX, IDC_LBL3_33, m_wndLbl3_33);
	DDX_Control(pDX, IDC_LBL3_34, m_wndLbl3_34);
	DDX_Control(pDX, IDC_LBL3_35, m_wndLbl3_35);
	DDX_Control(pDX, IDC_LBL3_36, m_wndLbl3_36);

	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit8);
	DDX_Control(pDX, IDC_EDIT9, m_wndEdit9);
	DDX_Control(pDX, IDC_EDIT10, m_wndEdit10);
	DDX_Control(pDX, IDC_EDIT11, m_wndEdit11);
	DDX_Control(pDX, IDC_EDIT12, m_wndEdit12);
	DDX_Control(pDX, IDC_EDIT13, m_wndEdit13);
	DDX_Control(pDX, IDC_EDIT14, m_wndEdit14);
	DDX_Control(pDX, IDC_EDIT15, m_wndEdit15);
	DDX_Control(pDX, IDC_EDIT16, m_wndEdit16);
	DDX_Control(pDX, IDC_EDIT17, m_wndEdit17);
	DDX_Control(pDX, IDC_EDIT18, m_wndEdit18);
	DDX_Control(pDX, IDC_EDIT19, m_wndEdit19);
	DDX_Control(pDX, IDC_EDIT20, m_wndEdit20);
	DDX_Control(pDX, IDC_EDIT21, m_wndEdit21);
	DDX_Control(pDX, IDC_EDIT22, m_wndEdit22);
	DDX_Control(pDX, IDC_EDIT23, m_wndEdit23);
	DDX_Control(pDX, IDC_EDIT24, m_wndEdit24);
	DDX_Control(pDX, IDC_EDIT26, m_wndEdit26);
	DDX_Control(pDX, IDC_EDIT27, m_wndEdit27);
	DDX_Control(pDX, IDC_EDIT28, m_wndEdit28);
	DDX_Control(pDX, IDC_EDIT29, m_wndEdit29);
	DDX_Control(pDX, IDC_EDIT30, m_wndEdit30);
	DDX_Control(pDX, IDC_EDIT31, m_wndEdit31);
	DDX_Control(pDX, IDC_EDIT32, m_wndEdit32);
	DDX_Control(pDX, IDC_EDIT33, m_wndEdit33);
	DDX_Control(pDX, IDC_EDIT1, m_wndEdit34);

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_COMBO2, m_wndCBox2);
	DDX_Control(pDX, IDC_COMBO3, m_wndCBox3);
	DDX_Control(pDX, IDC_COMBO4, m_wndCBox4);

	DDX_Control(pDX, IDC_CBOX_DATEPICKER, m_wndDatePicker);

	DDX_Control(pDX, IDC_GRP1, m_wndIDGroup);
	DDX_Control(pDX, IDC_GRP_TRAKT, m_wndTraktGroup);
	DDX_Control(pDX, IDC_GRP_ROADS, m_wndRoadGroup);
	DDX_Control(pDX, IDC_GRP_GY, m_wndGYGroup);
	DDX_Control(pDX, IDC_GRP_VOLUME, m_wndVolGroup);
	DDX_Control(pDX, IDC_GRP_PROP, m_wndPropGroup);
	DDX_Control(pDX, IDC_GRP_REST, m_wndRestGroup);
	DDX_Control(pDX, IDC_GRP_NOTES, m_wndNotesGroup);

	DDX_Control(pDX, IDC_BUTTON2, m_wndListMachineBtn);


	DDX_Control(pDX, IDC_LBL_TRAKT_FORRENSAT, m_wndLbl_Forrensat);
	DDX_Control(pDX, IDC_EDIT_TRAKT_FORRENSAT, m_wndEdit_Forrensat);		

	//Trakt Avslut
	DDX_Control(pDX, IDC_GRP_TRAKT_AVSLUT, m_wndGroup_Trakt_Avslut);

	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_SLUTFORD, m_wndLbl_Trakt_Avslut_Slutford);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_RATTATGARDSTIDP, m_wndLbl_Trakt_Avslut_RattAtgTp);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_FORKLARING, m_wndLbl_Trakt_Avslut_Forklaring);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_MASKINSTORLEK, m_wndLbl_Trakt_Avslut_Maskinstorlek);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_MASKINNUMMER, m_wndLbl_Trakt_Avslut_MaskinNummer);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_MYR, m_wndLbl_Trakt_Avslut_Myr);

	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_HALLMARK ,m_wndLbl_Trakt_Avslut_Hallmark);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_HANSYN ,m_wndLbl_Trakt_Avslut_Hansyn);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_FDKULTURMARK ,m_wndLbl_Trakt_Avslut_Fdkulturmark);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_KULTURMILJO ,m_wndLbl_Trakt_Avslut_Kulturmiljo);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_LOVANDEL ,m_wndLbl_Trakt_Avslut_Lovandel);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_SKYDDSZON ,m_wndLbl_Trakt_Avslut_Skyddszon);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_NVTRAD ,m_wndLbl_Trakt_Avslut_NVtrad);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_FRAMTIDSTRAD ,m_wndLbl_Trakt_Avslut_Framtidstrad);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_HOGSTUBBAR ,m_wndLbl_Trakt_Avslut_Hogstubbar);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_DODATRAD ,m_wndLbl_Trakt_Avslut_Dodatrad);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_MARKSKADOR ,m_wndLbl_Trakt_Avslut_Markskador);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_MARKSKADOR2 ,m_wndLbl_Trakt_Avslut_Markskador2);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_NEDSKRAPNING ,m_wndLbl_Trakt_Avslut_Nedskrapning);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_MINDREKORSKADA ,m_wndLbl_Trakt_Avslut_MindreKorskada);
	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_ALLVARLIGKORSKADA ,m_wndLbl_Trakt_Avslut_AllvarligKorskada);

	DDX_Control(pDX, IDC_LBL_TRAKT_AVSLUT_FRITEXT ,m_wndLbl_Trakt_Avslut_Fritext);

	DDX_Control(pDX, IDC_EDIT_TRAKT_AVSLUT_SLUTFORD, m_wndEdit_Trakt_Avslut_Slutford);					 
	DDX_Control(pDX, IDC_EDIT_TRAKT_AVSLUT_FORKLARING, m_wndEdit_Trakt_Avslut_Forklaring);
	DDX_Control(pDX, IDC_EDIT_TRAKT_AVSLUT_MASKINNUMMER, m_wndEdit_Trakt_Avslut_MaskinNummer);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_MYR, m_wndCBox_Trakt_Avslut_Myr);

	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_RATTATGTP, m_wndCBox_Trakt_Avslut_RattAtgTp);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_MASKINSTORLEK, m_wndCBox_Trakt_Avslut_Maskinstorlek);


	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_HALLMARK ,m_wndCBox_Trakt_Avslut_Hallmark);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_HANSYN ,m_wndCBox_Trakt_Avslut_Hansyn);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_FDKULTURMARK ,m_wndCBox_Trakt_Avslut_Fdkulturmark);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_KULTURMILJO ,m_wndCBox_Trakt_Avslut_Kulturmiljo);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_LOVANDEL ,m_wndCBox_Trakt_Avslut_Lovandel);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_SKYDDSZON ,m_wndCBox_Trakt_Avslut_Skyddszon);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_NVTRAD ,m_wndCBox_Trakt_Avslut_NVtrad);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_FRAMTIDSTRAD ,m_wndCBox_Trakt_Avslut_Framtidstrad);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_HOGSTUBBAR ,m_wndCBox_Trakt_Avslut_Hogstubbar);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_DODATRAD ,m_wndCBox_Trakt_Avslut_Dodatrad);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_MARKSKADOR ,m_wndCBox_Trakt_Avslut_Markskador);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_MARKSKADOR2 ,m_wndCBox_Trakt_Avslut_Markskador2);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_NEDSKRAPNING ,m_wndCBox_Trakt_Avslut_Nedskrapning);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_MINDREKORSKADA ,m_wndCBox_Trakt_Avslut_MindreKorskada);
	DDX_Control(pDX, IDC_CBOX_TRAKT_AVSLUT_ALLVARLIGKORSKADA ,m_wndCBox_Trakt_Avslut_AllvarligKorskada);




	//}}AFX_DATA_MAP
	DDX_CBString(pDX, IDC_CBOX_TRAKT_AVSLUT_RATTATGTP, m_Text);
}

// CMDIGallBasTraktFormView diagnostics

#ifdef _DEBUG
void CMDIGallBasTraktFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIGallBasTraktFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

void CMDIGallBasTraktFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_wndEdit5.SetEnabledColor(BLACK, WHITE );
	m_wndEdit5.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit5.SetReadOnly( TRUE );
	
	m_wndEdit6.SetEnabledColor(BLACK, WHITE );
	m_wndEdit6.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit6.SetReadOnly( TRUE );
	
	m_wndEdit7.SetEnabledColor(BLACK, WHITE );
	m_wndEdit7.SetDisabledColor(BLACK, INFOBK );
	
	m_wndEdit8.SetEnabledColor(BLACK, WHITE );
	m_wndEdit8.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit9.SetEnabledColor(BLACK, WHITE );
	m_wndEdit9.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit9.SetReadOnly( TRUE );

	m_wndEdit10.SetEnabledColor(BLACK, WHITE );
	m_wndEdit10.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit11.SetEnabledColor(BLACK, WHITE );
	m_wndEdit11.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit12.SetEnabledColor(BLACK, WHITE );
	m_wndEdit12.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit12.SetReadOnly( TRUE );

	m_wndEdit13.SetEnabledColor(BLACK, WHITE );
	m_wndEdit13.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit14.SetEnabledColor(BLACK, WHITE );
	m_wndEdit14.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit15.SetEnabledColor(BLACK, WHITE );
	m_wndEdit15.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit16.SetEnabledColor(BLACK, WHITE );
	m_wndEdit16.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit17.SetEnabledColor(BLACK, WHITE );
	m_wndEdit17.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit18.SetEnabledColor(BLACK, WHITE );
	m_wndEdit18.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit18.SetReadOnly( TRUE );

	m_wndEdit19.SetEnabledColor(BLACK, WHITE );
	m_wndEdit19.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit20.SetEnabledColor(BLACK, WHITE );
	m_wndEdit20.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit21.SetEnabledColor(BLACK, WHITE );
	m_wndEdit21.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit22.SetEnabledColor(BLACK, WHITE );
	m_wndEdit22.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit23.SetEnabledColor(BLACK, WHITE );
	m_wndEdit23.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit24.SetEnabledColor(BLACK, WHITE );
	m_wndEdit24.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit26.SetEnabledColor(BLACK, WHITE );
	m_wndEdit26.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit27.SetEnabledColor(BLACK, WHITE );
	m_wndEdit27.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit28.SetEnabledColor(BLACK, WHITE );
	m_wndEdit28.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit29.SetEnabledColor(BLACK, WHITE );
	m_wndEdit29.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit30.SetEnabledColor(BLACK, WHITE );
	m_wndEdit30.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit31.SetEnabledColor(BLACK, WHITE );
	m_wndEdit31.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit32.SetEnabledColor(BLACK, WHITE );
	m_wndEdit32.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit33.SetEnabledColor(BLACK, WHITE );
	m_wndEdit33.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit34.SetEnabledColor(BLACK, WHITE );
	m_wndEdit34.SetDisabledColor(BLACK, INFOBK );

	m_wndEdit_Forrensat.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Forrensat.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Forrensat.SetReadOnly( TRUE );

	m_wndEdit_Trakt_Avslut_Slutford.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Trakt_Avslut_Slutford.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Trakt_Avslut_Slutford.SetReadOnly( TRUE );

	m_wndEdit_Trakt_Avslut_Forklaring.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Trakt_Avslut_Forklaring.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Trakt_Avslut_Forklaring.SetReadOnly( TRUE );
	m_wndEdit_Trakt_Avslut_MaskinNummer.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Trakt_Avslut_MaskinNummer.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Trakt_Avslut_MaskinNummer.SetReadOnly( TRUE );

	m_wndCBox_Trakt_Avslut_RattAtgTp.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_RattAtgTp.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Maskinstorlek.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Maskinstorlek.SetDisabledColor(BLACK,INFOBK );

	m_wndCBox_Trakt_Avslut_Myr.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Myr.SetDisabledColor(BLACK,INFOBK );

	m_wndCBox_Trakt_Avslut_Hallmark.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Hallmark.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Hansyn.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Hansyn.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Fdkulturmark.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Fdkulturmark.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Kulturmiljo.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Kulturmiljo.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Lovandel.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Lovandel.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Skyddszon.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Skyddszon.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_NVtrad.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_NVtrad.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Framtidstrad.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Framtidstrad.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Hogstubbar.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Hogstubbar.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Dodatrad.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Dodatrad.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Markskador.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Markskador.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Markskador2.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Markskador2.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_Nedskrapning.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_Nedskrapning.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_MindreKorskada.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_MindreKorskada.SetDisabledColor(BLACK,INFOBK );
	m_wndCBox_Trakt_Avslut_AllvarligKorskada.SetEnabledColor(BLACK,WHITE );
	m_wndCBox_Trakt_Avslut_AllvarligKorskada.SetDisabledColor(BLACK,INFOBK );


	m_wndCBox1.SetEnabledColor(BLACK,WHITE );
	m_wndCBox1.SetDisabledColor(BLACK,INFOBK );
	
	m_wndCBox2.SetEnabledColor(BLACK,WHITE );
	m_wndCBox2.SetDisabledColor(BLACK,INFOBK );

	m_wndCBox3.SetEnabledColor(BLACK,WHITE );
	m_wndCBox3.SetDisabledColor(BLACK,INFOBK );

	m_wndCBox4.SetEnabledColor(BLACK,WHITE );
	m_wndCBox4.SetDisabledColor(BLACK,INFOBK );

	setAllReadOnly(TRUE,TRUE);

	m_wndDatePicker.setDateInComboBox();

	m_wndListMachineBtn.SetBitmap(CSize(18,14),IDB_BITMAP1);
	m_wndListMachineBtn.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	// Get regions; 061204 p�d
	getRegionsFromDB();
	addRegionsToCBox();
	// Get districts (and region info); 061011 p�d
	getDistrictFromDB();
	// Get machine(s); 061011 p�d
	getMachineFromDB();
	// Get data from database; 061010 p�d
	getTraktsFromDB();
	
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	setTraktAvslutArrays();

	// Setup what the user's done when adding data; 061201 p�d
	// MANUALLY = Adding data by hand
	// FROM_FILE = Adding from a datafile
	m_enumAction = NOTHING;
		
	setLanguage();

	// Set Default values for Type and Origin; 061010 p�d
	setDefaultTypeAndOrigin();

	m_nDBIndex = 0;
	populateData(m_nDBIndex);

	m_bIsDirty = FALSE;

}

BOOL CMDIGallBasTraktFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDIGallBasTraktFormView::OnSetFocus(CWnd*)
{
	if (!m_vecTraktData.empty())
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTraktData.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}
}


BOOL CMDIGallBasTraktFormView::getIsDirty(void)
{
	if (m_bIsDirty)
		return m_bIsDirty;

	if (m_wndEdit5.isDirty() ||
			m_wndEdit6.isDirty() ||
			m_wndEdit31.isDirty() ||
			m_wndEdit32.isDirty() ||
			m_wndEdit33.isDirty() ||
			m_wndEdit7.isDirty() ||
			m_wndEdit8.isDirty() ||
			m_wndEdit9.isDirty() ||
			m_wndEdit10.isDirty() ||
			m_wndEdit11.isDirty() ||
			m_wndEdit12.isDirty() ||
			m_wndEdit13.isDirty() ||
			m_wndEdit14.isDirty() ||
			m_wndEdit15.isDirty() ||
			m_wndEdit16.isDirty() ||
			m_wndEdit17.isDirty() ||
			m_wndEdit18.isDirty() ||
			m_wndEdit19.isDirty() ||
			m_wndEdit20.isDirty() ||
			m_wndEdit21.isDirty() ||
			m_wndEdit22.isDirty() ||
			m_wndEdit23.isDirty() ||
			m_wndEdit24.isDirty() ||
			m_wndEdit26.isDirty() ||
			m_wndEdit27.isDirty() ||
			m_wndEdit28.isDirty() ||
			m_wndEdit29.isDirty() ||
			m_wndEdit30.isDirty() ||
			m_wndEdit34.isDirty() ||
			m_wndEdit_Forrensat.isDirty() ||
			m_wndEdit_Trakt_Avslut_Slutford.isDirty() ||
			m_wndCBox_Trakt_Avslut_RattAtgTp.isDirty() ||
			m_wndEdit_Trakt_Avslut_Forklaring.isDirty() ||
			m_wndCBox_Trakt_Avslut_Maskinstorlek.isDirty() ||
			m_wndEdit_Trakt_Avslut_MaskinNummer.isDirty() ||
			m_wndCBox_Trakt_Avslut_Myr.isDirty() ||
			m_wndCBox_Trakt_Avslut_Hallmark.isDirty() ||
			m_wndCBox_Trakt_Avslut_Hansyn.isDirty() ||
			m_wndCBox_Trakt_Avslut_Fdkulturmark.isDirty() ||
			m_wndCBox_Trakt_Avslut_Kulturmiljo.isDirty() ||
			m_wndCBox_Trakt_Avslut_Lovandel.isDirty() ||
			m_wndCBox_Trakt_Avslut_Skyddszon.isDirty() ||
			m_wndCBox_Trakt_Avslut_NVtrad.isDirty() ||
			m_wndCBox_Trakt_Avslut_Framtidstrad.isDirty() ||
			m_wndCBox_Trakt_Avslut_Hogstubbar.isDirty() ||
			m_wndCBox_Trakt_Avslut_Dodatrad.isDirty() ||
			m_wndCBox_Trakt_Avslut_Markskador.isDirty() ||
			m_wndCBox_Trakt_Avslut_Markskador2.isDirty() ||
			m_wndCBox_Trakt_Avslut_Nedskrapning.isDirty() ||
			m_wndCBox_Trakt_Avslut_MindreKorskada.isDirty() ||
			m_wndCBox_Trakt_Avslut_AllvarligKorskada.isDirty() ||

			m_wndCBox1.isDirty() ||
			m_wndCBox2.isDirty() ||
			m_wndCBox3.isDirty() ||
			m_wndCBox4.isDirty() ) 
	{
		return TRUE;
	}

	return FALSE;
}

void CMDIGallBasTraktFormView::resetIsDirty(void)
{
	m_wndEdit5.resetIsDirty();
	m_wndEdit6.resetIsDirty();
	m_wndEdit31.resetIsDirty();
	m_wndEdit32.resetIsDirty();
	m_wndEdit33.resetIsDirty();
	m_wndEdit7.resetIsDirty();
	m_wndEdit8.resetIsDirty();
	m_wndEdit9.resetIsDirty();
	m_wndEdit10.resetIsDirty();
	m_wndEdit11.resetIsDirty();
	m_wndEdit12.resetIsDirty();
	m_wndEdit13.resetIsDirty();
	m_wndEdit14.resetIsDirty();
	m_wndEdit15.resetIsDirty();
	m_wndEdit16.resetIsDirty();
	m_wndEdit17.resetIsDirty();
	m_wndEdit18.resetIsDirty();
	m_wndEdit19.resetIsDirty();
	m_wndEdit20.resetIsDirty();
	m_wndEdit21.resetIsDirty();
	m_wndEdit22.resetIsDirty();
	m_wndEdit23.resetIsDirty();
	m_wndEdit24.resetIsDirty();
	m_wndEdit26.resetIsDirty();
	m_wndEdit27.resetIsDirty();
	m_wndEdit28.resetIsDirty();
	m_wndEdit29.resetIsDirty();
	m_wndEdit30.resetIsDirty();
	m_wndEdit34.resetIsDirty();
	m_wndEdit_Forrensat.resetIsDirty();
	m_wndEdit_Trakt_Avslut_Slutford.resetIsDirty();
	m_wndCBox_Trakt_Avslut_RattAtgTp.resetIsDirty();
	m_wndEdit_Trakt_Avslut_Forklaring.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Maskinstorlek.resetIsDirty();
	m_wndEdit_Trakt_Avslut_MaskinNummer.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Myr.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Hallmark.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Hansyn.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Fdkulturmark.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Kulturmiljo.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Lovandel.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Skyddszon.resetIsDirty();
	m_wndCBox_Trakt_Avslut_NVtrad.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Framtidstrad.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Hogstubbar.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Dodatrad.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Markskador.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Markskador2.resetIsDirty();
	m_wndCBox_Trakt_Avslut_Nedskrapning.resetIsDirty();
	m_wndCBox_Trakt_Avslut_MindreKorskada.resetIsDirty();
	m_wndCBox_Trakt_Avslut_AllvarligKorskada.resetIsDirty();


	m_wndCBox1.resetIsDirty();
	m_wndCBox2.resetIsDirty();
	m_wndCBox3.resetIsDirty();
	m_wndCBox4.resetIsDirty();

	
}


void CMDIGallBasTraktFormView::setTraktAvslutArrays()
{	
	TRAKT_AVSLUT_DATA data;
	CString s_Data=_T("");
	UINT i=0;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{

			vec_Trakt_Avslut_RattAtgTp.clear();
			vec_Trakt_Avslut_RattAtgTp.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6201)),xml->str(IDS_STRING6200)));
			vec_Trakt_Avslut_RattAtgTp.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6203)),xml->str(IDS_STRING6202)));

			vec_Avslut_Maskinstorlek.clear();
			vec_Avslut_Maskinstorlek.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6211)),xml->str(IDS_STRING6210)));
			vec_Avslut_Maskinstorlek.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6213)),xml->str(IDS_STRING6212)));
			vec_Avslut_Maskinstorlek.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6215)),xml->str(IDS_STRING6214)));


			vec_Trakt_Avslut_Myr.clear();
			vec_Trakt_Avslut_Myr.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6221)),xml->str(IDS_STRING6220)));
			vec_Trakt_Avslut_Myr.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6223)),xml->str(IDS_STRING6222)));
			vec_Trakt_Avslut_Myr.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6225)),xml->str(IDS_STRING6224)));

			vec_Trakt_Avslut_Hallmark.clear();
			vec_Trakt_Avslut_Hallmark.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6231)),xml->str(IDS_STRING6230)));
			vec_Trakt_Avslut_Hallmark.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6233)),xml->str(IDS_STRING6232)));
			vec_Trakt_Avslut_Hallmark.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6235)),xml->str(IDS_STRING6234)));

			vec_Trakt_Avslut_Hansyn.clear();
			vec_Trakt_Avslut_Hansyn.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6241)),xml->str(IDS_STRING6240)));
			vec_Trakt_Avslut_Hansyn.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6243)),xml->str(IDS_STRING6242)));
			vec_Trakt_Avslut_Hansyn.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6245)),xml->str(IDS_STRING6244)));
			vec_Trakt_Avslut_Hansyn.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6247)),xml->str(IDS_STRING6246)));

			vec_Trakt_Avslut_Fdkulturmark.clear();
			vec_Trakt_Avslut_Fdkulturmark.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6251)),xml->str(IDS_STRING6250)));
			vec_Trakt_Avslut_Fdkulturmark.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6253)),xml->str(IDS_STRING6252)));
			vec_Trakt_Avslut_Fdkulturmark.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6255)),xml->str(IDS_STRING6254)));

			vec_Trakt_Avslut_Kulturmiljo.clear();
			vec_Trakt_Avslut_Kulturmiljo.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6261)),xml->str(IDS_STRING6260)));
			vec_Trakt_Avslut_Kulturmiljo.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6263)),xml->str(IDS_STRING6262)));
			vec_Trakt_Avslut_Kulturmiljo.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6265)),xml->str(IDS_STRING6264)));

			vec_Trakt_Avslut_Lovandel.clear();
			vec_Trakt_Avslut_Lovandel.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6271)),xml->str(IDS_STRING6270)));
			vec_Trakt_Avslut_Lovandel.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6273)),xml->str(IDS_STRING6272)));
			vec_Trakt_Avslut_Lovandel.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6275)),xml->str(IDS_STRING6274)));
			vec_Trakt_Avslut_Lovandel.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6277)),xml->str(IDS_STRING6276)));

			vec_Trakt_Avslut_Skyddszon.clear();
			vec_Trakt_Avslut_Skyddszon.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6281)),xml->str(IDS_STRING6280)));
			vec_Trakt_Avslut_Skyddszon.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6283)),xml->str(IDS_STRING6282)));
			vec_Trakt_Avslut_Skyddszon.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6285)),xml->str(IDS_STRING6284)));
			vec_Trakt_Avslut_Skyddszon.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6287)),xml->str(IDS_STRING6286)));

			vec_Trakt_Avslut_NVtrad.clear();
			vec_Trakt_Avslut_NVtrad.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6291)),xml->str(IDS_STRING6290)));
			vec_Trakt_Avslut_NVtrad.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6293)),xml->str(IDS_STRING6292)));

			vec_Trakt_Avslut_Framtidstrad.clear();
			vec_Trakt_Avslut_Framtidstrad.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6301)),xml->str(IDS_STRING6300)));
			vec_Trakt_Avslut_Framtidstrad.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6303)),xml->str(IDS_STRING6302)));
			vec_Trakt_Avslut_Framtidstrad.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6305)),xml->str(IDS_STRING6304)));

			vec_Trakt_Avslut_Hogstubbar.clear();
			vec_Trakt_Avslut_Hogstubbar.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6311)),xml->str(IDS_STRING6310)));
			vec_Trakt_Avslut_Hogstubbar.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6313)),xml->str(IDS_STRING6312)));
			vec_Trakt_Avslut_Hogstubbar.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6315)),xml->str(IDS_STRING6314)));
			vec_Trakt_Avslut_Hogstubbar.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6317)),xml->str(IDS_STRING6316)));

			vec_Trakt_Avslut_Dodatrad.clear();
			vec_Trakt_Avslut_Dodatrad.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6321)),xml->str(IDS_STRING6320)));
			vec_Trakt_Avslut_Dodatrad.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6323)),xml->str(IDS_STRING6322)));
			vec_Trakt_Avslut_Dodatrad.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6325)),xml->str(IDS_STRING6324)));

			vec_Trakt_Avslut_Markskador.clear();
			vec_Trakt_Avslut_Markskador.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6331)),xml->str(IDS_STRING6330)));
			vec_Trakt_Avslut_Markskador.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6333)),xml->str(IDS_STRING6332)));
			vec_Trakt_Avslut_Markskador.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6335)),xml->str(IDS_STRING6334)));

			vec_Trakt_Avslut_Markskador2.clear();
			vec_Trakt_Avslut_Markskador2.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6341)),xml->str(IDS_STRING6340)));
			vec_Trakt_Avslut_Markskador2.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6343)),xml->str(IDS_STRING6342)));
			vec_Trakt_Avslut_Markskador2.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6345)),xml->str(IDS_STRING6344)));
			vec_Trakt_Avslut_Markskador2.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6347)),xml->str(IDS_STRING6346)));
			vec_Trakt_Avslut_Markskador2.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6349)),xml->str(IDS_STRING6348)));
			vec_Trakt_Avslut_Markskador2.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6351)),xml->str(IDS_STRING6350)));
			vec_Trakt_Avslut_Markskador2.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6353)),xml->str(IDS_STRING6352)));
			vec_Trakt_Avslut_Markskador2.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6355)),xml->str(IDS_STRING6354)));

			vec_Trakt_Avslut_Nedskrapning.clear();
			vec_Trakt_Avslut_Nedskrapning.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6361)),xml->str(IDS_STRING6360)));
			vec_Trakt_Avslut_Nedskrapning.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6363)),xml->str(IDS_STRING6362)));

			vec_Trakt_Avslut_MindreKorskada.clear();
			vec_Trakt_Avslut_MindreKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6371)),xml->str(IDS_STRING6370)));
			vec_Trakt_Avslut_MindreKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6373)),xml->str(IDS_STRING6372)));
			vec_Trakt_Avslut_MindreKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6375)),xml->str(IDS_STRING6374)));

			vec_Trakt_Avslut_AllvarligKorskada.clear();
			vec_Trakt_Avslut_AllvarligKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6381)),xml->str(IDS_STRING6380)));
			vec_Trakt_Avslut_AllvarligKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6383)),xml->str(IDS_STRING6382)));
			vec_Trakt_Avslut_AllvarligKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6385)),xml->str(IDS_STRING6384)));
			vec_Trakt_Avslut_AllvarligKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6387)),xml->str(IDS_STRING6386)));
			vec_Trakt_Avslut_AllvarligKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6389)),xml->str(IDS_STRING6388)));
			vec_Trakt_Avslut_AllvarligKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6391)),xml->str(IDS_STRING6390)));
			vec_Trakt_Avslut_AllvarligKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6393)),xml->str(IDS_STRING6392)));
			vec_Trakt_Avslut_AllvarligKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6395)),xml->str(IDS_STRING6394)));
			vec_Trakt_Avslut_AllvarligKorskada.push_back(_trakt_avslut_data(_tstoi(xml->str(IDS_STRING6397)),xml->str(IDS_STRING6396)));
		}
	}

			
	for (i = 0;i < vec_Trakt_Avslut_RattAtgTp.size();i++)
	{
		data = vec_Trakt_Avslut_RattAtgTp[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_RattAtgTp.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_RattAtgTp.SetItemData(i, data.m_nID);		
	}

	
		for (i = 0;i < vec_Avslut_Maskinstorlek.size();i++)
	{
		data = vec_Avslut_Maskinstorlek[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Maskinstorlek.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Maskinstorlek.SetItemData(i, data.m_nID);
	}



	for (i = 0;i < vec_Trakt_Avslut_Myr.size();i++)
	{
		data = vec_Trakt_Avslut_Myr[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Myr.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Myr.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Hallmark.size();i++)
	{
		data = vec_Trakt_Avslut_Hallmark[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Hallmark.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Hallmark.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Hansyn.size();i++)
	{
		data = vec_Trakt_Avslut_Hansyn[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Hansyn.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Hansyn.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Fdkulturmark.size();i++)
	{
		data = vec_Trakt_Avslut_Fdkulturmark[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Fdkulturmark.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Fdkulturmark.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Kulturmiljo.size();i++)
	{
		data = vec_Trakt_Avslut_Kulturmiljo[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Kulturmiljo.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Kulturmiljo.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Lovandel.size();i++)
	{
		data = vec_Trakt_Avslut_Lovandel[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Lovandel.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Lovandel.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Skyddszon.size();i++)
	{
		data = vec_Trakt_Avslut_Skyddszon[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Skyddszon.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Skyddszon.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_NVtrad.size();i++)
	{
		data = vec_Trakt_Avslut_NVtrad[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_NVtrad.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_NVtrad.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Framtidstrad.size();i++)
	{
		data = vec_Trakt_Avslut_Framtidstrad[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Framtidstrad.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Framtidstrad.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Hogstubbar.size();i++)
	{
		data = vec_Trakt_Avslut_Hogstubbar[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Hogstubbar.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Hogstubbar.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Dodatrad.size();i++)
	{
		data = vec_Trakt_Avslut_Dodatrad[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Dodatrad.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Dodatrad.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Markskador.size();i++)
	{
		data = vec_Trakt_Avslut_Markskador[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Markskador.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Markskador.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Markskador2.size();i++)
	{
		data = vec_Trakt_Avslut_Markskador2[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Markskador2.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Markskador2.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_Nedskrapning.size();i++)
	{
		data = vec_Trakt_Avslut_Nedskrapning[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_Nedskrapning.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_Nedskrapning.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_MindreKorskada.size();i++)
	{
		data = vec_Trakt_Avslut_MindreKorskada[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_MindreKorskada.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_MindreKorskada.SetItemData(i, data.m_nID);
	}
	for (i = 0;i < vec_Trakt_Avslut_AllvarligKorskada.size();i++)
	{
		data = vec_Trakt_Avslut_AllvarligKorskada[i];
		s_Data.Format(_T("%d - %s"),data.m_nID,data.m_sName);
		m_wndCBox_Trakt_Avslut_AllvarligKorskada.AddString(s_Data);
		m_wndCBox_Trakt_Avslut_AllvarligKorskada.SetItemData(i, data.m_nID);
	}
	
}

// CMDIGallBasTraktFormView message handlers

void CMDIGallBasTraktFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndLbl3_1.SetWindowText(xml->str(IDS_STRING2040));
			m_wndLbl3_2.SetWindowText(xml->str(IDS_STRING2041));
			m_wndLbl3_5.SetWindowText(xml->str(IDS_STRING107));
			m_wndLbl3_6.SetWindowText(xml->str(IDS_STRING108));
			// Trakt information
			m_wndTraktGroup.SetWindowText(xml->str(IDS_STRING247));
			m_wndLbl3_31.SetWindowText(xml->str(IDS_STRING248));
			m_wndLbl3_32.SetWindowText(xml->str(IDS_STRING249));
			m_wndLbl3_33.SetWindowText(xml->str(IDS_STRING250));
			m_wndLbl3_34.SetWindowText(xml->str(IDS_STRING251));
			m_wndLbl3_35.SetWindowText(xml->str(IDS_STRING254));
			m_wndLbl3_36.SetWindowText(xml->str(IDS_STRING255));

			// Roads "Stickv�gar"
			m_wndRoadGroup.SetWindowText(xml->str(IDS_STRING214));
			m_wndLbl3_7.SetWindowText(xml->str(IDS_STRING215));
			m_wndLbl3_8.SetWindowText(xml->str(IDS_STRING218));
			m_wndLbl3_9.SetWindowText(xml->str(IDS_STRING221));

			// Baselarea "Grundyta"
			m_wndGYGroup.SetWindowText(xml->str(IDS_STRING222));
			m_wndLbl3_10.SetWindowText(xml->str(IDS_STRING223));
			m_wndLbl3_11.SetWindowText(xml->str(IDS_STRING224));
			m_wndLbl3_12.SetWindowText(xml->str(IDS_STRING225));
			m_wndLbl3_13.SetWindowText(xml->str(IDS_STRING226));
	
			// Volumes
			m_wndVolGroup.SetWindowText(xml->str(IDS_STRING230));
			m_wndLbl3_14.SetWindowText(xml->str(IDS_STRING231));
			m_wndLbl3_15.SetWindowText(xml->str(IDS_STRING232));
			m_wndLbl3_16.SetWindowText(xml->str(IDS_STRING233));
			m_wndLbl3_17.SetWindowText(xml->str(IDS_STRING234));
			m_wndLbl3_18.SetWindowText(xml->str(IDS_STRING235));
			
			// Prop. species "Tr�dslagsf�rdelning"
			m_wndPropGroup.SetWindowText(xml->str(IDS_STRING236));
			m_wndLbl3_19.SetWindowText(xml->str(IDS_STRING237));
			m_wndLbl3_20.SetWindowText(xml->str(IDS_STRING238));
			m_wndLbl3_21.SetWindowText(xml->str(IDS_STRING239));
			m_wndLbl3_22.SetWindowText(xml->str(IDS_STRING240));

			// Remaining data
			m_wndRestGroup.SetWindowText(xml->str(IDS_STRING246));
			m_wndLbl3_23.SetWindowText(xml->str(IDS_STRING227));
			m_wndLbl3_24.SetWindowText(xml->str(IDS_STRING228));
//			m_wndLbl3_25.SetWindowText(xml->str(IDS_STRING229));	H25 not displayed
			m_wndLbl3_26.SetWindowText(xml->str(IDS_STRING241));
			m_wndLbl3_27.SetWindowText(xml->str(IDS_STRING242));
			m_wndLbl3_28.SetWindowText(xml->str(IDS_STRING243));
			m_wndLbl3_29.SetWindowText(xml->str(IDS_STRING244));
			m_wndLbl3_30.SetWindowText(xml->str(IDS_STRING245));
			
			m_wndLbl_Forrensat.SetWindowText(xml->str(IDS_STRING256));
			
				m_wndLbl_Trakt_Avslut_Slutford.SetWindowText(xml->str(IDS_STRING6101));
			m_wndLbl_Trakt_Avslut_RattAtgTp.SetWindowText(xml->str(IDS_STRING6102));
			m_wndLbl_Trakt_Avslut_Forklaring.SetWindowText(xml->str(IDS_STRING6103));
			m_wndLbl_Trakt_Avslut_Maskinstorlek.SetWindowText(xml->str(IDS_STRING6104));
			m_wndLbl_Trakt_Avslut_MaskinNummer.SetWindowText(xml->str(IDS_STRING6105));
			m_wndLbl_Trakt_Avslut_Myr.SetWindowText(xml->str(IDS_STRING6106));

			m_wndLbl_Trakt_Avslut_Hallmark.SetWindowText(xml->str(IDS_STRING6107));
			m_wndLbl_Trakt_Avslut_Hansyn.SetWindowText(xml->str(IDS_STRING6108));
			m_wndLbl_Trakt_Avslut_Fdkulturmark.SetWindowText(xml->str(IDS_STRING6109));
			m_wndLbl_Trakt_Avslut_Kulturmiljo.SetWindowText(xml->str(IDS_STRING6110));
			m_wndLbl_Trakt_Avslut_Lovandel.SetWindowText(xml->str(IDS_STRING6111));
			m_wndLbl_Trakt_Avslut_Skyddszon.SetWindowText(xml->str(IDS_STRING6112));
			m_wndLbl_Trakt_Avslut_NVtrad.SetWindowText(xml->str(IDS_STRING6113));
			m_wndLbl_Trakt_Avslut_Framtidstrad.SetWindowText(xml->str(IDS_STRING6114));
			m_wndLbl_Trakt_Avslut_Hogstubbar.SetWindowText(xml->str(IDS_STRING6115));
			m_wndLbl_Trakt_Avslut_Dodatrad.SetWindowText(xml->str(IDS_STRING6116));
			m_wndLbl_Trakt_Avslut_Markskador.SetWindowText(xml->str(IDS_STRING6117));
			m_wndLbl_Trakt_Avslut_Markskador2.SetWindowText(xml->str(IDS_STRING6118));
			m_wndLbl_Trakt_Avslut_Nedskrapning.SetWindowText(xml->str(IDS_STRING6119));
			m_wndLbl_Trakt_Avslut_MindreKorskada.SetWindowText(xml->str(IDS_STRING6120));
			m_wndLbl_Trakt_Avslut_AllvarligKorskada.SetWindowText(xml->str(IDS_STRING6121));
			m_wndLbl_Trakt_Avslut_Fritext.SetWindowText(xml->str(IDS_STRING257));


			m_sErrCap = xml->str(IDS_STRING109);
			m_sErrMsg = xml->str(IDS_STRING510);
			m_sNotOkToSave.Format(_T("%s\n\n%s\n\n%s"),
									xml->str(IDS_STRING5161),
									xml->str(IDS_STRING5162),
									xml->str(IDS_STRING5163));
			m_sSaveData.Format(_T("%s\n\n%s"),
									xml->str(IDS_STRING5171),
									xml->str(IDS_STRING5172));
			m_sUpdateTraktMsg.Format(_T("%s\n\n%s\n\n%s"),
									xml->str(IDS_STRING5220),
									xml->str(IDS_STRING5221),
									xml->str(IDS_STRING5222));

			}
		delete xml;
	}

}

void CMDIGallBasTraktFormView::populateData(UINT idx)
{
	DISTRICT_DATA data;
	CString s_Data=_T("");
	if (!m_vecTraktData.empty() && 
		  idx >= 0 && 
			idx < m_vecTraktData.size())
	{
		m_recActiveTrakt = m_vecTraktData[idx];

		for(int i=0; i<m_wndCBox3.GetCount(); i++)
		{
			if(m_wndCBox3.GetItemData(i) == m_recActiveTrakt.m_nRegionID)
			{
				m_wndCBox3.SetCurSel(i);
				break;
			}
		}
		addDistrictsToCBox();
		for(int i=0; i<m_wndCBox4.GetCount(); i++)
		{
			if(m_wndCBox4.GetItemData(i) == m_recActiveTrakt.m_nDistrictID)
			{
				m_wndCBox4.SetCurSel(i);
				break;
			}
		}

		m_wndEdit5.setInt(m_recActiveTrakt.m_nMachineID);
		m_wndEdit6.SetWindowText(getMachineEntr(m_recActiveTrakt.m_nRegionID,m_recActiveTrakt.m_nDistrictID,m_recActiveTrakt.m_nMachineID));

		m_wndEdit31.SetWindowText(m_recActiveTrakt.m_sTraktNum);
		m_wndEdit32.SetWindowText(m_recActiveTrakt.m_sTraktName);
		m_wndEdit33.setFloat(m_recActiveTrakt.m_fTraktAreal,1);
		m_wndDatePicker.SetWindowText(m_recActiveTrakt.m_sTraktDate);
		m_wndCBox1.SelectString(0,m_recActiveTrakt.m_sTypeName.Trim());
		m_wndCBox2.SetCurSel(m_recActiveTrakt.m_nOrigin-1);
		
		//  Road data
		m_wndEdit7.setFloat(m_recActiveTrakt.m_recData.getRWidth1(),1);
		m_wndEdit8.setFloat(m_recActiveTrakt.m_recData.getRDist1(),1);
		m_wndEdit9.setFloat(m_recActiveTrakt.m_recData.getRArea(),1);
		// Basel area
		m_wndEdit10.setFloat(m_recActiveTrakt.m_recData.getGYBefore(),1);
		m_wndEdit11.setFloat(m_recActiveTrakt.m_recData.getGYAfter(),1);
		m_wndEdit12.setFloat(m_recActiveTrakt.m_recData.getGYWDraw(),1);
		m_wndEdit13.setFloat(m_recActiveTrakt.m_recData.getGYQuota(),1);
		// Volumes
		m_wndEdit14.setFloat(m_recActiveTrakt.m_recData.getVolPine(),1);
		m_wndEdit15.setFloat(m_recActiveTrakt.m_recData.getVolSpruce(),1);
		m_wndEdit16.setFloat(m_recActiveTrakt.m_recData.getVolBirch(),1);
		m_wndEdit17.setFloat(m_recActiveTrakt.m_recData.getVolCont(),1);
		m_wndEdit18.setFloat(m_recActiveTrakt.m_recData.getVolTotal(),1);
		// Specie diviation
		m_wndEdit19.setFloat(m_recActiveTrakt.m_recData.getPropPine(),1);
		m_wndEdit20.setFloat(m_recActiveTrakt.m_recData.getPropSpruce(),1);
		m_wndEdit21.setFloat(m_recActiveTrakt.m_recData.getPropBirch(),1);
		m_wndEdit22.setFloat(m_recActiveTrakt.m_recData.getPropCont(),1);
		// Remaining data
		m_wndEdit23.setFloat(m_recActiveTrakt.m_recData.getStems(),0);
		m_wndEdit24.setFloat(m_recActiveTrakt.m_recData.getAvgDiam(),1);
		m_wndEdit26.setFloat(m_recActiveTrakt.m_recData.getDamaged(),1);
		m_wndEdit27.setFloat(m_recActiveTrakt.m_recData.getTracks(),1);
		m_wndEdit28.setFloat(m_recActiveTrakt.m_recData.getHighStumps(),0);
		m_wndEdit29.setFloat(m_recActiveTrakt.m_recData.getStumpTreat(),1);
		m_wndEdit30.setFloat(m_recActiveTrakt.m_recData.getTwigs(),1);
		m_wndEdit34.SetWindowText(m_recActiveTrakt.m_sAvslut_fritext);
		m_wndEdit_Forrensat.setFloat(m_recActiveTrakt.m_recData.getForrensat(),1);
		m_wndEdit_Trakt_Avslut_Slutford.SetWindowText(m_recActiveTrakt.m_sAvslut_datum_slutford);
		m_wndEdit_Trakt_Avslut_Forklaring.SetWindowText(m_recActiveTrakt.m_sAvslut_atgardstp_forklaring);
		m_wndEdit_Trakt_Avslut_MaskinNummer.setInt(m_recActiveTrakt.m_nMachineID);

		s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_atgardstp);
		m_wndCBox_Trakt_Avslut_RattAtgTp.SelectString(0,s_Data);

		s_Data.Format(_T("%d"),m_recActiveTrakt.m_nMaskinstorlek);
	m_wndCBox_Trakt_Avslut_Maskinstorlek.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_myr);
	m_wndCBox_Trakt_Avslut_Myr.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_hallmark);
	m_wndCBox_Trakt_Avslut_Hallmark.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_hansyn);
	m_wndCBox_Trakt_Avslut_Hansyn.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_fdkulturmark);
	m_wndCBox_Trakt_Avslut_Fdkulturmark.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_kulturmiljo);
	m_wndCBox_Trakt_Avslut_Kulturmiljo.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_lovandel);
	m_wndCBox_Trakt_Avslut_Lovandel.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_skyddszon);
	m_wndCBox_Trakt_Avslut_Skyddszon.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_nvtrad);
	m_wndCBox_Trakt_Avslut_NVtrad.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_framtidstrad);
	m_wndCBox_Trakt_Avslut_Framtidstrad.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_hogstubbar);
	m_wndCBox_Trakt_Avslut_Hogstubbar.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_dodatrad);
	m_wndCBox_Trakt_Avslut_Dodatrad.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_markskador);
	m_wndCBox_Trakt_Avslut_Markskador.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_markskador2);
	m_wndCBox_Trakt_Avslut_Markskador2.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_nedskrapning);
	m_wndCBox_Trakt_Avslut_Nedskrapning.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_mindrekorskada);
	m_wndCBox_Trakt_Avslut_MindreKorskada.SelectString(0,s_Data);
	s_Data.Format(_T("%d"),m_recActiveTrakt.m_nAvslut_allvarligkorskada);
	m_wndCBox_Trakt_Avslut_AllvarligKorskada.SelectString(0,s_Data);
	

		// This method set editboxes read or read only, depending on 
		// if there's Compartments and Plots; 061010 p�d
		setAllReadOnly(m_recActiveTrakt.n_nEnterType == 0,TRUE);

	}
	else
	{
		// This method set editboxes read or read only, depending on 
		// if there's Compartments and Plots; 061010 p�d
		setAllReadOnly( TRUE, TRUE );
		clearAll();
	}

}


// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 061010 p�d
LRESULT CMDIGallBasTraktFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			m_enumAction = MANUALLY;
			addTraktManually();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
			m_enumAction = FROM_FILE;
			addTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			saveTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			deleteTrakt();
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (!isDataChanged())
			{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			isDataChanged(); // Save any changes

			m_nDBIndex--;
			if (m_nDBIndex < 0)
				m_nDBIndex = 0;
			if (m_nDBIndex == 0)
			{
				setNavigationButtons(FALSE,TRUE);
			}
			else
			{
				setNavigationButtons(TRUE,TRUE);
			}
			populateData(m_nDBIndex);
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			isDataChanged(); // Save any changes

			m_nDBIndex++;
			if (m_nDBIndex > ((UINT)m_vecTraktData.size() - 1))
				m_nDBIndex = (UINT)m_vecTraktData.size() - 1;
				
			if (m_nDBIndex == (UINT)m_vecTraktData.size() - 1)
			{
				setNavigationButtons(TRUE,FALSE);
			}
			else
			{
				setNavigationButtons(TRUE,TRUE);
			}
			populateData(m_nDBIndex);
			break;
		}
		case ID_DBNAVIG_END :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (!isDataChanged())
			{
				m_nDBIndex = (UINT)m_vecTraktData.size()-1;
				setNavigationButtons(TRUE,FALSE);	
				populateData(m_nDBIndex);
			}
			break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

void CMDIGallBasTraktFormView::getTraktsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTrakts(m_vecTraktData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasTraktFormView::getRegionsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getRegions(m_vecRegionData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasTraktFormView::getDistrictFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getDistricts(m_vecDistrictData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasTraktFormView::getMachineFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getMachines(m_vecMachineData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasTraktFormView::addTraktManually(void)
{
	setAllReadOnly( FALSE,FALSE );	// Open up fields for editing; 061011 p�d

	clearAll();
	m_wndListMachineBtn.EnableWindow( TRUE );
	m_bIsDirty = TRUE;
}

void CMDIGallBasTraktFormView::addTrakt(void)
{
	if (createFromFile(m_dbConnectionData,m_sLangFN,&m_structTraktIdentifer))
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);
}

void CMDIGallBasTraktFormView::deleteTrakt(void)
{
	DISTRICT_DATA dataDistrict;
	TRAKT_DATA dataTrakt;
	CString sMsg;
	CString sText1;
	CString sText2;
	CString sText3;
	CString sText4;
	CString sRegion;
	CString sDistrict;
	CString sMachine;
	CString sTrakt;
	CString sCaption;
	CString sOKBtn;
	CString sCancelBtn;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sText1 = xml->str(IDS_STRING5001);
			sText2 = xml->str(IDS_STRING5011);
			sText3 = xml->str(IDS_STRING5021);
			sText4 = xml->str(IDS_STRING5031);
			sRegion = xml->str(IDS_STRING504);
			sDistrict = xml->str(IDS_STRING505);
			sMachine = xml->str(IDS_STRING506);
			sTrakt = xml->str(IDS_STRING507);

			sCaption = xml->str(IDS_STRING109);
			sOKBtn = xml->str(IDS_STRING110);
			sCancelBtn = xml->str(IDS_STRING111);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	// Check that there's any data in m_vecTraktData vector; 061010 p�d
	if (m_vecTraktData.size() > 0)
	{
		if (m_bConnected)
		{
			// Get Region information from Database server; 070122 p�d
			CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
			if (pDB != NULL)
			{
					// Get Regions in database; 061002 p�d	
					dataTrakt = m_vecTraktData[m_nDBIndex];
					// Setup a message for user upon deleting machine; 061010 p�d

					getRegionAndDistrictData(dataTrakt.m_nRegionID,dataTrakt.m_nDistrictID,dataDistrict);
					sMsg.Format(_T("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%s : <b>%d</b>   (%s)<br>%s : <b>%d</b>  (%s)<br>%s : <b>%d</b><br>%s : <b>%s</b><br><hr><br>%s<br>%s<br><br><font size=\"+4\" color=\"#ff0000\"><center>%s</center></font>"),
											sText1,
											sRegion,
											dataDistrict.m_nRegionID,
											dataDistrict.m_sRegionName,
											sDistrict,
											dataDistrict.m_nDistrictID,
											dataDistrict.m_sDistrictName,
											sMachine,
											dataTrakt.m_nMachineID,
											sTrakt,
											dataTrakt.m_sTraktNum,
											sText2,
											sText3,
											sText4);

					if (messageDialog(sCaption,sOKBtn,sCancelBtn,sMsg))
					{
						// Delete Machine and ALL underlying 
						//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
						pDB->delTrakt(dataTrakt);
					}

				delete pDB;
			}	// if (pDB != NULL)
		} // 	if (m_bConnected)
	}	// if (m_vecMachineData.size() > 0)
	m_bIsDirty = FALSE;
	resetTrakt(RESET_TO_LAST_SET_NB);
}

// PROTECTED
void CMDIGallBasTraktFormView::setDefaultTypeAndOrigin(void)
{
	CString sTmp;
	for (int i = 0;i < sizeof(INVENTORY_TYPE_ARRAY)/sizeof(INVENTORY_TYPE_ARRAY[0]);i++)
	{
		m_wndCBox1.AddString(INVENTORY_TYPE_ARRAY[i].sInvTypeName);
	}

	for (int i = 0;i < sizeof(ORIGIN_ARRAY)/sizeof(ORIGIN_ARRAY[0]);i++)
	{
		m_wndCBox2.AddString(ORIGIN_ARRAY[i].sOrigin);
	}
}

CString CMDIGallBasTraktFormView::getType_setByUser(void)
{
	int nIndex = m_wndCBox1.GetCurSel();
	if (nIndex != CB_ERR)
		return INVENTORY_TYPE_ARRAY[nIndex].sAbbrevInvTypeName;
	else
		return _T("");
}

CString CMDIGallBasTraktFormView::getTypeName_setByUser(void)
{
	int nIndex = m_wndCBox1.GetCurSel();
	if (nIndex != CB_ERR)
		return INVENTORY_TYPE_ARRAY[nIndex].sInvTypeName;
	else
		return _T("");
}

int CMDIGallBasTraktFormView::getOrigin_setByUser(void)
{
	int nIndex = m_wndCBox2.GetCurSel();
	if (nIndex != CB_ERR)
		return ORIGIN_ARRAY[nIndex].nOriginNum;
	else
		return -1;
}

CString CMDIGallBasTraktFormView::getOriginName_setByUser(void)
{
	int nIndex = m_wndCBox2.GetCurSel();
	if (nIndex != CB_ERR)
		return ORIGIN_ARRAY[nIndex].sOrigin;
	else
		return _T("");
}

// Get manually entered data; 061011 p�d
void CMDIGallBasTraktFormView::getEnteredData(void)
{
	// Setup data for new entry of Machine; 061011 p�d
	m_recNewMachine.m_nRegionID				= getRegionID();
	m_recNewMachine.m_nDistrictID			= getDistrictID();
	m_recNewMachine.m_nMachineID			= m_wndEdit5.getInt();
	m_recNewMachine.m_sContractorName = m_wndEdit6.getText();

	//#4938 20160503 J�
	//S�tter ny trakt till active trakt innan inl�sning av data fr�n editboxar etc g�rs annars tappas information om avslutsvariabler 
	//om man trycker p� spara vid en inl�sning av en xml fil fr�n klave
	//
	m_recNewTrakt=m_recActiveTrakt;

	// Identification information
	m_recNewTrakt.m_nRegionID		= getRegionID();
	m_recNewTrakt.m_nDistrictID	= getDistrictID();
	m_recNewTrakt.m_nMachineID	= m_wndEdit5.getInt();
	m_recNewTrakt.m_nOrigin			= getOrigin_setByUser();
	m_recNewTrakt.m_sOriginName	= getOriginName_setByUser();
	m_recNewTrakt.m_sType				= getType_setByUser();
	m_recNewTrakt.m_sTypeName		= getTypeName_setByUser();
	m_recNewTrakt.m_fTraktAreal	= m_wndEdit33.getFloat();
	// Trakt information
	CString sDate;
	m_wndDatePicker.GetWindowText(sDate);
	m_recNewTrakt.m_sTraktDate	= sDate;
	m_recNewTrakt.m_sTraktNum		= m_wndEdit31.getText();
	m_recNewTrakt.m_sTraktName	= m_wndEdit32.getText();
	// Notes
	m_recNewTrakt.m_sAvslut_fritext			= m_wndEdit34.getText();
	// Trakt data
	m_recNewTrakt.m_recData = CVariables(m_wndEdit7.getFloat(),
																				0.0,	// Road dist 2 not used
																				m_wndEdit8.getFloat(),
																				0.0,	// Road width 2 not used
																				m_wndEdit9.getFloat(),
																				m_wndEdit10.getFloat(),
																				m_wndEdit11.getFloat(),
																				m_wndEdit12.getFloat(),
																				m_wndEdit13.getFloat(),
																				m_wndEdit23.getFloat(),
																				m_wndEdit24.getFloat(),
																				0.0,
																				m_wndEdit14.getFloat(),
																				m_wndEdit15.getFloat(),
																				m_wndEdit16.getFloat(),
																				m_wndEdit17.getFloat(),
																				m_wndEdit18.getFloat(),
																				m_wndEdit19.getFloat(),
																				m_wndEdit20.getFloat(),
																				m_wndEdit21.getFloat(),
																				m_wndEdit22.getFloat(),
																				m_wndEdit26.getFloat(),
																				m_wndEdit27.getFloat(),
																				m_wndEdit28.getFloat(),
																				m_wndEdit29.getFloat(),
																				m_wndEdit30.getFloat(),
																				m_wndEdit_Forrensat.getFloat(),
																				_T(""),0,0,0,0,0,0,0.0,0.0,0.0,0.0,0.0,0,0,0,0,0,0,0,0);

	// Check RegionID and DistrictID. If they eq. -1 then
	// Set to m_recActiveTrakt values; 061011 p�d
	if (m_recNewMachine.m_nRegionID == -1 && m_recNewMachine.m_nDistrictID == -1)
	{
		m_recNewMachine.m_nRegionID = m_recActiveTrakt.m_nRegionID;
		m_recNewMachine.m_nDistrictID = m_recActiveTrakt.m_nDistrictID;
	}

	// Setup who data's entered, Maually or by file; 061201 p�d
	if (m_enumAction == MANUALLY)
		m_recNewTrakt.n_nEnterType = 1;
}

// Method used on createing a new Trakt manually.
// Clears ALL fileds for entering data. OBS! Region/District is entered
// using a Dialog; 061011 p�d
void CMDIGallBasTraktFormView::clearAll(void)
{
	m_wndEdit5.SetWindowText(_T(""));
	m_wndEdit6.SetWindowText(_T(""));
	m_wndEdit7.SetWindowText(_T(""));
	m_wndEdit8.SetWindowText(_T(""));
	m_wndEdit9.SetWindowText(_T(""));
	m_wndEdit10.SetWindowText(_T(""));
	m_wndEdit11.SetWindowText(_T(""));
	m_wndEdit12.SetWindowText(_T(""));
	m_wndEdit13.SetWindowText(_T(""));
	m_wndEdit14.SetWindowText(_T(""));
	m_wndEdit15.SetWindowText(_T(""));
	m_wndEdit16.SetWindowText(_T(""));
	m_wndEdit17.SetWindowText(_T(""));
	m_wndEdit18.SetWindowText(_T(""));
	m_wndEdit19.SetWindowText(_T(""));
	m_wndEdit20.SetWindowText(_T(""));
	m_wndEdit21.SetWindowText(_T(""));
	m_wndEdit22.SetWindowText(_T(""));
	m_wndEdit23.SetWindowText(_T(""));
	m_wndEdit24.SetWindowText(_T(""));
	m_wndEdit26.SetWindowText(_T(""));
	m_wndEdit27.SetWindowText(_T(""));
	m_wndEdit28.SetWindowText(_T(""));
	m_wndEdit29.SetWindowText(_T(""));
	m_wndEdit30.SetWindowText(_T(""));
	m_wndEdit31.SetWindowText(_T(""));
	m_wndEdit32.SetWindowText(_T(""));
	m_wndEdit33.SetWindowText(_T(""));
	m_wndEdit34.SetWindowText(_T(""));
	m_wndCBox1.SetCurSel(-1);
	m_wndCBox2.SetCurSel(-1);
	m_wndCBox3.SetCurSel(-1);
	m_wndCBox4.SetCurSel(-1);
	m_wndDatePicker.SetWindowText(_T(""));

	m_wndEdit_Forrensat.SetWindowText(_T(""));
	m_wndEdit_Trakt_Avslut_Slutford.SetWindowText(_T(""));
	m_wndEdit_Trakt_Avslut_Forklaring.SetWindowText(_T(""));
	m_wndEdit_Trakt_Avslut_MaskinNummer.SetWindowText(_T(""));

	m_wndCBox_Trakt_Avslut_RattAtgTp.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Maskinstorlek.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Myr.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Hallmark.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Hansyn.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Fdkulturmark.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Kulturmiljo.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Lovandel.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Skyddszon.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_NVtrad.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Framtidstrad.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Hogstubbar.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Dodatrad.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Markskador.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Markskador2.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_Nedskrapning.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_MindreKorskada.SetCurSel(-1);
	m_wndCBox_Trakt_Avslut_AllvarligKorskada.SetCurSel(-1);



	// Set m_recNewTrakt key-values to -1
	m_recNewTrakt.m_nRegionID	= -1;
	m_recNewTrakt.m_nDistrictID = -1;
	m_recNewTrakt.m_nMachineID	= -1;
	m_recNewTrakt.m_sTraktNum	= _T("");
	m_recNewTrakt.m_nOrigin		= -1;
	m_recNewTrakt.m_sOriginName = _T("");
	m_recNewTrakt.m_sType		= _T("");
	m_recNewTrakt.m_sTypeName	= _T("");
}

// Set all Editboxes to read or read only, depending on
// if data comes from database and holds Compartmant(s) and Plot(s); 061010 p�d
void CMDIGallBasTraktFormView::setAllReadOnly(BOOL ro1,BOOL ro2)
{
	m_wndListMachineBtn.EnableWindow( FALSE );

	m_wndEdit7.SetReadOnly( ro1 );
	m_wndEdit8.SetReadOnly( ro1 );
	m_wndEdit10.SetReadOnly( ro1 );
	m_wndEdit11.SetReadOnly( ro1 );
	m_wndEdit13.SetReadOnly( ro1 );
	m_wndEdit14.SetReadOnly( ro1 );
	m_wndEdit15.SetReadOnly( ro1 );
	m_wndEdit16.SetReadOnly( ro1 );
	m_wndEdit17.SetReadOnly( ro1 );
	m_wndEdit19.SetReadOnly( ro1 );
	m_wndEdit20.SetReadOnly( ro1 );
	m_wndEdit21.SetReadOnly( ro1 );
	m_wndEdit22.SetReadOnly( ro1 );
	m_wndEdit23.SetReadOnly( ro1 );
	m_wndEdit24.SetReadOnly( ro1 );
	m_wndEdit26.SetReadOnly( ro1 );
	m_wndEdit27.SetReadOnly( ro1 );
	m_wndEdit28.SetReadOnly( ro1 );
	m_wndEdit29.SetReadOnly( ro1 );
	m_wndEdit30.SetReadOnly( ro1 );
//	Commented out 2007-04-19 on request from Holmen
//	this field'll always be editable; 070419 p�d
//	m_wndEdit32.SetReadOnly( ro1 );

	m_wndEdit31.SetReadOnly( ro2 );
	m_wndCBox1.SetReadOnly( ro2 );
	m_wndCBox2.SetReadOnly( ro2 );
	m_wndCBox3.SetReadOnly( ro2 );
	m_wndCBox4.SetReadOnly( ro2 );

	m_wndEdit_Trakt_Avslut_Slutford.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Avslut_Forklaring.SetReadOnly( ro1 );
	m_wndEdit_Trakt_Avslut_MaskinNummer.SetReadOnly( ro1 );

	m_wndCBox_Trakt_Avslut_RattAtgTp.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Maskinstorlek.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Myr.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Hallmark.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Hansyn.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Fdkulturmark.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Kulturmiljo.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Lovandel.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Skyddszon.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_NVtrad.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Framtidstrad.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Hogstubbar.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Dodatrad.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Markskador.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Markskador2.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_Nedskrapning.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_MindreKorskada.SetReadOnly( ro2 );
	m_wndCBox_Trakt_Avslut_AllvarligKorskada.SetReadOnly( ro2 );
}

// Create a string containing Region and District in the same form as displayed in
// dialog for "Bekr�fta inl�sning"; 061011 p�d
CString CMDIGallBasTraktFormView::getRegionAndDistrictData(int region,int dist,DISTRICT_DATA &rec)
{
	CString sRetStr;
	DISTRICT_DATA data;
	if (m_vecDistrictData.empty())
	{
		return _T("");
	}

	for (UINT i = 0;i < m_vecDistrictData.size();i++)
	{
		data = m_vecDistrictData[i];
		if (data.m_nRegionID == region && data.m_nDistrictID == dist)
		{
			sRetStr.Format(_T("(%d) %s  -  (%d) %s"),data.m_nRegionID,
																					data.m_sRegionName,
																					data.m_nDistrictID,
																					data.m_sDistrictName);
			rec = data;
			break;
		}
	}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	return sRetStr;
}

// Create a string containing Region and District in the same form as displayed in
// dialog for "Bekr�fta inl�sning"; 061011 p�d
CString CMDIGallBasTraktFormView::getMachineEntr(int region,int dist,int machine)
{
	CString sRetStr;
	MACHINE_DATA data;
	if (m_vecMachineData.empty())
		return _T("");

	for (UINT i = 0;i < m_vecMachineData.size();i++)
	{
		data = m_vecMachineData[i];
		if (data.m_nRegionID == region && data.m_nDistrictID == dist && data.m_nMachineID == machine)
		{
			sRetStr = data.m_sContractorName;
			break;
		}
	}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	return sRetStr;
}

// Add regions in registry, into ComboBox3; 061204 p�d
void CMDIGallBasTraktFormView::addRegionsToCBox(void)
{
	CString sText;
	if (m_vecRegionData.size() > 0)
	{
		m_wndCBox3.ResetContent();
		for (UINT i = 0;i < m_vecRegionData.size();i++)
		{
			REGION_DATA data = m_vecRegionData[i];
			sText.Format(_T("%d - %s"),data.m_nRegionID,data.m_sRegionName);
			m_wndCBox3.AddString(sText);
			m_wndCBox3.SetItemData(i, data.m_nRegionID);
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}

// Try to find region, based on index; 061204 p�d
CString CMDIGallBasTraktFormView::getRegionSelected(int region_num)
{
	CString sText;
	if (m_vecRegionData.size() > 0)
	{
		for (UINT i = 0;i < m_vecRegionData.size();i++)
		{
			REGION_DATA data = m_vecRegionData[i];
			if (data.m_nRegionID == region_num)
			{
				sText.Format(_T("%d - %s"),data.m_nRegionID,data.m_sRegionName);
				return sText;
			}
		}
	}	// if (m_vecRegionData.size() > 0)
	return _T("");
}

// Try to find index of regionnumber in m_vecRegionData; 061204 p�d
int CMDIGallBasTraktFormView::getRegionID(void)
{
	int nIdx = m_wndCBox3.GetCurSel();
	if (nIdx != CB_ERR)
	{
		return m_wndCBox3.GetItemData(nIdx);
	}

	return -1;
}

// Add Districts into Combobox4, based on selected region; 061204 p�d
void CMDIGallBasTraktFormView::addDistrictsToCBox(void)
{
	int nRegionIndex = m_wndCBox3.GetCurSel();
	CString sText;
	int nCnt = 0;
	if (nRegionIndex != CB_ERR && m_vecDistrictData.size() > 0)
	{
		REGION_DATA dataRegion = m_vecRegionData[nRegionIndex];
		m_wndCBox4.ResetContent();
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA dataDist = m_vecDistrictData[i];
			if (dataDist.m_nRegionID == dataRegion.m_nRegionID)
			{
				sText.Format(_T("%d - %s"),dataDist.m_nDistrictID,dataDist.m_sDistrictName);
				m_wndCBox4.AddString(sText);
				m_wndCBox4.SetItemData(nCnt,dataDist.m_nDistrictID);
				nCnt++;
			}	// if (dataDist.m_nRegionID == dataRegion.m_nRegionID)
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (nRegionIndex != CB_ERR && m_vecDistrictData.size() > 0)
}

// Try to find district, based on index; 061204 p�d
CString CMDIGallBasTraktFormView::getDistrictSelected(int region_num,int district_num)
{
	CString sText;
	if (m_vecDistrictData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			if (data.m_nRegionID == region_num && data.m_nDistrictID == district_num)
			{
				sText.Format(_T("%d - %s"),data.m_nDistrictID,data.m_sDistrictName);
				return sText;
			}	// if (data.m_nRegionID == region_num && data.m_nDistrictID == district_num)
		}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
	
		return _T("");
}

// Try to find index of regionnumber in m_vecDistrictData; 061204 p�d
int CMDIGallBasTraktFormView::getDistrictID(void)
{
	int nIdx = m_wndCBox4.GetCurSel();
	if (nIdx != CB_ERR)
	{
		return m_wndCBox4.GetItemData(nIdx);
	}

	return -1;
}


// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIGallBasTraktFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

void CMDIGallBasTraktFormView::OnBnClickedButton2()
{
	MACHINE_REG_DATA recMachineReg;
	CSelectRegionDistDlg *dlg = new CSelectRegionDistDlg(WTL_MACHINE_REG);

	if (dlg->DoModal())
	{
		dlg->getMachineReg(recMachineReg);
		m_recNewMachine.m_nMachineID = recMachineReg.m_nMachineRegID;
		m_recNewMachine.m_sContractorName = recMachineReg.m_sContractorName;

		m_wndEdit5.setInt(recMachineReg.m_nMachineRegID);
		m_wndEdit6.SetWindowText(recMachineReg.m_sContractorName);
	}

	delete dlg;
}

// PUBLIC
void CMDIGallBasTraktFormView::saveTrakt(void)
{
	BOOL bDontSave = FALSE;

	if (isOKToSave() && m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			getEnteredData();
			// Setup index information; 061207 p�d
			m_structTraktIdentifer.nRegionID = m_recNewTrakt.m_nRegionID;
			m_structTraktIdentifer.nDistrictID	= m_recNewTrakt.m_nDistrictID;
			m_structTraktIdentifer.nMachineID = m_recNewTrakt.m_nMachineID;
			m_structTraktIdentifer.sTraktNum = m_recNewTrakt.m_sTraktNum;
			m_structTraktIdentifer.sType = m_recNewTrakt.m_sType;
			m_structTraktIdentifer.nOrigin = m_recNewTrakt.m_nOrigin;
			// Add or Update Machine, if not already in register; 061204 p�d
			if (!pDB->addMachine( m_recNewMachine ))
				pDB->updMachine( m_recNewMachine );

			if( (m_wndCBox3.isDirty() || m_wndCBox4.isDirty() || m_wndEdit5.isDirty() || m_wndCBox1.isDirty() || m_wndCBox2.isDirty() || m_wndEdit31.isDirty())
				  && ::MessageBox(0,m_sUpdateTraktMsg,m_sErrCap,MB_YESNO | MB_ICONSTOP) == IDNO )
			{
				bDontSave = TRUE;
			}

			if( !bDontSave )
			{
				if (!pDB->addTrakt( m_recNewTrakt ))
				{
					pDB->updTrakt( m_recNewTrakt );
				}
			}
			delete pDB;
		}	// if (pDB != NULL)

		m_bIsDirty = FALSE;
		resetIsDirty();
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);
		m_enumAction = NOTHING;

	}	// if (isOKToSave())
}

// Check that information needed to be abel to save Trakt, is enterd
// by user; 061113 p�d
BOOL CMDIGallBasTraktFormView::isOKToSave(void)
{
	if (_tcscmp(m_wndCBox3.getText().Trim(),_T("")) == 0 ||
		_tcscmp(m_wndCBox4.getText().Trim(),_T("")) == 0 ||
		m_wndEdit5.getInt() <= 0 ||
		_tcscmp(m_wndEdit31.getText().Trim(),_T("")) == 0 ||
		getType_setByUser().Trim() == _T("") ||
		getOrigin_setByUser() == -1) 
	{
		// Also tell user that there's insufficent data; 061113 p�d
		::MessageBox(0,m_sNotOkToSave,m_sErrCap,MB_ICONSTOP | MB_OK);
		return FALSE;
	}

	return TRUE;
}

// Check if active data's chnged by user.
// I.e. manually enterd data; 061113 p�d
BOOL CMDIGallBasTraktFormView::isDataChanged(void)
{
	if (getIsDirty())
	{
		saveTrakt();
		resetIsDirty();
		return TRUE;
	}
	return FALSE;
}

void CMDIGallBasTraktFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons(m_nDBIndex > 0,
											 m_nDBIndex < (m_vecTraktData.size()-1));
}

/*
	RESET_TO_FIRST_SET_NB,				// Set to first item and set Navigationbar
  RESET_TO_FIRST_NO_NB,					// Set to first item and no Navigationbar (disable all)
	RESET_TO_LAST_SET_NB,					// Set to last item and set Navigationbar
	RESET_TO_LAST_NO_NB,					// Set to last item and no Navigationbar (disable all)
	RESET_TO_JUST_ENTERED_SET_NB,	// Set to just entered data (from file or manually) and set Navigationbar
	RESET_TO_JUST_ENTERED_NO_NB		// Set to just entered data (from file or manually) and no Navigationbar
*/
// Reset and get data from database; 061010 p�d
void CMDIGallBasTraktFormView::resetTrakt(enumRESET reset)
{
	// Get updated data from Database
	getMachineFromDB();
	// Get updated data from Database
	getTraktsFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecTraktData.size() > 0)
	{

		if (reset == RESET_TO_FIRST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecTraktData.size()-1));
		}
		if (reset == RESET_TO_FIRST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_LAST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecTraktData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecTraktData.size()-1));
		}
		if (reset == RESET_TO_LAST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecTraktData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_JUST_ENTERED_SET_NB || reset == RESET_TO_JUST_ENTERED_NO_NB)
		{

			// Find itemindex for last entry, from file, and set on populateData; 061207 p�d
			for (UINT i = 0;i < m_vecTraktData.size();i++)
			{
				TRAKT_DATA data = m_vecTraktData[i];
				if (data.m_nRegionID == m_structTraktIdentifer.nRegionID &&
					  data.m_nDistrictID == m_structTraktIdentifer.nDistrictID &&
						data.m_nMachineID == m_structTraktIdentifer.nMachineID &&
						data.m_sTraktNum.Trim() == m_structTraktIdentifer.sTraktNum.Trim() &&
						data.m_sType.Trim() == m_structTraktIdentifer.sType.Trim() &&
						data.m_nOrigin == m_structTraktIdentifer.nOrigin)
				{
					// Reset to last item in m_vecTraktData
					m_nDBIndex = i;
					break;
				}	// if (data.m_nRegionID == m_structTraktIdentifer.nRegionID &&
			}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
			// Populate data on User interface
			populateData(m_nDBIndex);
			if (reset == RESET_TO_JUST_ENTERED_SET_NB)
			{
				setNavigationButtons(m_nDBIndex > 0,
					   								m_nDBIndex < (m_vecTraktData.size()-1));
			}
			else if (reset == RESET_TO_JUST_ENTERED_NO_NB)
			{
				setNavigationButtons(FALSE,FALSE);
			}
		}	// if (reset == RESET_TO_JUST_ENTERED_SET_NB)
	}	// if (m_vecTraktData.size() > 0)
	else 
	{
		setNavigationButtons(FALSE,FALSE);
		populateData(-1);	// Clear data
	}
}

// Calculation methods; 061113 p�d
void CMDIGallBasTraktFormView::calculateRoadSurface(void)
{
	double fRWidth,fRDist,fRArea;
	fRWidth = m_wndEdit7.getFloat();
	fRDist = m_wndEdit8.getFloat();
	if (fRWidth > 0.0 && fRDist > 0.0)
	{
		fRArea = ((fRWidth/fRDist) * 100.0);
		m_wndEdit9.setFloat(fRArea,1);
	}
}

void CMDIGallBasTraktFormView::calculateTotalVolume(void)
{
	double fVolPine,fVolSpruce,fVolBirch,fVolCont;
	fVolPine = m_wndEdit14.getFloat();
	fVolSpruce = m_wndEdit15.getFloat();
	fVolBirch = m_wndEdit16.getFloat();
	fVolCont = m_wndEdit17.getFloat();
	m_wndEdit18.setFloat(fVolPine + fVolSpruce + fVolBirch + fVolCont,1);
	// After calulating volume, set specie diviation; 061113 p�d
	calculateSpecieDiv();
}

// Calculate "Tr�dslagsandel", based on volume/specie; 061113 p�d
void CMDIGallBasTraktFormView::calculateSpecieDiv(void)
{
	double fVolPine,fVolSpruce,fVolBirch,fVolCont,fSumVol;
	fVolPine = m_wndEdit14.getFloat();
	fVolSpruce = m_wndEdit15.getFloat();
	fVolBirch = m_wndEdit16.getFloat();
	fVolCont = m_wndEdit17.getFloat();
	fSumVol = m_wndEdit18.getFloat();
	if (fVolPine > 0.0 && fSumVol > 0.0)
	{
		m_wndEdit19.setFloat((fVolPine/fSumVol)*100.0,1);
	}
	else
	{
		m_wndEdit19.setFloat(0.0,1);
	}
	if (fVolSpruce > 0.0 && fSumVol > 0.0)
	{
		m_wndEdit20.setFloat((fVolSpruce/fSumVol)*100.0,1);
	}
	else
	{
		m_wndEdit20.setFloat(0.0,1);
	}
	if (fVolBirch > 0.0 && fSumVol > 0.0)
	{
		m_wndEdit21.setFloat((fVolBirch/fSumVol)*100.0,1);
	}
	else
	{
		m_wndEdit21.setFloat(0.0,1);
	}
	if (fVolCont > 0.0 && fSumVol > 0.0)
	{
		m_wndEdit22.setFloat((fVolCont/fSumVol)*100.0,1);
	}
	else
	{
		m_wndEdit22.setFloat(0.0,1);
	}
}

void CMDIGallBasTraktFormView::claculateWDraw(void)
{
	double fGYBefore,fGYAfter,fWDraw;
	fGYBefore = m_wndEdit10.getFloat();
	fGYAfter = m_wndEdit11.getFloat();
	// Calcualte WDraw percent; 061113 p�d
	if (fGYBefore > 0.0 && fGYAfter > 0.0)
	{
		fWDraw = ((fGYBefore - fGYAfter)/fGYBefore);
		m_wndEdit12.setFloat(fWDraw * 100.0,1);
	}
}

// Event handles for data changed in Edit boxes; 061113 p�d
void CMDIGallBasTraktFormView::OnEnChangeEdit7()
{
	calculateRoadSurface();
}

void CMDIGallBasTraktFormView::OnEnChangeEdit8()
{
	calculateRoadSurface();
}

void CMDIGallBasTraktFormView::OnEnChangeEdit14()
{
	calculateTotalVolume();
}

void CMDIGallBasTraktFormView::OnEnChangeEdit15()
{
	calculateTotalVolume();
}

void CMDIGallBasTraktFormView::OnEnChangeEdit16()
{
	calculateTotalVolume();
}

void CMDIGallBasTraktFormView::OnEnChangeEdit17()
{
	calculateTotalVolume();
}

void CMDIGallBasTraktFormView::OnEnChangeEdit10()
{
	claculateWDraw();
}

void CMDIGallBasTraktFormView::OnEnChangeEdit11()
{
	claculateWDraw();
}

void CMDIGallBasTraktFormView::OnCbnSelchangeCombo3()
{
	addDistrictsToCBox();
}

