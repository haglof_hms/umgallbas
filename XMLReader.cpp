#include "StdAfx.h"
#include "XmlReader.h"
#include "xmlParser.h"
#include "AddDataToDataBase.h"
#include "ResLangFileReader.h"


//#4031 20140429 J� xml parser
CXmlReader::CXmlReader(void)
{

		m_sDatum=_T("");			// 1
		m_sRegion=_T("");			// 2
		m_sDistrikt=_T("");		// 3
		m_sTyp=_T("");			// 4
		m_sUrsprung=_T("");		// 5
		m_sObj_nr=_T("");			// 6
		m_sObj_namn=_T("");		// 7
		m_sAreal=_T("");			// 8
		m_sMask_nr=_T("");		// 9
		m_sEntr=_T("");			// 10
		m_sMask_storlek=_T("");	// 11
		m_sAtg_tidp_nr=_T("");	// 12
		m_sAtg_tidp_text=_T("");	// 13

		m_n_XmlVer=0;
		m_n_trakt_maskinstorlek=-1;
		m_n_trakt_avslut_myr=-1;
		m_n_trakt_avslut_hallmark=-1;
		m_n_trakt_avslut_hansyn=-1;
		m_n_trakt_avslut_fdkulturmark=-1;
		m_n_trakt_avslut_kulturmiljo=-1;
		m_n_trakt_avslut_lovandel=-1;
		m_n_trakt_avslut_skyddszon=-1;
		m_n_trakt_avslut_nvtrad=-1;
		m_n_trakt_avslut_framtidstrad=-1;
		m_n_trakt_avslut_dodatrad=-1;
		m_n_trakt_avslut_hogstubbar=-1;
		m_n_trakt_avslut_mindrekorskada=-1;
		m_n_trakt_avslut_allvarligkorskada=-1;
		m_n_trakt_avslut_markskador=-1;
		m_n_trakt_avslut_markskador2=-1;
		m_n_trakt_avslut_nedskrapning=-1;
		m_s_trakt_avslut_fritext=_T("");
		m_n_trakt_avslut_atgardstp=-1;
		m_s_trakt_avslut_atgardstp_forklaring=_T("");
		m_s_trakt_avslut_datum_slutford=_T("");
		m_n_trakt_calc_si_spec=-1;
		m_n_trakt_calc_si=-1;

		for(int i=0;i<NUM_OF_YT_VARS;i++)
			m_fTrakt_Medel[i]=0.0;
		Avdelningar.clear();

	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
}



int CXmlReader::OpenFile(char *filename)
{
	int n_Ret=RETURN_ERR_FILEOPEN;
	XMLNode xNode;
	n_Ret=HXL_CheckXmlVer(filename);
	if(n_Ret==1)
	{
		xNode = XMLNode::openFileHelper(filename,XML_TAG_TRAKT_GALLUPHXL);
		n_Ret=HXL_HandleHxlFile(xNode);
	}
	else
	{
		
	}
	return n_Ret;
}

int CXmlReader::HXL_HandleHxlFile(XMLNode xNode)
{
	int n_Ret=RETURN_ERR_FILEOPEN;

	if(xNode.isEmpty() != 1)
	{
		return HXL_RecTrakt(xNode);
	}
	else
	{

	}
	return n_Ret;
}

int CXmlReader::HXL_CheckXmlVer(char *filename)
{
	FILE *in,*out;
	CStringA s_NewFile="";
	CString s_Old_filename=_T("");
	CString	s_New_filename=_T("");
	CString s_Buff2=_T("");
	char buff;
	char *source = NULL;
	int n_Done=0,n_ReadTag=0,n_FoundVerTag=0,n_NrOfTags=0,n_Replaced=0,n_Num=0;

	if ((in = fopen(filename, "rt")) == NULL)
	{
		return RETURN_ERR_FILEOPEN;
	}

	do
	{
		buff=fgetc(in);
		if(buff=='<')
		{
			n_ReadTag=1;
			s_Buff2=_T("");
		}
		else
		{
			if(buff=='>')
			{
				n_NrOfTags++;
				n_ReadTag=0;
				if(s_Buff2.Compare(XML_TAG_VER)==0)
				{
				 n_FoundVerTag=1;
				 n_Done=1;
				}
				else
				{
					if(n_NrOfTags>4)
					{
					 n_FoundVerTag=0;
					 n_Done=1;
					}
				}
			}	 
			else
				if(n_ReadTag)
					s_Buff2+=buff;
		}
	}while(!feof(in) && !n_Done);

	fclose(in);
	if(!n_FoundVerTag)
	{
		if ((in = fopen(filename, "rt")) == NULL)
		{
			//ShowMessage("Kan ej �ppna fil");
			return RETURN_ERR_FILEOPEN;
		}

		/* Go to the end of the file. */
		if (fseek(in, 0L, SEEK_END) == 0) 
		{
			/* Get the size of the file. */
			long bufsize = ftell(in);
			if (bufsize == -1) 
			{ 
				fclose(in);
				return RETURN_ERR_FILEOPEN; 
			}

			/* Allocate our buffer to that size. */
			source = (char *)malloc(sizeof(char) *(bufsize + 1));

			/* Go back to the start of the file. */
			if (fseek(in, 0, SEEK_SET) != 0) 
			{ /* Error */ 
				fclose(in);
				free(source); /* Don't forget to call free() later! */
				return RETURN_ERR_FILEOPEN; 
			}

			/* Read the entire file into memory. */
			size_t newLen = fread(source, sizeof(char), bufsize, in);
			if (newLen == 0) 
			{
				fclose(in);
				free(source); /* Don't forget to call free() later! */
				return RETURN_ERR_FILEOPEN;
			} 
			else 
			{
				source[newLen] = '\0'; /* Just to be safe. */
			}
		}
		fclose(in);

		s_NewFile=source;



		n_Replaced=0;
		//AfxMessageBox(s_NewFile);
		//S�k efter byt ut > tecken
		n_Num=s_NewFile.Replace(XML_REPLACE_LOVANDEL_TEXT,XML_REPLACE_LOVANDEL_NEWTEXT);
		if(n_Num==1)
			n_Replaced=1;
		if(s_NewFile.Replace(XML_REPLACE_DODATRAD_TEXT,XML_REPLACE_DODATRAD_NEWTEXT)==1)
			n_Replaced=1;
		if(n_Replaced) //Byt filnamn och spara ny fil med gamla namnet		
		{
			s_Old_filename=filename;
			s_Old_filename.Replace(_T("."),_T("_kopia."));
			
			//rename(filename,s_Old_filename.GetString());

			
#ifdef UNICODE
			USES_CONVERSION;
			rename(filename,W2A(s_Old_filename.GetString()));
			if((out=fopen(filename,"w+"))!=NULL)
			{
				fwrite(s_NewFile.GetBuffer(),sizeof(char),s_NewFile.GetLength(),out);
				fclose(out);
			}

#else
			rename(filename,s_Old_filename.GetString());
			if((out=fopen(filename,"w+"))!=NULL)
			{
				fwrite(s_NewFile,1,sizeof(s_NewFile.GetString()),out);
				fclose(out);
			}

#endif
						


			/*CStdioFile file;
			if (file.Open(s_New_filename, CFile::modeCreate|CFile::modeWrite )) 
			{
				file.WriteString(s_NewFile.GetBuffer(0));
				file.Close();
			}*/
		}

	}
	free(source); /* Don't forget to call free() later! */
	return 1;
}

//--------------------------------------------------------------
// L�s in traktvariabler
//--------------------------------------------------------------
int CXmlReader::HXL_RecTrakt(XMLNode xNode)
{
	int nDocs=0,numChild=0,n_NumAvds=0,n_Ret=-2;
	XMLNode xDoc;

	//nDocs = xNode.nChildNode(XML_TAG_NODE_TRAKT);

	nDocs = xNode.nChildNode(XML_TAG_NODE_TRAKT);
	if(nDocs == 1)										
	{
		xDoc=xNode.getChildNode(XML_TAG_NODE_TRAKT);

		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Uppfdatum);
		if(numChild==1)
		{
			m_sDatum= xDoc.getChildNode(XML_TAG_TRAKT_Uppfdatum).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Region);
		if(numChild==1)
		{
			m_sRegion= xDoc.getChildNode(XML_TAG_TRAKT_Region).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Distrikt);
		if(numChild==1)
		{
			m_sDistrikt= xDoc.getChildNode(XML_TAG_TRAKT_Distrikt).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Typ);
		if(numChild==1)
		{
			m_sTyp= xDoc.getChildNode(XML_TAG_TRAKT_Typ).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Ursprung);
		if(numChild==1)
		{
			m_sUrsprung= xDoc.getChildNode(XML_TAG_TRAKT_Ursprung).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Objektsnummer);
		if(numChild==1)
		{
			m_sObj_nr= xDoc.getChildNode(XML_TAG_TRAKT_Objektsnummer).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Objektsnamn);
		if(numChild==1)
		{
			m_sObj_namn= xDoc.getChildNode(XML_TAG_TRAKT_Objektsnamn).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Areal);
		if(numChild==1)
		{
			m_sAreal= xDoc.getChildNode(XML_TAG_TRAKT_Areal).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Maskinnummer);
		if(numChild==1)
		{
			m_sMask_nr= xDoc.getChildNode(XML_TAG_TRAKT_Maskinnummer).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Entreprenor);
		if(numChild==1)
		{
			m_sEntr= xDoc.getChildNode(XML_TAG_TRAKT_Entreprenor).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Maskinstorlek);
		if(numChild==1)
		{
			m_sMask_storlek= xDoc.getChildNode(XML_TAG_TRAKT_Maskinstorlek).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Atgardstidpunkt3);
		if(numChild==1)
		{
			m_sAtg_tidp_nr= xDoc.getChildNode(XML_TAG_TRAKT_Atgardstidpunkt3).getText();	
		}
		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Atgardstidpunkt2);
		if(numChild==1)
		{
			m_sAtg_tidp_text= xDoc.getChildNode(XML_TAG_TRAKT_Atgardstidpunkt2).getText();	
		}



		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Maskinstorlek2);
		if(numChild==1)
		{
			m_n_trakt_maskinstorlek= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Maskinstorlek2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Myr2);
		if(numChild==1)
		{
			m_n_trakt_avslut_myr= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Myr2).getText());	
		}

		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Hallmark2);
		if(numChild==1)
		{
			m_n_trakt_avslut_hallmark= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Hallmark2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Hansyn2);
		if(numChild==1)
		{
			m_n_trakt_avslut_hansyn= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Hansyn2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_FdKulturmark2);
		if(numChild==1)
		{
			m_n_trakt_avslut_fdkulturmark= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_FdKulturmark2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Kulturmiljo2);
		if(numChild==1)
		{
			m_n_trakt_avslut_kulturmiljo= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Kulturmiljo2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Lovandel2);
		if(numChild==1)
		{
			m_n_trakt_avslut_lovandel= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Lovandel2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Skyddszon2);
		if(numChild==1)
		{
			m_n_trakt_avslut_skyddszon= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Skyddszon2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_NvTrad2);
		if(numChild==1)
		{
			m_n_trakt_avslut_nvtrad= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_NvTrad2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Framtidstrad2);
		if(numChild==1)
		{
			m_n_trakt_avslut_framtidstrad= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Framtidstrad2).getText());	
		}

		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Hogstubbar2);
		if(numChild==1)
		{
			m_n_trakt_avslut_hogstubbar= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Hogstubbar2).getText());	
		}

		numChild = xDoc.nChildNode(XML_TAG_TRAKT_DodaTrad2);
		if(numChild==1)
		{
			m_n_trakt_avslut_dodatrad= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_DodaTrad2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_MindreKorskada2);
		if(numChild==1)
		{
			m_n_trakt_avslut_mindrekorskada= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_MindreKorskada2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_AllvarligKorskada2);
		if(numChild==1)
		{
			m_n_trakt_avslut_allvarligkorskada= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_AllvarligKorskada2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Markskador2);
		if(numChild==1)
		{
			m_n_trakt_avslut_markskador= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Markskador2).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Markskador4);
		if(numChild==1)
		{
			m_n_trakt_avslut_markskador2= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Markskador4).getText());	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Nedskrapning2);
		if(numChild==1)
		{
			m_n_trakt_avslut_nedskrapning= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Nedskrapning2).getText());	
		}

		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Fritext);
		if(numChild==1)
		{
			m_s_trakt_avslut_fritext= xDoc.getChildNode(XML_TAG_TRAKT_Fritext).getText();	
		}


		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Atgardstidpunkt3);
		if(numChild==1)
		{
			m_n_trakt_avslut_atgardstp= _tstoi(xDoc.getChildNode(XML_TAG_TRAKT_Atgardstidpunkt3).getText());	
		}

		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Atgardstp_forklar);
		if(numChild==1)
		{
			m_s_trakt_avslut_atgardstp_forklaring= xDoc.getChildNode(XML_TAG_TRAKT_Atgardstp_forklar).getText();	
		}

		numChild = xDoc.nChildNode(XML_TAG_TRAKT_Slutford);
		if(numChild==1)
		{
			m_s_trakt_avslut_datum_slutford= xDoc.getChildNode(XML_TAG_TRAKT_Slutford).getText();	
		}



		n_NumAvds = xDoc.nChildNode(XML_TAG_NODE_AVDEL);
		if(n_NumAvds>0)
		{
			n_Ret=HXL_RecAvd(xDoc,n_NumAvds);
			HXL_Calculate_Trakt_Average();
		}
		else
			n_Ret=RETURN_ERR_AVD;
	}
	else
	{
		n_Ret=RETURN_ERR_TRAKT;
	}
	return n_Ret;
}

//--------------------------------------------------------------
// L�s in avdelningsvariabler
//--------------------------------------------------------------
int CXmlReader::HXL_RecAvd(XMLNode xNode,int n_NumAvds)
{
	int n_Docs=0,n_NumChild=0,n_P=0,n_NumPlots=0,n_Ret=RETURN_ERR_AVD,n_Iterator=0;
	XMLNode xAvd;

	if(n_NumAvds>0)
	{
		for(n_P=0;n_P<n_NumAvds;n_P++)
		{			
			xAvd=xNode.getChildNode(XML_TAG_NODE_AVDEL,&n_Iterator);
			if(xAvd.isEmpty() != 1)
			{
				n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Id);
				if(n_NumChild==1)
				{
					struct Avd_struct avdstr;
					avdstr.m_n_Id=-1;
					avdstr.m_n_compart_gallrtyp=-1;
					avdstr.m_n_compart_metod=-1;
					avdstr.m_s_compart_spec_and_si="";
					avdstr.m_n_compart_si_spec=-1;
					avdstr.m_n_compart_si=-1;
					avdstr.m_n_compart_lagsta_gy_enl_mall=-1;
					avdstr.m_n_compart_traktdir_gy_fore_m2ha=-1;
					avdstr.m_n_compart_traktdir_gy_efter_m2ha=-1;
					avdstr.m_n_compart_traktdir_ga_uttag=-1;
					avdstr.m_f_compart_gy_wdraw_forest=0.0;
					avdstr.m_f_compart_gy_wdraw_road=0.0;
					avdstr.m_f_compart_oh=0.0;
					avdstr.m_f_compart_areal=0.0;
					avdstr.m_f_compart_antal_stubbar=0.0;
					
					avdstr.mYta.clear();

					avdstr.m_n_Id=_tstoi(xAvd.getChildNode(XML_TAG_AVDEL_Id).getText());

					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Gallringstyp2);
					if(n_NumChild==1)
					{
						avdstr.m_n_compart_gallrtyp=_tstoi(xAvd.getChildNode(XML_TAG_AVDEL_Gallringstyp2).getText());
					}
					if(avdstr.m_n_compart_gallrtyp==-1)
					{
						n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Gallringstyp);
						if(n_NumChild==1)
						{
							CString s_compart_gallrtyp;
							s_compart_gallrtyp=xAvd.getChildNode(XML_TAG_AVDEL_Gallringstyp).getText();
							getAvdGallrTypIndexFromText(s_compart_gallrtyp,&avdstr);
						}

					}

					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Metod2);
					if(n_NumChild==1)
					{
						avdstr.m_n_compart_metod=_tstoi(xAvd.getChildNode(XML_TAG_AVDEL_Metod2).getText());
					}
					if(avdstr.m_n_compart_metod==-1)
					{
						n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Metod);
						if(n_NumChild==1)
						{
							CString s_compart_metod;
							s_compart_metod=xAvd.getChildNode(XML_TAG_AVDEL_Metod).getText();
							getAvdMetodIndexFromText(s_compart_metod,&avdstr);
						}

					}

					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_SI);
					if(n_NumChild>0)
					{
						avdstr.m_s_compart_spec_and_si=xAvd.getChildNode(XML_TAG_AVDEL_SI).getText();
					}

					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_SI_SPEC);
					if(n_NumChild==1)
					{
						avdstr.m_n_compart_si_spec=_tstoi(xAvd.getChildNode(XML_TAG_AVDEL_SI_SPEC).getText());
					}


					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_SI_NUM);
					if(n_NumChild==1)
					{
						avdstr.m_n_compart_si=_tstoi(xAvd.getChildNode(XML_TAG_AVDEL_SI_NUM).getText());
					}
					if(avdstr.m_n_compart_si_spec==-1 && avdstr.m_n_compart_si==-1)
					{
						getAvdSiSpecAndSiNum(&avdstr);
					}

					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_TraktDirLagstaGy);
					if(n_NumChild==1)
					{
						avdstr.m_n_compart_lagsta_gy_enl_mall=_tstoi(xAvd.getChildNode(XML_TAG_AVDEL_TraktDirLagstaGy).getText());
					}

					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_TraktDirGyfore);
					if(n_NumChild==1)
					{
						avdstr.m_n_compart_traktdir_gy_fore_m2ha=_tstoi(xAvd.getChildNode(XML_TAG_AVDEL_TraktDirGyfore).getText());
					}

					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_TraktDirGyefter);
					if(n_NumChild==1)
					{
						avdstr.m_n_compart_traktdir_gy_efter_m2ha=_tstoi(xAvd.getChildNode(XML_TAG_AVDEL_TraktDirGyefter).getText());
					}

					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_TraktDirGauttag);
					if(n_NumChild==1)
					{
						avdstr.m_n_compart_traktdir_ga_uttag=_tstoi(xAvd.getChildNode(XML_TAG_AVDEL_TraktDirGauttag).getText());
					}


					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Areal);
					if(n_NumChild==1)
					{
						avdstr.m_f_compart_areal=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Areal).getText());
					}
					
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Antal_Stubbar);
					if(n_NumChild==1)
					{
						avdstr.m_f_compart_antal_stubbar=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Antal_Stubbar).getText());
					}


					//---------------------------------------------------------------------------------------------------
					//V�gbredd1	0
					//#define thRWidth1			0
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Vagbredd);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thRWidth1]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Vagbredd).getText());
					}
					//V�gbredd2	1
					//#define thRWidth2			1
					avdstr.m_fAvdVars[thRWidth2]=0.0;
					//V�gavst1	2
					//#define thRDist1			2
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Vagavst);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thRDist1]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Vagavst).getText());
					}
					//V�gavst2	3
					//#define thRDist2			3
					avdstr.m_fAvdVars[thRDist2]=0.0;
					//V�gyta	4
					//#define thRArea				4
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Vagyta);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thRArea]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Vagyta).getText());
					}
					//�vreh�jd
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Ovrehojd);
					if(n_NumChild==1)
					{
						avdstr.m_f_compart_oh=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Ovrehojd).getText());
					}

					//#define thGYBefore			5
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Gyfore);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thGYBefore]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Gyfore).getText());
					}
					//#define thGYAfter			6
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Gyefter);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thGYAfter]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Gyefter).getText());
					}
					//#define thGYWDraw			7
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Gauttag);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thGYWDraw]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Gauttag).getText());
					}

					//Uttag i skog
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Gauttagiskog);
					if(n_NumChild==1)
					{
						avdstr.m_f_compart_gy_wdraw_forest=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Gauttagiskog).getText());
					}
					//Uttag i v�g
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Gauttagivag);
					if(n_NumChild==1)
					{
						avdstr.m_f_compart_gy_wdraw_road=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Gauttagivag).getText());
					}					

					//#define thGYQuota			8
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Gakvot);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thGYQuota]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Gakvot).getText());
					}
					//#define thStems				9
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Stammar);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thStems]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Stammar).getText());
					}
					//#define thAvgDiam			10
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Medeldiam);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thAvgDiam]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Medeldiam).getText());
					}
					//#define thH25				11
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_H25);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thH25]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_H25).getText());
					}
					//#define thVolPine			12
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_VolTall);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thVolPine]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_VolTall).getText());
					}
					//#define thVolSpruce			13
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_VolGran);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thVolSpruce]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_VolGran).getText());
					}
					//#define thVolBirch			14
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_VolLov);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thVolBirch]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_VolLov).getText());
					}
					//#define thVolCont			15
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_VolCont);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thVolCont]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_VolCont).getText());
					}
					//#define thVolTotal			16
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_VolTot);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thVolTotal]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_VolTot).getText());
					}
					//#define thPropPine			17
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_AndelTall);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thPropPine]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_AndelTall).getText());
					}
					//#define thPropSpruce		18
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_AndelGran);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thPropSpruce]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_AndelGran).getText());
					}
					//#define thPropBirch			19
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_AndelLov);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thPropBirch]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_AndelLov).getText());
					}
					//#define thPropCont			20
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_AndelCont);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thPropCont]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_AndelCont).getText());
					}
					//#define thDamaged			21
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Skadade);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thDamaged]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Skadade).getText());
					}
					//#define thTracks			22
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Sparbild);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thTracks]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Sparbild).getText());
					}
					//#define thHighStumps		23
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Hogast);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thHighStumps]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Hogast).getText());
					}
					//#define thGenomGallrat		24
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Genomgallrat);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thGenomGallrat]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Genomgallrat).getText());
					}
					//#define thTwigs				25
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Risning);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thTwigs]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Risning).getText());
					}
					//#define thForrensat			26
					n_NumChild = xAvd.nChildNode(XML_TAG_AVDEL_Forrensat);
					if(n_NumChild==1)
					{
						avdstr.m_fAvdVars[thForrensat]=my_atof(xAvd.getChildNode(XML_TAG_AVDEL_Forrensat).getText());
					}
					//---------------------------------------------------------------------------------------------------

					Avdelningar.resize(Avdelningar.size()+1,avdstr);

					n_NumPlots = xAvd.nChildNode(XML_TAG_NODE_YTA);
					if(n_NumPlots>0)
						n_Ret=HXL_RecPlots(xAvd,n_NumPlots);
				}
			}
		}

	}
	return n_Ret;
}

void CXmlReader::getAvdGallrTypIndexFromText(CString typ_text,AVDSTRUCT *Avdstr)
{
	CString m_sTyp1Text=_T("");
	CString m_sTyp2Text=_T("");
	CString m_sTyp1Num=_T("");
	CString m_sTyp2Num=_T("");
	RLFReader *xml = new RLFReader;
	if (xml->Load(m_sLangFN))
	{
		m_sTyp1Text = xml->str(IDS_STRING6005);
		m_sTyp2Text = xml->str(IDS_STRING6006);
		m_sTyp1Num = xml->str(IDS_STRING6007);
		m_sTyp2Num = xml->str(IDS_STRING6008);
	}	// if (xml->Load(m_sLangFN))
	delete xml;

	if(typ_text.Compare(m_sTyp1Text) == 0)
	{
		Avdstr->m_n_compart_gallrtyp=_tstoi(m_sTyp1Num);
	}
	else
	{
		if(typ_text.Compare(m_sTyp2Text) == 0)
		{
			Avdstr->m_n_compart_gallrtyp=_tstoi(m_sTyp2Num);
		}
	}

}

void CXmlReader::getAvdMetodIndexFromText(CString meth_text,AVDSTRUCT *Avdstr)
{
	CString m_sTyp1Text=_T("");
	CString m_sTyp2Text=_T("");
	CString m_sTyp1Num=_T("");
	CString m_sTyp2Num=_T("");
	RLFReader *xml = new RLFReader;
	if (xml->Load(m_sLangFN))
	{
		m_sTyp1Text = xml->str(IDS_STRING6001);
		m_sTyp2Text = xml->str(IDS_STRING6002);
		m_sTyp1Num = xml->str(IDS_STRING6003);
		m_sTyp2Num = xml->str(IDS_STRING6004);
	}	// if (xml->Load(m_sLangFN))
	delete xml;

	if(meth_text.Compare(m_sTyp1Text) == 0)
	{
		Avdstr->m_n_compart_metod=_tstoi(m_sTyp1Num);
	}else{
	if(meth_text.Compare(m_sTyp2Text) == 0)
	{
		Avdstr->m_n_compart_metod=_tstoi(m_sTyp2Num);
	}}

}

void CXmlReader::getAvdSiSpecAndSiNum(AVDSTRUCT *strAvd)
{
	int n_Spec=0,n_Num=0;
	CStringA s_Test="";
	if(strAvd->m_s_compart_spec_and_si[0]=='T')
		n_Spec=0;
	else
	{
	if(strAvd->m_s_compart_spec_and_si[0]=='G')
		n_Spec=1;
	}
	strAvd->m_n_compart_si_spec=n_Spec;

	s_Test.Format("%s",strAvd->m_s_compart_spec_and_si);
	s_Test=s_Test.Right(s_Test.GetLength()-1);
	n_Num=atoi(s_Test);
	if(n_Num>0)
		strAvd->m_n_compart_si=n_Num;
	else 
		strAvd->m_n_compart_si =0;
}
//--------------------------------------------------------------
// L�s in avdelningsvariabler
//--------------------------------------------------------------
int CXmlReader::HXL_RecPlots(XMLNode xNode,int n_NumPlots)
{
	int n_Docs=0,n_NumChild=0,n_P=0,n_Ret=RETURN_ERR_PLOT,n_Iterator=0;
	//int n_YtVarNr=0;
	YTSTRUCT strYta;
	XMLNode xPlot;

	if(n_NumPlots>0)
	{
		for(n_P=0;n_P<n_NumPlots;n_P++)
		{			
			strYta.m_n_Id=-1;

			memset(&strYta.m_f_YtVars,0,sizeof(strYta.m_f_YtVars));

			//n_YtVarNr=0;
			xPlot=xNode.getChildNode(XML_TAG_NODE_YTA,&n_Iterator);
			if(xPlot.isEmpty() != 1)
			{
				//Ytnummer
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Nr);
				if(n_NumChild==1)
				{
					strYta.m_n_Id=_tstoi(xPlot.getChildNode(XML_TAG_YTA_Nr).getText());
				}
				//V�gbredd1	0
				//#define thRWidth1			0
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Vagbredd);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thRWidth1]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Vagbredd).getText());				
				}
				//V�gbredd2	1
				//#define thRWidth2			1
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Vagbredd2);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thRWidth2]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Vagbredd2).getText());
				}
				//V�gavst1	2
				//#define thRDist1			2
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Vagavst);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thRDist1]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Vagavst).getText());
				}
				//V�gavst2	3
				//#define thRDist2			3
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Vagavst2);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thRDist2]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Vagavst2).getText());
				}
				//V�gyta	4
				//#define thRArea				4
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Vagyta);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thRArea]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Vagyta).getText());
				}
				//#define thGYBefore			5
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Gyfore);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thGYBefore]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Gyfore).getText());
				}
				//#define thGYAfter			6
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Gyefter);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thGYAfter]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Gyefter).getText());
				}
				//#define thGYWDraw			7
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Gauttag);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thGYWDraw]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Gauttag).getText());
				}
				//#define thGYQuota			8
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Gakvot);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thGYQuota]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Gakvot).getText());
				}
				//#define thStems				9
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Stammar);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thStems]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Stammar).getText());
				}
				//#define thAvgDiam			10
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Medeldiam);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thAvgDiam]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Medeldiam).getText());
				}
				//#define thH25				11
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_H25);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thH25]=my_atof(xPlot.getChildNode(XML_TAG_YTA_H25).getText());
				}
				//#define thVolPine			12
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_VolTall);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thVolPine]=my_atof(xPlot.getChildNode(XML_TAG_YTA_VolTall).getText());
				}
				//#define thVolSpruce			13
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_VolGran);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thVolSpruce]=my_atof(xPlot.getChildNode(XML_TAG_YTA_VolGran).getText());
				}
				//#define thVolBirch			14
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_VolLov);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thVolBirch]=my_atof(xPlot.getChildNode(XML_TAG_YTA_VolLov).getText());
				}
				//#define thVolCont			15
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_VolCont);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thVolCont]=my_atof(xPlot.getChildNode(XML_TAG_YTA_VolCont).getText());
				}
				//#define thVolTotal			16
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_VolTot);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thVolTotal]=my_atof(xPlot.getChildNode(XML_TAG_YTA_VolTot).getText());
				}
				//#define thPropPine			17
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_AndelTall);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thPropPine]=my_atof(xPlot.getChildNode(XML_TAG_YTA_AndelTall).getText());
				}
				//#define thPropSpruce		18
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_AndelGran);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thPropSpruce]=my_atof(xPlot.getChildNode(XML_TAG_YTA_AndelGran).getText());
				}
				//#define thPropBirch			19
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_AndelLov);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thPropBirch]=my_atof(xPlot.getChildNode(XML_TAG_YTA_AndelLov).getText());
				}
				//#define thPropCont			20
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_AndelCont);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thPropCont]=my_atof(xPlot.getChildNode(XML_TAG_YTA_AndelCont).getText());
				}
				//#define thDamaged			21
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Skadade);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thDamaged]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Skadade).getText());
				}
				//#define thTracks			22
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Sparbild);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thTracks]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Sparbild).getText());
				}
				//#define thHighStumps		23
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Hogast);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thHighStumps]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Hogast).getText());
				}
				//#define thGenomGallrat		24
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Genomgallrat);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thGenomGallrat]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Genomgallrat).getText());
				}
				//#define thTwigs				25
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Risning);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thTwigs]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Risning).getText());
				}

				//#define thForrensat			26
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Forrensat);
				if(n_NumChild==1)
				{
					strYta.m_f_YtVars[thForrensat]=my_atof(xPlot.getChildNode(XML_TAG_YTA_Forrensat).getText());
				}


				//int m_n_Ytradie;		
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Ytradie);
				if(n_NumChild==1)
				{
					strYta.m_n_Ytradie=_tstoi(xPlot.getChildNode(XML_TAG_YTA_Ytradie).getText());
				}

				//int m_n_AntalTrad;
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_AntalTrad);
				if(n_NumChild==1)
				{
					strYta.m_n_AntalTrad=_tstoi(xPlot.getChildNode(XML_TAG_YTA_AntalTrad).getText());
				}
				//int m_n_AntalStubbar;
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_AntalStubbar);
				if(n_NumChild==1)
				{
					strYta.m_n_AntalStubbar=_tstoi(xPlot.getChildNode(XML_TAG_YTA_AntalStubbar).getText());
				}
				//int m_n_AntalStubbarivag;
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_AntalStubbarivag);
				if(n_NumChild==1)
				{
					strYta.m_n_AntalStubbarivag=_tstoi(xPlot.getChildNode(XML_TAG_YTA_AntalStubbarivag).getText());
				}
				//int m_n_AntalStubbariskog;
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_AntalStubbariskog);
				if(n_NumChild==1)
				{
					strYta.m_n_AntalStubbariskog=_tstoi(xPlot.getChildNode(XML_TAG_YTA_AntalStubbariskog).getText());
				}
				//int m_n_Totdiamkvarvarande;
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Totdiamkvarvarande);
				if(n_NumChild==1)
				{
					strYta.m_n_Totdiamkvarvarande=_tstoi(xPlot.getChildNode(XML_TAG_YTA_Totdiamkvarvarande).getText());
				}
				//int m_n_Totdiamuttag;
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_Totdiamuttag);
				if(n_NumChild==1)
				{
					strYta.m_n_Totdiamuttag=_tstoi(xPlot.getChildNode(XML_TAG_YTA_Totdiamuttag).getText());
				}
				//int m_n_AntalHogaStubbar;
				n_NumChild = xPlot.nChildNode(XML_TAG_YTA_AntalHogaStubbar);
				if(n_NumChild==1)
				{
					strYta.m_n_AntalHogaStubbar=_tstoi(xPlot.getChildNode(XML_TAG_YTA_AntalHogaStubbar).getText());
				}


				Avdelningar[Avdelningar.size()-1].mYta.resize(Avdelningar[Avdelningar.size()-1].mYta.size()+1,strYta);
				n_Ret=1;
			}
		}
	}
	return n_Ret;
}


//--------------------------------------------------------------
// Ber�kna traktmedel
//--------------------------------------------------------------
void CXmlReader::HXL_Calculate_Trakt_Average(void)
{

	int found=0,size=Avdelningar.size(),avdI=0,ytaI=0,n_Var=0;
	double n_NumOf[NUM_OF_YT_VARS];
	int n_NumOfSi[2],n_SumSi[2];
	double d_SumVar[NUM_OF_YT_VARS];
	double d_Value=0.0,d_Area=0.0,d_Gy=0.0,d_Vol=0.0,d_Stems=0.0,d_Stubbar=0.0;

	CVariables cVar;
	for(int i=0;i<NUM_OF_YT_VARS;i++)
	{
	n_NumOf[i]=0.0;
	d_SumVar[i]=0.0;
	}
	n_NumOfSi[0]=0;
	n_NumOfSi[1]=0;
	n_SumSi[0]=0;
	n_SumSi[1]=0;
	if(size>0)
	{
		for(avdI=0;avdI<size;avdI++)
		{
			n_NumOfSi[Avdelningar[avdI].m_n_compart_si_spec]++;
			n_SumSi[Avdelningar[avdI].m_n_compart_si_spec]+=Avdelningar[avdI].m_n_compart_si;

			/*
Variabel		Traktdel						Trakt		
Areal			Delareal						Traktareal	
�HSI			Enligt klave					Inget		
Bredd			Medel mellan ytor				Arealv�gt mellan traktdelar
Avst			Medel  mellan ytor				-�-			
Yta%			Medel  mellan ytor				-�-			
GY f�re			Medel  mellan ytor				-�-			
GY efter		Medel  mellan ytor				-�-			
Uttag%			Summerat mellan ytor och delat	-�-			
Uttag skog		Summerat mellan ytor och delat	-�-			
Uttag v�g		Summerat mellan ytor och delat	-�-		
Kvot			Medel  mellan ytor (som idag)	-�-
Stam/ha			Medel  mellan ytor				-�-
DGV				Grundytev�gt mellan ytor		Grundytev�gt mellan traktdelar
H 25			Grundytev�gt mellan ytor		Grundytev�gt mellan traktdelar
Vol/tr�dslag	Medel mellan ytor				Volymv�gt mellan traktdelar
Tr�dslbl%		Grundytev�gt mellan ytor		-�-
Skad%			Ant skadade/tot klavstam		Stamv�gt mellan traktdelar
Sp�r m			Medel mellan ytor				Arealv�gt mellan traktdelar
H�ga stubb%		% av antal stubb				Stubbv�gt mellan traktdelar
Genomgall		% av ytor						Arealv�gt mellan traktdelar
Risning			% av ytor						Arealv�gt mellan traktdelar
F�rrensat		% av ytor						Arealv�gt mellan traktdelar
*/
			d_Area=Avdelningar[avdI].m_f_compart_areal;
			d_Stubbar=Avdelningar[avdI].m_f_compart_antal_stubbar;
			d_Gy=Avdelningar[avdI].m_fAvdVars[thGYAfter];
			d_Vol=Avdelningar[avdI].m_fAvdVars[thVolTotal];
			d_Stems=Avdelningar[avdI].m_fAvdVars[thStems];
			for(n_Var=0;n_Var<NUM_OF_YT_VARS;n_Var++)
			{
				d_Value=Avdelningar[avdI].m_fAvdVars[n_Var];								
				switch(n_Var)
				{
				//Arealv�gda
				case thRWidth1: 
				case thRWidth2:
				case thRDist1:
				case thRDist2:
				case thRArea:
				case thGYBefore:
				case thGYAfter:
				case thGYWDraw:
				case thGYQuota:
				case thStems:
				case thTracks:
				case thGenomGallrat:
				case thTwigs:
				case thForrensat:
					n_NumOf[n_Var]+=d_Area;
					d_SumVar[n_Var]+=d_Value*d_Area;	
					break;
				case thAvgDiam:
				case thH25:
					n_NumOf[n_Var]+=d_Gy;
					d_SumVar[n_Var]+=d_Value*d_Gy;	
					break;
					//Volymsv�gda
				case thVolPine:
				case thVolSpruce:
				case thVolBirch:
				case thVolCont:
				case thVolTotal:
				case thPropPine:
				case thPropSpruce:
				case thPropBirch:
				case thPropCont:
					n_NumOf[n_Var]+=d_Vol;
					d_SumVar[n_Var]+=d_Value*d_Vol;	
					break;
					//Antal stamv�gda
				case thDamaged:
					n_NumOf[n_Var]+=d_Stems;
					d_SumVar[n_Var]+=d_Value*d_Stems;	
					break;
					//Antal stubbv�gda
				case thHighStumps:
					n_NumOf[n_Var]+=d_Stubbar;
					d_SumVar[n_Var]+=d_Value*d_Stubbar;	
					break;
				}
			}

			/*
			storlek=Avdelningar[avdI].mYta.size();
			if(storlek!=0)
			{
				for(ytaI=0;ytaI<storlek;ytaI++)
				{	
					d_num_of_stumps+=Avdelningar[avdI].mYta[ytaI].m_n_AntalStubbar;
					for(n_Var=0;n_Var<NUM_OF_YT_VARS;n_Var++)
					{
						d_Value=Avdelningar[avdI].mYta[ytaI].m_f_YtVars[n_Var];
						if(d_Value>0.0)
						{
							switch(n_Var)
							{
							case thDamaged: //Specialare f�r skadade tr�d, v�gs med antal stam per ha
								n_NumOf[thDamaged]+=Avdelningar[avdI].mYta[ytaI].m_f_YtVars[thStems];
								d_SumVar[thDamaged]+=d_Value*Avdelningar[avdI].mYta[ytaI].m_f_YtVars[thStems];
								break;
							case thAvgDiam: //Specialare f�r medeldiameter, v�gs med gy efter per ha eftersom det �r en dgv
								n_NumOf[thAvgDiam]+=Avdelningar[avdI].mYta[ytaI].m_f_YtVars[thGYAfter];
								d_SumVar[thAvgDiam]+=d_Value*Avdelningar[avdI].mYta[ytaI].m_f_YtVars[thGYAfter];							
								break;
							default:
								n_NumOf[n_Var]+=1.0;
								d_SumVar[n_Var]+=d_Value;
								break;
							}
						}
					}
				}
			}*/
		}
	}

	if(n_NumOfSi[0]>n_NumOfSi[1])
	{
	m_n_trakt_calc_si_spec=0;
	m_n_trakt_calc_si=n_SumSi[0]/n_NumOfSi[0];
	}
	else
	{
	m_n_trakt_calc_si_spec=1;
	m_n_trakt_calc_si=n_SumSi[1]/n_NumOfSi[1];
	}

	//V�gbredd	0
	if(n_NumOf[thRWidth1]>0.0)
	{
		d_SumVar[thRWidth1]/=n_NumOf[thRWidth1];
	}
	//V�gavst�nd	2
	if(n_NumOf[thRDist1]>0.0)
	{
		d_SumVar[thRDist1]/=n_NumOf[thRDist1];
	}
	//V�gyta		4
	if(d_SumVar[thRDist1]>0)
	{
		d_SumVar[thRArea]=100.0*(d_SumVar[thRWidth1]/d_SumVar[thRDist1]);
	}
	//Gy f�re	5
	if(n_NumOf[thGYBefore]>0.0)
	{
		d_SumVar[thGYBefore]/=n_NumOf[thGYBefore];
	}
	//Gy efter	6
	if(n_NumOf[thGYAfter]>0.0)
	{
		d_SumVar[thGYAfter]/=n_NumOf[thGYAfter];
	}
	//Ga uttag % 7
	if(d_SumVar[thGYBefore]>0.0)
		d_SumVar[thGYWDraw]=100.0*(d_SumVar[thGYBefore]-d_SumVar[thGYAfter])/d_SumVar[thGYBefore];
	//Gakvot	8
	if(n_NumOf[thGYQuota]>0.0)
	{
		d_SumVar[thGYQuota]/=n_NumOf[thGYQuota];
	}	
	//Stammar	9
	if(n_NumOf[thStems]>0.0)
	{
		d_SumVar[thStems]/=n_NumOf[thStems];
	}
	//Medeldiam	10
	if(n_NumOf[thAvgDiam]>0.0)
	{
		d_SumVar[thAvgDiam]/=n_NumOf[thAvgDiam];
	}
	//H25	11
	if(n_NumOf[thH25]>0.0)
	{
		d_SumVar[thH25]/=n_NumOf[thH25];
	}
	//Vol Tall	12
	if(n_NumOf[thVolPine]>0.0)
	{
		d_SumVar[thVolPine]/=n_NumOf[thVolPine];
	}
	//Vol Gran	13
	if(n_NumOf[thVolSpruce]>0.0)
	{
		d_SumVar[thVolSpruce]/=n_NumOf[thVolSpruce];
	}
	//Vol L�v	14
	if(n_NumOf[thVolBirch]>0.0)
	{
		d_SumVar[thVolBirch]/=n_NumOf[thVolBirch];
	}
	//Vol Contorta	15
	if(n_NumOf[thVolCont]>0.0)
	{
		d_SumVar[thVolCont]/=n_NumOf[thVolCont];
	}
	//Vol Tot	16
	if(n_NumOf[thVolTotal]>0.0)
	{
		d_SumVar[thVolTotal]/=n_NumOf[thVolTotal];
	}
	//Andel Tall	17
	if(n_NumOf[thVolTotal]>0.0)
	{
		d_SumVar[thPropPine]=100.0*d_SumVar[thVolPine]/d_SumVar[thVolTotal];
	}
	//Andel Gran	18
	if(n_NumOf[thVolTotal]>0.0)
	{
		d_SumVar[thPropSpruce]=100.0*d_SumVar[thVolSpruce]/d_SumVar[thVolTotal];
	}
	//Andel L�v	19
	if(n_NumOf[thVolTotal]>0.0)
	{
		d_SumVar[thPropBirch]=100.0*d_SumVar[thVolBirch]/d_SumVar[thVolTotal];
	}
	//Andel Contorta	20
	if(n_NumOf[thVolTotal]>0.0)
	{
		d_SumVar[thPropCont]=100.0*d_SumVar[thVolCont]/d_SumVar[thVolTotal];
	}
	//Skadade	21
	//Antal skadade stammar 
	if(n_NumOf[thDamaged]>0.0)
	{
		d_SumVar[thDamaged]/=n_NumOf[thDamaged];
	}		
	//Sp�rbildning	22
	if(n_NumOf[thTracks]>0.0)
	{
		d_SumVar[thTracks]/=n_NumOf[thTracks];
	}		
	//H�ga stubbar andel av totala antal stubbar	23
	if(n_NumOf[thHighStumps]>0.0)
	{
		d_SumVar[thHighStumps]/=n_NumOf[thHighStumps];
	}		
	//Genomgallrat	24
	if(n_NumOf[thGenomGallrat]>0.0)
	{
		d_SumVar[thGenomGallrat]/=n_NumOf[thGenomGallrat];
	}		
	//Risning	25
	if(n_NumOf[thTwigs]>0.0)
	{
		d_SumVar[thTwigs]/=n_NumOf[thTwigs];
	}		
	//F�rrensat	26
	if(n_NumOf[thForrensat]>0.0)
	{
		d_SumVar[thForrensat]/=n_NumOf[thForrensat];
	}		

	for(n_Var=0;n_Var<NUM_OF_YT_VARS;n_Var++)
	{
		m_fTrakt_Medel[n_Var]= d_SumVar[n_Var];
	}
}