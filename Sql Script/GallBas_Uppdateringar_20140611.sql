
use "databasnamn"

--       Trakter 

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table' and column_name = 'trakt_maskinstorlek')  
alter table trakt_table add trakt_maskinstorlek int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table' and column_name = 'trakt_avslut_myr')  
alter table trakt_table add trakt_avslut_myr int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table' and column_name = 'trakt_avslut_hallmark')  
alter table trakt_table add trakt_avslut_hallmark int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table' and column_name = 'trakt_avslut_hansyn')  
alter table trakt_table add trakt_avslut_hansyn int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_fdkulturmark')  
alter table trakt_table add trakt_avslut_fdkulturmark int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_kulturmiljo')  
alter table trakt_table add trakt_avslut_kulturmiljo int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_lovandel')  
alter table trakt_table add trakt_avslut_lovandel int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_skyddszon')  
alter table trakt_table add trakt_avslut_skyddszon int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_nvtrad')  
alter table trakt_table add trakt_avslut_nvtrad int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_framtidstrad')  
alter table trakt_table add trakt_avslut_framtidstrad int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_hogstubbar')  
alter table trakt_table add trakt_avslut_hogstubbar int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_dodatrad')  
alter table trakt_table add trakt_avslut_dodatrad int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_mindrekorskada')  
alter table trakt_table add trakt_avslut_mindrekorskada int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_allvarligkorskada')  
alter table trakt_table add trakt_avslut_allvarligkorskada int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_markskador')  
alter table trakt_table add trakt_avslut_markskador int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_markskador2')  
alter table trakt_table add trakt_avslut_markskador2 int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_nedskrapning')  
alter table trakt_table add trakt_avslut_nedskrapning int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_fritext')  
alter table trakt_table add trakt_avslut_fritext nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_atgardstp')  
alter table trakt_table add trakt_avslut_atgardstp int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_atgardstp_forklaring')  
alter table trakt_table add trakt_avslut_atgardstp_forklaring nvarchar(50)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_avslut_datum_slutford')  
alter table trakt_table add trakt_avslut_datum_slutford nvarchar(8)


if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_calc_si_spec')  
alter table trakt_table add trakt_calc_si_spec int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_calc_si')  
alter table trakt_table add trakt_calc_si int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'trakt_table'  and column_name = 'trakt_forrensat')  
alter table trakt_table add trakt_forrensat float


--       Avdelning

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_gallrtyp')  
alter table compart_table add compart_gallrtyp int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_metod')  
alter table compart_table add compart_metod int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_spec_and_si')  
alter table compart_table add compart_spec_and_si nvarchar(5)

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_si_spec')  
alter table compart_table add compart_si_spec int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_si')  
alter table compart_table add compart_si int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_lagsta_gy_enl_mall')  
alter table compart_table add compart_lagsta_gy_enl_mall int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_traktdir_gy_fore_m2ha')  
alter table compart_table add compart_traktdir_gy_fore_m2ha int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_traktdir_gy_efter_m2ha')  
alter table compart_table add compart_traktdir_gy_efter_m2ha int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_traktdir_ga_uttag')  
alter table compart_table add compart_traktdir_ga_uttag int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_gy_wdraw_forest')  
alter table compart_table add compart_gy_wdraw_forest float

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_gy_wdraw_road')  
alter table compart_table add compart_gy_wdraw_road float

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_oh')  
alter table compart_table add compart_oh float

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_areal')  
alter table compart_table add compart_areal float

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_antal_stubbar')  
alter table compart_table add compart_antal_stubbar float

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'compart_table' and column_name = 'compart_forrensat')  
alter table compart_table add compart_forrensat float


--	Ytor

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'plot_table' and column_name = 'plot_Ytradie')  
alter table plot_table add plot_Ytradie int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'plot_table' and column_name = 'plot_AntalTrad')  
alter table plot_table add plot_AntalTrad int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'plot_table' and column_name = 'plot_AntalStubbar')  
alter table plot_table add plot_AntalStubbar int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'plot_table' and column_name = 'plot_AntalStubbarivag')  
alter table plot_table add plot_AntalStubbarivag int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'plot_table' and column_name = 'plot_AntalStubbariskog')  
alter table plot_table add plot_AntalStubbariskog int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'plot_table' and column_name = 'plot_Totdiamkvarvarande')  
alter table plot_table add plot_Totdiamkvarvarande int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'plot_table' and column_name = 'plot_Totdiamuttag')  
alter table plot_table add plot_Totdiamuttag int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'plot_table' and column_name = 'plot_AntalHogaStubbar')  
alter table plot_table add plot_AntalHogaStubbar int

if not exists (select column_name from INFORMATION_SCHEMA.columns 
where table_name = 'plot_table' and column_name = 'plot_forrensat')  
alter table plot_table add plot_forrensat float