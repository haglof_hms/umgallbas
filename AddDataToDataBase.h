#if !defined(AFX_ADDDATATODATABASE_H)
#define AFX_ADDDATATODATABASE_H

#include "StdAfx.h"
#include "XmlReader.h"
//#include "ParseTextFile.h"
#include "DBBaseClass_SQLApi.h"	// ... in DBTransaction_lib


double my_atof(LPCTSTR var);

// Setup prototypes for functioncall to GallBas_dll.dll (Jonas �.); 061003 p�d
typedef int (WINAPI *Func_OpenFile)(char *);

typedef int (WINAPI *Func_GetAntYt)(long avd);							// Argument = Compartment number
typedef int (WINAPI *Func_GetYtVar)(long avd,int yta,char *arr[]);		// Uses 26 items in array

typedef int (WINAPI *Func_GetAntAvd)();									// Returns number of compartments
typedef int (WINAPI *Func_GetAvdId)(int index,long *compart_num);		// Get id's for plots
typedef int (WINAPI *Func_GetAvdVar)(long compart_num,char *arr[]);		// Uses 26 items in array

typedef int (WINAPI *Func_GetTraktVar)(char *arr[]);					// Uses 26 items in array
typedef int (WINAPI *Func_GetTraktHead)(char *arr[]);					// Uses 10 items in array

// Setup name of function in GallBas_dll.dll (Jons �.); 061003 p�d

//#define FUNC_OPEN_FILE		"GallBas_OpenFile"
//#define FUNC_GET_TRAKT_HEAD	"GetTraktHead"
//#define FUNC_GET_TRAKT_VAR	"GetTraktVar"
//#define FUNC_GET_AVD_VAR	"GetAvdVar"
//#define FUNC_GET_AVD_ID		"GetAvdId"
//#define FUNC_GET_ANT_AVD	"GetAntAvd"
//#define FUNC_GET_YT_VAR		"GetYtVar"
//#define FUNC_GET_ANT_YT		"GetAntYt"

// Define type of value in array for Trakt header; 061003 p�d
#define thDATE				0
#define thDISTRICT			1
#define thAREAL				2
#define thTRAKT_NUM			3
#define thMACHINE_NUM		4
#define thOBJECT_NAME		5
#define thCONTRACTOR		6
#define thREGION			7
#define thTYPE				8
#define thORIGIN			9
#define th_trakt_avslut_fritext	10
#define th_trakt_avslut_atgardstp_forklaring	11
#define th_trakt_avslut_datum_slutford			12

#define th_trakt_maskinstorlek				0
#define th_trakt_avslut_myr					1
#define th_trakt_avslut_hallmark			2
#define th_trakt_avslut_hansyn				3
#define th_trakt_avslut_fdkulturmark		4
#define th_trakt_avslut_kulturmiljo			5
#define th_trakt_avslut_lovandel			6
#define th_trakt_avslut_skyddszon			7
#define th_trakt_avslut_nvtrad				8
#define th_trakt_avslut_framtidstrad		9
#define th_trakt_avslut_hogstubbar			10
#define th_trakt_avslut_dodatrad			11
#define th_trakt_avslut_mindrekorskada		12
#define th_trakt_avslut_allvarligkorskada	13
#define th_trakt_avslut_markskador			14
#define th_trakt_avslut_markskador2			15
#define th_trakt_avslut_nedskrapning		16
#define th_trakt_avslut_atgardstp			17
#define th_trakt_calc_si_spec				18
#define th_trakt_calc_si					19

#define TH_LEVEL_TRAKT			0
#define TH_LEVEL_COMPARTMENT	1
#define TH_LEVEL_PLOT			2

// Define type of value in array for Variables; 061005 p�d

#define thRWidth1			0
#define thRWidth2			1
#define thRDist1			2
#define thRDist2			3
#define thRArea				4
#define thGYBefore			5
#define thGYAfter			6
#define thGYWDraw			7
#define thGYQuota			8
#define thStems				9
#define thAvgDiam			10
#define thH25				11
#define thVolPine			12
#define thVolSpruce			13
#define thVolBirch			14
#define thVolCont			15
#define thVolTotal			16
#define thPropPine			17
#define thPropSpruce		18
#define thPropBirch			19
#define thPropCont			20
#define thDamaged			21
#define thTracks			22
#define thHighStumps		23
#define thGenomGallrat		24
#define thTwigs				25
#define	thForrensat			26

// Define name of GallBas data file reading dll; 061003 p�d

#define GALLBAS_DLL_FILENAME	_T("GallBas_Dll.dll")


// Setup classes to be used to hold data from data files; 061003 p�d


// CTraktHeader; holds information about region,district, machine etc.
class CTraktHeader
{
	CString sDate;
	CString sDistrict;
	double fAreal;
	CString sTraktNum;
	CString sMachineNum;
	CString sObjectName;
	CString sContractor;
	CString sRegion;
	CString sType;
	CString sOrigin;

	int m_n_trakt_maskinstorlek;
	int m_n_trakt_avslut_myr;
	int m_n_trakt_avslut_hallmark;
	int m_n_trakt_avslut_hansyn;
	int m_n_trakt_avslut_fdkulturmark;
	int m_n_trakt_avslut_kulturmiljo;
	int m_n_trakt_avslut_lovandel;
	int m_n_trakt_avslut_skyddszon;
	int m_n_trakt_avslut_nvtrad;
	int m_n_trakt_avslut_framtidstrad;
	int m_n_trakt_avslut_hogstubbar;
	int m_n_trakt_avslut_dodatrad;
	int m_n_trakt_avslut_mindrekorskada;
	int m_n_trakt_avslut_allvarligkorskada;
	int m_n_trakt_avslut_markskador;
	int m_n_trakt_avslut_markskador2;
	int m_n_trakt_avslut_nedskrapning;
	CString m_s_trakt_avslut_fritext;
	int m_n_trakt_avslut_atgardstp;
	CString m_s_trakt_avslut_atgardstp_forklaring;
	CString m_s_trakt_avslut_datum_slutford;
	int m_n_trakt_calc_si_spec;
	int m_n_trakt_calc_si;



public:
	CTraktHeader(void)
	{
		sDate			= _T("");
		sDistrict		= _T("");
		fAreal			= 0.0;
		sTraktNum		= _T("");
		sMachineNum		= _T("");
		sObjectName		= _T("");
		sContractor		= _T("");
		sRegion			= _T("");
		sType			= _T("");
		sOrigin			= _T("");

	m_n_trakt_maskinstorlek					= -1;
	m_n_trakt_avslut_myr					= -1;
	m_n_trakt_avslut_hallmark				= -1;
	m_n_trakt_avslut_hansyn					= -1;
	m_n_trakt_avslut_fdkulturmark			= -1;
	m_n_trakt_avslut_kulturmiljo			= -1;
	m_n_trakt_avslut_lovandel				= -1;
	m_n_trakt_avslut_skyddszon				= -1;
	m_n_trakt_avslut_nvtrad					= -1;
	m_n_trakt_avslut_framtidstrad			= -1;
	m_n_trakt_avslut_dodatrad				= -1;
	m_n_trakt_avslut_hogstubbar				= -1;
	m_n_trakt_avslut_mindrekorskada			= -1;
	m_n_trakt_avslut_allvarligkorskada		= -1;
	m_n_trakt_avslut_markskador				= -1;
	m_n_trakt_avslut_markskador2			= -1;
	m_n_trakt_avslut_nedskrapning			= -1;
	m_s_trakt_avslut_fritext				= _T("");
	m_n_trakt_avslut_atgardstp				= -1;
	m_s_trakt_avslut_atgardstp_forklaring	= _T("");
	m_s_trakt_avslut_datum_slutford			= _T("");
	m_n_trakt_calc_si_spec					= -1;
	m_n_trakt_calc_si						= -1;

	}

	CTraktHeader(LPCTSTR date,
							 LPCTSTR dist,
							 double areal,
							 LPCTSTR trakt_num,
							 LPCTSTR machine_num,
							 LPCTSTR obj_name,
							 LPCTSTR contr,
							 LPCTSTR region,
							 LPCTSTR type,
							 LPCTSTR origin,
							 int n_trakt_maskinstorlek,
							 int n_trakt_avslut_myr,
							 int n_trakt_avslut_hallmark,
							 int n_trakt_avslut_hansyn,
							 int n_trakt_avslut_fdkulturmark,
							 int n_trakt_avslut_kulturmiljo,
							 int n_trakt_avslut_lovandel,
							 int n_trakt_avslut_skyddszon,
							 int n_trakt_avslut_nvtrad,
							 int n_trakt_avslut_framtidstrad,
							 int n_trakt_avslut_hogstubbar,
							 int n_trakt_avslut_dodatrad,
							 int n_trakt_avslut_mindrekorskada,
							 int n_trakt_avslut_allvarligkorskada,
							 int n_trakt_avslut_markskador,
							 int n_trakt_avslut_markskador2,
							 int n_trakt_avslut_nedskrapning,
							 LPCTSTR s_trakt_avslut_fritext,
							 int n_trakt_avslut_atgardstp,
							 LPCTSTR s_trakt_avslut_atgardstp_forklaring,
							 LPCTSTR s_trakt_avslut_datum_slutford,
							 int n_trakt_calc_si_spec,
							 int n_trakt_calc_si
							 )
	{
		sDate			= date;
		sDistrict		= dist;
		fAreal			= areal;
		sTraktNum		= trakt_num;
		sMachineNum		= machine_num;
		sObjectName		= obj_name;
		sContractor		= contr;
		sRegion			= region;
		sType			= type;
		sOrigin			= origin;
		m_n_trakt_maskinstorlek					= n_trakt_maskinstorlek;
		m_n_trakt_avslut_myr					= n_trakt_avslut_myr;
		m_n_trakt_avslut_hallmark				= n_trakt_avslut_hallmark;
		m_n_trakt_avslut_hansyn					= n_trakt_avslut_hansyn;
		m_n_trakt_avslut_fdkulturmark			= n_trakt_avslut_fdkulturmark;
		m_n_trakt_avslut_kulturmiljo			= n_trakt_avslut_kulturmiljo;
		m_n_trakt_avslut_lovandel				= n_trakt_avslut_lovandel;
		m_n_trakt_avslut_skyddszon				= n_trakt_avslut_skyddszon;
		m_n_trakt_avslut_nvtrad					= n_trakt_avslut_nvtrad;
		m_n_trakt_avslut_framtidstrad			= n_trakt_avslut_framtidstrad;
		m_n_trakt_avslut_hogstubbar				= n_trakt_avslut_hogstubbar;
		m_n_trakt_avslut_dodatrad				= n_trakt_avslut_dodatrad;
		m_n_trakt_avslut_mindrekorskada			= n_trakt_avslut_mindrekorskada;
		m_n_trakt_avslut_allvarligkorskada		= n_trakt_avslut_allvarligkorskada;
		m_n_trakt_avslut_markskador				= n_trakt_avslut_markskador;
		m_n_trakt_avslut_markskador2			= n_trakt_avslut_markskador2;
		m_n_trakt_avslut_nedskrapning			= n_trakt_avslut_nedskrapning;
		m_s_trakt_avslut_fritext				= s_trakt_avslut_fritext;
		m_n_trakt_avslut_atgardstp				= n_trakt_avslut_atgardstp;
		m_s_trakt_avslut_atgardstp_forklaring	= s_trakt_avslut_atgardstp_forklaring;
		m_s_trakt_avslut_datum_slutford			= s_trakt_avslut_datum_slutford;
		m_n_trakt_calc_si_spec					= n_trakt_calc_si_spec;
		m_n_trakt_calc_si						= n_trakt_calc_si;
	}

	CTraktHeader(CXmlReader *Trakt)
	{
		sDate			= Trakt->m_sDatum;
		sDistrict		= Trakt->m_sDistrikt;
		fAreal			= my_atof(Trakt->m_sAreal);
		sTraktNum		= Trakt->m_sObj_nr;
		sMachineNum		= Trakt->m_sMask_nr;
		sObjectName		= Trakt->m_sObj_namn;
		sContractor		= Trakt->m_sEntr;
		sRegion			= Trakt->m_sRegion;
		sType			= Trakt->m_sTyp;
		sOrigin			= Trakt->m_sUrsprung;


		m_s_trakt_avslut_fritext				= Trakt->m_s_trakt_avslut_fritext;
		m_s_trakt_avslut_atgardstp_forklaring	= Trakt->m_s_trakt_avslut_atgardstp_forklaring;
		m_s_trakt_avslut_datum_slutford			= Trakt->m_s_trakt_avslut_datum_slutford;

		m_n_trakt_maskinstorlek	=	Trakt->m_n_trakt_maskinstorlek;
		m_n_trakt_avslut_myr		=	Trakt->m_n_trakt_avslut_myr;
		m_n_trakt_avslut_hallmark	=	Trakt->m_n_trakt_avslut_hallmark;
		m_n_trakt_avslut_hansyn	=	Trakt->m_n_trakt_avslut_hansyn;
		m_n_trakt_avslut_fdkulturmark	=	Trakt->m_n_trakt_avslut_fdkulturmark;
		m_n_trakt_avslut_kulturmiljo	=	Trakt->m_n_trakt_avslut_kulturmiljo;
		m_n_trakt_avslut_lovandel	=	Trakt->m_n_trakt_avslut_lovandel;
		m_n_trakt_avslut_skyddszon	=	Trakt->m_n_trakt_avslut_skyddszon;
		m_n_trakt_avslut_nvtrad		=	Trakt->m_n_trakt_avslut_nvtrad;
		m_n_trakt_avslut_framtidstrad	=	Trakt->m_n_trakt_avslut_framtidstrad;
		m_n_trakt_avslut_hogstubbar	=	Trakt->m_n_trakt_avslut_hogstubbar;
		m_n_trakt_avslut_dodatrad	=	Trakt->m_n_trakt_avslut_dodatrad;
		m_n_trakt_avslut_mindrekorskada=	Trakt->m_n_trakt_avslut_mindrekorskada;
		m_n_trakt_avslut_allvarligkorskada=	Trakt->m_n_trakt_avslut_allvarligkorskada;
		m_n_trakt_avslut_markskador	=	Trakt->m_n_trakt_avslut_markskador;
		m_n_trakt_avslut_markskador2	=	Trakt->m_n_trakt_avslut_markskador2;
		m_n_trakt_avslut_nedskrapning	=	Trakt->m_n_trakt_avslut_nedskrapning;
		m_n_trakt_avslut_atgardstp	=	Trakt->m_n_trakt_avslut_atgardstp;
		m_n_trakt_calc_si_spec		=	Trakt->m_n_trakt_calc_si_spec;
		m_n_trakt_calc_si			=	Trakt->m_n_trakt_calc_si;

	}

	CTraktHeader(const CTraktHeader &c)
	{
		*this = c;
	}

	CString getDate(void)			{ return sDate; }
	CString getDistrict(void)		{ return sDistrict; }
	double getAreal(void)			{ return fAreal; }
	CString getTraktNum(void)		{ return sTraktNum; }
	CString getMachineNum(void)		{ return sMachineNum; }
	CString getObjectName(void)		{ return sObjectName; }
	CString getContractor(void)		{ return sContractor; }
	CString getRegion(void)			{ return sRegion; }
	CString getType(void)			{ return sType; }
	CString getOrigin(void)			{ return sOrigin; }
};


// CVariables; holds information about "V�gbredd","V�gavst�nd" etc.
// This class's used both in trakt, compartment and plots.
// OBS! Only on Plots there's info in both "V�gbredd1" and "V�gbredd2",
// "V�gavst�nd1" and "V�gavst�nd2".
class CVariables
{
	double fRWidth1;		// "V�gabredd1"
	double fRWidth2;		// "V�gabredd2"
	double fRDist1;			// "V�gavst�nd1"
	double fRDist2;			// "V�gavst�nd2"
	double fRArea;			// "V�gyta"
	double fGYBefore;		
	double fGYAfter;
	double fGYWDraw;		// "Uttag"
	double fGYQuota;		// "Gallringskvot"
	double fStems;
	double fAvgDiam;		// "Medeldiameter"
	double fH25;
	double fVolPine;
	double fVolSpruce;
	double fVolBirch;
	double fVolCont;		// "Volym Contorta"
	double fVolTotal;		// "Total volym"
	double fPropPine;		// "Andel Tall"
	double fPropSpruce;
	double fPropBirch;
	double fPropCont;
	double fDamaged;
	double fTracks;			// "Sp�rbildning"
	double fHighStumps;		// "H�ga stubbar"
	double fStumpTreat;		// "Stubbebehandling"
	double fTwigs;			// "Risning"
	double fForrensat;		//F�rrensat
	long lCompartID;		// "Avdelningens id nummer"
	int nPlotID;			// "Ytans id nummer"

	//Avdelningsspecifika variabler
	CString m_s_Comp_Si;
	int m_n_Comp_Gallrtyp;
	int m_n_Comp_Metod;
	int m_n_Comp_TraktDir_LagstaGy;
	int m_n_Comp_TraktDir_GyFore;
	int m_n_Comp_TraktDir_GyEfter;
	int m_n_Comp_TraktDir_GaUttag;
	double m_f_Comp_gy_wdraw_forest;
	double m_f_Comp_gy_wdraw_road;
	double m_f_Comp_oh;
	double m_f_Comp_areal;
	double m_f_Comp_antal_stubbar;

	//Ytspecifika variabler
	int m_n_Plot_Ytradie;
	int m_n_Plot_AntalTrad;
	int m_n_Plot_AntalStubbar;
	int m_n_Plot_AntalStubbarivag;
	int m_n_Plot_AntalStubbariskog;
	int m_n_Plot_Totdiamkvarvarande;
	int m_n_Plot_Totdiamuttag;
	int m_n_Plot_AntalHogaStubbar;


	int size;
	int avdI;
	int plotI;
	int storlek;

public:
	

	CVariables(void)
	{
		fRWidth1		= 0.0;	// "V�gabredd1"
		fRWidth2		= 0.0;	// "V�gabredd2"
		fRDist1			= 0.0;	// "V�gavst�nd1"
		fRDist2			= 0.0;	// "V�gavst�nd2"
		fRArea			= 0.0;	// "V�gyta"
		fGYBefore		= 0.0;		
		fGYAfter		= 0.0;
		fGYWDraw		= 0.0;	// "Uttag"
		fGYQuota		= 0.0;	// "Gallringskvot"
		fStems			= 0.0;
		fAvgDiam		= 0.0;	// "Medeldiameter"
		fH25			= 0.0;
		fVolPine		= 0.0;
		fVolSpruce		= 0.0;
		fVolBirch		= 0.0;
		fVolCont		= 0.0;	// "Volym Contorta"
		fVolTotal		= 0.0;
		fPropPine		= 0.0;	// "Andel Tall"
		fPropSpruce		= 0.0;
		fPropBirch		= 0.0;
		fPropCont		= 0.0;
		fDamaged		= 0.0;
		fTracks			= 0.0;	// "Sp�rbildning"
		fHighStumps		= 0.0;	// "H�ga stubbar"
		fStumpTreat		= 0.0;	// "Stubbebehandling"
		fTwigs			= 0.0;	// "Risning"
		fForrensat		= 0.0;
		lCompartID		= -1;	// "Avdelningens id nummer"
		nPlotID			= -1;	// "Ytans id nummer"


		m_s_Comp_Si=_T("");
		m_n_Comp_Gallrtyp =0;
		m_n_Comp_Metod=0;
		m_n_Comp_TraktDir_LagstaGy=0;
		m_n_Comp_TraktDir_GyFore=0;
		m_n_Comp_TraktDir_GyEfter=0;
		m_n_Comp_TraktDir_GaUttag=0;
		m_f_Comp_gy_wdraw_forest=0.0;
		m_f_Comp_gy_wdraw_road=0.0;
		m_f_Comp_oh=0.0;
		m_f_Comp_areal=0.0;
		m_f_Comp_antal_stubbar=0.0;
		

	 m_n_Plot_Ytradie=0;
	 m_n_Plot_AntalTrad=0;
	 m_n_Plot_AntalStubbar=0;
	 m_n_Plot_AntalStubbarivag=0;
	 m_n_Plot_AntalStubbariskog=0;
	 m_n_Plot_Totdiamkvarvarande=0;
	 m_n_Plot_Totdiamuttag=0;
	 m_n_Plot_AntalHogaStubbar=0;

	}
	
	CVariables(double rwidth1,double rwidth2,double rdist1,double rdist2,double rarea,
						 double gybefore,double gyafter,double gywdraw,double gyquota,
						 double stems,double avg_diam,double h25,
						 double vol_pine,double vol_spruce,double vol_birch,double vol_cont,double vol_total,
						 double prop_pine,double prop_spruce,double prop_birch,double prop_cont,
						 double damaged,double tracks,double high_stumps,double stump_treat,double twigs,double Forrensat,
						 LPCTSTR s_Comp_Si,
						 int n_Comp_Gallrtyp,
						 int n_Comp_Metod,
						 int n_Comp_TraktDir_LagstaGy,
						 int n_Comp_TraktDir_GyFore,
						 int n_Comp_TraktDir_GyEfter,
						 int n_Comp_TraktDir_GaUttag,
						 double f_Comp_gy_wdraw_forest,
						 double f_Comp_gy_wdraw_road,
						 double f_Comp_oh,
						 double f_Comp_areal,
						 double f_Comp_antal_stubbar,
						 int n_Plot_Ytradie,
						 int n_Plot_AntalTrad,
						 int n_Plot_AntalStubbar,
						 int n_Plot_AntalStubbarivag,
						 int n_Plot_AntalStubbariskog,
						 int n_Plot_Totdiamkvarvarande,
						 int n_Plot_Totdiamuttag,
						 int n_Plot_AntalHogaStubbar,
						 long compart_id = -1,int plot_id = -1)	
	{
		fRWidth1		= rwidth1;
		fRWidth2		= rwidth2;
		fRDist1			= rdist1;	
		fRDist2			= rdist2;	
		fRArea			= rarea;	
		fGYBefore		= gybefore;		
		fGYAfter		= gyafter;
		fGYWDraw		= gywdraw;
		fGYQuota		= gyquota;
		fStems			= stems;
		fAvgDiam		= avg_diam;
		fH25			= h25;
		fVolPine		= vol_pine;
		fVolSpruce		= vol_spruce;
		fVolBirch		= vol_birch;
		fVolCont		= vol_cont;
		fVolTotal		= vol_total;
		fPropPine		= prop_pine;
		fPropSpruce		= prop_spruce;
		fPropBirch		= prop_birch;
		fPropCont		= prop_cont;
		fDamaged		= damaged;
		fTracks			= tracks;
		fHighStumps		= high_stumps;
		fStumpTreat		= stump_treat;
		fTwigs			= twigs;
		fForrensat		= Forrensat;
		lCompartID		= compart_id;
		nPlotID			= plot_id;
		
		m_s_Comp_Si					=s_Comp_Si;
		m_n_Comp_Gallrtyp			=n_Comp_Gallrtyp;
		m_n_Comp_Metod				=n_Comp_Metod;
		m_n_Comp_TraktDir_LagstaGy	=n_Comp_TraktDir_LagstaGy;
		m_n_Comp_TraktDir_GyFore	=n_Comp_TraktDir_GyFore;
		m_n_Comp_TraktDir_GyEfter	=n_Comp_TraktDir_GyEfter;
		m_n_Comp_TraktDir_GaUttag	=n_Comp_TraktDir_GaUttag;
		m_f_Comp_gy_wdraw_forest	=f_Comp_gy_wdraw_forest;
		m_f_Comp_gy_wdraw_road		=f_Comp_gy_wdraw_road;
		m_f_Comp_oh					=f_Comp_oh;
		m_f_Comp_areal				=f_Comp_areal;
		m_f_Comp_antal_stubbar		=f_Comp_antal_stubbar;

			 m_n_Plot_Ytradie=n_Plot_Ytradie;
	 m_n_Plot_AntalTrad=n_Plot_AntalTrad;
	 m_n_Plot_AntalStubbar=n_Plot_AntalStubbar;
	 m_n_Plot_AntalStubbarivag=n_Plot_AntalStubbarivag;
	 m_n_Plot_AntalStubbariskog=n_Plot_AntalStubbariskog;
	 m_n_Plot_Totdiamkvarvarande=n_Plot_Totdiamkvarvarande;
	 m_n_Plot_Totdiamuttag=n_Plot_Totdiamuttag;
	 m_n_Plot_AntalHogaStubbar=n_Plot_AntalHogaStubbar;
	}

	CVariables(CXmlReader *Trakt,int n_Level,long compart_id = -1,int plot_id = -1)
	{
		switch(n_Level)
		{
		case TH_LEVEL_TRAKT:
			fRWidth1		= Trakt->m_fTrakt_Medel[thRWidth1];
			fRWidth2		=Trakt->m_fTrakt_Medel[thRWidth2];
			fRDist1			=Trakt->m_fTrakt_Medel[thRDist1];
			fRDist2			=Trakt->m_fTrakt_Medel[thRDist2];
			fRArea			=Trakt->m_fTrakt_Medel[thRArea];
			fGYBefore		=Trakt->m_fTrakt_Medel[thGYBefore];
			fGYAfter		=Trakt->m_fTrakt_Medel[thGYAfter];
			fGYWDraw		=Trakt->m_fTrakt_Medel[thGYWDraw];
			fGYQuota		=Trakt->m_fTrakt_Medel[thGYQuota];
			fStems			=Trakt->m_fTrakt_Medel[thStems];
			fAvgDiam		=Trakt->m_fTrakt_Medel[thAvgDiam];
			fH25			=Trakt->m_fTrakt_Medel[thH25];
			fVolPine		=Trakt->m_fTrakt_Medel[thVolPine];
			fVolSpruce		=Trakt->m_fTrakt_Medel[thVolSpruce];
			fVolBirch		=Trakt->m_fTrakt_Medel[thVolBirch];
			fVolCont		=Trakt->m_fTrakt_Medel[thVolCont];
			fVolTotal		=Trakt->m_fTrakt_Medel[thVolTotal];
			fPropPine		=Trakt->m_fTrakt_Medel[thPropPine];
			fPropSpruce		=Trakt->m_fTrakt_Medel[thPropSpruce];
			fPropBirch		=Trakt->m_fTrakt_Medel[thPropBirch];
			fPropCont		=Trakt->m_fTrakt_Medel[thPropCont];
			fDamaged		=Trakt->m_fTrakt_Medel[thDamaged];
			fTracks			=Trakt->m_fTrakt_Medel[thTracks];
			fHighStumps		=Trakt->m_fTrakt_Medel[thHighStumps];
			fStumpTreat		=Trakt->m_fTrakt_Medel[thGenomGallrat];
			fTwigs			=Trakt->m_fTrakt_Medel[thTwigs];
			fForrensat		=Trakt->m_fTrakt_Medel[thForrensat];

			lCompartID		= compart_id;
			nPlotID			= plot_id;

			m_s_Comp_Si					=_T("");
			m_n_Comp_Gallrtyp			=0;
			m_n_Comp_Metod				=0;
			m_n_Comp_TraktDir_LagstaGy	=0;
			m_n_Comp_TraktDir_GyFore	=0;
			m_n_Comp_TraktDir_GyEfter	=0;
			m_n_Comp_TraktDir_GaUttag	=0;
			m_f_Comp_gy_wdraw_forest	=0.0;
			m_f_Comp_gy_wdraw_road		=0.0;
			m_f_Comp_oh					=0.0;
			m_f_Comp_areal				=0.0;
			m_f_Comp_antal_stubbar		=0.0;

			m_n_Plot_Ytradie=0;
			m_n_Plot_AntalTrad=0;
			m_n_Plot_AntalStubbar=0;
			m_n_Plot_AntalStubbarivag=0;
			m_n_Plot_AntalStubbariskog=0;
			m_n_Plot_Totdiamkvarvarande=0;
			m_n_Plot_Totdiamuttag=0;
			m_n_Plot_AntalHogaStubbar=0;

			break;
		case TH_LEVEL_COMPARTMENT:
			size=0;
			avdI=0;
			size=Trakt->Avdelningar.size();
			if(size>0)
			{
				for(avdI=0;avdI<size;avdI++)
				{

					if(Trakt->Avdelningar[avdI].m_n_Id==compart_id)
					{
						fRWidth1		= Trakt->Avdelningar[avdI].m_fAvdVars[thRWidth1];
						fRWidth2		=Trakt->Avdelningar[avdI].m_fAvdVars[thRWidth2];
						fRDist1			=Trakt->Avdelningar[avdI].m_fAvdVars[thRDist1];
						fRDist2			=Trakt->Avdelningar[avdI].m_fAvdVars[thRDist2];
						fRArea			=Trakt->Avdelningar[avdI].m_fAvdVars[thRArea];
						fGYBefore		=Trakt->Avdelningar[avdI].m_fAvdVars[thGYBefore];
						fGYAfter		=Trakt->Avdelningar[avdI].m_fAvdVars[thGYAfter];
						fGYWDraw		=Trakt->Avdelningar[avdI].m_fAvdVars[thGYWDraw];
						fGYQuota		=Trakt->Avdelningar[avdI].m_fAvdVars[thGYQuota];
						fStems			=Trakt->Avdelningar[avdI].m_fAvdVars[thStems];
						fAvgDiam		=Trakt->Avdelningar[avdI].m_fAvdVars[thAvgDiam];
						fH25			=Trakt->Avdelningar[avdI].m_fAvdVars[thH25];
						fVolPine		=Trakt->Avdelningar[avdI].m_fAvdVars[thVolPine];
						fVolSpruce		=Trakt->Avdelningar[avdI].m_fAvdVars[thVolSpruce];
						fVolBirch		=Trakt->Avdelningar[avdI].m_fAvdVars[thVolBirch];
						fVolCont		=Trakt->Avdelningar[avdI].m_fAvdVars[thVolCont];
						fVolTotal		=Trakt->Avdelningar[avdI].m_fAvdVars[thVolTotal];
						fPropPine		=Trakt->Avdelningar[avdI].m_fAvdVars[thPropPine];
						fPropSpruce		=Trakt->Avdelningar[avdI].m_fAvdVars[thPropSpruce];
						fPropBirch		=Trakt->Avdelningar[avdI].m_fAvdVars[thPropBirch];
						fPropCont		=Trakt->Avdelningar[avdI].m_fAvdVars[thPropCont];
						fDamaged		=Trakt->Avdelningar[avdI].m_fAvdVars[thDamaged];
						fTracks			=Trakt->Avdelningar[avdI].m_fAvdVars[thTracks];
						fHighStumps		=Trakt->Avdelningar[avdI].m_fAvdVars[thHighStumps];
						fStumpTreat		=Trakt->Avdelningar[avdI].m_fAvdVars[thGenomGallrat];
						fTwigs			=Trakt->Avdelningar[avdI].m_fAvdVars[thTwigs];
						fForrensat		=Trakt->Avdelningar[avdI].m_fAvdVars[thForrensat];

						m_s_Comp_Si=Trakt->Avdelningar[avdI].m_s_compart_spec_and_si;
						m_n_Comp_Gallrtyp =Trakt->Avdelningar[avdI].m_n_compart_gallrtyp;
						m_n_Comp_Metod=Trakt->Avdelningar[avdI].m_n_compart_metod;
						m_n_Comp_TraktDir_LagstaGy=Trakt->Avdelningar[avdI].m_n_compart_lagsta_gy_enl_mall;
						m_n_Comp_TraktDir_GyFore=Trakt->Avdelningar[avdI].m_n_compart_traktdir_gy_fore_m2ha;
						m_n_Comp_TraktDir_GyEfter=Trakt->Avdelningar[avdI].m_n_compart_traktdir_gy_efter_m2ha;
						m_n_Comp_TraktDir_GaUttag=Trakt->Avdelningar[avdI].m_n_compart_traktdir_ga_uttag;
						m_f_Comp_gy_wdraw_forest=Trakt->Avdelningar[avdI].m_f_compart_gy_wdraw_forest;
						m_f_Comp_gy_wdraw_road=Trakt->Avdelningar[avdI].m_f_compart_gy_wdraw_road;
						m_f_Comp_oh=Trakt->Avdelningar[avdI].m_f_compart_oh;
						m_f_Comp_areal=Trakt->Avdelningar[avdI].m_f_compart_areal;
						m_f_Comp_antal_stubbar=Trakt->Avdelningar[avdI].m_f_compart_antal_stubbar;
						
						m_n_Plot_Ytradie=0;
						m_n_Plot_AntalTrad=0;
						m_n_Plot_AntalStubbar=0;
						m_n_Plot_AntalStubbarivag=0;
						m_n_Plot_AntalStubbariskog=0;
						m_n_Plot_Totdiamkvarvarande=0;
						m_n_Plot_Totdiamuttag=0;
						m_n_Plot_AntalHogaStubbar=0;
					}
				}
			}
			lCompartID		= compart_id;
			nPlotID			= plot_id;
			break;
		case TH_LEVEL_PLOT:
			size=Trakt->Avdelningar.size();
			avdI=0;
			storlek=0;
			plotI=0;

			if(size>0)
			{
				for(avdI=0;avdI<size;avdI++)
				{
					if(Trakt->Avdelningar[avdI].m_n_Id==compart_id)
					{
						storlek=Trakt->Avdelningar[avdI].mYta.size();
						if(storlek!=0)
						{
							for(plotI=0;plotI<storlek;plotI++)
							{
								if(Trakt->Avdelningar[avdI].mYta[plotI].m_n_Id==plot_id)
								{
									fRWidth1		= Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thRWidth1];
									fRWidth2		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thRWidth2];
									fRDist1			=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thRDist1];
									fRDist2			=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thRDist2];
									fRArea			=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thRArea];
									fGYBefore		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thGYBefore];
									fGYAfter		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thGYAfter];
									fGYWDraw		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thGYWDraw];
									fGYQuota		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thGYQuota];
									fStems			=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thStems];
									fAvgDiam		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thAvgDiam];
									fH25			=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thH25];
									fVolPine		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thVolPine];
									fVolSpruce		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thVolSpruce];
									fVolBirch		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thVolBirch];
									fVolCont		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thVolCont];
									fVolTotal		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thVolTotal];
									fPropPine		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thPropPine];
									fPropSpruce		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thPropSpruce];
									fPropBirch		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thPropBirch];
									fPropCont		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thPropCont];
									fDamaged		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thDamaged];
									fTracks			=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thTracks];
									fHighStumps		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thHighStumps];
									fStumpTreat		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thGenomGallrat];
									fTwigs			=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thTwigs];
									fForrensat		=Trakt->Avdelningar[avdI].mYta[plotI].m_f_YtVars[thForrensat];

									m_s_Comp_Si					=_T("");
									m_n_Comp_Gallrtyp			=0;
									m_n_Comp_Metod				=0;
									m_n_Comp_TraktDir_LagstaGy	=0;
									m_n_Comp_TraktDir_GyFore	=0;
									m_n_Comp_TraktDir_GyEfter	=0;
									m_n_Comp_TraktDir_GaUttag	=0;
									m_f_Comp_gy_wdraw_forest	=0.0;
									m_f_Comp_gy_wdraw_road		=0.0;
									m_f_Comp_oh					=0.0;
									m_f_Comp_areal				=0.0;
									m_f_Comp_antal_stubbar		=0.0;

									m_n_Plot_Ytradie=Trakt->Avdelningar[avdI].mYta[plotI].m_n_Ytradie;
									m_n_Plot_AntalTrad=Trakt->Avdelningar[avdI].mYta[plotI].m_n_AntalTrad;
									m_n_Plot_AntalStubbar=Trakt->Avdelningar[avdI].mYta[plotI].m_n_AntalStubbar;
									m_n_Plot_AntalStubbarivag=Trakt->Avdelningar[avdI].mYta[plotI].m_n_AntalStubbarivag;
									m_n_Plot_AntalStubbariskog=Trakt->Avdelningar[avdI].mYta[plotI].m_n_AntalStubbariskog;
									m_n_Plot_Totdiamkvarvarande=Trakt->Avdelningar[avdI].mYta[plotI].m_n_Totdiamkvarvarande;
									m_n_Plot_Totdiamuttag=Trakt->Avdelningar[avdI].mYta[plotI].m_n_Totdiamuttag;
									m_n_Plot_AntalHogaStubbar=Trakt->Avdelningar[avdI].mYta[plotI].m_n_AntalHogaStubbar;

									lCompartID		= compart_id;
									nPlotID			= plot_id;

								}
							}
						}
					}
				}
			}
			break;

		}
	}


	CVariables(const CVariables &c)
	{
		*this = c;
	}
	double getRWidth1(void)			{ return fRWidth1; }
	double getRWidth2(void)			{ return fRWidth2; }
	double getRDist1(void)			{ return fRDist1;	}
	double getRDist2(void)			{ return fRDist2; }
	double getRArea(void)			{ return fRArea; }

	double getGYBefore(void)		{ return fGYBefore; }
	double getGYAfter(void)			{ return fGYAfter; }
	double getGYWDraw(void)			{ return fGYWDraw; }
	double getGYQuota(void)			{ return fGYQuota; }
	
	double getStems(void)			{ return fStems; }
	double getAvgDiam(void)			{ return fAvgDiam; }
	double getH25(void)				{ return fH25; }
	
	double getVolPine(void)			{ return fVolPine; }
	double getVolSpruce(void)		{ return fVolSpruce; }
	double getVolBirch(void)		{ return fVolBirch; }
	double getVolCont(void)			{ return fVolCont; }
	double getVolTotal(void)		{ return fVolTotal; }

	double getPropPine(void)		{ return fPropPine; }
	double getPropSpruce(void)		{ return fPropSpruce; }
	double getPropBirch(void)		{ return fPropBirch; }
	double getPropCont(void)		{ return fPropCont; }
	void setPropCont(double v)		{ fPropCont = v; }

	double getDamaged(void)			{ return fDamaged; }
	double getTracks(void)			{ return fTracks; }
	void setTracks(double var)		{ fTracks = var; }
	double getHighStumps(void)		{ return fHighStumps; }
	void setHighStumps(double var)	{ fHighStumps = var; }
	double getStumpTreat(void)		{ return fStumpTreat; }
	double getTwigs(void)			{ return fTwigs; }
	double getForrensat(void)		{ return fForrensat; }

	CString getCompSpecAndSi(void )	{ return m_s_Comp_Si;}
	int getCompGallrTyp(void)		{return m_n_Comp_Gallrtyp;}
	int getCompMetod(void)			{return m_n_Comp_Metod;}
	int getCompLagstaGy(void)		{return m_n_Comp_TraktDir_LagstaGy;}
	int getCompGyFore(void)			{return m_n_Comp_TraktDir_GyFore;}
	int getCompGyEfter(void)		{return m_n_Comp_TraktDir_GyEfter;}
	int getCompGaUttag(void)		{return m_n_Comp_TraktDir_GaUttag;}


	int getPlot_Ytradie(void)	{return m_n_Plot_Ytradie;}
	int getPlot_AntalTrad(void)	{return m_n_Plot_AntalTrad;}
	int getPlot_AntalStubbar(void)	{return m_n_Plot_AntalStubbar;}
	int getPlot_AntalStubbarivag(void)	{return m_n_Plot_AntalStubbarivag;}
	int getPlot_AntalStubbariskog(void)	{return m_n_Plot_AntalStubbariskog;}
	int getPlot_Totdiamkvarvarande(void)	{return m_n_Plot_Totdiamkvarvarande;}
	int getPlot_Totdiamuttag(void)	{return m_n_Plot_Totdiamuttag;}
	int getPlot_AntalHogaStubbar(void)	{return m_n_Plot_AntalHogaStubbar;}

	double getGYWDrawForest(void)	{return m_f_Comp_gy_wdraw_forest;}
	double getGYWDrawRoad(void)	{return m_f_Comp_gy_wdraw_road;}
	double getOh(void)	{return m_f_Comp_oh;}
	double getAreal(void)	{return m_f_Comp_areal;}
	double getAntalStubbar(void)	{return m_f_Comp_antal_stubbar;}

	//#4031 20140429 J�
	double getVar(int n_Var)
	{
		switch(n_Var)
		{
		case 0:
			return fRWidth1;		// "V�gabredd1"
			break;
		case 1:
			return fRWidth2;		// "V�gabredd2"
			break;
		case 2:
			return fRDist1;			// "V�gavst�nd1"
			break;
		case 3:
			return fRDist2;			// "V�gavst�nd2"
			break;
		case 4:
			return fRArea;			// "V�gyta"
			break;
		case 5:
			return fGYBefore;		
			break;
		case 6:
			return fGYAfter;
			break;
		case 7:
			return fGYWDraw;		// "Uttag"
			break;
		case 8:
			return fGYQuota;		// "Gallringskvot"
			break;
		case 9:
			return fStems;
			break;
		case 10:
			return fAvgDiam;		// "Medeldiameter"
			break;
		case 11:
			return fH25;
			break;
		case 12:
			return fVolPine;
			break;
		case 13:
			return fVolSpruce;
			break;
		case 14:
			return fVolBirch;
			break;
		case 15:
			return fVolCont;		// "Volym Contorta"
			break;
		case 16:
			return fVolTotal;		// "Total volym"
			break;
		case 17:
			return fPropPine;		// "Andel Tall"
			break;
		case 18:
			return fPropSpruce;
			break;
		case 19:
			return fPropBirch;
			break;
		case 20:
			return fPropCont;
			break;
		case 21:
			return fDamaged;
			break;
		case 22:
			return fTracks;			// "Sp�rbildning"
			break;
		case 23:
			return fHighStumps;		// "H�ga stubbar"
			break;
		case 24:
			return fStumpTreat;		// "Stubbebehandling"
			break;
		case 25:
			return fTwigs;			// "Risning"
			break;
		case 26:
			return fForrensat;			// "Risning"
			break;
		}
	}


	long getCompartID(void)			{ return lCompartID; }
	int getPlotID(void)				{ return nPlotID; }


};

typedef std::vector<CVariables> vecVariables;

// Structrure, for identifying Trakt, entered from a File; 061207 p�d

typedef struct _trakt_identifer_struct
{
	int nRegionID;
	int nDistrictID;
	int nMachineID;
	CString sTraktNum;
	CString sType;
	int nOrigin;

} TRAKT_IDENTIFER;


// Create my own CFileDialog 

class CMyFileDialog : public CFileDialog
{
	CString m_sTitle;
public:
	CMyFileDialog(LPCTSTR,BOOL,LPCTSTR,LPCTSTR,DWORD,LPCTSTR);

	virtual BOOL OnInitDialog();
};

// Handle Jonas �. DLL.
// I'll use LoadLibrary for now; 060922 p�d
class CJonasDLL_handling
{
//private:
	HINSTANCE m_hModule;

	CTraktHeader m_clsHeader;
	CVariables m_clsTraktVar;
	vecVariables m_vecCompartVar;
	vecVariables m_vecPlotsVar;

	int m_nTotNumOfCompart;
	int m_nTotNumOfPlots;

protected:
	int openHnd(char *);

	BOOL getFileHeader(void);
	BOOL getTraktVar(void);
	BOOL getCompartVar(void);
	BOOL getPlotsVarPerCompart(void);
	CXmlReader mTrakt;
public:
	CJonasDLL_handling(void) 
	{

	}
	~CJonasDLL_handling(void)
	{
		if (m_hModule != NULL)
			FreeLibrary(m_hModule);
	}

	BOOL enterDataToDB(DB_CONNECTION_DATA con,char *,char *,TRAKT_IDENTIFER *,
										 LPCTSTR cap = _T(""),LPCTSTR msg_1 = _T(""),LPCTSTR msg_2 = _T(""),LPCTSTR msg_3 = _T(""),LPCTSTR msg_4 = _T(""));

};

#endif
