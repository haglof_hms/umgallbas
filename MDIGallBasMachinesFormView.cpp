// MDIGallBasMachinesFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIGallBasMachinesFormView.h"

#include "ResLangFileReader.h"

#include "UMGallBasGenerics.h"

// CMDIGallBasMachinesFormView

IMPLEMENT_DYNCREATE(CMDIGallBasMachinesFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIGallBasMachinesFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()


CMDIGallBasMachinesFormView::CMDIGallBasMachinesFormView()
	: CXTResizeFormView(CMDIGallBasMachinesFormView::IDD)
{
}

CMDIGallBasMachinesFormView::~CMDIGallBasMachinesFormView()
{
}

BOOL CMDIGallBasMachinesFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


void CMDIGallBasMachinesFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL5, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL6, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL7, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL8, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL15, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL16, m_wndLbl7);

	DDX_Control(pDX, IDC_EDIT3, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT6, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit6);

	DDX_Control(pDX, IDC_GROUP, m_wndGroup);
	//}}AFX_DATA_MAP
}

// CMDIGallBasMachinesFormView diagnostics

#ifdef _DEBUG
void CMDIGallBasMachinesFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIGallBasMachinesFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


void CMDIGallBasMachinesFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_wndEdit1.SetEnabledColor(BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit1.SetReadOnly( TRUE );

	m_wndEdit2.SetEnabledColor(BLACK, WHITE );
	m_wndEdit2.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit2.SetReadOnly( TRUE );
	
	m_wndEdit3.SetEnabledColor(BLACK, WHITE );
	m_wndEdit3.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit3.SetReadOnly( TRUE );
	
	m_wndEdit4.SetEnabledColor(BLACK, WHITE );
	m_wndEdit4.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit4.SetReadOnly( TRUE );
	
	m_wndEdit5.SetEnabledColor(BLACK, WHITE );
	m_wndEdit5.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit5.SetReadOnly( TRUE );

	m_wndEdit6.SetEnabledColor(BLACK, WHITE );
	m_wndEdit6.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit6.SetReadOnly( TRUE );

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	// Get data from database; 061010 p�d
	getMachinesFromDB();

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	setLanguage();
	
	m_nDBIndex = 0;
	populateData(m_nDBIndex);
}

BOOL CMDIGallBasMachinesFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDIGallBasMachinesFormView::OnSetFocus(CWnd*)
{
	if (!m_vecMachineData.empty())
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecMachineData.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}
}


// CMDIGallBasMachinesFormView message handlers

void CMDIGallBasMachinesFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING101) + _T(" :"));
			m_wndLbl3.SetWindowText(xml->str(IDS_STRING102) + _T(" :"));
			m_wndLbl4.SetWindowText(xml->str(IDS_STRING104) + _T(" :"));
			m_wndLbl5.SetWindowText(xml->str(IDS_STRING105) + _T(" :"));
			m_wndLbl6.SetWindowText(xml->str(IDS_STRING107) + _T(" :"));
			m_wndLbl7.SetWindowText(xml->str(IDS_STRING108) + _T(" :"));

		}
		delete xml;
	}

}

void CMDIGallBasMachinesFormView::populateData(UINT idx)
{
	MACHINE_DATA data;
	if (!m_vecMachineData.empty() && 
		  idx >= 0 && 
			idx < m_vecMachineData.size())
	{
		data = m_vecMachineData[idx];

		m_wndEdit1.setInt(data.m_nRegionID);
		m_wndEdit2.SetWindowText(data.m_sRegionName);
		m_wndEdit3.setInt(data.m_nDistrictID);
		m_wndEdit4.SetWindowText(data.m_sDistrictName);
		m_wndEdit5.setInt(data.m_nMachineID);
		m_wndEdit6.SetWindowText(data.m_sContractorName);
	}
	else
	{
		m_wndEdit1.SetWindowText(_T(""));
		m_wndEdit2.SetWindowText(_T(""));
		m_wndEdit3.SetWindowText(_T(""));
		m_wndEdit4.SetWindowText(_T(""));
		m_wndEdit5.SetWindowText(_T(""));
		m_wndEdit6.SetWindowText(_T(""));
	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CMDIGallBasMachinesFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
			addMachine();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			saveMachine();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			deleteMachine();
			resetMachine((int)lParam);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			resetMachine((int)lParam);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_PREV :
		{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;

				if (m_nDBIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_NEXT :
		{
				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecMachineData.size() - 1))
					m_nDBIndex = (UINT)m_vecMachineData.size() - 1;
				
				if (m_nDBIndex == (UINT)m_vecMachineData.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_END :
		{
				m_nDBIndex = (UINT)m_vecMachineData.size()-1;

				setNavigationButtons(TRUE,FALSE);
			
				populateData(m_nDBIndex);
				break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

void CMDIGallBasMachinesFormView::getMachinesFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getMachines(m_vecMachineData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIGallBasMachinesFormView::addMachine(void)
{
	TRAKT_IDENTIFER m_structTraktIdentifer;
	if (createFromFile(m_dbConnectionData,m_sLangFN,&m_structTraktIdentifer))
		resetMachine(0);
}

void CMDIGallBasMachinesFormView::deleteMachine(void)
{
	MACHINE_DATA data;
	CString sMsg;
	CString sText1;
	CString sText2;
	CString sText3;
	CString sText4;
	CString sRegion;
	CString sDistrict;
	CString sMachine;
	CString sCaption;
	CString sOKBtn;
	CString sCancelBtn;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sText1 = xml->str(IDS_STRING5000);
			sText2 = xml->str(IDS_STRING5010);
			sText3 = xml->str(IDS_STRING5020);
			sText4 = xml->str(IDS_STRING5030);
			sRegion = xml->str(IDS_STRING504);
			sDistrict = xml->str(IDS_STRING505);
			sMachine = xml->str(IDS_STRING506);

			sCaption = xml->str(IDS_STRING109);
			sOKBtn = xml->str(IDS_STRING110);
			sCancelBtn = xml->str(IDS_STRING111);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecMachineData.size() > 0)
	{
		// Get Regions in database; 061002 p�d	
		data = m_vecMachineData[m_nDBIndex];
		// Setup a message for user upon deleting machine; 061010 p�d
		sMsg.Format(_T("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%s : <b>%d</b>   (%s)<br>%s : <b>%d</b>  (%s)<br>%s : <b>%d</b><br><hr><br>%s<br>%s<br><br><font size=\"+4\" color=\"#ff0000\"><center>%s</center></font>"),
								sText1,
								sRegion,
								data.m_nRegionID,
								data.m_sRegionName,
								sDistrict,
								data.m_nDistrictID,
								data.m_sDistrictName,
								sMachine,
								data.m_nMachineID,
								sText2,
								sText3,
								sText4);

		if (messageDialog(sCaption,sOKBtn,sCancelBtn,sMsg))
		{
			if (m_bConnected)
			{
				// Get Region information from Database server; 070122 p�d
				CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
				if (pDB != NULL)
				{
					// Delete Machine and ALL underlying 
					//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
					pDB->delMachine(data);
					delete pDB;
				}	// if (pDB != NULL)
			} // 	if (m_bConnected)
		}
	}	// if (m_vecMachineData.size() > 0)
	resetMachine(0);
}


// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIGallBasMachinesFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

// PUBLIC

void CMDIGallBasMachinesFormView::saveMachine(void)
{
	MACHINE_DATA data;
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecMachineData.size() > 0 && m_bConnected)

	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			// Get Regions in database; 061002 p�d	
			data = m_vecMachineData[m_nDBIndex];

			data.m_nMachineID	= m_wndEdit5.getInt();
			data.m_sContractorName = m_wndEdit6.getText();

			pDB->updMachine(data);
			delete pDB;
		}	// if (pDB != NULL)
	}	// if (m_vecMachineData.size() > 0)

	resetMachine(0);
}

void CMDIGallBasMachinesFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons(m_nDBIndex > 0,
												m_nDBIndex < (m_vecMachineData.size()-1));
}

// Reset and get data from database; 061010 p�d
void CMDIGallBasMachinesFormView::resetMachine(int todo)
{
	// Get updated data from Database
	getMachinesFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecMachineData.size() > 0)
	{

		// Reset to first item in m_vecMachineData
		m_nDBIndex = 0;
		// Populate data (first item) on User interface
		populateData(m_nDBIndex);
		// Set toolbar on HMSShell
		if (todo >= 0)
		{
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecMachineData.size()-1));
		}
	}	// if (m_vecMachineData.size() > 0)
	else 
	{
		if (todo >= 0)
		{
			setNavigationButtons(m_nDBIndex > 0,
			    								m_nDBIndex < (m_vecMachineData.size()-1));
			populateData(-1);	// Clear data
		}
	}
}
