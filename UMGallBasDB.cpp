#include "StdAfx.h"

#include "UMGallBasDB.h"

//////////////////////////////////////////////////////////////////////////////////
// CGallBasDBDB; Handle ALL transactions for GallBas

CGallBasDB::CGallBasDB() 
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CGallBasDB::CGallBasDB(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CGallBasDB::CGallBasDB(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}


// PROTECTED

// Check if USER set a question for the Machine. Can be by Region
// or by Region and District; 061010 p�d
void CGallBasDB::getSQLMachineQuestion(LPTSTR sql)
{
	FILE *fp;
	SQL_MACHINE_QUESTION recMachine;
	CString sPathAndFN;

	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_MACHINE_QUESTION_FN);

	if (fileExists(sPathAndFN))
	{
		if ((fp = _tfopen(sPathAndFN,_T("rb"))) != NULL)
		{
			fread(&recMachine,sizeof(SQL_MACHINE_QUESTION),1,fp);
			fclose(fp);
			_tcscpy(sql,recMachine.szSQL);
		}
	}
	else
	{
		_stprintf(sql,SQL_MACHINE_QUESTION1,TBL_MACHINE,TBL_DISTRICT,TBL_REGION);
	}
}

void CGallBasDB::getSQLTraktQuestion(LPTSTR sql)
{
	FILE *fp;
	SQL_TRAKT_QUESTION recTrakt;
	CString sPathAndFN;
	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_TRAKT_QUESTION_FN);

	if (fileExists(sPathAndFN))
	{
		if ((fp = _tfopen(sPathAndFN,_T("rb"))) != NULL)
		{
			fread(&recTrakt,sizeof(SQL_TRAKT_QUESTION),1,fp);
			fclose(fp);
			_tcscpy(sql,recTrakt.szSQL);
		}
	}
	else
	{
		_stprintf(sql,SQL_TRAKT_QUESTION1,TBL_TRAKT);
	}
}

void CGallBasDB::getSQLCompartQuestion(LPTSTR sql)
{
	FILE *fp;
	SQL_COMPART_QUESTION recCompart;
	CString sPathAndFN;

	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_COMPART_QUESTION_FN);

	if (fileExists(sPathAndFN))
	{
		if ((fp = _tfopen(sPathAndFN,_T("rb"))) != NULL)
		{
			fread(&recCompart,sizeof(SQL_COMPART_QUESTION),1,fp);
			fclose(fp);
			_tcscpy(sql,recCompart.szSQL);
		}
	}
	else
	{
		_stprintf(sql,SQL_COMPART_QUESTION1,TBL_COMPART);
	}
}

void CGallBasDB::getSQLPlotQuestion(LPTSTR sql)
{
	FILE *fp;
	SQL_PLOT_QUESTION recPlot;
	CString sPathAndFN;

	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_PLOT_QUESTION_FN);

	if (fileExists(sPathAndFN))
	{
		if ((fp = _tfopen(sPathAndFN,_T("rb"))) != NULL)
		{
			fread(&recPlot,sizeof(SQL_PLOT_QUESTION),1,fp);
			fclose(fp);
			_tcscpy(sql,recPlot.szSQL);
		}
	}
	else
	{
		_stprintf(sql,SQL_PLOT_QUESTION1,TBL_PLOT);
	}
}

// PUBLIC

// REGIONS

// Handle data in region_table; 061002 p�d
BOOL CGallBasDB::getRegions(vecRegionData &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_REGION);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(_region_data(m_saCommand.Field(1).asShort(), m_saCommand.Field(2).asString()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

// DISTRICTS

// Handle data in district_table; 061002 p�d
BOOL CGallBasDB::getDistricts(vecDistrictData &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select a.region_num,a.region_name,b.district_num,b.district_name from ")
					_T("%s a,%s b ")
					_T("where b.district_region_num = a.region_num ")
					_T("order by a.region_num, b.district_num"),TBL_REGION,TBL_DISTRICT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(_district_data(m_saCommand.Field(1).asShort(),
										 m_saCommand.Field(2).asString(),
										 m_saCommand.Field(3).asShort(),
										 m_saCommand.Field(4).asString()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

// MACHINES

// Handle data in machine_table; 061005 p�d

BOOL CGallBasDB::isThereAnyMachineData(void)
{
	int	nCnt = 0;
	BOOL bReturn;
	TCHAR szBuffer[1024];
	try
	{
		getSQLMachineQuestion(szBuffer);
		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{	
			nCnt++;
			if (nCnt > 0)
				break;
		}		

		m_saConnection.Commit();

		bReturn = (nCnt > 0);

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CGallBasDB::getMachines(vecMachineData &vec)
{

	TCHAR szBuffer[1024];
	try
	{
		vec.clear();
		getSQLMachineQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(_machine_data(m_saCommand.Field(1).asShort(),
																m_saCommand.Field(2).asString(),
																m_saCommand.Field(3).asShort(),
																m_saCommand.Field(4).asString(),
																m_saCommand.Field(5).asShort(),
																m_saCommand.Field(6).asString()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CGallBasDB::addMachine(MACHINE_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where machine_num = %d and machine_district_num = %d and machine_region_num = %d"),
			TBL_MACHINE,rec.m_nMachineID,rec.m_nDistrictID,rec.m_nRegionID);
		if (!exists(sSQL))
		{
			sSQL.Format(_T("insert into %s (machine_num,machine_region_num,machine_district_num,machine_contractor_num) values(:1,:2,:3,:4)"),TBL_MACHINE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_nMachineID;
			m_saCommand.Param(2).setAsShort() = rec.m_nRegionID;
			m_saCommand.Param(3).setAsShort() = rec.m_nDistrictID;
			m_saCommand.Param(4).setAsString() = rec.m_sContractorName;

			m_saCommand.Execute();
			
			m_saConnection.Commit();
			
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

// Update contractor, based on Region,District and Machine; 061010 p�d
BOOL CGallBasDB::updMachine(MACHINE_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	try
	{
		sSQL.Format(_T("select * from %s where machine_num = %d and machine_district_num = %d and machine_region_num = %d"),
					TBL_MACHINE,rec.m_nMachineID,rec.m_nDistrictID,rec.m_nRegionID);
		if (exists(sSQL))
		{
			sSQL.Format(_T("update %s set machine_contractor_num = :1 ")
						_T("where machine_num = :2 and machine_region_num = :3 and machine_district_num = :4"),TBL_MACHINE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString() = rec.m_sContractorName;
			m_saCommand.Param(2).setAsShort() = rec.m_nMachineID;
			m_saCommand.Param(3).setAsShort() = rec.m_nRegionID;
			m_saCommand.Param(4).setAsShort() = rec.m_nDistrictID;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CGallBasDB::delMachine(MACHINE_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where machine_num = %d and machine_district_num = %d and machine_region_num = %d"),
					TBL_MACHINE,rec.m_nMachineID,rec.m_nDistrictID,rec.m_nRegionID);
		if (exists(sSQL))
		{
			sSQL.Format(_T("delete from %s where machine_num = :1 and machine_region_num = :2 and machine_district_num = :3"),TBL_MACHINE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_nMachineID;
			m_saCommand.Param(2).setAsShort() = rec.m_nRegionID;
			m_saCommand.Param(3).setAsShort() = rec.m_nDistrictID;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

// TRAKT

// Handle data in trakt_table; 061005 p�d

BOOL CGallBasDB::isThereAnyTraktData(void)
{
	int	nCnt = 0;
	BOOL bReturn;
	TCHAR szBuffer[1024];
	try
	{
		getSQLTraktQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{	
			nCnt++;
			if (nCnt > 0)
				break;
		}		

		m_saConnection.Commit();

		bReturn = (nCnt > 0);

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CGallBasDB::getTrakts(vecTraktData &vec)
{
	TCHAR szBuffer[1024];
	CVariables rec;
	try
	{
		vec.clear();
		getSQLTraktQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			rec	= CVariables(m_saCommand.Field(10).asDouble(),
											 0.0,
											 m_saCommand.Field(11).asDouble(),
											 0.0,
											 m_saCommand.Field(12).asDouble(),
											 m_saCommand.Field(13).asDouble(),
											 m_saCommand.Field(14).asDouble(),
											 m_saCommand.Field(15).asDouble(),
											 m_saCommand.Field(16).asDouble(),
											 m_saCommand.Field(17).asDouble(),
											 m_saCommand.Field(18).asDouble(),
											 m_saCommand.Field(19).asDouble(),
											 m_saCommand.Field(20).asDouble(),
											 m_saCommand.Field(21).asDouble(),
											 m_saCommand.Field(22).asDouble(),
											 m_saCommand.Field(23).asDouble(),
											 m_saCommand.Field(24).asDouble(),
											 m_saCommand.Field(25).asDouble(),
											 m_saCommand.Field(26).asDouble(),
											 m_saCommand.Field(27).asDouble(),
											 m_saCommand.Field(28).asDouble(),
											 m_saCommand.Field(29).asDouble(),
											 m_saCommand.Field(30).asDouble(),
											 m_saCommand.Field(31).asDouble(),
											 m_saCommand.Field(32).asDouble(),
											 m_saCommand.Field(33).asDouble(),
											 m_saCommand.Field("trakt_forrensat").asDouble(),
											 _T(""),0,0,0,0,0,0,0.0,0.0,0.0,0.0,0.0,0,0,0,0,0,0,0,0);
			vec.push_back(_trakt_data(m_saCommand.Field(1).asString(),
																m_saCommand.Field(2).asString(),
																m_saCommand.Field(3).asShort(),
																m_saCommand.Field(4).asShort(),
																m_saCommand.Field(5).asShort(),
																m_saCommand.Field(6).asShort(),
																m_saCommand.Field(7).asString(),
																m_saCommand.Field(8).asString(),
																m_saCommand.Field(34).asShort(),
																m_saCommand.Field(35).asString(),
																m_saCommand.Field(9).asDouble(),
																m_saCommand.Field(36).asString(),
																m_saCommand.Field(37).asString(),
																m_saCommand.Field(38).asString(),
																rec,
																
																m_saCommand.Field("trakt_maskinstorlek").asShort(),
																m_saCommand.Field("trakt_avslut_myr").asShort(),
																m_saCommand.Field("trakt_avslut_hallmark").asShort(),
																m_saCommand.Field("trakt_avslut_hansyn").asShort(),
																m_saCommand.Field("trakt_avslut_fdkulturmark").asShort(),
																m_saCommand.Field("trakt_avslut_kulturmiljo").asShort(),
																m_saCommand.Field("trakt_avslut_lovandel").asShort(),
																m_saCommand.Field("trakt_avslut_skyddszon").asShort(),
																m_saCommand.Field("trakt_avslut_nvtrad").asShort(),
																m_saCommand.Field("trakt_avslut_framtidstrad").asShort(),
																m_saCommand.Field("trakt_avslut_hogstubbar").asShort(),
																m_saCommand.Field("trakt_avslut_dodatrad").asShort(),
																m_saCommand.Field("trakt_avslut_mindrekorskada").asShort(),
																m_saCommand.Field("trakt_avslut_allvarligkorskada").asShort(),
																m_saCommand.Field("trakt_avslut_markskador").asShort(),
																m_saCommand.Field("trakt_avslut_markskador2").asShort(),
																m_saCommand.Field("trakt_avslut_nedskrapning").asShort(),
																m_saCommand.Field("trakt_avslut_fritext").asString(),
																m_saCommand.Field("trakt_avslut_atgardstp").asShort(),
																m_saCommand.Field("trakt_avslut_atgardstp_forklaring").asString(),
																m_saCommand.Field("trakt_avslut_datum_slutford").asString(),
																m_saCommand.Field("trakt_calc_si_spec").asShort(),
																m_saCommand.Field("trakt_calc_si").asShort()
																));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CGallBasDB::addTrakt(TRAKT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{

		sSQL.Format(_T("select * from %s where trakt_machine_num = %d and trakt_district_num = %d and trakt_region_num = %d and ")
					_T("trakt_num = '%s' and trakt_type ='%s' and trakt_origin = %d"),
					TBL_TRAKT,rec.m_nMachineID,rec.m_nDistrictID,rec.m_nRegionID,rec.m_sTraktNum,
					rec.m_sType.Trim(),rec.m_nOrigin);
		if (!exists(sSQL))
		{

			sSQL.Format(_T("insert into %s (trakt_num,trakt_type,trakt_origin,trakt_region_num,trakt_district_num,trakt_machine_num,trakt_name,trakt_date,trakt_areal, ")
				_T("trakt_rwidth,trakt_rdist,trakt_rarea,trakt_gy_before,trakt_gy_after,trakt_gy_wdraw,trakt_gy_quota,trakt_stems,trakt_avg_diam, ")
				_T("trakt_h25,trakt_vol_pine,trakt_vol_spruce,trakt_vol_birch,trakt_vol_cont,trakt_vol_tot,trakt_pine_prop,trakt_spruce_prop, ")
				_T("trakt_birch_prop,trakt_cont_prop,trakt_damaged,trakt_tracks,trakt_high_stumps,trakt_stump_treat,trakt_twigs,trakt_forrensat,trakt_enter_type, ")
				_T("trakt_origin_name,trakt_type_name,trakt_notes, ")
				_T("trakt_maskinstorlek,trakt_avslut_myr,trakt_avslut_hallmark,trakt_avslut_hansyn,trakt_avslut_fdkulturmark,trakt_avslut_kulturmiljo, ")
				_T("trakt_avslut_lovandel,trakt_avslut_skyddszon,trakt_avslut_nvtrad,trakt_avslut_framtidstrad,trakt_avslut_dodatrad, ")
				_T("trakt_avslut_mindrekorskada,trakt_avslut_allvarligkorskada,trakt_avslut_markskador,trakt_avslut_markskador2,trakt_avslut_nedskrapning, ")
				_T("trakt_avslut_fritext,trakt_avslut_atgardstp,trakt_avslut_atgardstp_forklaring,trakt_avslut_datum_slutford,trakt_calc_si_spec,trakt_calc_si,trakt_avslut_hogstubbar) ")
				_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:33,:34,:35,:36,:37,")
				_T(":38,:39,:40,:41,:42,:43,:44,:45,:46,:47,:48,:49,:50,:51,:52,:53,:54,:55,:56,:57,:58,:59,:60,:61)"),TBL_TRAKT);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.m_sTraktNum;
			m_saCommand.Param(2).setAsString()	= rec.m_sType.Trim();
			m_saCommand.Param(3).setAsShort()	= rec.m_nOrigin;
			m_saCommand.Param(4).setAsShort()	= rec.m_nRegionID;
			m_saCommand.Param(5).setAsShort()	= rec.m_nDistrictID;
			m_saCommand.Param(6).setAsShort()	= rec.m_nMachineID;
			m_saCommand.Param(7).setAsString()	= rec.m_sTraktName;
			m_saCommand.Param(8).setAsString()	= rec.m_sTraktDate;
			m_saCommand.Param(9).setAsDouble()	= rec.m_fTraktAreal;
			m_saCommand.Param(10).setAsDouble()	= rec.m_recData.getRWidth1();
			m_saCommand.Param(11).setAsDouble()	= rec.m_recData.getRDist1();
			m_saCommand.Param(12).setAsDouble()	= rec.m_recData.getRArea();
			m_saCommand.Param(13).setAsDouble()	= rec.m_recData.getGYBefore();
			m_saCommand.Param(14).setAsDouble()	= rec.m_recData.getGYAfter();
			m_saCommand.Param(15).setAsDouble()	= rec.m_recData.getGYWDraw();
			m_saCommand.Param(16).setAsDouble()	= rec.m_recData.getGYQuota();
			m_saCommand.Param(17).setAsDouble()	= rec.m_recData.getStems();
			m_saCommand.Param(18).setAsDouble()	= rec.m_recData.getAvgDiam();
			m_saCommand.Param(19).setAsDouble()	= rec.m_recData.getH25();
			m_saCommand.Param(20).setAsDouble()	= rec.m_recData.getVolPine();
			m_saCommand.Param(21).setAsDouble()	= rec.m_recData.getVolSpruce();
			m_saCommand.Param(22).setAsDouble()	= rec.m_recData.getVolBirch();
			m_saCommand.Param(23).setAsDouble()	= rec.m_recData.getVolCont();
			m_saCommand.Param(24).setAsDouble()	= rec.m_recData.getVolTotal();
			m_saCommand.Param(25).setAsDouble()	= rec.m_recData.getPropPine();
			m_saCommand.Param(26).setAsDouble()	= rec.m_recData.getPropSpruce();
			m_saCommand.Param(27).setAsDouble()	= rec.m_recData.getPropBirch();
			m_saCommand.Param(28).setAsDouble()	= rec.m_recData.getPropCont();
			m_saCommand.Param(29).setAsDouble()	= rec.m_recData.getDamaged();
			m_saCommand.Param(30).setAsDouble()	= rec.m_recData.getTracks();
			m_saCommand.Param(31).setAsDouble()	= rec.m_recData.getHighStumps();
			m_saCommand.Param(32).setAsDouble()	= rec.m_recData.getStumpTreat();
			m_saCommand.Param(33).setAsDouble()	= rec.m_recData.getTwigs();
			m_saCommand.Param(34).setAsDouble()	= rec.m_recData.getForrensat();
			m_saCommand.Param(35).setAsShort()	= rec.n_nEnterType;
			m_saCommand.Param(36).setAsString()	= rec.m_sOriginName;
			m_saCommand.Param(37).setAsString()	= rec.m_sTypeName;
			m_saCommand.Param(38).setAsCLob()	= rec.m_sNotes;

			m_saCommand.Param(39).setAsShort()	= rec.m_nMaskinstorlek;
			m_saCommand.Param(40).setAsShort()	= rec.m_nAvslut_myr;
			m_saCommand.Param(41).setAsShort()	= rec.m_nAvslut_hallmark;
			m_saCommand.Param(42).setAsShort()	= rec.m_nAvslut_hansyn;
			m_saCommand.Param(43).setAsShort()	= rec.m_nAvslut_fdkulturmark;
			m_saCommand.Param(44).setAsShort()	= rec.m_nAvslut_kulturmiljo;
			m_saCommand.Param(45).setAsShort()	= rec.m_nAvslut_lovandel;
			m_saCommand.Param(46).setAsShort()	= rec.m_nAvslut_skyddszon;
			m_saCommand.Param(47).setAsShort()	= rec.m_nAvslut_nvtrad;
			m_saCommand.Param(48).setAsShort()	= rec.m_nAvslut_framtidstrad;
			
			m_saCommand.Param(49).setAsShort()	= rec.m_nAvslut_dodatrad;
			m_saCommand.Param(50).setAsShort()	= rec.m_nAvslut_mindrekorskada;
			m_saCommand.Param(51).setAsShort()	= rec.m_nAvslut_allvarligkorskada;
			m_saCommand.Param(52).setAsShort()	= rec.m_nAvslut_markskador;
			m_saCommand.Param(53).setAsShort()	= rec.m_nAvslut_markskador2;
			m_saCommand.Param(54).setAsShort()	= rec.m_nAvslut_nedskrapning;
			m_saCommand.Param(55).setAsString()	= rec.m_sAvslut_fritext;
			m_saCommand.Param(56).setAsShort()	= rec.m_nAvslut_atgardstp;
			m_saCommand.Param(57).setAsString()	= rec.m_sAvslut_atgardstp_forklaring;
			m_saCommand.Param(58).setAsString()	= rec.m_sAvslut_datum_slutford;
			m_saCommand.Param(59).setAsShort()	= rec.m_nCalc_si_spec;
			m_saCommand.Param(60).setAsShort()	= rec.m_nCalc_si;
			m_saCommand.Param(61).setAsShort()	= rec.m_nAvslut_hogstubbar;

			m_saCommand.Prepare();

			m_saCommand.Execute();

			m_saConnection.Commit();

			bReturn = TRUE;
		}
	}
	catch(SAException &)
	{
		 // print error message
//		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CGallBasDB::updTrakt(TRAKT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where trakt_machine_num = %d and trakt_district_num = %d and trakt_region_num = %d and ")
					_T("trakt_num = '%s' and trakt_type ='%s' and trakt_origin = %d"),
					TBL_TRAKT,rec.m_nMachineID,rec.m_nDistrictID,rec.m_nRegionID,rec.m_sTraktNum,rec.m_sType.Trim(),rec.m_nOrigin);
		if (exists(sSQL))
		{

			sSQL.Format(_T("update %s set trakt_name=:1,trakt_date=:2,trakt_areal=:3,trakt_rwidth=:4,trakt_rdist=:5,trakt_rarea=:6,trakt_gy_before=:7,")
						_T("trakt_gy_after=:8,trakt_gy_wdraw=:9,trakt_gy_quota=:10,trakt_stems=:11,trakt_avg_diam=:12,trakt_h25=:13,")
						_T("trakt_vol_pine=:14,trakt_vol_spruce=:15,trakt_vol_birch=:16,trakt_vol_cont=:17,trakt_vol_tot=:18,")
						_T("trakt_pine_prop=:19,trakt_spruce_prop=:20,trakt_birch_prop=:21,trakt_cont_prop=:22,trakt_damaged=:23,")
						_T("trakt_tracks=:24,trakt_high_stumps=:25,trakt_stump_treat=:26,trakt_twigs=:27,trakt_forrensat=:28,trakt_notes=:29,")
						_T("trakt_maskinstorlek=:30,")
						_T("trakt_avslut_myr=:31,")
						_T("trakt_avslut_hallmark=:32,")
						_T("trakt_avslut_hansyn=:33,")
						_T("trakt_avslut_fdkulturmark=:34,")
						_T("trakt_avslut_kulturmiljo=:35, ")
						_T("trakt_avslut_lovandel=:36,")
						_T("trakt_avslut_skyddszon=:37,")
						_T("trakt_avslut_nvtrad=:38,")
						_T("trakt_avslut_framtidstrad=:39,")
						_T("trakt_avslut_dodatrad=:40, ")
						_T("trakt_avslut_mindrekorskada=:41,")
						_T("trakt_avslut_allvarligkorskada=:42,")
						_T("trakt_avslut_markskador=:43,")
						_T("trakt_avslut_markskador2=:44,")
						_T("trakt_avslut_nedskrapning=:45, ")
						_T("trakt_avslut_fritext=:46,")
						_T("trakt_avslut_atgardstp=:47,")
						_T("trakt_avslut_atgardstp_forklaring=:48,")
						_T("trakt_avslut_datum_slutford=:49,")
						_T("trakt_calc_si_spec=:50,")
						_T("trakt_calc_si=:51, ")
						_T("trakt_avslut_hogstubbar=:52 ")						
						_T("where trakt_num=:53 and trakt_type=:54 and trakt_origin=:55 and ")
						_T("trakt_region_num=:56 and trakt_district_num=:57 and trakt_machine_num=:58"),TBL_TRAKT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.m_sTraktName;
			m_saCommand.Param(2).setAsString()	= rec.m_sTraktDate;
			m_saCommand.Param(3).setAsDouble()	= rec.m_fTraktAreal;
			m_saCommand.Param(4).setAsDouble()	= rec.m_recData.getRWidth1();
			m_saCommand.Param(5).setAsDouble()	= rec.m_recData.getRDist1();
			m_saCommand.Param(6).setAsDouble()	= rec.m_recData.getRArea();
			m_saCommand.Param(7).setAsDouble()	= rec.m_recData.getGYBefore();
			m_saCommand.Param(8).setAsDouble()	= rec.m_recData.getGYAfter();
			m_saCommand.Param(9).setAsDouble()	= rec.m_recData.getGYWDraw();
			m_saCommand.Param(10).setAsDouble()	= rec.m_recData.getGYQuota();
			m_saCommand.Param(11).setAsDouble()	= rec.m_recData.getStems();
			m_saCommand.Param(12).setAsDouble()	= rec.m_recData.getAvgDiam();
			m_saCommand.Param(13).setAsDouble()	= rec.m_recData.getH25();
			m_saCommand.Param(14).setAsDouble()	= rec.m_recData.getVolPine();
			m_saCommand.Param(15).setAsDouble()	= rec.m_recData.getVolSpruce();
			m_saCommand.Param(16).setAsDouble()	= rec.m_recData.getVolBirch();
			m_saCommand.Param(17).setAsDouble()	= rec.m_recData.getVolCont();
			m_saCommand.Param(18).setAsDouble()	= rec.m_recData.getVolTotal();
			m_saCommand.Param(19).setAsDouble()	= rec.m_recData.getPropPine();
			m_saCommand.Param(20).setAsDouble()	= rec.m_recData.getPropSpruce();
			m_saCommand.Param(21).setAsDouble()	= rec.m_recData.getPropBirch();
			m_saCommand.Param(22).setAsDouble()	= rec.m_recData.getPropCont();
			m_saCommand.Param(23).setAsDouble()	= rec.m_recData.getDamaged();
			m_saCommand.Param(24).setAsDouble()	= rec.m_recData.getTracks();
			m_saCommand.Param(25).setAsDouble()	= rec.m_recData.getHighStumps();
			m_saCommand.Param(26).setAsDouble()	= rec.m_recData.getStumpTreat();
			m_saCommand.Param(27).setAsDouble()	= rec.m_recData.getTwigs();
			m_saCommand.Param(28).setAsDouble()	= rec.m_recData.getForrensat();
			m_saCommand.Param(29).setAsCLob()	= rec.m_sNotes;

			m_saCommand.Param(30).setAsShort()	= rec.m_nMaskinstorlek;
			m_saCommand.Param(31).setAsShort()	= rec.m_nAvslut_myr;
			m_saCommand.Param(32).setAsShort()	= rec.m_nAvslut_hallmark;
			m_saCommand.Param(33).setAsShort()	= rec.m_nAvslut_hansyn;
			m_saCommand.Param(34).setAsShort()	= rec.m_nAvslut_fdkulturmark;
			m_saCommand.Param(35).setAsShort()	= rec.m_nAvslut_kulturmiljo;
			m_saCommand.Param(36).setAsShort()	= rec.m_nAvslut_lovandel;
			m_saCommand.Param(37).setAsShort()	= rec.m_nAvslut_skyddszon;
			m_saCommand.Param(38).setAsShort()	= rec.m_nAvslut_nvtrad;
			m_saCommand.Param(39).setAsShort()	= rec.m_nAvslut_framtidstrad;
			m_saCommand.Param(40).setAsShort()	= rec.m_nAvslut_dodatrad;
			m_saCommand.Param(41).setAsShort()	= rec.m_nAvslut_mindrekorskada;
			m_saCommand.Param(42).setAsShort()	= rec.m_nAvslut_allvarligkorskada;
			m_saCommand.Param(43).setAsShort()	= rec.m_nAvslut_markskador;
			m_saCommand.Param(44).setAsShort()	= rec.m_nAvslut_markskador2;
			m_saCommand.Param(45).setAsShort()	= rec.m_nAvslut_nedskrapning;
			m_saCommand.Param(46).setAsString()	= rec.m_sAvslut_fritext;
			m_saCommand.Param(47).setAsShort()	= rec.m_nAvslut_atgardstp;
			m_saCommand.Param(48).setAsString()	= rec.m_sAvslut_atgardstp_forklaring;
			m_saCommand.Param(49).setAsString()	= rec.m_sAvslut_datum_slutford;
			m_saCommand.Param(50).setAsShort()	= rec.m_nCalc_si_spec;
			m_saCommand.Param(51).setAsShort()	= rec.m_nCalc_si;
			m_saCommand.Param(52).setAsShort()	= rec.m_nAvslut_hogstubbar;

			m_saCommand.Param(53).setAsString()	= rec.m_sTraktNum;
			m_saCommand.Param(54).setAsString()	= rec.m_sType.Trim();
			m_saCommand.Param(55).setAsShort()	= rec.m_nOrigin;
			m_saCommand.Param(56).setAsShort()	= rec.m_nRegionID;
			m_saCommand.Param(57).setAsShort()	= rec.m_nDistrictID;
			m_saCommand.Param(58).setAsShort()	= rec.m_nMachineID;

			m_saCommand.Prepare();

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

// Delete trakt associated to a specific machine,district and region
BOOL CGallBasDB::delTrakt(TRAKT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where trakt_machine_num = %d and trakt_district_num = %d and trakt_region_num = %d and ")
					_T("trakt_num = '%s' and trakt_type ='%s' and trakt_origin = %d"),
					TBL_TRAKT,rec.m_nMachineID,rec.m_nDistrictID,rec.m_nRegionID,rec.m_sTraktNum,rec.m_sType.Trim(),rec.m_nOrigin);
		if (exists(sSQL))
		{
			sSQL.Format(_T("delete from %s where trakt_district_num=:1 and trakt_region_num=:2 and trakt_machine_num=:3 and trakt_type=:4 and trakt_origin=:5 and trakt_num=:6"),TBL_TRAKT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_nDistrictID;
			m_saCommand.Param(2).setAsShort() = rec.m_nRegionID;
			m_saCommand.Param(3).setAsShort() = rec.m_nMachineID;
			m_saCommand.Param(4).setAsString() = rec.m_sType.Trim();
			m_saCommand.Param(5).setAsShort() = rec.m_nOrigin;
			m_saCommand.Param(6).setAsString() = rec.m_sTraktNum;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}


	return bReturn;
}

// COMPARTMENTS


// Handle data in compart_table; 061012 p�d
BOOL CGallBasDB::isThereAnyCompartData(void)
{
	int	nCnt = 0;
	BOOL bReturn;
	TCHAR szBuffer[1024];
	try
	{
		getSQLCompartQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{	
			nCnt++;
			if (nCnt > 0)
				break;
		}		

		m_saConnection.Commit();

		bReturn = (nCnt > 0);

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CGallBasDB::getCompart(vecCompartData &vec)
{
	TCHAR szBuffer[1024];
	CVariables rec;
	try
	{
		vec.clear();
		getSQLCompartQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			rec	= CVariables(m_saCommand.Field(8).asDouble(),
											 0.0,
											 m_saCommand.Field(9).asDouble(),
											 0.0,
											 m_saCommand.Field(10).asDouble(),
											 m_saCommand.Field(11).asDouble(),
											 m_saCommand.Field(12).asDouble(),
											 m_saCommand.Field(13).asDouble(),
											 m_saCommand.Field(14).asDouble(),
											 m_saCommand.Field(15).asDouble(),
											 m_saCommand.Field(16).asDouble(),
											 m_saCommand.Field(17).asDouble(),
											 m_saCommand.Field(18).asDouble(),
											 m_saCommand.Field(19).asDouble(),
											 m_saCommand.Field(20).asDouble(),
											 m_saCommand.Field(21).asDouble(),
											 m_saCommand.Field(22).asDouble(),
											 m_saCommand.Field(23).asDouble(),
											 m_saCommand.Field(24).asDouble(),
											 m_saCommand.Field(25).asDouble(),
											 m_saCommand.Field(26).asDouble(),
											 m_saCommand.Field(27).asDouble(),
											 m_saCommand.Field(28).asDouble(),
											 m_saCommand.Field(29).asDouble(),
											 m_saCommand.Field(30).asDouble(),
											 m_saCommand.Field(31).asDouble(),
											 m_saCommand.Field("compart_forrensat").asDouble(),
											 m_saCommand.Field("compart_spec_and_si").asString(),
											 m_saCommand.Field("compart_gallrtyp").asShort(),
											 m_saCommand.Field("compart_metod").asShort(),											 
											 m_saCommand.Field("compart_lagsta_gy_enl_mall").asShort(),
											 m_saCommand.Field("compart_traktdir_gy_fore_m2ha").asShort(),
											 m_saCommand.Field("compart_traktdir_gy_efter_m2ha").asShort(),
											 m_saCommand.Field("compart_traktdir_ga_uttag").asShort(),
											 m_saCommand.Field("compart_gy_wdraw_forest").asShort(),
											 m_saCommand.Field("compart_gy_wdraw_road").asShort(),
											 m_saCommand.Field("compart_oh").asDouble(),
											 m_saCommand.Field("compart_areal").asDouble(),
											 m_saCommand.Field("compart_antal_stubbar").asDouble(),
											 0,0,0,0,0,0,0,0);
			vec.push_back(_compart_data(m_saCommand.Field(1).asLong(),
																m_saCommand.Field(2).asString(),
																m_saCommand.Field(3).asShort(),
																m_saCommand.Field(4).asShort(),
																m_saCommand.Field(5).asShort(),
																m_saCommand.Field(6).asShort(),
																m_saCommand.Field(7).asString(),
																m_saCommand.Field(32).asString(),
																rec,
																m_saCommand.Field("compart_gallrtyp").asShort(),
																m_saCommand.Field("compart_metod").asShort(),
																m_saCommand.Field("compart_spec_and_si").asString(),
																m_saCommand.Field("compart_si_spec").asShort(),
																m_saCommand.Field("compart_si").asShort(),
																m_saCommand.Field("compart_lagsta_gy_enl_mall").asShort(),
																m_saCommand.Field("compart_traktdir_gy_fore_m2ha").asShort(),
																m_saCommand.Field("compart_traktdir_gy_efter_m2ha").asShort(),
																m_saCommand.Field("compart_traktdir_ga_uttag").asShort(),
																m_saCommand.Field("compart_gy_wdraw_forest").asDouble(),
																m_saCommand.Field("compart_gy_wdraw_road").asDouble(),
																m_saCommand.Field("compart_oh").asDouble(),
																m_saCommand.Field("compart_areal").asDouble(),
																m_saCommand.Field("compart_antal_stubbar").asDouble()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CGallBasDB::addCompart(COMPART_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where compart_num = %d and compart_machine_num = %d and compart_district_num = %d and compart_region_num = %d and ")
					_T("compart_trakt_num = '%s' and compart_type ='%s' and compart_origin = %d"),
					TBL_COMPART,
					rec.m_lCompartID,
					rec.m_nMachineID,
					rec.m_nDistrictID,
					rec.m_nRegionID,
					rec.m_sTraktNum,
					rec.m_sType.Trim(),
					rec.m_nOrigin);
		if (!exists(sSQL))
		{

			sSQL.Format(_T("insert into %s (compart_num,compart_type,compart_origin,compart_region_num,compart_district_num,compart_machine_num,compart_trakt_num, ")
						_T("compart_rwidth,compart_rdist,compart_rarea,compart_gy_before,compart_gy_after,compart_gy_wdraw,compart_gy_quota,compart_stems,compart_avg_diam, ")
						_T("compart_h25,compart_vol_pine,compart_vol_spruce,compart_vol_birch,compart_vol_cont,compart_vol_tot,compart_pine_prop,compart_spruce_prop, ")						
						_T("compart_birch_prop,compart_cont_prop,compart_damaged,compart_tracks,compart_high_stumps,compart_stump_treat,compart_twigs, ")
						_T("compart_spec_and_si,compart_gallrtyp,compart_metod,compart_lagsta_gy_enl_mall,compart_traktdir_gy_fore_m2ha,compart_traktdir_gy_efter_m2ha,compart_traktdir_ga_uttag,compart_gy_wdraw_forest,compart_gy_wdraw_road,compart_oh,compart_areal,compart_antal_stubbar,compart_forrensat) ") 
						_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:33,:34,:35,:36,:37,:38,:39,:40,:41,:42,:43,:44)"),
						TBL_COMPART);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.m_lCompartID;
			m_saCommand.Param(2).setAsString()	= rec.m_sType.Trim();
			m_saCommand.Param(3).setAsShort()	= rec.m_nOrigin;
			m_saCommand.Param(4).setAsShort()	= rec.m_nRegionID;
			m_saCommand.Param(5).setAsShort()	= rec.m_nDistrictID;
			m_saCommand.Param(6).setAsShort()	= rec.m_nMachineID;
			m_saCommand.Param(7).setAsString()	= rec.m_sTraktNum;
			m_saCommand.Param(8).setAsDouble()	= rec.m_recData.getRWidth1();
			m_saCommand.Param(9).setAsDouble()	= rec.m_recData.getRDist1();
			m_saCommand.Param(10).setAsDouble()	= rec.m_recData.getRArea();
			m_saCommand.Param(11).setAsDouble()	= rec.m_recData.getGYBefore();
			m_saCommand.Param(12).setAsDouble()	= rec.m_recData.getGYAfter();
			m_saCommand.Param(13).setAsDouble()	= rec.m_recData.getGYWDraw();
			m_saCommand.Param(14).setAsDouble()	= rec.m_recData.getGYQuota();
			m_saCommand.Param(15).setAsDouble()	= rec.m_recData.getStems();
			m_saCommand.Param(16).setAsDouble()	= rec.m_recData.getAvgDiam();
			m_saCommand.Param(17).setAsDouble()	= rec.m_recData.getH25();
			m_saCommand.Param(18).setAsDouble()	= rec.m_recData.getVolPine();
			m_saCommand.Param(19).setAsDouble()	= rec.m_recData.getVolSpruce();
			m_saCommand.Param(20).setAsDouble()	= rec.m_recData.getVolBirch();
			m_saCommand.Param(21).setAsDouble()	= rec.m_recData.getVolCont();
			m_saCommand.Param(22).setAsDouble()	= rec.m_recData.getVolTotal();
			m_saCommand.Param(23).setAsDouble()	= rec.m_recData.getPropPine();
			m_saCommand.Param(24).setAsDouble()	= rec.m_recData.getPropSpruce();
			m_saCommand.Param(25).setAsDouble()	= rec.m_recData.getPropBirch();
			m_saCommand.Param(26).setAsDouble()	= rec.m_recData.getPropCont();
			m_saCommand.Param(27).setAsDouble()	= rec.m_recData.getDamaged();
			m_saCommand.Param(28).setAsDouble()	= rec.m_recData.getTracks();
			m_saCommand.Param(29).setAsDouble()	= rec.m_recData.getHighStumps();
			m_saCommand.Param(30).setAsDouble()	= rec.m_recData.getStumpTreat();
			m_saCommand.Param(31).setAsDouble()	= rec.m_recData.getTwigs();

			m_saCommand.Param(32).setAsString() = rec.m_recData.getCompSpecAndSi();
			m_saCommand.Param(33).setAsShort() = rec.m_recData.getCompGallrTyp();
			m_saCommand.Param(34).setAsShort() = rec.m_recData.getCompMetod();
			m_saCommand.Param(35).setAsShort() = rec.m_recData.getCompLagstaGy();
			m_saCommand.Param(36).setAsShort() = rec.m_recData.getCompGyFore();
			m_saCommand.Param(37).setAsShort() = rec.m_recData.getCompGyEfter();
			m_saCommand.Param(38).setAsShort() = rec.m_recData.getCompGaUttag();

			m_saCommand.Param(39).setAsDouble()= rec.m_recData.getGYWDrawForest();
			m_saCommand.Param(40).setAsDouble()= rec.m_recData.getGYWDrawRoad();
			m_saCommand.Param(41).setAsDouble()= rec.m_recData.getOh();
			m_saCommand.Param(42).setAsDouble()= rec.m_recData.getAreal();
			m_saCommand.Param(43).setAsDouble()= rec.m_recData.getAntalStubbar();
			m_saCommand.Param(44).setAsDouble()= rec.m_recData.getForrensat();
			

			m_saCommand.Prepare();

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CGallBasDB::updCompart(COMPART_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{

		sSQL.Format(_T("select * from %s where compart_num = %d and compart_machine_num = %d and compart_district_num = %d and compart_region_num = %d and ")
					_T("compart_trakt_num = '%s' and compart_type ='%s' and compart_origin = %d"),
					TBL_COMPART,
					rec.m_lCompartID,
					rec.m_nMachineID,
					rec.m_nDistrictID,
					rec.m_nRegionID,
					rec.m_sTraktNum,
					rec.m_sType.Trim(),
					rec.m_nOrigin);
		if (exists(sSQL))
		{

			sSQL.Format(_T("update %s set compart_rwidth=:1,compart_rdist=:2,compart_rarea=:3,compart_gy_before=:4,")
						_T("compart_gy_after=:5,compart_gy_wdraw=:6,compart_gy_quota=:7,compart_stems=:8,compart_avg_diam=:9,compart_h25=:10,")
						_T("compart_vol_pine=:11,compart_vol_spruce=:12,compart_vol_birch=:13,compart_vol_cont=:14,compart_vol_tot=:15,")
						_T("compart_pine_prop=:16,compart_spruce_prop=:17,compart_birch_prop=:18,compart_cont_prop=:19,compart_damaged=:20,")
						_T("compart_tracks=:21,compart_high_stumps=:22,compart_stump_treat=:23,compart_twigs=:24, ")
						_T("compart_spec_and_si=:25,compart_gallrtyp=:26,compart_metod=:27,compart_lagsta_gy_enl_mall=:28,compart_traktdir_gy_fore_m2ha=:29,compart_traktdir_gy_efter_m2ha=:30,compart_traktdir_ga_uttag=:31,compart_gy_wdraw_forest=:32,compart_gy_wdraw_road=:33 ,compart_oh=:34,compart_areal=:35,compart_antal_stubbar=:36,compart_forrensat=:37 ")
						_T("where compart_num=:38 and compart_type=:39 and compart_origin=:40 and ")
						_T("compart_region_num=:41 and compart_district_num=:42 and compart_machine_num=:43 and compart_trakt_num=:44"),
						TBL_COMPART);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsDouble()	= rec.m_recData.getRWidth1();
			m_saCommand.Param(2).setAsDouble()	= rec.m_recData.getRDist1();
			m_saCommand.Param(3).setAsDouble()	= rec.m_recData.getRArea();
			m_saCommand.Param(4).setAsDouble()	= rec.m_recData.getGYBefore();
			m_saCommand.Param(5).setAsDouble()	= rec.m_recData.getGYAfter();
			m_saCommand.Param(6).setAsDouble()	= rec.m_recData.getGYWDraw();
			m_saCommand.Param(7).setAsDouble()	= rec.m_recData.getGYQuota();
			m_saCommand.Param(8).setAsDouble()	= rec.m_recData.getStems();
			m_saCommand.Param(9).setAsDouble()	= rec.m_recData.getAvgDiam();
			m_saCommand.Param(10).setAsDouble()	= rec.m_recData.getH25();
			m_saCommand.Param(11).setAsDouble()	= rec.m_recData.getVolPine();
			m_saCommand.Param(12).setAsDouble()	= rec.m_recData.getVolSpruce();
			m_saCommand.Param(13).setAsDouble()	= rec.m_recData.getVolBirch();
			m_saCommand.Param(14).setAsDouble()	= rec.m_recData.getVolCont();
			m_saCommand.Param(15).setAsDouble()	= rec.m_recData.getVolTotal();
			m_saCommand.Param(16).setAsDouble()	= rec.m_recData.getPropPine();
			m_saCommand.Param(17).setAsDouble()	= rec.m_recData.getPropSpruce();
			m_saCommand.Param(18).setAsDouble()	= rec.m_recData.getPropBirch();
			m_saCommand.Param(19).setAsDouble()	= rec.m_recData.getPropCont();
			m_saCommand.Param(20).setAsDouble()	= rec.m_recData.getDamaged();
			m_saCommand.Param(21).setAsDouble()	= rec.m_recData.getTracks();
			m_saCommand.Param(22).setAsDouble()	= rec.m_recData.getHighStumps();
			m_saCommand.Param(23).setAsDouble()	= rec.m_recData.getStumpTreat();
			m_saCommand.Param(24).setAsDouble()	= rec.m_recData.getTwigs();

			m_saCommand.Param(25).setAsString() = rec.m_recData.getCompSpecAndSi();
			m_saCommand.Param(26).setAsShort() = rec.m_recData.getCompGallrTyp();
			m_saCommand.Param(27).setAsShort() = rec.m_recData.getCompMetod();
			m_saCommand.Param(28).setAsShort() = rec.m_recData.getCompLagstaGy();
			m_saCommand.Param(29).setAsShort() = rec.m_recData.getCompGyFore();
			m_saCommand.Param(30).setAsShort() = rec.m_recData.getCompGyEfter();
			m_saCommand.Param(31).setAsShort() = rec.m_recData.getCompGaUttag();
			m_saCommand.Param(32).setAsDouble() = rec.m_recData.getGYWDrawForest();
			m_saCommand.Param(33).setAsDouble() = rec.m_recData.getGYWDrawRoad();
			m_saCommand.Param(34).setAsDouble() = rec.m_recData.getOh();
			m_saCommand.Param(35).setAsDouble() = rec.m_recData.getAreal();
			m_saCommand.Param(36).setAsDouble() = rec.m_recData.getAntalStubbar();
			m_saCommand.Param(37).setAsDouble() = rec.m_recData.getForrensat();


			m_saCommand.Param(38).setAsLong()	= rec.m_lCompartID;
			m_saCommand.Param(39).setAsString()	= rec.m_sType.Trim();
			m_saCommand.Param(40).setAsShort()	= rec.m_nOrigin;
			m_saCommand.Param(41).setAsShort()	= rec.m_nRegionID;
			m_saCommand.Param(42).setAsShort()	= rec.m_nDistrictID;
			m_saCommand.Param(43).setAsShort()	= rec.m_nMachineID;
			m_saCommand.Param(44).setAsString()	= rec.m_sTraktNum;

			

			m_saCommand.Prepare();

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

// Delete compartment (avdelning) associated to a specific machine,district,region and machine
BOOL CGallBasDB::delCompart(COMPART_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where  compart_district_num = %d and compart_region_num = %d and compart_machine_num = %d and compart_trakt_num = '%s' and compart_type = '%s' and compart_origin = %d"),
					TBL_COMPART,
					rec.m_nDistrictID,
					rec.m_nRegionID,
					rec.m_nMachineID,
					rec.m_sTraktNum,
					rec.m_sType.Trim(),
					rec.m_nOrigin);

		if (exists(sSQL))
		{
			sSQL.Format(_T("delete from %s where compart_district_num=:1 and compart_region_num=:2 and compart_machine_num=:3 and compart_trakt_num=:4 and compart_type=:5 and compart_origin=:6 and compart_num=:7"),
						TBL_COMPART);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort()		= rec.m_nDistrictID;
			m_saCommand.Param(2).setAsShort()		= rec.m_nRegionID;
			m_saCommand.Param(3).setAsShort()		= rec.m_nMachineID;
			m_saCommand.Param(4).setAsString()	= rec.m_sTraktNum;
			m_saCommand.Param(5).setAsString()	= rec.m_sType.Trim();
			m_saCommand.Param(6).setAsShort()		= rec.m_nOrigin;
			m_saCommand.Param(7).setAsLong()		= rec.m_lCompartID;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}


	return bReturn;
}


// PLOTS


// Handle data in compart_table; 061012 p�d
BOOL CGallBasDB::isThereAnyPlotData(void)
{
	int	nCnt = 0;
	BOOL bReturn;
	TCHAR szBuffer[1024];
	try
	{
		getSQLPlotQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{	
			nCnt++;
			if (nCnt > 0)
				break;
		}		

		m_saConnection.Commit();

		bReturn = (nCnt > 0);

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}
BOOL CGallBasDB::getPlot(vecPlotData &vec)
{
	TCHAR szBuffer[1024];
	CVariables rec;
	try
	{
		vec.clear();
		getSQLPlotQuestion(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			rec	= CVariables(m_saCommand.Field(9).asDouble(),
											 m_saCommand.Field(10).asDouble(),
											 m_saCommand.Field(11).asDouble(),
											 m_saCommand.Field(12).asDouble(),
											 m_saCommand.Field(13).asDouble(),
											 m_saCommand.Field(14).asDouble(),
											 m_saCommand.Field(15).asDouble(),
											 m_saCommand.Field(16).asDouble(),
											 m_saCommand.Field(17).asDouble(),
											 m_saCommand.Field(18).asDouble(),
											 m_saCommand.Field(19).asDouble(),
											 m_saCommand.Field(20).asDouble(),
											 m_saCommand.Field(21).asDouble(),
											 m_saCommand.Field(22).asDouble(),
											 m_saCommand.Field(23).asDouble(),
											 m_saCommand.Field(24).asDouble(),
											 m_saCommand.Field(25).asDouble(),
											 m_saCommand.Field(26).asDouble(),
											 m_saCommand.Field(27).asDouble(),
											 m_saCommand.Field(28).asDouble(),
											 m_saCommand.Field(29).asDouble(),
											 m_saCommand.Field(30).asDouble(),
											 m_saCommand.Field(31).asDouble(),
											 m_saCommand.Field(32).asDouble(),
											 m_saCommand.Field(33).asDouble(),
											 m_saCommand.Field(34).asDouble(),
											 m_saCommand.Field("plot_forrensat").asDouble(),
											 _T(""),0,0,0,0,0,0,0.0,0.0,0.0,0.0,0.0,
											 m_saCommand.Field("plot_Ytradie").asShort(),
			m_saCommand.Field("plot_AntalTrad").asShort(),
			m_saCommand.Field("plot_AntalStubbar").asShort(),
			m_saCommand.Field("plot_AntalStubbarivag").asShort(),
			m_saCommand.Field("plot_AntalStubbariskog").asShort(),
			m_saCommand.Field("plot_Totdiamkvarvarande").asShort(),
			m_saCommand.Field("plot_Totdiamuttag").asShort(),
			m_saCommand.Field("plot_AntalHogaStubbar").asShort());
			vec.push_back(_plot_data(m_saCommand.Field(1).asShort(),
															 m_saCommand.Field(2).asString(),
															 m_saCommand.Field(3).asShort(),
															 m_saCommand.Field(4).asShort(),
															 m_saCommand.Field(5).asShort(),
															 m_saCommand.Field(6).asShort(),
															 m_saCommand.Field(7).asString(),
															 m_saCommand.Field(8).asLong(),
															 m_saCommand.Field(35).asString(),
															 rec));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CGallBasDB::addPlot(PLOT_DATA &rec)
{
	CString sSQL;
	CString tmp;
	BOOL bReturn = FALSE;
	try
	{

		sSQL.Format(_T("select * from %s where plot_num = %d and plot_machine_num = %d and plot_district_num = %d and plot_region_num = %d and ")
					_T("plot_trakt_num = '%s' and plot_compart_num = %d and plot_type ='%s' and plot_origin = %d"),
					TBL_PLOT,
					rec.m_nPlotID,
					rec.m_nMachineID,
					rec.m_nDistrictID,
					rec.m_nRegionID,
					rec.m_sTraktNum,
					rec.m_lCompartID,
					rec.m_sType.Trim(),
					rec.m_nOrigin);
		if (!exists(sSQL))
		{

			sSQL.Format(_T("insert into %s (plot_num,plot_type,plot_origin,plot_region_num,plot_district_num,plot_machine_num,plot_trakt_num,plot_compart_num, ")
						_T("plot_rwidth1,plot_rwidth2,plot_rdist1,plot_rdist2,plot_rarea,plot_gy_before,plot_gy_after,plot_gy_wdraw,plot_gy_quota,plot_stems,plot_avg_diam, ")
						_T("plot_h25,plot_vol_pine,plot_vol_spruce,plot_vol_birch,plot_vol_cont,plot_vol_tot,plot_pine_prop,plot_spruce_prop, ")
						_T("plot_birch_prop,plot_cont_prop,plot_damaged,plot_tracks,plot_high_stumps,plot_stump_treat,plot_twigs,")
						_T("plot_Ytradie,plot_AntalTrad,plot_AntalStubbar,plot_AntalStubbarivag,plot_AntalStubbariskog,plot_Totdiamkvarvarande,plot_Totdiamuttag,plot_AntalHogaStubbar,plot_forrensat) ")
						_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:33,:34,:35,:36,:37,:38,:39,:40,:41,:42,:43 )"),
						TBL_PLOT);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort()	= rec.m_nPlotID;
			m_saCommand.Param(2).setAsString()	= rec.m_sType.Trim();
			m_saCommand.Param(3).setAsShort()	= rec.m_nOrigin;
			m_saCommand.Param(4).setAsShort()	= rec.m_nRegionID;
			m_saCommand.Param(5).setAsShort()	= rec.m_nDistrictID;
			m_saCommand.Param(6).setAsShort()	= rec.m_nMachineID;
			m_saCommand.Param(7).setAsString()	= rec.m_sTraktNum;
			m_saCommand.Param(8).setAsLong()	= rec.m_lCompartID;
			m_saCommand.Param(9).setAsDouble()	= rec.m_recData.getRWidth1();
			m_saCommand.Param(10).setAsDouble()	= rec.m_recData.getRWidth2();
			m_saCommand.Param(11).setAsDouble()	= rec.m_recData.getRDist1();
			m_saCommand.Param(12).setAsDouble()	= rec.m_recData.getRDist2();
			m_saCommand.Param(13).setAsDouble()	= rec.m_recData.getRArea();
			m_saCommand.Param(14).setAsDouble()	= rec.m_recData.getGYBefore();
			m_saCommand.Param(15).setAsDouble()	= rec.m_recData.getGYAfter();
			m_saCommand.Param(16).setAsDouble()	= rec.m_recData.getGYWDraw();
			m_saCommand.Param(17).setAsDouble()	= rec.m_recData.getGYQuota();
			m_saCommand.Param(18).setAsDouble()	= rec.m_recData.getStems();
			m_saCommand.Param(19).setAsDouble()	= rec.m_recData.getAvgDiam();
			m_saCommand.Param(20).setAsDouble()	= rec.m_recData.getH25();
			m_saCommand.Param(21).setAsDouble()	= rec.m_recData.getVolPine();
			m_saCommand.Param(22).setAsDouble()	= rec.m_recData.getVolSpruce();
			m_saCommand.Param(23).setAsDouble()	= rec.m_recData.getVolBirch();
			m_saCommand.Param(24).setAsDouble()	= rec.m_recData.getVolCont();
			m_saCommand.Param(25).setAsDouble()	= rec.m_recData.getVolTotal();
			m_saCommand.Param(26).setAsDouble()	= rec.m_recData.getPropPine();
			m_saCommand.Param(27).setAsDouble()	= rec.m_recData.getPropSpruce();
			m_saCommand.Param(28).setAsDouble()	= rec.m_recData.getPropBirch();
			m_saCommand.Param(29).setAsDouble()	= rec.m_recData.getPropCont();
			m_saCommand.Param(30).setAsDouble()	= rec.m_recData.getDamaged();
			m_saCommand.Param(31).setAsDouble()	= rec.m_recData.getTracks();
			m_saCommand.Param(32).setAsDouble()	= rec.m_recData.getHighStumps();
			m_saCommand.Param(33).setAsDouble()	= rec.m_recData.getStumpTreat();
			m_saCommand.Param(34).setAsDouble()	= rec.m_recData.getTwigs();

			m_saCommand.Param(35).setAsShort()	= rec.m_recData.getPlot_Ytradie();
			m_saCommand.Param(36).setAsShort()	= rec.m_recData.getPlot_AntalTrad();
			m_saCommand.Param(37).setAsShort()	= rec.m_recData.getPlot_AntalStubbar();
			m_saCommand.Param(38).setAsShort()	= rec.m_recData.getPlot_AntalStubbarivag();
			m_saCommand.Param(39).setAsShort()	= rec.m_recData.getPlot_AntalStubbariskog();
			m_saCommand.Param(40).setAsShort()	= rec.m_recData.getPlot_Totdiamkvarvarande();
			m_saCommand.Param(41).setAsShort()	= rec.m_recData.getPlot_Totdiamuttag();
			m_saCommand.Param(42).setAsShort()	= rec.m_recData.getPlot_AntalHogaStubbar();
			m_saCommand.Param(43).setAsDouble()	= rec.m_recData.getForrensat();

			m_saCommand.Prepare();

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CGallBasDB::updPlot(PLOT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where plot_num = %d and plot_machine_num = %d and plot_district_num = %d and plot_region_num = %d and ")
					_T("plot_trakt_num = '%s' and plot_compart_num = %d and plot_type ='%s' and plot_origin = %d"),
					TBL_PLOT,
					rec.m_nPlotID,
					rec.m_nMachineID,
					rec.m_nDistrictID,
					rec.m_nRegionID,
					rec.m_sTraktNum,
					rec.m_lCompartID,
					rec.m_sType.Trim(),
					rec.m_nOrigin);

		if (exists(sSQL))
		{

			sSQL.Format(_T("update %s set plot_rwidth1=:1,plot_rwidth2=:2,plot_rdist1=:3,plot_rdist2=:4,plot_rarea=:5,plot_gy_before=:6,")
						_T("plot_gy_after=:7,plot_gy_wdraw=:8,plot_gy_quota=:9,plot_stems=:10,plot_avg_diam=:11,plot_h25=:12,")
						_T("plot_vol_pine=:13,plot_vol_spruce=:14,plot_vol_birch=:15,plot_vol_cont=:16,plot_vol_tot=:17,")
						_T("plot_pine_prop=:18,plot_spruce_prop=:19,plot_birch_prop=:20,plot_cont_prop=:21,plot_damaged=:22,")
						_T("plot_tracks=:23,plot_high_stumps=:24,plot_stump_treat=:25,plot_twigs=:26,")
						_T("plot_Ytradie=:27,plot_AntalTrad=:28,plot_AntalStubbar=:29,plot_AntalStubbarivag=:30,plot_AntalStubbariskog=:31,plot_Totdiamkvarvarande=:32,plot_Totdiamuttag=:33,plot_AntalHogaStubbar=:34,plot_forrensat=:35 ")
						_T("where plot_num=:36 and plot_type=:37 and plot_origin=:38 and ")
						_T("plot_region_num=:39 and plot_district_num=:40 and plot_machine_num=:41 and plot_trakt_num=:42 and plot_compart_num=:43"),
						TBL_PLOT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsDouble()	= rec.m_recData.getRWidth1();
			m_saCommand.Param(2).setAsDouble()	= rec.m_recData.getRWidth2();
			m_saCommand.Param(3).setAsDouble()	= rec.m_recData.getRDist1();
			m_saCommand.Param(4).setAsDouble()	= rec.m_recData.getRDist2();
			m_saCommand.Param(5).setAsDouble()	= rec.m_recData.getRArea();
			m_saCommand.Param(6).setAsDouble()	= rec.m_recData.getGYBefore();
			m_saCommand.Param(7).setAsDouble()	= rec.m_recData.getGYAfter();
			m_saCommand.Param(8).setAsDouble()	= rec.m_recData.getGYWDraw();
			m_saCommand.Param(9).setAsDouble()	= rec.m_recData.getGYQuota();
			m_saCommand.Param(10).setAsDouble()	= rec.m_recData.getStems();
			m_saCommand.Param(11).setAsDouble()	= rec.m_recData.getAvgDiam();
			m_saCommand.Param(12).setAsDouble()	= rec.m_recData.getH25();
			m_saCommand.Param(13).setAsDouble()	= rec.m_recData.getVolPine();
			m_saCommand.Param(14).setAsDouble()	= rec.m_recData.getVolSpruce();
			m_saCommand.Param(15).setAsDouble()	= rec.m_recData.getVolBirch();
			m_saCommand.Param(16).setAsDouble()	= rec.m_recData.getVolCont();
			m_saCommand.Param(17).setAsDouble()	= rec.m_recData.getVolTotal();
			m_saCommand.Param(18).setAsDouble()	= rec.m_recData.getPropPine();
			m_saCommand.Param(19).setAsDouble()	= rec.m_recData.getPropSpruce();
			m_saCommand.Param(20).setAsDouble()	= rec.m_recData.getPropBirch();
			m_saCommand.Param(21).setAsDouble()	= rec.m_recData.getPropCont();
			m_saCommand.Param(22).setAsDouble()	= rec.m_recData.getDamaged();
			m_saCommand.Param(23).setAsDouble()	= rec.m_recData.getTracks();
			m_saCommand.Param(24).setAsDouble()	= rec.m_recData.getHighStumps();
			m_saCommand.Param(25).setAsDouble()	= rec.m_recData.getStumpTreat();
			m_saCommand.Param(26).setAsDouble()	= rec.m_recData.getTwigs();

			m_saCommand.Param(27).setAsShort()	= rec.m_recData.getPlot_Ytradie();
				m_saCommand.Param(28).setAsShort()	= rec.m_recData.getPlot_AntalTrad();
				m_saCommand.Param(29).setAsShort()	= rec.m_recData.getPlot_AntalStubbar();
				m_saCommand.Param(30).setAsShort()	= rec.m_recData.getPlot_AntalStubbarivag();
				m_saCommand.Param(31).setAsShort()	= rec.m_recData.getPlot_AntalStubbariskog();
				m_saCommand.Param(32).setAsShort()	= rec.m_recData.getPlot_Totdiamkvarvarande();
				m_saCommand.Param(33).setAsShort()	= rec.m_recData.getPlot_Totdiamuttag();
				m_saCommand.Param(34).setAsShort()	= rec.m_recData.getPlot_AntalHogaStubbar();
				m_saCommand.Param(35).setAsDouble()	= rec.m_recData.getForrensat();

			m_saCommand.Param(36).setAsShort()	= rec.m_nPlotID;
			m_saCommand.Param(37).setAsString()	= rec.m_sType.Trim();
			m_saCommand.Param(38).setAsShort()	= rec.m_nOrigin;
			m_saCommand.Param(39).setAsShort()	= rec.m_nRegionID;
			m_saCommand.Param(40).setAsShort()	= rec.m_nDistrictID;
			m_saCommand.Param(41).setAsShort()	= rec.m_nMachineID;
			m_saCommand.Param(42).setAsString()	= rec.m_sTraktNum;
			m_saCommand.Param(43).setAsLong()	= rec.m_lCompartID;

			m_saCommand.Prepare();

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

// Delete plot (yta) associated to a specific machine,district,region,machine and compartment
BOOL CGallBasDB::delPlot(PLOT_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where plot_num = %d and plot_machine_num = %d and plot_district_num = %d and plot_region_num = %d and ")
					_T("plot_trakt_num = '%s' and plot_compart_num = %d and plot_type ='%s' and plot_origin = %d"),
					TBL_PLOT,
					rec.m_nPlotID,
					rec.m_nMachineID,
					rec.m_nDistrictID,
					rec.m_nRegionID,
					rec.m_sTraktNum,
					rec.m_lCompartID,
					rec.m_sType.Trim(),
					rec.m_nOrigin);

		if (exists(sSQL))
		{
			sSQL.Format(_T("delete from %s where plot_num=:1 and plot_region_num=:2 and plot_district_num=:3 and plot_machine_num=:4 and plot_trakt_num=:5 and plot_compart_num=:6 and plot_type=:7 and plot_origin=:8"),
						TBL_PLOT);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort()	= rec.m_nPlotID;
			m_saCommand.Param(2).setAsShort()	= rec.m_nRegionID;
			m_saCommand.Param(3).setAsShort()	= rec.m_nDistrictID;
			m_saCommand.Param(4).setAsShort()	= rec.m_nMachineID;
			m_saCommand.Param(5).setAsString()	= rec.m_sTraktNum;
			m_saCommand.Param(6).setAsLong()	= rec.m_lCompartID;
			m_saCommand.Param(7).setAsString()	= rec.m_sType.Trim();
			m_saCommand.Param(8).setAsShort()	= rec.m_nOrigin;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}



// MACHINE REGISTER

// Handle data in machine_table; 061005 p�d

BOOL CGallBasDB::isThereAnyMachineRegData(void)
{
	int	nCnt = 0;
	BOOL bReturn;
	TCHAR szBuffer[1024];
	try
	{
		// Setup SQL question; 061110 p�d
		_stprintf(szBuffer,_T("select * from %s"),TBL_ACTIVE_MACHINE);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{	
			nCnt++;
			if (nCnt > 0)
				break;
		}		

		m_saConnection.Commit();

		bReturn = (nCnt > 0);

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CGallBasDB::getMachineReg(vecMachineRegData &vec)
{

	TCHAR szBuffer[1024];
	try
	{
		vec.clear();

		// Setup SQL question; 061110 p�d
		_stprintf(szBuffer,_T("select * from %s order by active_machine_num"),TBL_ACTIVE_MACHINE);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(_machine_reg_data(m_saCommand.Field(1).asShort(), m_saCommand.Field(2).asString() ) );
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CGallBasDB::getMachineInRegByNumber(MACHINE_REG_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
			sSQL.Format(_T("select * from %s where active_machine_num = %d"),TBL_ACTIVE_MACHINE,rec.m_nMachineRegID);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				rec = MACHINE_REG_DATA(m_saCommand.Field(1).asShort(), m_saCommand.Field(2).asString());
			}
	
			m_saConnection.Commit();
			
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CGallBasDB::addMachineReg(MACHINE_REG_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where active_machine_num = %d"),TBL_ACTIVE_MACHINE,rec.m_nMachineRegID);
		if (!exists(sSQL))
		{
			sSQL.Format(_T("insert into %s (active_machine_num,active_machine_name) values(:1,:2)"),TBL_ACTIVE_MACHINE);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_nMachineRegID;
			m_saCommand.Param(2).setAsString() = rec.m_sContractorName;

			m_saCommand.Execute();
			
			m_saConnection.Commit();
			
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

// Update contractor, based on Region,District and Machine; 061010 p�d
BOOL CGallBasDB::updMachineReg(MACHINE_REG_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;

	try
	{
		sSQL.Format(_T("select * from %s where active_machine_num = %d"),TBL_ACTIVE_MACHINE,rec.m_nMachineRegID);
		if (exists(sSQL))
		{
			sSQL.Format(_T("update %s set active_machine_name = :1 where active_machine_num = :2"),TBL_ACTIVE_MACHINE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString() = rec.m_sContractorName;
			m_saCommand.Param(2).setAsShort() = rec.m_nMachineRegID;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CGallBasDB::delMachineReg(MACHINE_REG_DATA &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where active_machine_num = %d"),TBL_ACTIVE_MACHINE,rec.m_nMachineRegID);
		if (exists(sSQL))
		{
			sSQL.Format(_T("delete from %s where active_machine_num = :1"),TBL_ACTIVE_MACHINE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.m_nMachineRegID;

			m_saCommand.Execute();
			
			m_saConnection.Commit();

			bReturn = TRUE;

		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

