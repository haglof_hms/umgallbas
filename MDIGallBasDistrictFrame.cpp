// MDIDataBaseFrame.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"

#include "AddDataToDataBase.h"
#include "MDIGallBasDistrictFrame.h"

#include "ResLangFileReader.h"

#include "UMGallBasGenerics.h"
/////////////////////////////////////////////////////////////////////////////
// CMDIGallBasDistrictFrame

IMPLEMENT_DYNCREATE(CMDIGallBasDistrictFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIGallBasDistrictFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIGallBasDistrictFrame)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_COPYDATA()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIGallBasDistrictFrame::CMDIGallBasDistrictFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDIGallBasDistrictFrame::~CMDIGallBasDistrictFrame()
{
}

void CMDIGallBasDistrictFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_GALLBAS_DISTRICT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

void CMDIGallBasDistrictFrame::OnClose(void)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070905 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnClose();
}

int CMDIGallBasDistrictFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CMDIGallBasDistrictFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_GALLBAS_DISTRICT_KEY);
		LoadPlacement(this, csBuf);
  }
}

BOOL CMDIGallBasDistrictFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CMDIChildWnd::OnCopyData(pWnd, pData);
}

void CMDIGallBasDistrictFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

}

BOOL CMDIGallBasDistrictFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIGallBasDistrictFrame diagnostics

#ifdef _DEBUG
void CMDIGallBasDistrictFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIGallBasDistrictFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIGallBasDistrictFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_GALLBAS_DISTRICT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_GALLBAS_DISTRICT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIGallBasDistrictFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIGallBasDistrictFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIGallBasDistrictFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	// Check the wparam to see what the user wants to do; 060922 p�d
	if (wParam == ID_OPEN_ITEM)
	{
		setupFiles();
	}
	else	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW7,m_sLangFN);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	} 

	return 0L;
}

// MY METHODS

void CMDIGallBasDistrictFrame::setupFiles(void)
{
	CString sTitle;
	CString sFileName;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sTitle = xml->str(IDS_STRING200);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	CMyFileDialog fdlg(sTitle,TRUE,DEFAULT_EXTENSION,_T(""),OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT,FILEOPEN_FILTER);
	if (fdlg.DoModal() == IDOK)
	{
		CJonasDLL_handling *hnd = new CJonasDLL_handling();
		if (hnd)
		{
			POSITION pos = fdlg.GetStartPosition();
			while (pos)
			{
				sFileName = fdlg.GetNextPathName(pos);
#ifdef UNICODE
				USES_CONVERSION;
#endif
				hnd->enterDataToDB(m_dbConnectionData,
#ifdef UNICODE
													W2A(sFileName.GetString()),
													W2A(extractFileName(sFileName).GetString()),
#else
													sFileName.GetString(),
													extractFileName(sFileName).GetString(),
#endif
													&m_structTraktIdentifer);
			}
			
			delete hnd;
		}


	}	// if (fdlg.DoModal() == IDOK)
}

// CMDIGallBasDistrictFrame message handlers


/////////////////////////////////////////////////////////////////////////////
// CGallBasDistrictSelectionListFrame

IMPLEMENT_DYNCREATE(CGallBasDistrictSelectionListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CGallBasDistrictSelectionListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CGallBasDistrictSelectionListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CGallBasDistrictSelectionListFrame::CGallBasDistrictSelectionListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CGallBasDistrictSelectionListFrame::~CGallBasDistrictSelectionListFrame()
{
}

void CGallBasDistrictSelectionListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_GALLBAS_LIST_DISTRICT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CGallBasDistrictSelectionListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CGallBasDistrictSelectionListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_GALLBAS_LIST_DISTRICT_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CGallBasDistrictSelectionListFrame::OnSetFocus(CWnd*)
{
}

BOOL CGallBasDistrictSelectionListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CGallBasDistrictSelectionListFrame diagnostics

#ifdef _DEBUG
void CGallBasDistrictSelectionListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CGallBasDistrictSelectionListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CGallBasDistrictSelectionListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_GALLBAS_LIST_DISTRICT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_GALLBAS_LIST_DISTRICT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CGallBasDistrictSelectionListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CGallBasDistrictSelectionListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CGallBasDistrictSelectionListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

// MY METHODS


// CGallBasDistrictSelectionListFrame message handlers


