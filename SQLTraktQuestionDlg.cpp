// SQLTraktQuestionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMGallBasDB.h"
#include "SQLTraktQuestionDlg.h"


#include "ResLangFileReader.h"
#include ".\sqltraktquestiondlg.h"

// CSQLTraktQuestionDlg dialog

IMPLEMENT_DYNAMIC(CSQLTraktQuestionDlg, CDialog)

BEGIN_MESSAGE_MAP(CSQLTraktQuestionDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_WM_COPYDATA()
	ON_CBN_SELCHANGE(IDC_COMBO3, &CSQLTraktQuestionDlg::OnCbnSelchangeCombo3)
END_MESSAGE_MAP()

CSQLTraktQuestionDlg::CSQLTraktQuestionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSQLTraktQuestionDlg::IDD, pParent)
{
}

CSQLTraktQuestionDlg::~CSQLTraktQuestionDlg()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

void CSQLTraktQuestionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL10, m_wndLbl7);

	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_COMBO2, m_wndCBox2);

	DDX_Control(pDX, IDC_COMBO3, m_wndCBox3);
	DDX_Control(pDX, IDC_COMBO4, m_wndCBox4);

	DDX_Control(pDX, IDC_BUTTON1, m_wndBtn1);
	DDX_Control(pDX, IDOK, m_wndOK);
	//}}AFX_DATA_MAP
}

BOOL CSQLTraktQuestionDlg::OnInitDialog()
{

	CDialog::OnInitDialog();

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	
	m_wndEdit3.SetEnabledColor(BLACK, WHITE );
	m_wndEdit3.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit3.SetAsNumeric();

	m_wndEdit4.SetEnabledColor(BLACK, WHITE );
	m_wndEdit4.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit4.SetAsNumeric();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	setLanguage();

	// Get regions; 061204 p�d
	getRegionsFromDB();
	addRegionsToCBox();
	// Get districts (and region info); 061011 p�d
	getDistrictFromDB();

	setSQLQuestionData();

	return TRUE;
}

BOOL CSQLTraktQuestionDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CGallBasDB(m_dbConnectionData);
		}

	}
	return CDialog::OnCopyData(pWnd, pData);
}

// MY METHODS

void CSQLTraktQuestionDlg::setLanguage(void)
{

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText(xml->str(IDS_STRING401));
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING4021));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING403));
			m_wndLbl3.SetWindowText(xml->str(IDS_STRING404));
			m_wndLbl4.SetWindowText(xml->str(IDS_STRING405));
			m_wndLbl5.SetWindowText(xml->str(IDS_STRING4060));
			m_wndLbl6.SetWindowText(xml->str(IDS_STRING4061));
			m_wndLbl7.SetWindowText(xml->str(IDS_STRING4062));

			m_wndBtn1.SetWindowText(xml->str(IDS_STRING4001));

			m_sMsgCap = xml->str(IDS_STRING109);
			m_sMsg1 = xml->str(IDS_STRING508);

		}
		delete xml;
	}
}

// Read an setup the SQL question last set into dialog fields; 061013 p�d
void CSQLTraktQuestionDlg::setSQLQuestionData(void)
{
	FILE *fp;
	SQL_TRAKT_QUESTION recTrakt;
	CString sPathAndFN;
	CString sTmp;
	int nCnt;
	int nIndex;
	CString csPath;

	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_TRAKT_QUESTION_FN);

	if (fileExists(sPathAndFN))
	{
		if ((fp = _tfopen(sPathAndFN,_T("rb"))) != NULL)
		{
			fread(&recTrakt,sizeof(SQL_TRAKT_QUESTION),1,fp);
			fclose(fp);

			if (recTrakt.nRegionID > 0)
				setRegionLastSelected(recTrakt.nRegionID);
			if (recTrakt.nDistrictID > 0)
				setDistrictLastSelected(recTrakt.nRegionID,recTrakt.nDistrictID);
			if (recTrakt.nMachineID > 0)
				m_wndEdit3.setInt(recTrakt.nMachineID);
			if (recTrakt.nTraktID > 0)
				m_wndEdit4.setInt(recTrakt.nTraktID);

		}	// if ((fp = fopen(sPathAndFN,"rb")) != NULL)
	}	// if (fileExists(sPathAndFN))

	nCnt = 0;
	nIndex = -1;
	for (int i = 0;i < (sizeof(INVENTORY_TYPE_ARRAY)/sizeof(INVENTORY_TYPE_ARRAY[0])) + 1;i++)
	{
		if (i > 0)
		{
			m_wndCBox1.AddString(INVENTORY_TYPE_ARRAY[nCnt].sInvTypeName);
			if (_tcscmp(recTrakt.szType,INVENTORY_TYPE_ARRAY[nCnt].sAbbrevInvTypeName) == 0)
			{
				nIndex = i;
			}
			nCnt++;
		}
		else
		{
			m_wndCBox1.AddString(_T(""));
		}
	}
	m_wndCBox1.SetCurSel(nIndex);

	nCnt = 0;
	nIndex = -1;
	for (int i = 0;i < (sizeof(ORIGIN_ARRAY)/sizeof(ORIGIN_ARRAY[0])) + 1;i++)
	{
		if (i > 0)
		{
			m_wndCBox2.AddString(ORIGIN_ARRAY[nCnt].sOrigin);
			if (recTrakt.nOrigin == ORIGIN_ARRAY[nCnt].nOriginNum)
			{
				nIndex = i;
			}
			nCnt++;
		}
		else
		{
			m_wndCBox2.AddString(_T(""));
		}
	} // for (int i = 0;i < (sizeof(ORIGIN_ARRAY)/sizeof(ORIGIN_ARRAY[0])) + 1;i++)
	m_wndCBox2.SetCurSel(nIndex);

}

void CSQLTraktQuestionDlg::getRegionsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		if (m_pDB != NULL)
		{
			m_pDB->getRegions(m_vecRegionData);
		}	// if (m_pDB != NULL)
	} // 	if (m_bConnected)
}

void CSQLTraktQuestionDlg::setRegionLastSelected(int reg_id)
{
	if (m_wndCBox3.GetCount() > 0 && m_vecRegionData.size() > 0)
	{
		for (UINT i = 0;i < m_vecRegionData.size();i++)
		{
			REGION_DATA data = m_vecRegionData[i];
			if (data.m_nRegionID == reg_id)
			{
				m_wndCBox3.SetCurSel(i+1);
				addDistrictToCBox(data.m_nRegionID);
			}	// if (data.m_nRegionID == reg_id)
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_wndCBox3.GetCount() > 0 && m_vecRegionData.size() > 0)
}

// Add regions in registry, into ComboBox3; 061204 p�d
void CSQLTraktQuestionDlg::addRegionsToCBox(void)
{
	CString sText;
	if (m_vecRegionData.size() > 0)
	{
		m_wndCBox3.ResetContent();
		m_wndCBox3.AddString(sText);	// To set empty box; 070419 p�d
		m_wndCBox3.SetItemData(0,-1);
		for (UINT i = 0;i < m_vecRegionData.size();i++)
		{
			REGION_DATA data = m_vecRegionData[i];
			sText.Format(_T("%d - %s"),data.m_nRegionID,data.m_sRegionName);
			m_wndCBox3.AddString(sText);
			m_wndCBox3.SetItemData(i+1,data.m_nRegionID);
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}

void CSQLTraktQuestionDlg::getDistrictFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		if (m_pDB != NULL)
		{
			m_pDB->getDistricts(m_vecDistrictData);
		}	// if (m_pDB != NULL)
	} // 	if (m_bConnected)
}

void CSQLTraktQuestionDlg::setDistrictLastSelected(int region_id,int district_id)
{
	int nIndex = -1;
	CString sStr;
	if (m_wndCBox4.GetCount() > 0 && m_vecDistrictData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			if (data.m_nRegionID == region_id && data.m_nDistrictID == district_id)
			{
				sStr.Format(_T("%d - %s"),data.m_nDistrictID,data.m_sDistrictName);
				nIndex = m_wndCBox4.FindString(0,sStr);
				if (nIndex != CB_ERR)
				{
					m_wndCBox4.SetCurSel(nIndex);
					break;
				}
			}
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_wndCBox3.GetCount() > 0 && m_vecRegionData.size() > 0)
}

// Add regions in registry, into ComboBox3; 061204 p�d
void CSQLTraktQuestionDlg::addDistrictToCBox(int reg_id)
{
	CString sText;
	int nCounter = 1;
	if (m_vecDistrictData.size() > 0)
	{
		m_wndCBox4.ResetContent();
		m_wndCBox4.AddString(sText);	// To set empty box; 070419 p�d
		m_wndCBox4.SetItemData(0,-1);
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			if (data.m_nRegionID == reg_id)
			{
				sText.Format(_T("%d - %s"),data.m_nDistrictID,data.m_sDistrictName);
				m_wndCBox4.AddString(sText);
				m_wndCBox4.SetItemData(nCounter,data.m_nDistrictID);
				nCounter++;
			}
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}

CString CSQLTraktQuestionDlg::getType(void)
{
	CString sText = _T("");
	int nIdx = m_wndCBox1.GetCurSel();
	if (nIdx != CB_ERR && nIdx > 0)
	{
		sText = INVENTORY_TYPE_ARRAY[nIdx-1].sAbbrevInvTypeName;
	}
	return sText;
}

int CSQLTraktQuestionDlg::getOrigin(void)
{
	int nNum = -1;
	int nIdx = m_wndCBox2.GetCurSel();
	if (nIdx != CB_ERR && nIdx > 0)
	{
		nNum = ORIGIN_ARRAY[nIdx-1].nOriginNum;
	}
	return nNum;
}

// CSQLTraktQuestionDlg message handlers
void CSQLTraktQuestionDlg::OnBnClickedOk()
{
	FILE *fp;
	int nRegionID;
	int nDistrictID;
	int nMachineID;
	int nTraktID;
	CString sType;
	int nOrigin;
	BOOL bRegionSet = FALSE;
	BOOL bDistrictSet = FALSE;
	BOOL bMachineSet = FALSE;
	BOOL bTraktSet = FALSE;
	BOOL bTypeSet = FALSE;
	BOOL bOriginSet = FALSE;

	int nNumOf = 0;
	CString sPathAndFN;
	CString sSQL;
	CString sSQLSet;
	SQL_TRAKT_QUESTION recTrakt;

	CString S;

	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_TRAKT_QUESTION_FN);

	nRegionID = m_wndCBox3.GetItemData(m_wndCBox3.GetCurSel());
	nDistrictID = m_wndCBox4.GetItemData(m_wndCBox4.GetCurSel());
	nMachineID = m_wndEdit3.getInt();
	nTraktID = m_wndEdit4.getInt();
	sType = getType();
	nOrigin = getOrigin();

	recTrakt.nRegionID = nRegionID;
	recTrakt.nDistrictID = nDistrictID;
	recTrakt.nMachineID = nMachineID;
	recTrakt.nTraktID = nTraktID;
	_tcscpy(recTrakt.szType,sType);
	recTrakt.nOrigin = nOrigin;
	
	sSQL.Format(SQL_SELECT,TBL_TRAKT);
	// Find out how many fields in were clause of select qestion; 061013 p�d
	if (nRegionID > 0) nNumOf++;
	if (nDistrictID > 0) nNumOf++;
	if (nMachineID > 0) nNumOf++;
	if (nTraktID > 0) nNumOf++;
	if (sType.GetLength() > 0) nNumOf++;
	if (nOrigin > 0) nNumOf++;

	if (nNumOf > 0)
	{
		for (int i = 0;i < nNumOf;i++)
		{
			if (nRegionID > 0 && !bRegionSet)
			{
				sSQLSet.Format(SQL_TRAKT_REGION_QUEST,nRegionID);
				sSQL += sSQLSet + _T(" and ");

				bRegionSet = TRUE;

			}
			if (nDistrictID > 0 && !bDistrictSet)
			{
				sSQLSet.Format(SQL_TRAKT_DISTRICT_QUEST,nDistrictID);
				sSQL += sSQLSet + _T(" and ");

				bDistrictSet = TRUE;
			}
			if (nMachineID > 0 && !bMachineSet)
			{
				sSQLSet.Format(SQL_TRAKT_MACHINE_QUEST,nMachineID);
				sSQL += sSQLSet + _T(" and ");
		
				bMachineSet = TRUE;
			}
			if (nTraktID > 0 && !bTraktSet)
			{
				sSQLSet.Format(SQL_TRAKT_QUEST,nTraktID);
				sSQL += sSQLSet + _T(" and ");
		
				bTraktSet = TRUE;
			}
			if (sType.GetLength() > 0 && !bTypeSet)
			{
				sSQLSet.Format(SQL_TRAKT_TYPE_QUEST,sType);
				sSQL += sSQLSet + _T(" and ");
		
				bTypeSet = TRUE;
			}
			if (nOrigin > 0 && !bOriginSet)
			{
				sSQLSet.Format(SQL_TRAKT_ORIGIN_QUEST,nOrigin);
				sSQL += sSQLSet + _T(" and ");
		
				bOriginSet = TRUE;
			}


		}
		// Remove last and in sSQL string
		sSQL.Delete(sSQL.GetLength()-5,5);
		sSQLSet = SQL_TRAKT_ORDER_BY;
		sSQL += sSQLSet;
	}
	else
	{
		sSQL.Format(SQL_TRAKT_QUESTION1,TBL_TRAKT);
	}
	if ((fp = _tfopen(sPathAndFN,_T("wb"))) != NULL)
	{
		_tcscpy(recTrakt.szSQL,sSQL);

		fwrite(&recTrakt,sizeof(SQL_TRAKT_QUESTION),1,fp);

		fclose(fp);
	}
	
	// Check if SQL Question gives any result. If not, tell user to redefine the question; 061012 p�d
	if (doesSQLQuestionReturnsAResult())
		OnOK();
	else
		::MessageBox(0,m_sMsg1,m_sMsgCap,MB_OK);
}

BOOL CSQLTraktQuestionDlg::doesSQLQuestionReturnsAResult(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		if (m_pDB != NULL)
		{
			bReturn = m_pDB->isThereAnyTraktData();
		}	// if (m_pDB != NULL)
	} // 	if (m_bConnected)
	return bReturn;
}

void CSQLTraktQuestionDlg::OnBnClickedButton1()
{
	FILE *fp;
	SQL_TRAKT_QUESTION recTrakt;
	TCHAR szBuffer[512];
	TCHAR szSQL[512];
	CString sPathAndFN;

	CString csPath;
	csPath.Format(_T("%s\\HMS"), getMyDocumentsDir());
	CreateDirectory(csPath, NULL);
	sPathAndFN.Format(_T("%s\\%s"), csPath, SQL_TRAKT_QUESTION_FN);

	_tcscpy(szBuffer,SQL_TRAKT_QUESTION1);
	_stprintf(szSQL,szBuffer,TBL_TRAKT);

	recTrakt.nRegionID = 0;
	recTrakt.nDistrictID = 0;
	recTrakt.nMachineID = 0;
	recTrakt.nTraktID = 0;
	recTrakt.nOrigin = 0;
	_tcscpy(recTrakt.szType,_T(""));
	_tcscpy(recTrakt.szSQL,szSQL);

	if ((fp = _tfopen(sPathAndFN,_T("wb"))) != NULL)
	{

		fwrite(&recTrakt,sizeof(SQL_TRAKT_QUESTION),1,fp);

		fclose(fp);
	}

	OnOK();
}

void CSQLTraktQuestionDlg::OnCbnSelchangeCombo3()
{
	int nIndex = m_wndCBox3.GetCurSel();	
	int nRegId = m_wndCBox3.GetItemData(nIndex);
	addDistrictToCBox(nRegId);

}
