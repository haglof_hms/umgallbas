// MDIGallBasRegionFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIGallBasRegionFormView.h"

#include "ResLangFileReader.h"

#include "UMGallBasGenerics.h"
// CMDIGallBasRegionFormView

IMPLEMENT_DYNCREATE(CMDIGallBasRegionFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIGallBasRegionFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CMDIGallBasRegionFormView::CMDIGallBasRegionFormView()
	: CXTResizeFormView(CMDIGallBasRegionFormView::IDD)
{
	m_bConnected = FALSE;
}

CMDIGallBasRegionFormView::~CMDIGallBasRegionFormView()
{
}

void CMDIGallBasRegionFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);

	DDX_Control(pDX, IDC_GROUP, m_wndGroup);
	//}}AFX_DATA_MAP
}


BOOL CMDIGallBasRegionFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIGallBasRegionFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_wndEdit1.SetEnabledColor(BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit1.SetReadOnly( TRUE );

	m_wndEdit2.SetEnabledColor(BLACK, WHITE );
	m_wndEdit2.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit2.SetReadOnly( TRUE );

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getRegions(m_vecRegionData);
			delete pDB;
		}	// if (pDB != NULL)
		m_nDBIndex = 0;
		populateData(m_nDBIndex);
	}	// if (m_bConnected)

	setLanguage();
	
}

void CMDIGallBasRegionFormView::OnSetFocus(CWnd*)
{
	setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecRegionData.size()-1));
}

BOOL CMDIGallBasRegionFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CMDIGallBasRegionFormView diagnostics

#ifdef _DEBUG
void CMDIGallBasRegionFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIGallBasRegionFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CMDIGallBasRegionFormView message handlers

void CMDIGallBasRegionFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING101) + _T(" :"));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING102) + _T(" :"));

		}
		delete xml;
	}

}

void CMDIGallBasRegionFormView::populateData(UINT idx)
{
	REGION_DATA data;
	if (!m_vecRegionData.empty() && 
		  idx >= 0 && 
			idx < m_vecRegionData.size())
	{
		data = m_vecRegionData[idx];

		m_wndEdit1.setInt(data.m_nRegionID);
		m_wndEdit2.SetWindowText(data.m_sRegionName);

	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CMDIGallBasRegionFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_PREV :
		{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;

				if (m_nDBIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_NEXT :
		{
				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecRegionData.size() - 1))
					m_nDBIndex = (UINT)m_vecRegionData.size() - 1;
				
				if (m_nDBIndex == (UINT)m_vecRegionData.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}

				populateData(m_nDBIndex);
				break;
		}
		case ID_DBNAVIG_END :
		{
				m_nDBIndex = (UINT)m_vecRegionData.size()-1;

				setNavigationButtons(TRUE,FALSE);
			
				populateData(m_nDBIndex);
				break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIGallBasRegionFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

// PUBLIC
void CMDIGallBasRegionFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons(m_nDBIndex > 0,
										   m_nDBIndex < (m_vecRegionData.size()-1));
}
