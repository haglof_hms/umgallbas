// MDIDataBaseFrame.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "MDIGallBasActiveMachineFrame.h"
#include "MDIGallBasActiveMachineFormView.h"

#include "ResLangFileReader.h"

#include "UMGallBasGenerics.h"

/////////////////////////////////////////////////////////////////////////////
// CMDIGallBasActiveMachineFrame

IMPLEMENT_DYNCREATE(CMDIGallBasActiveMachineFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIGallBasActiveMachineFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIGallBasActiveMachineFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIGallBasActiveMachineFrame::CMDIGallBasActiveMachineFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDIGallBasActiveMachineFrame::~CMDIGallBasActiveMachineFrame()
{
}

void CMDIGallBasActiveMachineFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_GALLBAS_MACHINE_REG_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
	CWinApp *pApp = AfxGetApp();
}

int CMDIGallBasActiveMachineFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	if (!isDBConnection(m_sLangFN))
		return -1;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sMsgCap = xml->str(IDS_STRING109);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml->str(IDS_STRING5181),
						xml->str(IDS_STRING5182));
		}	// if (xml->Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDIGallBasActiveMachineFrame::OnClose(void)
{

	CMDIGallBasActiveMachineFormView *pView = (CMDIGallBasActiveMachineFormView *)GetActiveView();
	if (pView)
	{
		if (pView->getIsDirty())
		{
			if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
			{
				pView->saveMachineRegToDB();
			}	// if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
		}	// if (pView->getIsDirty())
	}	// if (pView)
	
	CMDIChildWnd::OnClose();
}

// load the placement in OnShowWindow()
void CMDIGallBasActiveMachineFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_GALLBAS_MACHINE_REG_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIGallBasActiveMachineFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

BOOL CMDIGallBasActiveMachineFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIGallBasActiveMachineFrame diagnostics

#ifdef _DEBUG
void CMDIGallBasActiveMachineFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIGallBasActiveMachineFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIGallBasActiveMachineFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_GALLBAS_MACHINE_REG;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_GALLBAS_MACHINE_REG;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIGallBasActiveMachineFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIGallBasActiveMachineFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIGallBasActiveMachineFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW10,m_sLangFN);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}

// MY METHODS


// CMDIGallBasActiveMachineFrame message handlers

