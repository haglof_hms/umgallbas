// UMDataBase.cpp : Defines the initialization routines for the DLL.
//
#include "StdAfx.h"
#include "UMGallBas.h"
#include "Resource.h"

#include "MDIGallBasRegionFrame.h"
#include "MDIGallBasDistrictFrame.h"
#include "MDIGallBasMachineFrame.h"
#include "MDIGallBasTraktFrame.h"
#include "MDIGallBasCompartFrame.h"
#include "MDIGallBasPlotsFrame.h"
#include "MDIGallBasActiveMachineFrame.h"

#include "MDIGallBasRegionFormView.h"
#include "MDIGallBasDistrictFormView.h"
#include "MDIGallBasMachinesFormView.h"
#include "MDIGallBasTraktFormView.h"
#include "MDIGallBasCompartFormView.h"
#include "MDIGallBasPlotsFormView.h"
#include "MDIGallBasActiveMachineFormView.h"

#include "GallBasRegionSelectList.h"
#include "GallBasDistrictSelectList.h"
#include "GallBasMachineSelectList.h"
#include "GallBasTraktSelectList.h"
#include "GallBasCompartSelectList.h"
#include "GallBasPlotsSelectList.h"

#include "UMGallBasGenerics.h"
#include "AlterTables.h"

static AFX_EXTENSION_MODULE UMGallBasDLL = { NULL, NULL };

HINSTANCE hInst = NULL;

// Initialize the DLL, register the classes etc
extern "C" AFX_EXT_API void InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);
extern "C" void AFX_EXT_API DoAlterTables(LPCTSTR);
extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMGallBas.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMGallBasDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMEstimate.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMGallBasDLL);
	}
	hInst = hInstance;
	return 1;   // ok
}

// Initialize the DLL, register the classes etc
void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &idx,vecINFO_TABLE &info)
{
	new CDynLinkLibrary(UMGallBasDLL);
	CString sReportSettingsFN;
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;
	CString sPath;
	// Setup the searchpath and name of the program.
	// This information is used e.g. in setting up the
	// Language filename in an OpenSuite() function; 051214 p�d
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	// Form view to enter data

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIGallBasRegionFrame),
			RUNTIME_CLASS(CMDIGallBasRegionFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW1,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIGallBasDistrictFrame),
			RUNTIME_CLASS(CMDIGallBasDistrictFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW1,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW2,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIGallBasMachineFrame),
			RUNTIME_CLASS(CMDIGallBasMachinesFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW2,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW3,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIGallBasTraktFrame),
			RUNTIME_CLASS(CMDIGallBasTraktFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW3,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW4,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIGallBasCompartFrame),
			RUNTIME_CLASS(CMDIGallBasCompartFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW4,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW5,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIGallBasPlotsFrame),
			RUNTIME_CLASS(CMDIGallBasPlotsFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW5,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW12,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIGallBasActiveMachineFrame),
			RUNTIME_CLASS(CMDIGallBasActiveMachineFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW12,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW6,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CGallBasRegionsSelectionListFrame),
			RUNTIME_CLASS(CGallBasRegionSelectList)));
	
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW7,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CGallBasDistrictSelectionListFrame),
			RUNTIME_CLASS(CGallBasDistrictSelectList)));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW8,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CGallBasMachineSelectListFrame),
			RUNTIME_CLASS(CGallBasMachineSelectList)));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW9,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CGallBasTraktSelectListFrame),
			RUNTIME_CLASS(CGallBasTraktSelectList)));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW10,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CGallBasCompartSelectListFrame),
			RUNTIME_CLASS(CGallBasCompartSelectList)));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW11,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CGallBasPlotsSelectListFrame),
			RUNTIME_CLASS(CGallBasPlotsSelectList)));

	// Get version information; 060803 p�d
	LPCTSTR VER_NUMBER		= _T("FileVersion");
	LPCTSTR VER_COMPANY		= _T("CompanyName");
	LPCTSTR VER_COPYRIGHT	= _T("LegalCopyright");

	sVersion		= getVersionInfo(hInst,VER_NUMBER);
	sCopyright	= getVersionInfo(hInst,VER_COPYRIGHT);
	sCompany		= getVersionInfo(hInst,VER_COMPANY);

	info.push_back(INFO_TABLE(-999,2 /* User module */,
									sLangFN.GetString(),
									sVersion.GetString(),
									sCopyright.GetString(),
									sCompany.GetString()));

	// Check if there's a connection set; 070329 p�d
	if (getIsDBConSet() == 1)
	{

		// Check if exists/create report settings file; 061023 p�d
		sReportSettingsFN.Format(_T("%s%s"),getProgDir(),REPORT_SETTINGS_FILE);
		if (!fileExists(sReportSettingsFN))
		{
			FILE *fp;
			if ((fp = _tfopen(sReportSettingsFN,_T("wt"))) != NULL)
			{
				fclose(fp);
			}
		}

		// Check if exists/create report settings file; 061023 p�d
		sReportSettingsFN.Format(_T("%s%s"),getProgDir(),REPORT_SETTINGS2_FILE);
		if (!fileExists(sReportSettingsFN))
		{
			FILE *fp;
			if ((fp = _tfopen(sReportSettingsFN,_T("wt"))) != NULL)
			{
				fclose(fp);
			}
		}

		// Check if exists/create report settings file; 070925 p�d
		sReportSettingsFN.Format(_T("%s%s"),getProgDir(),REPORT_SETTINGS3_FILE);
		if (!fileExists(sReportSettingsFN))
		{
			FILE *fp;
			if ((fp = _tfopen(sReportSettingsFN,_T("wt"))) != NULL)
			{
				fclose(fp);
			}
		}

		sPath.Format(_T("%s\\%s\\%s"),getProgDir(),SUBDIR_SCRIPTS,GALLBAS_TABLES);
		runSQLScriptFile(sPath,TBL_REGION);
		
		sPath.Format(_T("%s\\%s\\%s"),getProgDir(),SUBDIR_SCRIPTS,GALLBAS_MACHINES_TABLE);
		runSQLScriptFile(sPath,TBL_ACTIVE_MACHINE);
	}

	DoAlterTables(_T(""));	// Empty arg = use default database;

}

void AFX_EXT_API DoAlterTables(LPCTSTR db_name)
{
	vecScriptFiles vec;
	if (getIsDBConSet() == 1)
	{
		// Alter tables
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable2,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable3,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable4,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable5,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable6,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable7,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable8,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable9,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable10,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable11,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable12,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable13,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable14,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable15,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable16,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable17,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable18,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable19,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable20,db_name));		
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable21,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable22,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable23,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable24,db_name));
		vec.push_back(Scripts(TBL_TRAKT,alter_TraktTable25,db_name));

		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable1,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable2,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable3,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable4,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable5,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable6,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable7,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable8,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable9,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable10,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable11,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable12,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable13,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable14,db_name));
		vec.push_back(Scripts(TBL_COMPART,alter_AvdelnTable15,db_name));


		vec.push_back(Scripts(TBL_PLOT,alter_PlotTable1,db_name));
		vec.push_back(Scripts(TBL_PLOT,alter_PlotTable2,db_name));
		vec.push_back(Scripts(TBL_PLOT,alter_PlotTable3,db_name));
		vec.push_back(Scripts(TBL_PLOT,alter_PlotTable4,db_name));
		vec.push_back(Scripts(TBL_PLOT,alter_PlotTable5,db_name));
		vec.push_back(Scripts(TBL_PLOT,alter_PlotTable6,db_name));
		vec.push_back(Scripts(TBL_PLOT,alter_PlotTable7,db_name));
		vec.push_back(Scripts(TBL_PLOT,alter_PlotTable8,db_name));
		vec.push_back(Scripts(TBL_PLOT,alter_PlotTable9,db_name));
		

		runSQLScriptFileEx1(vec,Scripts::TBL_ALTER);
		vec.clear();
	}	// if (getIsDBConSet() == 1)
}	


