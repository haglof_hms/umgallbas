#pragma once

#include "Resource.h"

#include "UMGallBasDB.h"
// CGallBasMachineSelectList form view

class CGallBasMachineSelectList : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CGallBasMachineSelectList)

	CString m_sLangAbbrev;
	CString m_sLangFN;

protected:
	CGallBasMachineSelectList();           // protected constructor used by dynamic creation
	virtual ~CGallBasMachineSelectList();

	CButton m_wndBtn1;

	BOOL m_bConnected;

	CMyReportCtrl m_wndMachineReport;
	BOOL setupReport(void);
	void populateReport(void);

	vecMachineData m_vecMachineData;

	void getMachineFromDB(void);

	DB_CONNECTION_DATA m_dbConnectionData;

public:
	enum { IDD = IDD_FORMVIEW8 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
};


