#pragma once

#include "Resource.h"

// Enumerated todo

typedef enum _enum_list
{
	WTL_NOTHING,
	WTL_REGION_DISTRICT,
	WTL_MACHINE,
	WTL_MACHINE_REG,
	WTL_TRAKT
} enumWHAT_TO_LIST;


// CSelectRegionDistDlg dialog

class CSelectRegionDistDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectRegionDistDlg)

	CString m_sLangAbbrev;
	CString m_sLangFN;
protected:
	CMyReportCtrl m_wndReport;

	enumWHAT_TO_LIST m_enumWTL;

	BOOL setupReport(void);
	void populateReport(void);
	
	int m_nRegionID;
	int m_nDistrictID;
	int m_nMachineID;

	CString m_sEnteredManual;
	CString m_sEnteredFromFile;

	DISTRICT_DATA m_recDistrict;
	vecDistrictData m_vecDistrictData;
	MACHINE_DATA m_recMachine;
	vecMachineData m_vecMachineData;
	MACHINE_REG_DATA m_recMachineReg;
	vecMachineRegData m_vecMachineRegData;
	TRAKT_DATA m_recTrakt;
	vecTraktData m_vecTraktData;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:
	CSelectRegionDistDlg(CWnd* pParent = NULL);   // standard constructor
	CSelectRegionDistDlg(enumWHAT_TO_LIST,CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectRegionDistDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

	void getRegionAndDistrict(DISTRICT_DATA &v)
	{
		v = m_recDistrict;
	}

	void getMachine(MACHINE_DATA &v)
	{
		v = m_recMachine;
	}

	void getMachineReg(MACHINE_REG_DATA &v)
	{
		v = m_recMachineReg;
	}

	void getTrakt(TRAKT_DATA &v)
	{
		v = m_recTrakt;
	}
protected:
	//{{AFX_VIRTUAL(CSelectRegionDistDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	//{{AFX_MSG(CSelectRegionDistDlg)
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
