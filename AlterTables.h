#if !defined(AFX_ALTERTABLES_H)
#define AFX_ALTERTABLES_H

#include "StdAfx.h"



LPCTSTR alter_TraktTable2 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_maskinstorlek')  \
alter table %s add trakt_maskinstorlek int");

LPCTSTR alter_TraktTable3 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_myr')  \
alter table %s add trakt_avslut_myr int");

LPCTSTR alter_TraktTable4 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_hallmark')  \
alter table %s add trakt_avslut_hallmark int");

LPCTSTR alter_TraktTable5 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_hansyn')  \
alter table %s add trakt_avslut_hansyn int");

//------------------
LPCTSTR alter_TraktTable6 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_fdkulturmark')  \
alter table %s add trakt_avslut_fdkulturmark int");
LPCTSTR alter_TraktTable7 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_kulturmiljo')  \
alter table %s add trakt_avslut_kulturmiljo int");
LPCTSTR alter_TraktTable8 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_lovandel')  \
alter table %s add trakt_avslut_lovandel int");
LPCTSTR alter_TraktTable9 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_skyddszon')  \
alter table %s add trakt_avslut_skyddszon int");
LPCTSTR alter_TraktTable10 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_nvtrad')  \
alter table %s add trakt_avslut_nvtrad int");
LPCTSTR alter_TraktTable11 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_framtidstrad')  \
alter table %s add trakt_avslut_framtidstrad int");
LPCTSTR alter_TraktTable12 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_hogstubbar')  \
alter table %s add trakt_avslut_hogstubbar int");
LPCTSTR alter_TraktTable13 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_dodatrad')  \
alter table %s add trakt_avslut_dodatrad int");
LPCTSTR alter_TraktTable14 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_mindrekorskada')  \
alter table %s add trakt_avslut_mindrekorskada int");
LPCTSTR alter_TraktTable15 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_allvarligkorskada')  \
alter table %s add trakt_avslut_allvarligkorskada int");
LPCTSTR alter_TraktTable16 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_markskador')  \
alter table %s add trakt_avslut_markskador int");
LPCTSTR alter_TraktTable17 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_markskador2')  \
alter table %s add trakt_avslut_markskador2 int");
LPCTSTR alter_TraktTable18 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_nedskrapning')  \
alter table %s add trakt_avslut_nedskrapning int");
LPCTSTR alter_TraktTable19 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_fritext')  \
alter table %s add trakt_avslut_fritext nvarchar(50)");
LPCTSTR alter_TraktTable20 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_atgardstp')  \
alter table %s add trakt_avslut_atgardstp int");
LPCTSTR alter_TraktTable21 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_atgardstp_forklaring')  \
alter table %s add trakt_avslut_atgardstp_forklaring nvarchar(50)");
LPCTSTR alter_TraktTable22 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_avslut_datum_slutford')  \
alter table %s add trakt_avslut_datum_slutford nvarchar(8)");

LPCTSTR alter_TraktTable23 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_calc_si_spec')  \
alter table %s add trakt_calc_si_spec int");
LPCTSTR alter_TraktTable24 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_calc_si')  \
alter table %s add trakt_calc_si int");
LPCTSTR alter_TraktTable25 = 
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'trakt_forrensat')  \
alter table %s add trakt_forrensat float");



LPCTSTR alter_AvdelnTable1 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_gallrtyp')  \
alter table %s add compart_gallrtyp int");
LPCTSTR alter_AvdelnTable2 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_metod')  \
alter table %s add compart_metod int");
LPCTSTR alter_AvdelnTable3 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_spec_and_si')  \
alter table %s add compart_spec_and_si nvarchar(5)");
LPCTSTR alter_AvdelnTable4 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_si_spec')  \
alter table %s add compart_si_spec int");
LPCTSTR alter_AvdelnTable5 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_si')  \
alter table %s add compart_si int");
LPCTSTR alter_AvdelnTable6 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_lagsta_gy_enl_mall')  \
alter table %s add compart_lagsta_gy_enl_mall int");
LPCTSTR alter_AvdelnTable7 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_traktdir_gy_fore_m2ha')  \
alter table %s add compart_traktdir_gy_fore_m2ha int");
LPCTSTR alter_AvdelnTable8 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_traktdir_gy_efter_m2ha')  \
alter table %s add compart_traktdir_gy_efter_m2ha int");
LPCTSTR alter_AvdelnTable9 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_traktdir_ga_uttag')  \
alter table %s add compart_traktdir_ga_uttag int");

LPCTSTR alter_AvdelnTable10 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_gy_wdraw_forest')  \
alter table %s add compart_gy_wdraw_forest float");
LPCTSTR alter_AvdelnTable11 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_gy_wdraw_road')  \
alter table %s add compart_gy_wdraw_road float");
LPCTSTR alter_AvdelnTable12 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_oh')  \
alter table %s add compart_oh float");
LPCTSTR alter_AvdelnTable13 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_areal')  \
alter table %s add compart_areal float");
LPCTSTR alter_AvdelnTable14 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_antal_stubbar')  \
alter table %s add compart_antal_stubbar float");
LPCTSTR alter_AvdelnTable15 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'compart_forrensat')  \
alter table %s add compart_forrensat float");



LPCTSTR alter_PlotTable1 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'plot_Ytradie')  \
alter table %s add plot_Ytradie int");
LPCTSTR alter_PlotTable2 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'plot_AntalTrad')  \
alter table %s add plot_AntalTrad int");
LPCTSTR alter_PlotTable3 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'plot_AntalStubbar')  \
alter table %s add plot_AntalStubbar int");
LPCTSTR alter_PlotTable4 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'plot_AntalStubbarivag')  \
alter table %s add plot_AntalStubbarivag int");
LPCTSTR alter_PlotTable5 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'plot_AntalStubbariskog')  \
alter table %s add plot_AntalStubbariskog int");
LPCTSTR alter_PlotTable6 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'plot_Totdiamkvarvarande')  \
alter table %s add plot_Totdiamkvarvarande int");
LPCTSTR alter_PlotTable7 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'plot_Totdiamuttag')  \
alter table %s add plot_Totdiamuttag int");
LPCTSTR alter_PlotTable8 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'plot_AntalHogaStubbar')  \
alter table %s add plot_AntalHogaStubbar int");
LPCTSTR alter_PlotTable9 =
_T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
where table_name = '%s' and column_name = 'plot_forrensat')  \
alter table %s add plot_forrensat float");


#endif
