#pragma once

#include "Resource.h"

#include "UMGallBasDB.h"
// CMDIGallBasActiveMachineFormView form view

class CMDIGallBasActiveMachineFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIGallBasActiveMachineFormView)

protected:
	CMDIGallBasActiveMachineFormView();           // protected constructor used by dynamic creation
	virtual ~CMDIGallBasActiveMachineFormView();

	BOOL m_bIsDirty;
	CString m_sLangFN;

	CString m_sText1;
	CString m_sText2;
	CString m_sText3;
	CString m_sMachineNum;
	CString m_sMachineName;
	CString m_sCaption;
	CString m_sOKBtn;
	CString m_sCancelBtn;
	CString m_sDelMsg1;
	CString m_sDelMsg2;

	CMyReportCtrl m_wndMachineRegReport;
	BOOL setupReport(void);
	void populateReport(void);

	vecMachineData m_vecMachineData;
	vecMachineRegData m_vecMachineRegData;

	void getMachineFromDB(void);

	void getMachineRegFromDB(void);
	void deleteMachineReg(void);

	void setNewItem(void);

	BOOL isMachineUsed(MACHINE_REG_DATA &);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
public:
	enum { IDD = IDD_FORMVIEW12 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	// Need to set as Public, used in Frame; 061113 p�d
	void saveMachineRegToDB(void);

	BOOL getIsDirty(void)
	{
		return m_bIsDirty;
	}
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};