#pragma once
#include <SQLAPI.h> // main SQLAPI++ header

#include "UMGallBasDB.h"

#include "Resource.h"


// CMDIGallBasDistrictFormView form view

class CMDIGallBasDistrictFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIGallBasDistrictFormView)

	CString m_sLangAbbrev;
	CString m_sLangFN;

protected:
	CMDIGallBasDistrictFormView();           // protected constructor used by dynamic creation
	virtual ~CMDIGallBasDistrictFormView();

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;

	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;

	CXTResizeGroupBox m_wndGroup;
	vecDistrictData m_vecDistrictData;
	UINT m_nDBIndex;

	void setLanguage(void);

	void populateData(UINT);

	void setNavigationButtons(BOOL,BOOL);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
public:
	enum { IDD = IDD_FORMVIEW1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void doPopulate(UINT);
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIGallBasRegionFormView)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


