// GallBasTraktSelectList.cpp : implementation file
//

#include "stdafx.h"
#include "UMGallBasDB.h"
#include "UMGallBasGenerics.h"
#include "GallBasTraktSelectList.h"

#include "ResLangFileReader.h"

#include "SQLTraktQuestionDlg.h"
#include ".\gallbastraktselectlist.h"

#include "MDIGallBasTraktFormView.h"
// CGallBasTraktSelectList

IMPLEMENT_DYNCREATE(CGallBasTraktSelectList, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CGallBasTraktSelectList, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_CLICK, IDC_TRAKT_REPORT, OnReportItemDblClick)
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CGallBasTraktSelectList::CGallBasTraktSelectList()
	: CXTResizeFormView(CGallBasTraktSelectList::IDD)
{
}

CGallBasTraktSelectList::~CGallBasTraktSelectList()
{
}

void CGallBasTraktSelectList::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_BUTTON1, m_wndBtn1);
	//}}AFX_DATA_MAP
}

void CGallBasTraktSelectList::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	getTraktsFromDB();

	setupReport();
}

BOOL CGallBasTraktSelectList::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

BOOL CGallBasTraktSelectList::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


// CGallBasTraktSelectList diagnostics

#ifdef _DEBUG
void CGallBasTraktSelectList::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CGallBasTraktSelectList::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CGallBasTraktSelectList message handlers

// CGallBasTraktSelectList message handlers
void CGallBasTraktSelectList::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndTraktReport.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndTraktReport,1,40,rect.right - 2,rect.bottom - 42);
	}
}

void CGallBasTraktSelectList::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CGallBasTraktSelectList::setupReport(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndTraktReport.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndTraktReport.Create(this, IDC_TRAKT_REPORT,FALSE,FALSE ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}
	
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				// Get text from languagefile; 061207 p�d
				m_sEnteredManual	= xml->str(IDS_STRING112);
				m_sEnteredFromFile	= xml->str(IDS_STRING113);

				if (m_wndTraktReport.GetSafeHwnd() != NULL)
				{

					m_wndTraktReport.ShowWindow( SW_NORMAL );
					pCol = m_wndTraktReport.AddColumn(new CXTPReportColumn(0, xml->str(IDS_STRING300), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndTraktReport.AddColumn(new CXTPReportColumn(1, xml->str(IDS_STRING302), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndTraktReport.AddColumn(new CXTPReportColumn(2, xml->str(IDS_STRING304), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndTraktReport.AddColumn(new CXTPReportColumn(3, xml->str(IDS_STRING306), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_RIGHT );

					pCol = m_wndTraktReport.AddColumn(new CXTPReportColumn(4, xml->str(IDS_STRING307), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndTraktReport.AddColumn(new CXTPReportColumn(5, xml->str(IDS_STRING308), 90));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndTraktReport.AddColumn(new CXTPReportColumn(6, xml->str(IDS_STRING255), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndTraktReport.AddColumn(new CXTPReportColumn(7, xml->str(IDS_STRING311), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 061002 p�d
					setResize(&m_wndTraktReport,1,40,rect.right - 2,rect.bottom - 42);

					populateReport();
				}	// if (m_wndTraktReport.GetSafeHwnd() != NULL)


				// Also set language for Buttons etc; 061009 p�d
				m_wndBtn1.SetWindowText(xml->str(IDS_STRING401));
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CGallBasTraktSelectList::populateReport(void)
{
	CString sInvType,sOrigin,sEnteredAs;
	m_wndTraktReport.ClearReport();
	for (UINT i = 0;i < m_vecTraktData.size();i++)
	{
		TRAKT_DATA data = m_vecTraktData[i];
		if (data.n_nEnterType == 1)
			sEnteredAs = m_sEnteredManual;
		else
			sEnteredAs = m_sEnteredFromFile;
		m_wndTraktReport.AddRecord(new CTraktReportDataRec(i,data.m_nRegionID,
																												 data.m_nDistrictID,
																												 data.m_nMachineID,
																												 data.m_sTraktNum,
																												 getInvType(data.m_sType.Trim()),
																												 data.m_sOriginName,
																												 data.m_sTraktDate,
																												 sEnteredAs));
	}
	m_wndTraktReport.Populate();
	m_wndTraktReport.UpdateWindow();
}

void CGallBasTraktSelectList::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify->pRow)
	{
		CTraktReportDataRec *pRec = (CTraktReportDataRec*)pItemNotify->pItem->GetRecord();
		CMDIGallBasTraktFormView *pView = (CMDIGallBasTraktFormView *)getFormViewByID(IDD_FORMVIEW3);
		if (pView)
		{
			pView->doPopulate(pRec->getIndex());
		}
	}
}

// CGallBasTraktSelectList message handlers

void CGallBasTraktSelectList::OnBnClickedButton1()
{
	BOOL bRet;
	CSQLTraktQuestionDlg *pDlg = new CSQLTraktQuestionDlg();

	if (pDlg)
	{

		bRet = (pDlg->DoModal() == IDOK);
	
		delete pDlg;

		if (bRet)
		{
			getTraktsFromDB();
			populateReport();
			CMDIGallBasTraktFormView *pView = (CMDIGallBasTraktFormView *)getFormViewByID(IDD_FORMVIEW3);
			if (pView)
			{
				pView->resetTrakt(RESET_TO_LAST_NO_NB);
			}
		}

	}

}

void CGallBasTraktSelectList::getTraktsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTrakts(m_vecTraktData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

