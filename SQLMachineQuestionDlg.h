#pragma once

#include "UMGallBasDB.h"
#include "Resource.h"

// CSQLMachineQuestionDlg dialog

class CSQLMachineQuestionDlg : public CDialog
{
	DECLARE_DYNAMIC(CSQLMachineQuestionDlg)

public:
	CSQLMachineQuestionDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSQLMachineQuestionDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG3 };
protected:
	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;

	CComboBox m_wndCBox1;
	CComboBox m_wndCBox2;

	CButton m_wndBtn1;
	CButton m_wndOK;

	CString m_sMsgCap;
	CString m_sMsg1;

	CString m_sLangFN;
	void setLanguage(void);
	void setSQLQuestionData(void);
	BOOL doesSQLQuestionReturnsAResult(void);

	vecRegionData m_vecRegionData;
	vecDistrictData m_vecDistrictData;
	void getRegionsFromDB(void);
	void setRegionLastSelected(int reg_id);
	void addRegionsToCBox(void);

	void getDistrictFromDB(void);
	void setDistrictLastSelected(int district_id);
	void addDistrictToCBox(int reg_id);

	CGallBasDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	//{{AFX_VIRTUAL(CSQLMachineQuestionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	//{{AFX_MSG(CMDIGallBasRegionFormView)
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton1();
public:
	afx_msg void OnCbnSelchangeCombo1();
};
