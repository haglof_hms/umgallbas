#pragma once
#include <SQLAPI.h> // main SQLAPI++ header

#include "UMGallBasDB.h"

#include "Resource.h"

// CMDIGallBasPlotsFormView form view

class CMDIGallBasPlotsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIGallBasPlotsFormView)

protected:
	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;
	CMyExtEdit m_wndEdit7;
	CMyExtEdit m_wndEdit8;
	CMyExtEdit m_wndEdit9;
	CMyExtEdit m_wndEdit10;
	CMyExtEdit m_wndEdit11;
	CMyExtEdit m_wndEdit12;
	CMyExtEdit m_wndEdit13;
	CMyExtEdit m_wndEdit14;
	CMyExtEdit m_wndEdit15;
	CMyExtEdit m_wndEdit16;
	CMyExtEdit m_wndEdit17;
	CMyExtEdit m_wndEdit18;
	CMyExtEdit m_wndEdit19;
	CMyExtEdit m_wndEdit20;
	CMyExtEdit m_wndEdit21;
	CMyExtEdit m_wndEdit22;
	CMyExtEdit m_wndEdit23;
	CMyExtEdit m_wndEdit24;
	CMyExtEdit m_wndEdit25;
	CMyExtEdit m_wndEdit26;
	CMyExtEdit m_wndEdit27;
	CMyExtEdit m_wndEdit28;
	CMyExtEdit m_wndEdit29;
	CMyExtEdit m_wndEdit30;
	CMyExtEdit m_wndEdit31;
	CMyExtEdit m_wndEdit32;

	CMyExtStatic m_wndLbl4_1;
	CMyExtStatic m_wndLbl4_2;
	CMyExtStatic m_wndLbl4_3;
	CMyExtStatic m_wndLbl4_4;
	CMyExtStatic m_wndLbl4_5;
	CMyExtStatic m_wndLbl4_7;
	CMyExtStatic m_wndLbl4_8;
	CMyExtStatic m_wndLbl4_9;
	CMyExtStatic m_wndLbl4_10;
	CMyExtStatic m_wndLbl4_11;
	CMyExtStatic m_wndLbl4_12;
	CMyExtStatic m_wndLbl4_13;
	CMyExtStatic m_wndLbl4_14;
	CMyExtStatic m_wndLbl4_15;
	CMyExtStatic m_wndLbl4_16;
	CMyExtStatic m_wndLbl4_17;
	CMyExtStatic m_wndLbl4_18;
	CMyExtStatic m_wndLbl4_19;
	CMyExtStatic m_wndLbl4_20;
	CMyExtStatic m_wndLbl4_21;
	CMyExtStatic m_wndLbl4_22;
	CMyExtStatic m_wndLbl4_23;
	CMyExtStatic m_wndLbl4_24;
	CMyExtStatic m_wndLbl4_25;
	CMyExtStatic m_wndLbl4_26;
	CMyExtStatic m_wndLbl4_27;
	CMyExtStatic m_wndLbl4_28;
	CMyExtStatic m_wndLbl4_29;
	CMyExtStatic m_wndLbl4_30;
	CMyExtStatic m_wndLbl4_31;
	CMyExtStatic m_wndLbl4_32;

	CXTResizeGroupBox m_wndIDGroup;
	CXTResizeGroupBox m_wndRoadGroup;
	CXTResizeGroupBox m_wndGYGroup;
	CXTResizeGroupBox m_wndVolGroup;
	CXTResizeGroupBox m_wndPropGroup;
	CXTResizeGroupBox m_wndRestGroup;

	vecDistrictData m_vecDistrictData;
	vecMachineData m_vecMachineData;
	vecTraktData m_vecTraktData;
	vecCompartData m_vecCompartData;
	vecPlotData m_vecPlotsData;
	CString m_sMachineIDInfo;
	UINT m_nDBIndex;

	CString m_sLangFN;

	void setLanguage(void);

	void getDistrictFromDB(void);
	void getMachineFromDB(void);
	void getTraktFromDB(void);
	void getCompartFromDB(void);
	void getPlotsFromDB(void);

	void populateData(UINT);

	void setNavigationButtons(BOOL,BOOL);

	void setAllReadOnly(BOOL ro);
	void clearAll(void);
	void addPlot(void);
	void deletePlot(void);

	CString getRegionAndDistrictData(int region,int dist,DISTRICT_DATA &);
	CString getTraktInfo(int region,int dist,int machine,LPCTSTR trakt);
	CString getMachineEntr(int region,int dist,int machine);

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

protected:
	CMDIGallBasPlotsFormView();           // protected constructor used by dynamic creation
	virtual ~CMDIGallBasPlotsFormView();


public:
	enum { IDD = IDD_FORMVIEW5 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void resetPlots(int todo);

	void doPopulate(UINT);
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIGallBasRegionFormView)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


