// GallBasMachineSelectList.cpp : implementation file
//

#include "stdafx.h"
#include "UMGallBasGenerics.h"
#include "GallBasMachineSelectList.h"

#include "ResLangFileReader.h"
#include ".\gallbasmachineselectlist.h"

#include "SQLMachineQuestionDlg.h"
#include "MDIGallBasMachinesFormView.h"
// CGallBasMachineSelectList

IMPLEMENT_DYNCREATE(CGallBasMachineSelectList, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CGallBasMachineSelectList, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_CLICK, IDC_MACHINE_REPORT, OnReportItemDblClick)
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CGallBasMachineSelectList::CGallBasMachineSelectList()
	: CXTResizeFormView(CGallBasMachineSelectList::IDD)
{
}

CGallBasMachineSelectList::~CGallBasMachineSelectList()
{
}

BOOL CGallBasMachineSelectList::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CGallBasMachineSelectList::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_BUTTON1, m_wndBtn1);
	//}}AFX_DATA_MAP

}

void CGallBasMachineSelectList::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);

	getMachineFromDB();

	setupReport();
}

BOOL CGallBasMachineSelectList::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CGallBasMachineSelectList diagnostics

#ifdef _DEBUG
void CGallBasMachineSelectList::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CGallBasMachineSelectList::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CGallBasMachineSelectList message handlers
void CGallBasMachineSelectList::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndMachineReport.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndMachineReport,1,40,rect.right - 2,rect.bottom - 42);
	}
}

void CGallBasMachineSelectList::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CGallBasMachineSelectList::setupReport(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndMachineReport.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndMachineReport.Create(this, IDC_MACHINE_REPORT,FALSE,FALSE ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndMachineReport.GetSafeHwnd() != NULL)
				{

					m_wndMachineReport.ShowWindow( SW_NORMAL );
					pCol = m_wndMachineReport.AddColumn(new CXTPReportColumn(0, xml->str(IDS_STRING300), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndMachineReport.AddColumn(new CXTPReportColumn(1, xml->str(IDS_STRING301), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_LEFT );
					pCol->SetAlignment( DT_LEFT );

					pCol = m_wndMachineReport.AddColumn(new CXTPReportColumn(2, xml->str(IDS_STRING302), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndMachineReport.AddColumn(new CXTPReportColumn(3, xml->str(IDS_STRING303), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_LEFT );
					pCol->SetAlignment( DT_LEFT );

					pCol = m_wndMachineReport.AddColumn(new CXTPReportColumn(4, xml->str(IDS_STRING304), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = m_wndMachineReport.AddColumn(new CXTPReportColumn(5, xml->str(IDS_STRING305), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_LEFT );
					pCol->SetAlignment( DT_LEFT );

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 061002 p�d
					setResize(&m_wndMachineReport,1,40,rect.right - 2,rect.bottom - 42);

					populateReport();
				}	// if (m_wndMachineReport.GetSafeHwnd() != NULL)


				// Also set language for Buttons etc; 061009 p�d
				m_wndBtn1.SetWindowText(xml->str(IDS_STRING401));
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CGallBasMachineSelectList::populateReport(void)
{
	m_wndMachineReport.ClearReport();
	for (UINT i = 0;i < m_vecMachineData.size();i++)
	{
		MACHINE_DATA data = m_vecMachineData[i];
		m_wndMachineReport.AddRecord(new CMachineReportDataRec(i,data.m_nRegionID,
																														 data.m_sRegionName,
																														 data.m_nDistrictID,
																														 data.m_sDistrictName,
																														 data.m_nMachineID,
																														 data.m_sContractorName));
	}
	m_wndMachineReport.Populate();
	m_wndMachineReport.UpdateWindow();
}

void CGallBasMachineSelectList::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify->pRow)
	{
		CMachineReportDataRec *pRec = (CMachineReportDataRec*)pItemNotify->pItem->GetRecord();
		CMDIGallBasMachinesFormView *pView = (CMDIGallBasMachinesFormView *)getFormViewByID(IDD_FORMVIEW2);
		if (pView)
		{
			pView->doPopulate(pRec->getIndex());
		}
	}
}

// CGallBasMachineSelectList message handlers

void CGallBasMachineSelectList::OnBnClickedButton1()
{
	BOOL bRet;
	CSQLMachineQuestionDlg *pDlg = new CSQLMachineQuestionDlg();

	if (pDlg)
	{

		bRet = (pDlg->DoModal() == IDOK);
	
		delete pDlg;

		if (bRet)
		{
			getMachineFromDB();
			populateReport();
			CMDIGallBasMachinesFormView *pView = (CMDIGallBasMachinesFormView *)getFormViewByID(IDD_FORMVIEW2);
			if (pView)
			{
				pView->resetMachine(0);
			}
		}

	}
}

void CGallBasMachineSelectList::getMachineFromDB(void)
{
	if (m_bConnected)
	{
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getMachines(m_vecMachineData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}
