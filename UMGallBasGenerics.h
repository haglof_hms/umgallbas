#if !defined(AFX_UMGALLBASGENERICS_H)
#define AFX_UMGALLBASGENERICS_H

// Methods used by ALL (Region,District etc.)

#include "StdAfx.h"
#include "UMGallBasDB.h"

// Open the formview for selecting e.g. Region,District etc; 061011 p�d
void showFormView(int idd,LPCTSTR lang_fn);

// Get CView fy ID of form. E.g. IDD_FORMVIEW8; 061020 p�d
CView *getFormViewByID(int idd);

// Display a message dialog
BOOL messageDialog(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR cancel_btn,LPCTSTR msg);

// Open file to create data from; 061013 p�d
BOOL createFromFile(DB_CONNECTION_DATA con,LPCTSTR lang_fn,	TRAKT_IDENTIFER *);

// Get integer value from a Combo box; 061017 p�d
int getCBoxInt(const CComboBox *);

// Get string value from a Combo box; 061017 p�d
CString getCBoxStr(const CComboBox *);

// Check if there's a database to connect to; 061027 p�d
BOOL isDBConnection(LPCTSTR lang_fn);

// Create database tables form SQL scriptfiles; 061109 p�d
BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table);

#endif